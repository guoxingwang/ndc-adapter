# 已完成
    1.集成Jaxb及shape，简化xsd2java过程，不同版本xsd2java一键生成，规范package为com.hpe.cms.ndc.server.${ndc.version}.model
    2.整合不同版本Web Service服务，搭建框架
        2.1服务统一发布----重写了部分spring源码
        2.2简化服务发布流程，服务发布只需要关注EndPoint
    3.集成安全认证方式
        Wss4j
            USERNAME_TOKEN
            TIMESTAMP
    4.集成Dozer、jackson、jaxb，不同版本bean相互转换

# 工程介绍
## xsd2java
    1.通过jaxb、shade将不同版本xsd转换为javabean，并规范javabean的package：com.hpe.cms.ndc.server.${ndc.version}.model，javabean直接install至maven仓库，方便引入；
    2.根据不同版本xsd生成java bean，入参为ndc.version,jar包命名如下
        <dependency>
            <groupId>com.hpe.cms</groupId>
            <artifactId>ndc-ws-xsd2java-${ndc.version}</artifactId>
            <version>${project.version}</version>
        </dependency>
    2.1.执行install-xsd2java-bean.cmd，生成不同版本的bean
    3.问题目前v18_1生成有冲突：
        edist_commontypes.xsd
        edist_commontypes_for_AIDM.xsd
        
## wsdl发布
    1.spring-ws server代码
    2.不同版本wsdl地址,以AirShopping范例如下:
        http://localhost:8152/ndc-adapter/ws/v15_2/AirShopping.wsdl
        http://localhost:8152/ndc-adapter/ws/v16_1/AirShopping.wsdl
        http://localhost:8152/ndc-adapter/ws/v16_2/AirShopping.wsdl
        http://localhost:8152/ndc-adapter/ws/v17_1/AirShopping.wsdl
        http://localhost:8152/ndc-adapter/ws/v17_2/AirShopping.wsdl
        http://localhost:8152/ndc-adapter/ws/v18_1/AirShopping.wsdl
        http://localhost:8152/ndc-adapter/ws/v18_2/IATA_AirShopping.wsdl
    3.不规范的xsd（请求\返回不为RQ\RS）,wsdl发布方法：
        3.1手动编写wsdl：如static/ndc/v15_2_wsdl/OrderChange_v15_2.wsdl
        3.2编写wsdl config类，如：OrderChangeWsServerConfigurer
 
## Endpoint发布
    1.v18.2版本的namespace需要增加后缀interfaceName
    
        

