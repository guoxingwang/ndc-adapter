/*
 * Copyright 2005-2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.ws.server.endpoint.mapping;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.ws.URLUtil;
import org.springframework.ws.server.endpoint.MethodEndpoint;
import org.springframework.ws.server.endpoint.annotation.Endpoint;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;

/**
 * Abstract base for {@link org.springframework.ws.server.EndpointMapping} implementations that map classes tagged with
 * an annotation. By default the annotation is {@link Endpoint}, but this can be overriden in subclasses.
 *
 * <p>The methods of each bean carrying @Endpoint will be registered using {@link #registerMethods(String)}.
 *
 * @author Arjen Poutsma
 * @since 1.0.0
 */
public abstract class AbstractAnnotationMethodEndpointMapping<T> extends AbstractMethodEndpointMapping<T> {

    private boolean detectEndpointsInAncestorContexts = false;

    /**
     * Set whether to detect endpoint beans in ancestor ApplicationContexts.
     *
     * <p>Default is "false": Only endpoint beans in the current ApplicationContext will be detected, i.e. only in the
     * context that this EndpointMapping itself is defined in (typically the current MessageDispatcherServlet's
     * context).
     *
     * <p>Switch this flag on to detect endpoint beans in ancestor contexts (typically the Spring root
     * WebApplicationContext) as well.
     */
    public void setDetectEndpointsInAncestorContexts(boolean detectEndpointsInAncestorContexts) {
        this.detectEndpointsInAncestorContexts = detectEndpointsInAncestorContexts;
    }

    /**
     * Returns the 'endpoint' annotation type. Default is {@link Endpoint}.
     */
    protected Class<? extends Annotation> getEndpointAnnotationType() {
        return Endpoint.class;
    }

    @Override
    protected void initApplicationContext() throws BeansException {
        super.initApplicationContext();
        if (logger.isDebugEnabled()) {
            logger.debug("Looking for endpoints in application context: " + getApplicationContext());
        }
        String[] beanNames = (this.detectEndpointsInAncestorContexts ?
                BeanFactoryUtils.beanNamesForTypeIncludingAncestors(getApplicationContext(), Object.class) :
                getApplicationContext().getBeanNamesForType(Object.class));

        for (String beanName : beanNames) {
            Class<?> endpointClass = getApplicationContext().getType(beanName);
            if (endpointClass != null &&
                    AnnotationUtils.findAnnotation(endpointClass, getEndpointAnnotationType()) != null) {
                registerMethods(beanName);
            }
        }
    }

    /**
     * add by zengzhou.wang@hpe.com
     * 重写，根据URL中的版本调用接口
     *
     * @param key
     * @return
     */
    @Override
    protected MethodEndpoint lookupEndpoint(T key) {
        if (key instanceof javax.xml.namespace.QName) {
            javax.xml.namespace.QName qnameKey = (javax.xml.namespace.QName) key;

            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            String requestVersionName = URLUtil.getVersionFromUrlPath(request.getRequestURI());
            String localPart = qnameKey.getLocalPart();
            if (StringUtils.isNotEmpty(requestVersionName)) {
                localPart = localPart + requestVersionName;
            }
            javax.xml.namespace.QName qnameKey2 = new javax.xml.namespace.QName(qnameKey.getNamespaceURI(), localPart, qnameKey.getPrefix());
            return super.lookupEndpoint((T) qnameKey2);
        } else {
            return super.lookupEndpoint(key);
        }
    }

}
