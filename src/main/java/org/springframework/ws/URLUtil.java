package org.springframework.ws;

import org.apache.commons.lang3.StringUtils;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 21:40 2019/7/23
 * @ Description：
 * @ Modified By：
 */
public class URLUtil {


    public static final String V = "/v";

    /**
     * 从url获取版本
     *
     * @param urlPath
     * @return
     */
    public static String getVersionFromUrlPath(String urlPath) {
        String verStr = StringUtils.substringBetween(urlPath, V, "/");
        if (StringUtils.isNotEmpty(verStr)) {
            return "_v" + verStr;
        } else if (StringUtils.contains(urlPath, V)) {
            return "_v" + StringUtils.substringAfter(urlPath, V);
        }
        return "";
    }
}
