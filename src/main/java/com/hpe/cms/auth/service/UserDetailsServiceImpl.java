package com.hpe.cms.auth.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 22:43 2019/7/23
 * @ Description：
 * @ Modified By：
 */
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        /*根据入参s获取用户信息*/
        //todo 从DB获取用户信息

        /*将用户信息*/
        PasswordEncoder passwordEncoder;
        String username = "admin";
        String password;
        /*默认PasswordEncoder为bcrypt，每次加密都不一样*/
        //passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        //password = passwordEncoder.encode("secret");

        /**
         * spring 5.0.0以上版本已经废弃MD5加密，认为MD5不安全
         * 参考：https://stackoverflow.com/questions/49633672/spring-security-5-0-0-removed-md5passwordencoder
         */
        //passwordEncoder = new MessageDigestPasswordEncoder("MD5");
        //password = "{MD5}" + passwordEncoder.encode("secret");
        password = "{MD5}5ebe2294ecd0e0f08eab7690d2a6ee69";  //密码为secret

        System.out.println("s = " + s);
        System.out.println("p = " + password);

        return User.withUsername(username).password(password).roles("user").build();
    }
}
