package com.hpe.cms;

import com.hpe.cms.ndc.server.util.WsServerConfigurerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 19:13 2019/7/5
 * @ Description：
 * @ Modified By：
 */
@SpringBootApplication
@Slf4j
public class Application {
    private String version;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
        //先注册XsdSchema Bean，后续注册wsdl Bean需要引用 XsdSchema Bean
        WsServerConfigurerUtil.registerXsdSchemasByVersion("v15_2");
        WsServerConfigurerUtil.registerXsdSchemasByVersion("v16_1");
        WsServerConfigurerUtil.registerXsdSchemasByVersion("v16_2");
        WsServerConfigurerUtil.registerXsdSchemasByVersion("v17_1");
        WsServerConfigurerUtil.registerXsdSchemasByVersion("v17_2");
        WsServerConfigurerUtil.registerXsdSchemasByVersion("v18_1");
        WsServerConfigurerUtil.registerXsdSchemasByVersion("v18_2");

        //注册wsdl Bean
        WsServerConfigurerUtil.registerNDCDefaultWsdl11DefinitionBeanByVersion("v15_2");
        WsServerConfigurerUtil.registerNDCDefaultWsdl11DefinitionBeanByVersion("v16_1");
        WsServerConfigurerUtil.registerNDCDefaultWsdl11DefinitionBeanByVersion("v16_2");
        WsServerConfigurerUtil.registerNDCDefaultWsdl11DefinitionBeanByVersion("v17_1");
        WsServerConfigurerUtil.registerNDCDefaultWsdl11DefinitionBeanByVersion("v17_2");
        WsServerConfigurerUtil.registerNDCDefaultWsdl11DefinitionBeanByVersion("v18_1");
        WsServerConfigurerUtil.registerNDCDefaultWsdl11DefinitionBeanByVersion("v18_2");
        log.info("spring-boot-webservice-server-start");
    }
}
