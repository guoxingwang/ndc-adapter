package com.hpe.cms.config;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 19:18 2019/7/6
 * @ Description：
 * @ Modified By：
 */
@Slf4j
public class WebServiceList {

    public static final String RQ_SUFFIX = "RQ";
    public static final String RS_SUFFIX = "RS";

    /**
     * 获取规范的ws服务列表（后缀为RQ\RS）
     * @param version
     * @return
     */
    public static List getWebServiceListByVersion(String version) {
        List<String> wsList = new ArrayList<String>();
        List<String> wsTmpList = getWebServiceXsdListByVersion(version);
        wsTmpList.stream().forEach(name -> {
            if (name.endsWith(RQ_SUFFIX) && wsTmpList.contains(name.replace(RQ_SUFFIX, RS_SUFFIX))) {
                String correctName = name.replace(RQ_SUFFIX, "");
                if (!wsList.contains(correctName)) {
                    log.debug("correct xsd file:" + correctName);
                    wsList.add(name.replace(RQ_SUFFIX, ""));
                }
            }
        });
        return wsList;
    }

    /**
     * 或者所有的xsd文件列表
     * @param version
     * @return
     */
    public static List<String> getWebServiceXsdListByVersion(String version) {
        List<String> xsdList = new ArrayList<String>();
        String path = "static/ndc/" + version + "_schemas";
        File fileDir = new File(Thread.currentThread().getContextClassLoader().getResource(path).getPath());
        File[] files = fileDir.listFiles();
        for (File file : files) {
            String fileName = file.getName();
            if (fileName.equalsIgnoreCase("xmldsig-core-schema.xsd") || fileName.endsWith(".wsdl")) { //忽略：xmldsig-core-schema.xsd以及wsdl文件
                continue;
            }
            log.debug("fileName = " + fileName);
            String correctName = fileName.replace(".xsd", "");
            xsdList.add(correctName);
        }
        return xsdList;
    }
}
