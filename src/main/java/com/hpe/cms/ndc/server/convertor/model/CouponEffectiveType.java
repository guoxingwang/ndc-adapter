//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * COUPON EFFECTIVE PERIOD definition.
 * 
 * <p>CouponEffectiveType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="CouponEffectiveType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="EffectiveDatePeriod" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}DatePeriodRepType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Duration" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="EffectiveDateTimePeriod" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}DateTimePeriodRepType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Duration" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CouponEffectiveType", propOrder = {
    "effectiveDatePeriod",
    "effectiveDateTimePeriod"
})
public class CouponEffectiveType
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "EffectiveDatePeriod")
    protected CouponEffectiveType.EffectiveDatePeriod effectiveDatePeriod;
    @XmlElement(name = "EffectiveDateTimePeriod")
    protected CouponEffectiveType.EffectiveDateTimePeriod effectiveDateTimePeriod;

    /**
     * 获取effectiveDatePeriod属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponEffectiveType.EffectiveDatePeriod }
     *     
     */
    public CouponEffectiveType.EffectiveDatePeriod getEffectiveDatePeriod() {
        return effectiveDatePeriod;
    }

    /**
     * 设置effectiveDatePeriod属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponEffectiveType.EffectiveDatePeriod }
     *     
     */
    public void setEffectiveDatePeriod(CouponEffectiveType.EffectiveDatePeriod value) {
        this.effectiveDatePeriod = value;
    }

    /**
     * 获取effectiveDateTimePeriod属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponEffectiveType.EffectiveDateTimePeriod }
     *     
     */
    public CouponEffectiveType.EffectiveDateTimePeriod getEffectiveDateTimePeriod() {
        return effectiveDateTimePeriod;
    }

    /**
     * 设置effectiveDateTimePeriod属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponEffectiveType.EffectiveDateTimePeriod }
     *     
     */
    public void setEffectiveDateTimePeriod(CouponEffectiveType.EffectiveDateTimePeriod value) {
        this.effectiveDateTimePeriod = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}DatePeriodRepType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Duration" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "duration"
    })
    public static class EffectiveDatePeriod
        extends DatePeriodRepType
    {

        @XmlElement(name = "Duration")
        protected Duration duration;

        /**
         * 获取duration属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getDuration() {
            return duration;
        }

        /**
         * 设置duration属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setDuration(Duration value) {
            this.duration = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}DateTimePeriodRepType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Duration" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "duration"
    })
    public static class EffectiveDateTimePeriod
        extends DateTimePeriodRepType
    {

        @XmlElement(name = "Duration")
        protected Duration duration;

        /**
         * 获取duration属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getDuration() {
            return duration;
        }

        /**
         * 设置duration属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setDuration(Duration value) {
            this.duration = value;
        }

    }

}
