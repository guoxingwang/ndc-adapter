//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * NDC Capability Model: CODESET
 * 
 *  ============
 * Representation(s)
 *  ============
 * • IATA PADIS Encoding
 * • Third-Pary Standard Encoding
 * • Implementer-Proprietary Encoding
 * 
 *  ============
 * Feature(s)
 *  ============
 * • Code value, code definition and code table name may be specified
 * • URI to codeset information source may be specified
 * • Supports Language Localization (Alternate Language Values)
 * • Codeset Source Details (owner, location, etc.) may be specified
 * 
 *  ============
 * Metadata
 *  ============
 * • Yes
 * 
 * <p>CodesetType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="CodesetType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Code" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
 *         &lt;element name="Definition" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="TableName" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="Link" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodesetType", propOrder = {
    "code",
    "definition",
    "tableName",
    "link"
})
@XmlSeeAlso({
    InstantPurchaseType.class,
    com.hpe.cms.ndc.server.convertor.model.PointOfSaleType.TouchPoint.Device.class,
    com.hpe.cms.ndc.server.convertor.model.Failed.Code.class,
    com.hpe.cms.ndc.server.convertor.model.InvDiscrepencyAlertType.Code.class,
    com.hpe.cms.ndc.server.convertor.model.PaymentRulesNoticeType.FormOfPayment.Results.Code.class,
    FareCodeType.class,
    FlightStatusType.class,
    FlightMealsType.class,
    com.hpe.cms.ndc.server.convertor.model.AircraftFeaturesQueryType.Feature.class,
    com.hpe.cms.ndc.server.convertor.model.SeatLocationType.Characteristics.Characteristic.class,
    RFICType.class,
    InstrSpecialBookingType.class,
    com.hpe.cms.ndc.server.convertor.model.AlliancePreferencesType.Alliance.class,
    com.hpe.cms.ndc.server.convertor.model.FlightPreferencesType.Aircraft.Cabins.Cabin.class,
    com.hpe.cms.ndc.server.convertor.model.FlightPreferencesType.Aircraft.Seats.Characteristic.class,
    com.hpe.cms.ndc.server.convertor.model.FlightPreferencesType.Aircraft.Seats.Position.class,
    com.hpe.cms.ndc.server.convertor.model.FlightPreferencesType.Aircraft.Rows.Feature.class,
    com.hpe.cms.ndc.server.convertor.model.FarePreferencesType.Types.Type.class,
    com.hpe.cms.ndc.server.convertor.model.TransferPreferencesType.Connection.Interline.class,
    AircraftCabinType.class,
    com.hpe.cms.ndc.server.convertor.model.PaymentIssueAlertType.Payment.Failed.Code.class,
    com.hpe.cms.ndc.server.convertor.model.PaymentIssueAlertType.Payment.Incomplete.Code.class,
    com.hpe.cms.ndc.server.convertor.model.DeviceType.Type.class,
    com.hpe.cms.ndc.server.convertor.model.PaymentStatusType.IncompletePayment.StatusCode.class,
    com.hpe.cms.ndc.server.convertor.model.ItinReshopAlertsType.OrderItemExceptions.Exception.Code.class,
    CabinType.class,
    MarriedSegmentType.class
})
public class CodesetType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Definition")
    protected String definition;
    @XmlElement(name = "TableName")
    protected String tableName;
    @XmlElement(name = "Link")
    @XmlSchemaType(name = "anyURI")
    protected String link;

    /**
     * 获取code属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置code属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * 获取definition属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefinition() {
        return definition;
    }

    /**
     * 设置definition属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefinition(String value) {
        this.definition = value;
    }

    /**
     * 获取tableName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * 设置tableName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTableName(String value) {
        this.tableName = value;
    }

    /**
     * 获取link属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLink() {
        return link;
    }

    /**
     * 设置link属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLink(String value) {
        this.link = value;
    }

}
