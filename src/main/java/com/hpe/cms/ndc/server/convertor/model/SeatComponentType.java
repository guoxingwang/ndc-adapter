//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * SEAT MAP COMPONENT definition.
 * 
 * <p>SeatComponentType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="SeatComponentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Locations"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Location" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                           &lt;choice&gt;
 *                             &lt;element name="Space"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="RowRange" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;choice maxOccurs="2"&gt;
 *                                                 &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
 *                                                 &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
 *                                               &lt;/choice&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="ColumnRange"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;choice maxOccurs="2"&gt;
 *                                                 &lt;element name="Begin" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
 *                                                 &lt;element name="End" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
 *                                               &lt;/choice&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="Row"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;sequence&gt;
 *                                         &lt;element name="Position" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
 *                                         &lt;element name="Orientation" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                                       &lt;/sequence&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                               &lt;element name="Column"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;sequence&gt;
 *                                         &lt;element name="Position" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
 *                                         &lt;element name="Orientation" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                                       &lt;/sequence&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/choice&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}CodesetType"/&gt;
 *         &lt;element name="Dimensions" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="RowEquivalent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                     &lt;element name="ColumnEquivalent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                   &lt;/sequence&gt;
 *                   &lt;element name="ActualSize"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="UOM" type="{http://www.iata.org/IATA/EDIST}SizeUnitSimpleType"/&gt;
 *                             &lt;element name="Length" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
 *                             &lt;element name="Width" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/choice&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeatComponentType", propOrder = {
    "locations",
    "type",
    "dimensions"
})
public class SeatComponentType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "Locations", required = true)
    protected SeatComponentType.Locations locations;
    @XmlElement(name = "Type", required = true)
    protected CodesetType type;
    @XmlElement(name = "Dimensions")
    protected SeatComponentType.Dimensions dimensions;

    /**
     * 获取locations属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SeatComponentType.Locations }
     *     
     */
    public SeatComponentType.Locations getLocations() {
        return locations;
    }

    /**
     * 设置locations属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SeatComponentType.Locations }
     *     
     */
    public void setLocations(SeatComponentType.Locations value) {
        this.locations = value;
    }

    /**
     * 获取type属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getType() {
        return type;
    }

    /**
     * 设置type属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setType(CodesetType value) {
        this.type = value;
    }

    /**
     * 获取dimensions属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SeatComponentType.Dimensions }
     *     
     */
    public SeatComponentType.Dimensions getDimensions() {
        return dimensions;
    }

    /**
     * 设置dimensions属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SeatComponentType.Dimensions }
     *     
     */
    public void setDimensions(SeatComponentType.Dimensions value) {
        this.dimensions = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *       &lt;choice&gt;
     *         &lt;sequence&gt;
     *           &lt;element name="RowEquivalent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *           &lt;element name="ColumnEquivalent" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *         &lt;/sequence&gt;
     *         &lt;element name="ActualSize"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="UOM" type="{http://www.iata.org/IATA/EDIST}SizeUnitSimpleType"/&gt;
     *                   &lt;element name="Length" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
     *                   &lt;element name="Width" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rowEquivalent",
        "columnEquivalent",
        "actualSize"
    })
    public static class Dimensions
        extends AssociatedObjectBaseType
    {

        @XmlElement(name = "RowEquivalent")
        protected BigDecimal rowEquivalent;
        @XmlElement(name = "ColumnEquivalent")
        protected BigDecimal columnEquivalent;
        @XmlElement(name = "ActualSize")
        protected SeatComponentType.Dimensions.ActualSize actualSize;

        /**
         * 获取rowEquivalent属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getRowEquivalent() {
            return rowEquivalent;
        }

        /**
         * 设置rowEquivalent属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setRowEquivalent(BigDecimal value) {
            this.rowEquivalent = value;
        }

        /**
         * 获取columnEquivalent属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getColumnEquivalent() {
            return columnEquivalent;
        }

        /**
         * 设置columnEquivalent属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setColumnEquivalent(BigDecimal value) {
            this.columnEquivalent = value;
        }

        /**
         * 获取actualSize属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SeatComponentType.Dimensions.ActualSize }
         *     
         */
        public SeatComponentType.Dimensions.ActualSize getActualSize() {
            return actualSize;
        }

        /**
         * 设置actualSize属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SeatComponentType.Dimensions.ActualSize }
         *     
         */
        public void setActualSize(SeatComponentType.Dimensions.ActualSize value) {
            this.actualSize = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="UOM" type="{http://www.iata.org/IATA/EDIST}SizeUnitSimpleType"/&gt;
         *         &lt;element name="Length" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
         *         &lt;element name="Width" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "uom",
            "length",
            "width"
        })
        public static class ActualSize {

            @XmlElement(name = "UOM", required = true)
            @XmlSchemaType(name = "string")
            protected SizeUnitSimpleType uom;
            @XmlElement(name = "Length", required = true)
            protected String length;
            @XmlElement(name = "Width", required = true)
            protected String width;

            /**
             * 获取uom属性的值。
             * 
             * @return
             *     possible object is
             *     {@link SizeUnitSimpleType }
             *     
             */
            public SizeUnitSimpleType getUOM() {
                return uom;
            }

            /**
             * 设置uom属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link SizeUnitSimpleType }
             *     
             */
            public void setUOM(SizeUnitSimpleType value) {
                this.uom = value;
            }

            /**
             * 获取length属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLength() {
                return length;
            }

            /**
             * 设置length属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLength(String value) {
                this.length = value;
            }

            /**
             * 获取width属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getWidth() {
                return width;
            }

            /**
             * 设置width属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setWidth(String value) {
                this.width = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Location" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *                 &lt;choice&gt;
     *                   &lt;element name="Space"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="RowRange" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;choice maxOccurs="2"&gt;
     *                                       &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
     *                                       &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
     *                                     &lt;/choice&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="ColumnRange"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;choice maxOccurs="2"&gt;
     *                                       &lt;element name="Begin" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
     *                                       &lt;element name="End" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
     *                                     &lt;/choice&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="Row"&gt;
     *                       &lt;complexType&gt;
     *                         &lt;complexContent&gt;
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                             &lt;sequence&gt;
     *                               &lt;element name="Position" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
     *                               &lt;element name="Orientation" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *                             &lt;/sequence&gt;
     *                           &lt;/restriction&gt;
     *                         &lt;/complexContent&gt;
     *                       &lt;/complexType&gt;
     *                     &lt;/element&gt;
     *                     &lt;element name="Column"&gt;
     *                       &lt;complexType&gt;
     *                         &lt;complexContent&gt;
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                             &lt;sequence&gt;
     *                               &lt;element name="Position" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
     *                               &lt;element name="Orientation" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *                             &lt;/sequence&gt;
     *                           &lt;/restriction&gt;
     *                         &lt;/complexContent&gt;
     *                       &lt;/complexType&gt;
     *                     &lt;/element&gt;
     *                   &lt;/sequence&gt;
     *                 &lt;/choice&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class Locations {

        @XmlElement(name = "Location", required = true)
        protected List<SeatComponentType.Locations.Location> location;

        /**
         * Gets the value of the location property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the location property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLocation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SeatComponentType.Locations.Location }
         * 
         * 
         */
        public List<SeatComponentType.Locations.Location> getLocation() {
            if (location == null) {
                location = new ArrayList<SeatComponentType.Locations.Location>();
            }
            return this.location;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
         *       &lt;choice&gt;
         *         &lt;element name="Space"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="RowRange" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;choice maxOccurs="2"&gt;
         *                             &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
         *                             &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
         *                           &lt;/choice&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="ColumnRange"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;choice maxOccurs="2"&gt;
         *                             &lt;element name="Begin" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
         *                             &lt;element name="End" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
         *                           &lt;/choice&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;sequence&gt;
         *           &lt;element name="Row"&gt;
         *             &lt;complexType&gt;
         *               &lt;complexContent&gt;
         *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                   &lt;sequence&gt;
         *                     &lt;element name="Position" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
         *                     &lt;element name="Orientation" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
         *                   &lt;/sequence&gt;
         *                 &lt;/restriction&gt;
         *               &lt;/complexContent&gt;
         *             &lt;/complexType&gt;
         *           &lt;/element&gt;
         *           &lt;element name="Column"&gt;
         *             &lt;complexType&gt;
         *               &lt;complexContent&gt;
         *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                   &lt;sequence&gt;
         *                     &lt;element name="Position" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
         *                     &lt;element name="Orientation" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
         *                   &lt;/sequence&gt;
         *                 &lt;/restriction&gt;
         *               &lt;/complexContent&gt;
         *             &lt;/complexType&gt;
         *           &lt;/element&gt;
         *         &lt;/sequence&gt;
         *       &lt;/choice&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "space",
            "row",
            "column"
        })
        public static class Location
            extends AssociatedObjectBaseType
        {

            @XmlElement(name = "Space")
            protected SeatComponentType.Locations.Location.Space space;
            @XmlElement(name = "Row")
            protected SeatComponentType.Locations.Location.Row row;
            @XmlElement(name = "Column")
            protected SeatComponentType.Locations.Location.Column column;

            /**
             * 获取space属性的值。
             * 
             * @return
             *     possible object is
             *     {@link SeatComponentType.Locations.Location.Space }
             *     
             */
            public SeatComponentType.Locations.Location.Space getSpace() {
                return space;
            }

            /**
             * 设置space属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link SeatComponentType.Locations.Location.Space }
             *     
             */
            public void setSpace(SeatComponentType.Locations.Location.Space value) {
                this.space = value;
            }

            /**
             * 获取row属性的值。
             * 
             * @return
             *     possible object is
             *     {@link SeatComponentType.Locations.Location.Row }
             *     
             */
            public SeatComponentType.Locations.Location.Row getRow() {
                return row;
            }

            /**
             * 设置row属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link SeatComponentType.Locations.Location.Row }
             *     
             */
            public void setRow(SeatComponentType.Locations.Location.Row value) {
                this.row = value;
            }

            /**
             * 获取column属性的值。
             * 
             * @return
             *     possible object is
             *     {@link SeatComponentType.Locations.Location.Column }
             *     
             */
            public SeatComponentType.Locations.Location.Column getColumn() {
                return column;
            }

            /**
             * 设置column属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link SeatComponentType.Locations.Location.Column }
             *     
             */
            public void setColumn(SeatComponentType.Locations.Location.Column value) {
                this.column = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Position" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
             *         &lt;element name="Orientation" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "position",
                "orientation"
            })
            public static class Column {

                @XmlElement(name = "Position", required = true)
                protected String position;
                @XmlElement(name = "Orientation")
                protected CodesetType orientation;

                /**
                 * 获取position属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPosition() {
                    return position;
                }

                /**
                 * 设置position属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPosition(String value) {
                    this.position = value;
                }

                /**
                 * 获取orientation属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodesetType }
                 *     
                 */
                public CodesetType getOrientation() {
                    return orientation;
                }

                /**
                 * 设置orientation属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodesetType }
                 *     
                 */
                public void setOrientation(CodesetType value) {
                    this.orientation = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Position" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
             *         &lt;element name="Orientation" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "position",
                "orientation"
            })
            public static class Row {

                @XmlElement(name = "Position", required = true)
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger position;
                @XmlElement(name = "Orientation")
                protected CodesetType orientation;

                /**
                 * 获取position属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getPosition() {
                    return position;
                }

                /**
                 * 设置position属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setPosition(BigInteger value) {
                    this.position = value;
                }

                /**
                 * 获取orientation属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodesetType }
                 *     
                 */
                public CodesetType getOrientation() {
                    return orientation;
                }

                /**
                 * 设置orientation属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodesetType }
                 *     
                 */
                public void setOrientation(CodesetType value) {
                    this.orientation = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="RowRange" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;choice maxOccurs="2"&gt;
             *                   &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
             *                   &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
             *                 &lt;/choice&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="ColumnRange"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;choice maxOccurs="2"&gt;
             *                   &lt;element name="Begin" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
             *                   &lt;element name="End" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
             *                 &lt;/choice&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rowRange",
                "columnRange"
            })
            public static class Space {

                @XmlElement(name = "RowRange", required = true)
                protected List<SeatComponentType.Locations.Location.Space.RowRange> rowRange;
                @XmlElement(name = "ColumnRange", required = true)
                protected SeatComponentType.Locations.Location.Space.ColumnRange columnRange;

                /**
                 * Gets the value of the rowRange property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the rowRange property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getRowRange().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link SeatComponentType.Locations.Location.Space.RowRange }
                 * 
                 * 
                 */
                public List<SeatComponentType.Locations.Location.Space.RowRange> getRowRange() {
                    if (rowRange == null) {
                        rowRange = new ArrayList<SeatComponentType.Locations.Location.Space.RowRange>();
                    }
                    return this.rowRange;
                }

                /**
                 * 获取columnRange属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link SeatComponentType.Locations.Location.Space.ColumnRange }
                 *     
                 */
                public SeatComponentType.Locations.Location.Space.ColumnRange getColumnRange() {
                    return columnRange;
                }

                /**
                 * 设置columnRange属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SeatComponentType.Locations.Location.Space.ColumnRange }
                 *     
                 */
                public void setColumnRange(SeatComponentType.Locations.Location.Space.ColumnRange value) {
                    this.columnRange = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;choice maxOccurs="2"&gt;
                 *         &lt;element name="Begin" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
                 *         &lt;element name="End" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
                 *       &lt;/choice&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "beginOrEnd"
                })
                public static class ColumnRange {

                    @XmlElementRefs({
                        @XmlElementRef(name = "Begin", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "End", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false)
                    })
                    protected List<JAXBElement<String>> beginOrEnd;

                    /**
                     * Gets the value of the beginOrEnd property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the beginOrEnd property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getBeginOrEnd().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * {@link JAXBElement }{@code <}{@link String }{@code >}
                     * 
                     * 
                     */
                    public List<JAXBElement<String>> getBeginOrEnd() {
                        if (beginOrEnd == null) {
                            beginOrEnd = new ArrayList<JAXBElement<String>>();
                        }
                        return this.beginOrEnd;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;choice maxOccurs="2"&gt;
                 *         &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
                 *         &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
                 *       &lt;/choice&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "beginOrEnd"
                })
                public static class RowRange {

                    @XmlElementRefs({
                        @XmlElementRef(name = "Begin", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false),
                        @XmlElementRef(name = "End", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false)
                    })
                    protected List<JAXBElement<BigInteger>> beginOrEnd;

                    /**
                     * Gets the value of the beginOrEnd property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the beginOrEnd property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getBeginOrEnd().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
                     * 
                     * 
                     */
                    public List<JAXBElement<BigInteger>> getBeginOrEnd() {
                        if (beginOrEnd == null) {
                            beginOrEnd = new ArrayList<JAXBElement<BigInteger>>();
                        }
                        return this.beginOrEnd;
                    }

                }

            }

        }

    }

}
