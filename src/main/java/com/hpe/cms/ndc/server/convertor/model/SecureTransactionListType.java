//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>SecureTransactionListType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="SecureTransactionListType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Authorize"/&gt;
 *     &lt;enumeration value="CheckEnrollment"/&gt;
 *     &lt;enumeration value="Complete"/&gt;
 *     &lt;enumeration value="Purchase"/&gt;
 *     &lt;enumeration value="Refund"/&gt;
 *     &lt;enumeration value="Validate"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SecureTransactionListType")
@XmlEnum
public enum SecureTransactionListType {


    /**
     * Amount is authorized. No funds are transferred.
     * 
     */
    @XmlEnumValue("Authorize")
    AUTHORIZE("Authorize"),
    @XmlEnumValue("CheckEnrollment")
    CHECK_ENROLLMENT("CheckEnrollment"),

    /**
     * 
     *             Complete a previous authorization. Funds are transferred.
     *           
     * 
     */
    @XmlEnumValue("Complete")
    COMPLETE("Complete"),

    /**
     * Purchase. Funds are transferred immediately.
     * 
     */
    @XmlEnumValue("Purchase")
    PURCHASE("Purchase"),

    /**
     * Refund. Funds transferred immediately.
     * 
     */
    @XmlEnumValue("Refund")
    REFUND("Refund"),

    /**
     * 
     *             Validation Transaction. Effects a $1.00 Authorization to validate card details including expiry date, address.
     *           
     * 
     */
    @XmlEnumValue("Validate")
    VALIDATE("Validate");
    private final String value;

    SecureTransactionListType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SecureTransactionListType fromValue(String v) {
        for (SecureTransactionListType c: SecureTransactionListType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
