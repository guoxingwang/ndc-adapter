//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Traveler Authentication definition.
 * 
 * Notes:
 *  1. The AuthQuestion is returned as an Alert in a Shopping or Order response message.
 *  2. The AuthAnswer is returned as an Alert in a Shopping or Order request message.
 *  3. Note that the CustomerInputRQ may be used to send an authentication response in lieue of using an alert in a request message.
 * 
 * <p>PIN_AuthTravelerType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PIN_AuthTravelerType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}TrxProcessObjectBaseType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="AuthRequest"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}PinPhraseQuestionType"&gt;
 *                 &lt;attribute name="AuthenticationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="MaximumTrxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="RetryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AuthResponse" type="{http://www.iata.org/IATA/EDIST}PinPhraseAnswerType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PIN_AuthTravelerType", propOrder = {
    "authRequest",
    "authResponse"
})
public class PINAuthTravelerType
    extends TrxProcessObjectBaseType
{

    @XmlElement(name = "AuthRequest")
    protected PINAuthTravelerType.AuthRequest authRequest;
    @XmlElement(name = "AuthResponse")
    protected PinPhraseAnswerType authResponse;

    /**
     * 获取authRequest属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PINAuthTravelerType.AuthRequest }
     *     
     */
    public PINAuthTravelerType.AuthRequest getAuthRequest() {
        return authRequest;
    }

    /**
     * 设置authRequest属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PINAuthTravelerType.AuthRequest }
     *     
     */
    public void setAuthRequest(PINAuthTravelerType.AuthRequest value) {
        this.authRequest = value;
    }

    /**
     * 获取authResponse属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PinPhraseAnswerType }
     *     
     */
    public PinPhraseAnswerType getAuthResponse() {
        return authResponse;
    }

    /**
     * 设置authResponse属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PinPhraseAnswerType }
     *     
     */
    public void setAuthResponse(PinPhraseAnswerType value) {
        this.authResponse = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}PinPhraseQuestionType"&gt;
     *       &lt;attribute name="AuthenticationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="MaximumTrxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="RetryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AuthRequest
        extends PinPhraseQuestionType
    {

        @XmlAttribute(name = "AuthenticationInd")
        protected Boolean authenticationInd;
        @XmlAttribute(name = "MaximumTrxInd")
        protected Boolean maximumTrxInd;
        @XmlAttribute(name = "RetryInd")
        protected Boolean retryInd;

        /**
         * 获取authenticationInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isAuthenticationInd() {
            return authenticationInd;
        }

        /**
         * 设置authenticationInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setAuthenticationInd(Boolean value) {
            this.authenticationInd = value;
        }

        /**
         * 获取maximumTrxInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isMaximumTrxInd() {
            return maximumTrxInd;
        }

        /**
         * 设置maximumTrxInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setMaximumTrxInd(Boolean value) {
            this.maximumTrxInd = value;
        }

        /**
         * 获取retryInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isRetryInd() {
            return retryInd;
        }

        /**
         * 设置retryInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setRetryInd(Boolean value) {
            this.retryInd = value;
        }

    }

}
