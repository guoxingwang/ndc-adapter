//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Offer/ Offer Item: PRICE GUARANTEE Time Limit definition.
 * 
 * Price Guarantee Time Limit value.
 * Examples: 2017-05-30T09:00:00,  2015-01-13T13:59:38Z
 * 
 * Notes:
 *  1. This is the period for which an Offer price is guaranteed.
 * 
 *  2. On expiry an Offer may be re-priced up to the point an accountable document is issued.
 * 
 *  3. A Price Guarantee cannot extend beyond the Offer Expiration Time Limit unless an Order has been created.
 * 
 * <p>PriceGuarTimeLimitType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PriceGuarTimeLimitType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceGuarTimeLimitType")
@XmlSeeAlso({
    PriceGuaranteeTimeLimit.class
})
public class PriceGuarTimeLimitType
    extends KeyWithMetaObjectBaseType
{


}
