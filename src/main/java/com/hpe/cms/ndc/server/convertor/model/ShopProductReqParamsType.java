//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Shop Product Request Message Parameters.
 * 
 * <p>ShopProductReqParamsType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ShopProductReqParamsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}MessageParamsBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Group" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PrimaryContact" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShopProductReqParamsType", propOrder = {
    "group"
})
public class ShopProductReqParamsType
    extends MessageParamsBaseType
{

    @XmlElement(name = "Group")
    protected ShopProductReqParamsType.Group group;

    /**
     * 获取group属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShopProductReqParamsType.Group }
     *     
     */
    public ShopProductReqParamsType.Group getGroup() {
        return group;
    }

    /**
     * 设置group属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShopProductReqParamsType.Group }
     *     
     */
    public void setGroup(ShopProductReqParamsType.Group value) {
        this.group = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PrimaryContact" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "primaryContact"
    })
    public static class Group {

        @XmlElement(name = "PrimaryContact")
        protected PrimaryContact primaryContact;

        /**
         * 获取primaryContact属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PrimaryContact }
         *     
         */
        public PrimaryContact getPrimaryContact() {
            return primaryContact;
        }

        /**
         * 设置primaryContact属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PrimaryContact }
         *     
         */
        public void setPrimaryContact(PrimaryContact value) {
            this.primaryContact = value;
        }

    }

}
