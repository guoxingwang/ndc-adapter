//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Policy" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}PolicyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DefaultPolicy"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PolicyAugmentation"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policy"
})
@XmlRootElement(name = "Policies")
public class Policies {

    @XmlElement(name = "Policy", required = true)
    protected List<Policies.Policy> policy;

    /**
     * Gets the value of the policy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Policies.Policy }
     * 
     * 
     */
    public List<Policies.Policy> getPolicy() {
        if (policy == null) {
            policy = new ArrayList<Policies.Policy>();
        }
        return this.policy;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}PolicyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DefaultPolicy"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PolicyAugmentation"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "defaultPolicy",
        "policyAugmentation"
    })
    public static class Policy
        extends PolicyType
    {

        @XmlElement(name = "DefaultPolicy", required = true)
        protected PolicyDefaultType defaultPolicy;
        @XmlElement(name = "PolicyAugmentation", required = true)
        protected PolicyAugmentationType policyAugmentation;

        /**
         * 获取defaultPolicy属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PolicyDefaultType }
         *     
         */
        public PolicyDefaultType getDefaultPolicy() {
            return defaultPolicy;
        }

        /**
         * 设置defaultPolicy属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PolicyDefaultType }
         *     
         */
        public void setDefaultPolicy(PolicyDefaultType value) {
            this.defaultPolicy = value;
        }

        /**
         * 获取policyAugmentation属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PolicyAugmentationType }
         *     
         */
        public PolicyAugmentationType getPolicyAugmentation() {
            return policyAugmentation;
        }

        /**
         * 设置policyAugmentation属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PolicyAugmentationType }
         *     
         */
        public void setPolicyAugmentation(PolicyAugmentationType value) {
            this.policyAugmentation = value;
        }

    }

}
