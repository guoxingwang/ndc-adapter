//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type definition for Named Association Type.
 * 
 * Named Association(s).
 * 
 * Note: This capability supports implementer-specified, or "Named" reference associations to other (target) instantiated objects with @Key, @GroupKey and/ or @ListKey attributes.
 * 
 * <p>NamedAssocType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="NamedAssocType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NamedAssoc" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="GroupKeyRef" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
 *                           &lt;attribute name="TokenRef" type="{http://www.iata.org/IATA/EDIST}TokenID_SimpleType" /&gt;
 *                           &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="List" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="ListKeyRef" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
 *                           &lt;attribute name="TokenRef" type="{http://www.iata.org/IATA/EDIST}TokenID_SimpleType" /&gt;
 *                           &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="UniqueKeyID" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="UniqueID_Ref" use="required" type="{http://www.iata.org/IATA/EDIST}UniqueID_SimpleType" /&gt;
 *                           &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="Target" use="required" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                 &lt;attribute name="KeyRef" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
 *                 &lt;attribute name="From" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                 &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NamedAssocType", propOrder = {
    "namedAssoc"
})
public class NamedAssocType {

    @XmlElement(name = "NamedAssoc", required = true)
    protected java.util.List<NamedAssocType.NamedAssoc> namedAssoc;

    /**
     * Gets the value of the namedAssoc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the namedAssoc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNamedAssoc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NamedAssocType.NamedAssoc }
     * 
     * 
     */
    public java.util.List<NamedAssocType.NamedAssoc> getNamedAssoc() {
        if (namedAssoc == null) {
            namedAssoc = new ArrayList<NamedAssocType.NamedAssoc>();
        }
        return this.namedAssoc;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Group" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="GroupKeyRef" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
     *                 &lt;attribute name="TokenRef" type="{http://www.iata.org/IATA/EDIST}TokenID_SimpleType" /&gt;
     *                 &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="List" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="ListKeyRef" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
     *                 &lt;attribute name="TokenRef" type="{http://www.iata.org/IATA/EDIST}TokenID_SimpleType" /&gt;
     *                 &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="UniqueKeyID" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="UniqueID_Ref" use="required" type="{http://www.iata.org/IATA/EDIST}UniqueID_SimpleType" /&gt;
     *                 &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="Target" use="required" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *       &lt;attribute name="KeyRef" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
     *       &lt;attribute name="From" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *       &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "group",
        "list",
        "uniqueKeyID"
    })
    public static class NamedAssoc {

        @XmlElement(name = "Group")
        protected java.util.List<NamedAssocType.NamedAssoc.Group> group;
        @XmlElement(name = "List")
        protected java.util.List<NamedAssocType.NamedAssoc.List> list;
        @XmlElement(name = "UniqueKeyID")
        protected java.util.List<NamedAssocType.NamedAssoc.UniqueKeyID> uniqueKeyID;
        @XmlAttribute(name = "Target", required = true)
        protected String target;
        @XmlAttribute(name = "KeyRef")
        @XmlIDREF
        protected java.util.List<Object> keyRef;
        @XmlAttribute(name = "From")
        protected String from;
        @XmlAttribute(name = "Seq")
        protected BigInteger seq;

        /**
         * Gets the value of the group property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the group property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NamedAssocType.NamedAssoc.Group }
         * 
         * 
         */
        public java.util.List<NamedAssocType.NamedAssoc.Group> getGroup() {
            if (group == null) {
                group = new ArrayList<NamedAssocType.NamedAssoc.Group>();
            }
            return this.group;
        }

        /**
         * Gets the value of the list property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the list property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getList().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NamedAssocType.NamedAssoc.List }
         * 
         * 
         */
        public java.util.List<NamedAssocType.NamedAssoc.List> getList() {
            if (list == null) {
                list = new ArrayList<NamedAssocType.NamedAssoc.List>();
            }
            return this.list;
        }

        /**
         * Gets the value of the uniqueKeyID property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the uniqueKeyID property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUniqueKeyID().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link NamedAssocType.NamedAssoc.UniqueKeyID }
         * 
         * 
         */
        public java.util.List<NamedAssocType.NamedAssoc.UniqueKeyID> getUniqueKeyID() {
            if (uniqueKeyID == null) {
                uniqueKeyID = new ArrayList<NamedAssocType.NamedAssoc.UniqueKeyID>();
            }
            return this.uniqueKeyID;
        }

        /**
         * 获取target属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTarget() {
            return target;
        }

        /**
         * 设置target属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTarget(String value) {
            this.target = value;
        }

        /**
         * Gets the value of the keyRef property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the keyRef property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getKeyRef().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public java.util.List<Object> getKeyRef() {
            if (keyRef == null) {
                keyRef = new ArrayList<Object>();
            }
            return this.keyRef;
        }

        /**
         * 获取from属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFrom() {
            return from;
        }

        /**
         * 设置from属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFrom(String value) {
            this.from = value;
        }

        /**
         * 获取seq属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSeq() {
            return seq;
        }

        /**
         * 设置seq属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSeq(BigInteger value) {
            this.seq = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="GroupKeyRef" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
         *       &lt;attribute name="TokenRef" type="{http://www.iata.org/IATA/EDIST}TokenID_SimpleType" /&gt;
         *       &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Group {

            @XmlAttribute(name = "GroupKeyRef")
            @XmlIDREF
            protected java.util.List<Object> groupKeyRef;
            @XmlAttribute(name = "TokenRef")
            protected String tokenRef;
            @XmlAttribute(name = "Seq")
            protected BigInteger seq;

            /**
             * Gets the value of the groupKeyRef property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the groupKeyRef property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getGroupKeyRef().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public java.util.List<Object> getGroupKeyRef() {
                if (groupKeyRef == null) {
                    groupKeyRef = new ArrayList<Object>();
                }
                return this.groupKeyRef;
            }

            /**
             * 获取tokenRef属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTokenRef() {
                return tokenRef;
            }

            /**
             * 设置tokenRef属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTokenRef(String value) {
                this.tokenRef = value;
            }

            /**
             * 获取seq属性的值。
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSeq() {
                return seq;
            }

            /**
             * 设置seq属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSeq(BigInteger value) {
                this.seq = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="ListKeyRef" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
         *       &lt;attribute name="TokenRef" type="{http://www.iata.org/IATA/EDIST}TokenID_SimpleType" /&gt;
         *       &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class List {

            @XmlAttribute(name = "ListKeyRef")
            @XmlIDREF
            protected java.util.List<Object> listKeyRef;
            @XmlAttribute(name = "TokenRef")
            protected String tokenRef;
            @XmlAttribute(name = "Seq")
            protected BigInteger seq;

            /**
             * Gets the value of the listKeyRef property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the listKeyRef property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getListKeyRef().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public java.util.List<Object> getListKeyRef() {
                if (listKeyRef == null) {
                    listKeyRef = new ArrayList<Object>();
                }
                return this.listKeyRef;
            }

            /**
             * 获取tokenRef属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTokenRef() {
                return tokenRef;
            }

            /**
             * 设置tokenRef属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTokenRef(String value) {
                this.tokenRef = value;
            }

            /**
             * 获取seq属性的值。
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSeq() {
                return seq;
            }

            /**
             * 设置seq属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSeq(BigInteger value) {
                this.seq = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="UniqueID_Ref" use="required" type="{http://www.iata.org/IATA/EDIST}UniqueID_SimpleType" /&gt;
         *       &lt;attribute name="Seq" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class UniqueKeyID {

            @XmlAttribute(name = "UniqueID_Ref", required = true)
            protected String uniqueIDRef;
            @XmlAttribute(name = "Seq")
            protected BigInteger seq;

            /**
             * 获取uniqueIDRef属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUniqueIDRef() {
                return uniqueIDRef;
            }

            /**
             * 设置uniqueIDRef属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUniqueIDRef(String value) {
                this.uniqueIDRef = value;
            }

            /**
             * 获取seq属性的值。
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getSeq() {
                return seq;
            }

            /**
             * 设置seq属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setSeq(BigInteger value) {
                this.seq = value;
            }

        }

    }

}
