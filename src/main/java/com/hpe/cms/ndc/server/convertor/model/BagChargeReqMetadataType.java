//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Baggage Charges Request Metadata definition.
 * 
 * <p>BagChargeReqMetadataType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="BagChargeReqMetadataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Shopping" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ShopMetadataGroup"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Traveler" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelerMetadata"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Other" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="OtherMetadata" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;choice&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AircraftMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CityMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CodesetMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ContactMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CountryMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DescriptionMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}EquivalentID_Metadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}LanguageMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentFormMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}RuleMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}StateProvMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ZoneMetadatas"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AddressMetadatas"/&gt;
 *                           &lt;/choice&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BagChargeReqMetadataType", propOrder = {
    "shopping",
    "traveler",
    "other"
})
public class BagChargeReqMetadataType {

    @XmlElement(name = "Shopping")
    protected BagChargeReqMetadataType.Shopping shopping;
    @XmlElement(name = "Traveler")
    protected BagChargeReqMetadataType.Traveler traveler;
    @XmlElement(name = "Other")
    protected BagChargeReqMetadataType.Other other;

    /**
     * 获取shopping属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BagChargeReqMetadataType.Shopping }
     *     
     */
    public BagChargeReqMetadataType.Shopping getShopping() {
        return shopping;
    }

    /**
     * 设置shopping属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BagChargeReqMetadataType.Shopping }
     *     
     */
    public void setShopping(BagChargeReqMetadataType.Shopping value) {
        this.shopping = value;
    }

    /**
     * 获取traveler属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BagChargeReqMetadataType.Traveler }
     *     
     */
    public BagChargeReqMetadataType.Traveler getTraveler() {
        return traveler;
    }

    /**
     * 设置traveler属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BagChargeReqMetadataType.Traveler }
     *     
     */
    public void setTraveler(BagChargeReqMetadataType.Traveler value) {
        this.traveler = value;
    }

    /**
     * 获取other属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BagChargeReqMetadataType.Other }
     *     
     */
    public BagChargeReqMetadataType.Other getOther() {
        return other;
    }

    /**
     * 设置other属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BagChargeReqMetadataType.Other }
     *     
     */
    public void setOther(BagChargeReqMetadataType.Other value) {
        this.other = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="OtherMetadata" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;choice&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AircraftMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CityMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CodesetMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ContactMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CountryMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DescriptionMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}EquivalentID_Metadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}LanguageMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentFormMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}RuleMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}StateProvMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ZoneMetadatas"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AddressMetadatas"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "otherMetadata"
    })
    public static class Other {

        @XmlElement(name = "OtherMetadata", required = true)
        protected List<BagChargeReqMetadataType.Other.OtherMetadata> otherMetadata;

        /**
         * Gets the value of the otherMetadata property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the otherMetadata property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOtherMetadata().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BagChargeReqMetadataType.Other.OtherMetadata }
         * 
         * 
         */
        public List<BagChargeReqMetadataType.Other.OtherMetadata> getOtherMetadata() {
            if (otherMetadata == null) {
                otherMetadata = new ArrayList<BagChargeReqMetadataType.Other.OtherMetadata>();
            }
            return this.otherMetadata;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AircraftMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CityMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CodesetMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ContactMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CountryMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DescriptionMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}EquivalentID_Metadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}LanguageMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentFormMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}RuleMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}StateProvMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ZoneMetadatas"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AddressMetadatas"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "aircraftMetadatas",
            "airportMetadatas",
            "cityMetadatas",
            "codesetMetadatas",
            "contactMetadatas",
            "countryMetadatas",
            "currencyMetadatas",
            "descriptionMetadatas",
            "equivalentIDMetadatas",
            "languageMetadatas",
            "paymentCardMetadatas",
            "paymentFormMetadatas",
            "priceMetadatas",
            "ruleMetadatas",
            "stateProvMetadatas",
            "zoneMetadatas",
            "addressMetadatas"
        })
        public static class OtherMetadata {

            @XmlElement(name = "AircraftMetadatas")
            protected AircraftMetadatas aircraftMetadatas;
            @XmlElement(name = "AirportMetadatas")
            protected AirportMetadatas airportMetadatas;
            @XmlElement(name = "CityMetadatas")
            protected CityMetadatas cityMetadatas;
            @XmlElement(name = "CodesetMetadatas")
            protected CodesetMetadatas codesetMetadatas;
            @XmlElement(name = "ContactMetadatas")
            protected ContactMetadatas contactMetadatas;
            @XmlElement(name = "CountryMetadatas")
            protected CountryMetadatas countryMetadatas;
            @XmlElement(name = "CurrencyMetadatas")
            protected CurrencyMetadatas currencyMetadatas;
            @XmlElement(name = "DescriptionMetadatas")
            protected DescriptionMetadatas descriptionMetadatas;
            @XmlElement(name = "EquivalentID_Metadatas")
            protected EquivalentIDMetadatas equivalentIDMetadatas;
            @XmlElement(name = "LanguageMetadatas")
            protected LanguageMetadatas languageMetadatas;
            @XmlElement(name = "PaymentCardMetadatas")
            protected PaymentCardMetadatas paymentCardMetadatas;
            @XmlElement(name = "PaymentFormMetadatas")
            protected PaymentFormMetadatas paymentFormMetadatas;
            @XmlElement(name = "PriceMetadatas")
            protected PriceMetadatas priceMetadatas;
            @XmlElement(name = "RuleMetadatas")
            protected RuleMetadatas ruleMetadatas;
            @XmlElement(name = "StateProvMetadatas")
            protected StateProvMetadatas stateProvMetadatas;
            @XmlElement(name = "ZoneMetadatas")
            protected ZoneMetadatas zoneMetadatas;
            @XmlElement(name = "AddressMetadatas")
            protected AddressMetadatas addressMetadatas;

            /**
             * 获取aircraftMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AircraftMetadatas }
             *     
             */
            public AircraftMetadatas getAircraftMetadatas() {
                return aircraftMetadatas;
            }

            /**
             * 设置aircraftMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AircraftMetadatas }
             *     
             */
            public void setAircraftMetadatas(AircraftMetadatas value) {
                this.aircraftMetadatas = value;
            }

            /**
             * 获取airportMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AirportMetadatas }
             *     
             */
            public AirportMetadatas getAirportMetadatas() {
                return airportMetadatas;
            }

            /**
             * 设置airportMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AirportMetadatas }
             *     
             */
            public void setAirportMetadatas(AirportMetadatas value) {
                this.airportMetadatas = value;
            }

            /**
             * 获取cityMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CityMetadatas }
             *     
             */
            public CityMetadatas getCityMetadatas() {
                return cityMetadatas;
            }

            /**
             * 设置cityMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CityMetadatas }
             *     
             */
            public void setCityMetadatas(CityMetadatas value) {
                this.cityMetadatas = value;
            }

            /**
             * 获取codesetMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CodesetMetadatas }
             *     
             */
            public CodesetMetadatas getCodesetMetadatas() {
                return codesetMetadatas;
            }

            /**
             * 设置codesetMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CodesetMetadatas }
             *     
             */
            public void setCodesetMetadatas(CodesetMetadatas value) {
                this.codesetMetadatas = value;
            }

            /**
             * 获取contactMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ContactMetadatas }
             *     
             */
            public ContactMetadatas getContactMetadatas() {
                return contactMetadatas;
            }

            /**
             * 设置contactMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ContactMetadatas }
             *     
             */
            public void setContactMetadatas(ContactMetadatas value) {
                this.contactMetadatas = value;
            }

            /**
             * 获取countryMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CountryMetadatas }
             *     
             */
            public CountryMetadatas getCountryMetadatas() {
                return countryMetadatas;
            }

            /**
             * 设置countryMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CountryMetadatas }
             *     
             */
            public void setCountryMetadatas(CountryMetadatas value) {
                this.countryMetadatas = value;
            }

            /**
             * 获取currencyMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CurrencyMetadatas }
             *     
             */
            public CurrencyMetadatas getCurrencyMetadatas() {
                return currencyMetadatas;
            }

            /**
             * 设置currencyMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CurrencyMetadatas }
             *     
             */
            public void setCurrencyMetadatas(CurrencyMetadatas value) {
                this.currencyMetadatas = value;
            }

            /**
             * 获取descriptionMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link DescriptionMetadatas }
             *     
             */
            public DescriptionMetadatas getDescriptionMetadatas() {
                return descriptionMetadatas;
            }

            /**
             * 设置descriptionMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link DescriptionMetadatas }
             *     
             */
            public void setDescriptionMetadatas(DescriptionMetadatas value) {
                this.descriptionMetadatas = value;
            }

            /**
             * 获取equivalentIDMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link EquivalentIDMetadatas }
             *     
             */
            public EquivalentIDMetadatas getEquivalentIDMetadatas() {
                return equivalentIDMetadatas;
            }

            /**
             * 设置equivalentIDMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link EquivalentIDMetadatas }
             *     
             */
            public void setEquivalentIDMetadatas(EquivalentIDMetadatas value) {
                this.equivalentIDMetadatas = value;
            }

            /**
             * 获取languageMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link LanguageMetadatas }
             *     
             */
            public LanguageMetadatas getLanguageMetadatas() {
                return languageMetadatas;
            }

            /**
             * 设置languageMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link LanguageMetadatas }
             *     
             */
            public void setLanguageMetadatas(LanguageMetadatas value) {
                this.languageMetadatas = value;
            }

            /**
             * 获取paymentCardMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link PaymentCardMetadatas }
             *     
             */
            public PaymentCardMetadatas getPaymentCardMetadatas() {
                return paymentCardMetadatas;
            }

            /**
             * 设置paymentCardMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link PaymentCardMetadatas }
             *     
             */
            public void setPaymentCardMetadatas(PaymentCardMetadatas value) {
                this.paymentCardMetadatas = value;
            }

            /**
             * 获取paymentFormMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link PaymentFormMetadatas }
             *     
             */
            public PaymentFormMetadatas getPaymentFormMetadatas() {
                return paymentFormMetadatas;
            }

            /**
             * 设置paymentFormMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link PaymentFormMetadatas }
             *     
             */
            public void setPaymentFormMetadatas(PaymentFormMetadatas value) {
                this.paymentFormMetadatas = value;
            }

            /**
             * 获取priceMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link PriceMetadatas }
             *     
             */
            public PriceMetadatas getPriceMetadatas() {
                return priceMetadatas;
            }

            /**
             * 设置priceMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link PriceMetadatas }
             *     
             */
            public void setPriceMetadatas(PriceMetadatas value) {
                this.priceMetadatas = value;
            }

            /**
             * 获取ruleMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link RuleMetadatas }
             *     
             */
            public RuleMetadatas getRuleMetadatas() {
                return ruleMetadatas;
            }

            /**
             * 设置ruleMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link RuleMetadatas }
             *     
             */
            public void setRuleMetadatas(RuleMetadatas value) {
                this.ruleMetadatas = value;
            }

            /**
             * 获取stateProvMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link StateProvMetadatas }
             *     
             */
            public StateProvMetadatas getStateProvMetadatas() {
                return stateProvMetadatas;
            }

            /**
             * 设置stateProvMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link StateProvMetadatas }
             *     
             */
            public void setStateProvMetadatas(StateProvMetadatas value) {
                this.stateProvMetadatas = value;
            }

            /**
             * 获取zoneMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ZoneMetadatas }
             *     
             */
            public ZoneMetadatas getZoneMetadatas() {
                return zoneMetadatas;
            }

            /**
             * 设置zoneMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ZoneMetadatas }
             *     
             */
            public void setZoneMetadatas(ZoneMetadatas value) {
                this.zoneMetadatas = value;
            }

            /**
             * 获取addressMetadatas属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AddressMetadatas }
             *     
             */
            public AddressMetadatas getAddressMetadatas() {
                return addressMetadatas;
            }

            /**
             * 设置addressMetadatas属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AddressMetadatas }
             *     
             */
            public void setAddressMetadatas(AddressMetadatas value) {
                this.addressMetadatas = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ShopMetadataGroup"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "shopMetadataGroup"
    })
    public static class Shopping {

        @XmlElement(name = "ShopMetadataGroup", required = true)
        protected ShopMetadataGroup shopMetadataGroup;

        /**
         * 获取shopMetadataGroup属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ShopMetadataGroup }
         *     
         */
        public ShopMetadataGroup getShopMetadataGroup() {
            return shopMetadataGroup;
        }

        /**
         * 设置shopMetadataGroup属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ShopMetadataGroup }
         *     
         */
        public void setShopMetadataGroup(ShopMetadataGroup value) {
            this.shopMetadataGroup = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelerMetadata"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "travelerMetadata"
    })
    public static class Traveler {

        @XmlElement(name = "TravelerMetadata", required = true)
        protected TravelerMetadataType travelerMetadata;

        /**
         * 获取travelerMetadata属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TravelerMetadataType }
         *     
         */
        public TravelerMetadataType getTravelerMetadata() {
            return travelerMetadata;
        }

        /**
         * 设置travelerMetadata属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TravelerMetadataType }
         *     
         */
        public void setTravelerMetadata(TravelerMetadataType value) {
            this.travelerMetadata = value;
        }

    }

}
