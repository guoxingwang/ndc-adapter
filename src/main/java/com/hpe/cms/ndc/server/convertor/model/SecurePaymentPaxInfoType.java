//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * SECURE PAYMENT AIRLINE RESERVATION definition.
 * 
 * 
 * <p>SecurePaymentPaxInfoType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="SecurePaymentPaxInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Carriers" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Carrier" maxOccurs="4"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirlineID_Type"&gt;
 *                           &lt;attribute name="Application" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DateTimes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="DateTime" maxOccurs="4"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="Date" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *                           &lt;attribute name="Time" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType" /&gt;
 *                           &lt;attribute name="Application" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ClassesOfService" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ClassOfService" maxOccurs="4"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="StopLocations" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="StopLocation" maxOccurs="4"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FareBasisCodes" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FareBasisCode" maxOccurs="4"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FilghtNumbers" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightNumber" maxOccurs="4"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PassengerName" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="ResidenceCode" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CountrySimpleType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PassengerTktNbr" type="{http://www.iata.org/IATA/EDIST}TicketNumberSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="AgencyInfo" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecurePaymentPaxInfoType", propOrder = {
    "carriers",
    "dateTimes",
    "classesOfService",
    "stopLocations",
    "fareBasisCodes",
    "filghtNumbers",
    "passengerName",
    "residenceCode",
    "passengerTktNbr",
    "agencyInfo"
})
@XmlSeeAlso({
    ReservationInfo.class
})
public class SecurePaymentPaxInfoType
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "Carriers")
    protected SecurePaymentPaxInfoType.Carriers carriers;
    @XmlElement(name = "DateTimes")
    protected SecurePaymentPaxInfoType.DateTimes dateTimes;
    @XmlElement(name = "ClassesOfService")
    protected SecurePaymentPaxInfoType.ClassesOfService classesOfService;
    @XmlElement(name = "StopLocations")
    protected SecurePaymentPaxInfoType.StopLocations stopLocations;
    @XmlElement(name = "FareBasisCodes")
    protected SecurePaymentPaxInfoType.FareBasisCodes fareBasisCodes;
    @XmlElement(name = "FilghtNumbers")
    protected SecurePaymentPaxInfoType.FilghtNumbers filghtNumbers;
    @XmlElement(name = "PassengerName")
    protected String passengerName;
    @XmlElement(name = "ResidenceCode")
    protected SecurePaymentPaxInfoType.ResidenceCode residenceCode;
    @XmlElement(name = "PassengerTktNbr")
    protected String passengerTktNbr;
    @XmlElement(name = "AgencyInfo")
    protected String agencyInfo;

    /**
     * 获取carriers属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentPaxInfoType.Carriers }
     *     
     */
    public SecurePaymentPaxInfoType.Carriers getCarriers() {
        return carriers;
    }

    /**
     * 设置carriers属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentPaxInfoType.Carriers }
     *     
     */
    public void setCarriers(SecurePaymentPaxInfoType.Carriers value) {
        this.carriers = value;
    }

    /**
     * 获取dateTimes属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentPaxInfoType.DateTimes }
     *     
     */
    public SecurePaymentPaxInfoType.DateTimes getDateTimes() {
        return dateTimes;
    }

    /**
     * 设置dateTimes属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentPaxInfoType.DateTimes }
     *     
     */
    public void setDateTimes(SecurePaymentPaxInfoType.DateTimes value) {
        this.dateTimes = value;
    }

    /**
     * 获取classesOfService属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentPaxInfoType.ClassesOfService }
     *     
     */
    public SecurePaymentPaxInfoType.ClassesOfService getClassesOfService() {
        return classesOfService;
    }

    /**
     * 设置classesOfService属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentPaxInfoType.ClassesOfService }
     *     
     */
    public void setClassesOfService(SecurePaymentPaxInfoType.ClassesOfService value) {
        this.classesOfService = value;
    }

    /**
     * 获取stopLocations属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentPaxInfoType.StopLocations }
     *     
     */
    public SecurePaymentPaxInfoType.StopLocations getStopLocations() {
        return stopLocations;
    }

    /**
     * 设置stopLocations属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentPaxInfoType.StopLocations }
     *     
     */
    public void setStopLocations(SecurePaymentPaxInfoType.StopLocations value) {
        this.stopLocations = value;
    }

    /**
     * 获取fareBasisCodes属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentPaxInfoType.FareBasisCodes }
     *     
     */
    public SecurePaymentPaxInfoType.FareBasisCodes getFareBasisCodes() {
        return fareBasisCodes;
    }

    /**
     * 设置fareBasisCodes属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentPaxInfoType.FareBasisCodes }
     *     
     */
    public void setFareBasisCodes(SecurePaymentPaxInfoType.FareBasisCodes value) {
        this.fareBasisCodes = value;
    }

    /**
     * 获取filghtNumbers属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentPaxInfoType.FilghtNumbers }
     *     
     */
    public SecurePaymentPaxInfoType.FilghtNumbers getFilghtNumbers() {
        return filghtNumbers;
    }

    /**
     * 设置filghtNumbers属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentPaxInfoType.FilghtNumbers }
     *     
     */
    public void setFilghtNumbers(SecurePaymentPaxInfoType.FilghtNumbers value) {
        this.filghtNumbers = value;
    }

    /**
     * 获取passengerName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerName() {
        return passengerName;
    }

    /**
     * 设置passengerName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerName(String value) {
        this.passengerName = value;
    }

    /**
     * 获取residenceCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentPaxInfoType.ResidenceCode }
     *     
     */
    public SecurePaymentPaxInfoType.ResidenceCode getResidenceCode() {
        return residenceCode;
    }

    /**
     * 设置residenceCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentPaxInfoType.ResidenceCode }
     *     
     */
    public void setResidenceCode(SecurePaymentPaxInfoType.ResidenceCode value) {
        this.residenceCode = value;
    }

    /**
     * 获取passengerTktNbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerTktNbr() {
        return passengerTktNbr;
    }

    /**
     * 设置passengerTktNbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerTktNbr(String value) {
        this.passengerTktNbr = value;
    }

    /**
     * 获取agencyInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyInfo() {
        return agencyInfo;
    }

    /**
     * 设置agencyInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyInfo(String value) {
        this.agencyInfo = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Carrier" maxOccurs="4"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirlineID_Type"&gt;
     *                 &lt;attribute name="Application" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "carrier"
    })
    public static class Carriers {

        @XmlElement(name = "Carrier", required = true)
        protected List<SecurePaymentPaxInfoType.Carriers.Carrier> carrier;

        /**
         * Gets the value of the carrier property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the carrier property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCarrier().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SecurePaymentPaxInfoType.Carriers.Carrier }
         * 
         * 
         */
        public List<SecurePaymentPaxInfoType.Carriers.Carrier> getCarrier() {
            if (carrier == null) {
                carrier = new ArrayList<SecurePaymentPaxInfoType.Carriers.Carrier>();
            }
            return this.carrier;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirlineID_Type"&gt;
         *       &lt;attribute name="Application" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Carrier
            extends AirlineIDType
        {

            @XmlAttribute(name = "Application")
            protected String application;

            /**
             * 获取application属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getApplication() {
                return application;
            }

            /**
             * 设置application属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setApplication(String value) {
                this.application = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ClassOfService" maxOccurs="4"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "classOfService"
    })
    public static class ClassesOfService {

        @XmlElement(name = "ClassOfService", required = true)
        protected List<FlightCOSCoreType> classOfService;

        /**
         * Gets the value of the classOfService property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the classOfService property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getClassOfService().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FlightCOSCoreType }
         * 
         * 
         */
        public List<FlightCOSCoreType> getClassOfService() {
            if (classOfService == null) {
                classOfService = new ArrayList<FlightCOSCoreType>();
            }
            return this.classOfService;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="DateTime" maxOccurs="4"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="Date" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
     *                 &lt;attribute name="Time" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType" /&gt;
     *                 &lt;attribute name="Application" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dateTime"
    })
    public static class DateTimes {

        @XmlElement(name = "DateTime", required = true)
        protected List<SecurePaymentPaxInfoType.DateTimes.DateTime> dateTime;

        /**
         * Gets the value of the dateTime property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dateTime property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDateTime().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SecurePaymentPaxInfoType.DateTimes.DateTime }
         * 
         * 
         */
        public List<SecurePaymentPaxInfoType.DateTimes.DateTime> getDateTime() {
            if (dateTime == null) {
                dateTime = new ArrayList<SecurePaymentPaxInfoType.DateTimes.DateTime>();
            }
            return this.dateTime;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="Date" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
         *       &lt;attribute name="Time" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType" /&gt;
         *       &lt;attribute name="Application" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class DateTime {

            @XmlAttribute(name = "Date")
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar date;
            @XmlAttribute(name = "Time")
            protected String time;
            @XmlAttribute(name = "Application")
            protected String application;

            /**
             * 获取date属性的值。
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDate() {
                return date;
            }

            /**
             * 设置date属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDate(XMLGregorianCalendar value) {
                this.date = value;
            }

            /**
             * 获取time属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTime() {
                return time;
            }

            /**
             * 设置time属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTime(String value) {
                this.time = value;
            }

            /**
             * 获取application属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getApplication() {
                return application;
            }

            /**
             * 设置application属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setApplication(String value) {
                this.application = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FareBasisCode" maxOccurs="4"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fareBasisCode"
    })
    public static class FareBasisCodes {

        @XmlElement(name = "FareBasisCode", required = true)
        protected List<FareBasisCodeType> fareBasisCode;

        /**
         * Gets the value of the fareBasisCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fareBasisCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFareBasisCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FareBasisCodeType }
         * 
         * 
         */
        public List<FareBasisCodeType> getFareBasisCode() {
            if (fareBasisCode == null) {
                fareBasisCode = new ArrayList<FareBasisCodeType>();
            }
            return this.fareBasisCode;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightNumber" maxOccurs="4"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "flightNumber"
    })
    public static class FilghtNumbers {

        @XmlElement(name = "FlightNumber", required = true)
        protected List<FlightNumber> flightNumber;

        /**
         * Gets the value of the flightNumber property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the flightNumber property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFlightNumber().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FlightNumber }
         * 
         * 
         */
        public List<FlightNumber> getFlightNumber() {
            if (flightNumber == null) {
                flightNumber = new ArrayList<FlightNumber>();
            }
            return this.flightNumber;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CountrySimpleType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class ResidenceCode {

        @XmlValue
        protected String value;

        /**
         * A data type for Country Code encoding constraint: ISO 3166-1 (two character) Country Code
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="StopLocation" maxOccurs="4"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stopLocation"
    })
    public static class StopLocations {

        @XmlElement(name = "StopLocation", required = true)
        protected List<SecurePaymentPaxInfoType.StopLocations.StopLocation> stopLocation;

        /**
         * Gets the value of the stopLocation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the stopLocation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStopLocation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SecurePaymentPaxInfoType.StopLocations.StopLocation }
         * 
         * 
         */
        public List<SecurePaymentPaxInfoType.StopLocations.StopLocation> getStopLocation() {
            if (stopLocation == null) {
                stopLocation = new ArrayList<SecurePaymentPaxInfoType.StopLocations.StopLocation>();
            }
            return this.stopLocation;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "airportCode"
        })
        public static class StopLocation {

            @XmlElement(name = "AirportCode", required = true)
            protected AirportCode airportCode;

            /**
             * 获取airportCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AirportCode }
             *     
             */
            public AirportCode getAirportCode() {
                return airportCode;
            }

            /**
             * 设置airportCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AirportCode }
             *     
             */
            public void setAirportCode(AirportCode value) {
                this.airportCode = value;
            }

        }

    }

}
