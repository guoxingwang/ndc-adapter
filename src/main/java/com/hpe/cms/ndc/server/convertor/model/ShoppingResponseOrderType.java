//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * ORDER INPUT SHOPPING RESPONSE object definition.
 * 
 * <p>ShoppingResponseOrderType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ShoppingResponseOrderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineDesigSimpleType"/&gt;
 *         &lt;element name="ResponseID"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;UniqueStringID_SimpleType"&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Offers"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Offer" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="OfferID" type="{http://www.iata.org/IATA/EDIST}ItemID_Type"/&gt;
 *                             &lt;element name="OfferItems"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="OfferItem" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}OfferItemID"/&gt;
 *                                                 &lt;element name="Passengers" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="TotalPrice" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;choice&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
 *                                                         &lt;/choice&gt;
 *                                                         &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}ApplicableFlight" minOccurs="0"/&gt;
 *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}IncludedService" minOccurs="0"/&gt;
 *                                                 &lt;element name="AssociatedServices" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="AssociatedService" maxOccurs="unbounded"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" minOccurs="0"/&gt;
 *                                                                     &lt;element name="Passengers" minOccurs="0"&gt;
 *                                                                       &lt;complexType&gt;
 *                                                                         &lt;complexContent&gt;
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                             &lt;sequence&gt;
 *                                                                               &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
 *                                                                             &lt;/sequence&gt;
 *                                                                           &lt;/restriction&gt;
 *                                                                         &lt;/complexContent&gt;
 *                                                                       &lt;/complexType&gt;
 *                                                                     &lt;/element&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/extension&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="Details" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                         &lt;/sequence&gt;
 *                                                         &lt;attribute name="WaitListInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/extension&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="TotalPrice" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;choice&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
 *                                     &lt;/choice&gt;
 *                                     &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShoppingResponseOrderType", propOrder = {
    "owner",
    "responseID",
    "offers"
})
public class ShoppingResponseOrderType
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "Owner", required = true)
    protected String owner;
    @XmlElement(name = "ResponseID", required = true)
    protected ShoppingResponseOrderType.ResponseID responseID;
    @XmlElement(name = "Offers", required = true)
    protected ShoppingResponseOrderType.Offers offers;

    /**
     * 获取owner属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * 设置owner属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * 获取responseID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShoppingResponseOrderType.ResponseID }
     *     
     */
    public ShoppingResponseOrderType.ResponseID getResponseID() {
        return responseID;
    }

    /**
     * 设置responseID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShoppingResponseOrderType.ResponseID }
     *     
     */
    public void setResponseID(ShoppingResponseOrderType.ResponseID value) {
        this.responseID = value;
    }

    /**
     * 获取offers属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShoppingResponseOrderType.Offers }
     *     
     */
    public ShoppingResponseOrderType.Offers getOffers() {
        return offers;
    }

    /**
     * 设置offers属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShoppingResponseOrderType.Offers }
     *     
     */
    public void setOffers(ShoppingResponseOrderType.Offers value) {
        this.offers = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Offer" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="OfferID" type="{http://www.iata.org/IATA/EDIST}ItemID_Type"/&gt;
     *                   &lt;element name="OfferItems"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="OfferItem" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}OfferItemID"/&gt;
     *                                       &lt;element name="Passengers" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="TotalPrice" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;choice&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
     *                                               &lt;/choice&gt;
     *                                               &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}ApplicableFlight" minOccurs="0"/&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}IncludedService" minOccurs="0"/&gt;
     *                                       &lt;element name="AssociatedServices" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="AssociatedService" maxOccurs="unbounded"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" minOccurs="0"/&gt;
     *                                                           &lt;element name="Passengers" minOccurs="0"&gt;
     *                                                             &lt;complexType&gt;
     *                                                               &lt;complexContent&gt;
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                                   &lt;sequence&gt;
     *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
     *                                                                   &lt;/sequence&gt;
     *                                                                 &lt;/restriction&gt;
     *                                                               &lt;/complexContent&gt;
     *                                                             &lt;/complexType&gt;
     *                                                           &lt;/element&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/extension&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="Details" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                               &lt;attribute name="WaitListInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/extension&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="TotalPrice" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;choice&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
     *                           &lt;/choice&gt;
     *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offer"
    })
    public static class Offers {

        @XmlElement(name = "Offer", required = true)
        protected List<ShoppingResponseOrderType.Offers.Offer> offer;

        /**
         * Gets the value of the offer property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the offer property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOffer().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ShoppingResponseOrderType.Offers.Offer }
         * 
         * 
         */
        public List<ShoppingResponseOrderType.Offers.Offer> getOffer() {
            if (offer == null) {
                offer = new ArrayList<ShoppingResponseOrderType.Offers.Offer>();
            }
            return this.offer;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="OfferID" type="{http://www.iata.org/IATA/EDIST}ItemID_Type"/&gt;
         *         &lt;element name="OfferItems"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="OfferItem" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OfferItemID"/&gt;
         *                             &lt;element name="Passengers" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="TotalPrice" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;choice&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
         *                                     &lt;/choice&gt;
         *                                     &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ApplicableFlight" minOccurs="0"/&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}IncludedService" minOccurs="0"/&gt;
         *                             &lt;element name="AssociatedServices" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="AssociatedService" maxOccurs="unbounded"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" minOccurs="0"/&gt;
         *                                                 &lt;element name="Passengers" minOccurs="0"&gt;
         *                                                   &lt;complexType&gt;
         *                                                     &lt;complexContent&gt;
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                                         &lt;sequence&gt;
         *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
         *                                                         &lt;/sequence&gt;
         *                                                       &lt;/restriction&gt;
         *                                                     &lt;/complexContent&gt;
         *                                                   &lt;/complexType&gt;
         *                                                 &lt;/element&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/extension&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="Details" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                     &lt;attribute name="WaitListInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/extension&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="TotalPrice" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;choice&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
         *                 &lt;/choice&gt;
         *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "offerID",
            "offerItems",
            "totalPrice"
        })
        public static class Offer {

            @XmlElement(name = "OfferID", required = true)
            protected ItemIDType offerID;
            @XmlElement(name = "OfferItems", required = true)
            protected ShoppingResponseOrderType.Offers.Offer.OfferItems offerItems;
            @XmlElement(name = "TotalPrice")
            protected ShoppingResponseOrderType.Offers.Offer.TotalPrice totalPrice;

            /**
             * 获取offerID属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ItemIDType }
             *     
             */
            public ItemIDType getOfferID() {
                return offerID;
            }

            /**
             * 设置offerID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ItemIDType }
             *     
             */
            public void setOfferID(ItemIDType value) {
                this.offerID = value;
            }

            /**
             * 获取offerItems属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems }
             *     
             */
            public ShoppingResponseOrderType.Offers.Offer.OfferItems getOfferItems() {
                return offerItems;
            }

            /**
             * 设置offerItems属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems }
             *     
             */
            public void setOfferItems(ShoppingResponseOrderType.Offers.Offer.OfferItems value) {
                this.offerItems = value;
            }

            /**
             * 获取totalPrice属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ShoppingResponseOrderType.Offers.Offer.TotalPrice }
             *     
             */
            public ShoppingResponseOrderType.Offers.Offer.TotalPrice getTotalPrice() {
                return totalPrice;
            }

            /**
             * 设置totalPrice属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ShoppingResponseOrderType.Offers.Offer.TotalPrice }
             *     
             */
            public void setTotalPrice(ShoppingResponseOrderType.Offers.Offer.TotalPrice value) {
                this.totalPrice = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="OfferItem" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OfferItemID"/&gt;
             *                   &lt;element name="Passengers" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="TotalPrice" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;choice&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
             *                           &lt;/choice&gt;
             *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ApplicableFlight" minOccurs="0"/&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}IncludedService" minOccurs="0"/&gt;
             *                   &lt;element name="AssociatedServices" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="AssociatedService" maxOccurs="unbounded"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" minOccurs="0"/&gt;
             *                                       &lt;element name="Passengers" minOccurs="0"&gt;
             *                                         &lt;complexType&gt;
             *                                           &lt;complexContent&gt;
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                               &lt;sequence&gt;
             *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
             *                                               &lt;/sequence&gt;
             *                                             &lt;/restriction&gt;
             *                                           &lt;/complexContent&gt;
             *                                         &lt;/complexType&gt;
             *                                       &lt;/element&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/extension&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="Details" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                           &lt;attribute name="WaitListInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/extension&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offerItem"
            })
            public static class OfferItems {

                @XmlElement(name = "OfferItem", required = true)
                protected List<ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem> offerItem;

                /**
                 * Gets the value of the offerItem property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the offerItem property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getOfferItem().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem }
                 * 
                 * 
                 */
                public List<ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem> getOfferItem() {
                    if (offerItem == null) {
                        offerItem = new ArrayList<ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem>();
                    }
                    return this.offerItem;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OfferItemID"/&gt;
                 *         &lt;element name="Passengers" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="TotalPrice" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;choice&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
                 *                 &lt;/choice&gt;
                 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ApplicableFlight" minOccurs="0"/&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}IncludedService" minOccurs="0"/&gt;
                 *         &lt;element name="AssociatedServices" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="AssociatedService" maxOccurs="unbounded"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" minOccurs="0"/&gt;
                 *                             &lt;element name="Passengers" minOccurs="0"&gt;
                 *                               &lt;complexType&gt;
                 *                                 &lt;complexContent&gt;
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                                     &lt;sequence&gt;
                 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
                 *                                     &lt;/sequence&gt;
                 *                                   &lt;/restriction&gt;
                 *                                 &lt;/complexContent&gt;
                 *                               &lt;/complexType&gt;
                 *                             &lt;/element&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/extension&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="Details" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *                 &lt;attribute name="WaitListInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/extension&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "offerItemID",
                    "passengers",
                    "totalPrice",
                    "applicableFlight",
                    "includedService",
                    "associatedServices",
                    "details"
                })
                public static class OfferItem
                    extends AssociatedObjectBaseType
                {

                    @XmlElement(name = "OfferItemID", required = true)
                    protected ItemIDType offerItemID;
                    @XmlElement(name = "Passengers")
                    protected ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Passengers passengers;
                    @XmlElement(name = "TotalPrice")
                    protected ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.TotalPrice totalPrice;
                    @XmlElement(name = "ApplicableFlight")
                    protected ApplicableFlight applicableFlight;
                    @XmlElement(name = "IncludedService")
                    protected ServiceInfoAssocType includedService;
                    @XmlElement(name = "AssociatedServices")
                    protected ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices associatedServices;
                    @XmlElement(name = "Details")
                    protected ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details details;

                    /**
                     * 获取offerItemID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ItemIDType }
                     *     
                     */
                    public ItemIDType getOfferItemID() {
                        return offerItemID;
                    }

                    /**
                     * 设置offerItemID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ItemIDType }
                     *     
                     */
                    public void setOfferItemID(ItemIDType value) {
                        this.offerItemID = value;
                    }

                    /**
                     * 获取passengers属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Passengers }
                     *     
                     */
                    public ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Passengers getPassengers() {
                        return passengers;
                    }

                    /**
                     * 设置passengers属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Passengers }
                     *     
                     */
                    public void setPassengers(ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Passengers value) {
                        this.passengers = value;
                    }

                    /**
                     * 获取totalPrice属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.TotalPrice }
                     *     
                     */
                    public ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.TotalPrice getTotalPrice() {
                        return totalPrice;
                    }

                    /**
                     * 设置totalPrice属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.TotalPrice }
                     *     
                     */
                    public void setTotalPrice(ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.TotalPrice value) {
                        this.totalPrice = value;
                    }

                    /**
                     * 获取applicableFlight属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ApplicableFlight }
                     *     
                     */
                    public ApplicableFlight getApplicableFlight() {
                        return applicableFlight;
                    }

                    /**
                     * 设置applicableFlight属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ApplicableFlight }
                     *     
                     */
                    public void setApplicableFlight(ApplicableFlight value) {
                        this.applicableFlight = value;
                    }

                    /**
                     * 获取includedService属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ServiceInfoAssocType }
                     *     
                     */
                    public ServiceInfoAssocType getIncludedService() {
                        return includedService;
                    }

                    /**
                     * 设置includedService属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ServiceInfoAssocType }
                     *     
                     */
                    public void setIncludedService(ServiceInfoAssocType value) {
                        this.includedService = value;
                    }

                    /**
                     * 获取associatedServices属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices }
                     *     
                     */
                    public ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices getAssociatedServices() {
                        return associatedServices;
                    }

                    /**
                     * 设置associatedServices属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices }
                     *     
                     */
                    public void setAssociatedServices(ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices value) {
                        this.associatedServices = value;
                    }

                    /**
                     * 获取details属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details }
                     *     
                     */
                    public ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details getDetails() {
                        return details;
                    }

                    /**
                     * 设置details属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details }
                     *     
                     */
                    public void setDetails(ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details value) {
                        this.details = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="AssociatedService" maxOccurs="unbounded"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" minOccurs="0"/&gt;
                     *                   &lt;element name="Passengers" minOccurs="0"&gt;
                     *                     &lt;complexType&gt;
                     *                       &lt;complexContent&gt;
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                           &lt;sequence&gt;
                     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
                     *                           &lt;/sequence&gt;
                     *                         &lt;/restriction&gt;
                     *                       &lt;/complexContent&gt;
                     *                     &lt;/complexType&gt;
                     *                   &lt;/element&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/extension&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "associatedService"
                    })
                    public static class AssociatedServices {

                        @XmlElement(name = "AssociatedService", required = true)
                        protected List<ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService> associatedService;

                        /**
                         * Gets the value of the associatedService property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the associatedService property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getAssociatedService().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService }
                         * 
                         * 
                         */
                        public List<ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService> getAssociatedService() {
                            if (associatedService == null) {
                                associatedService = new ArrayList<ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService>();
                            }
                            return this.associatedService;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" minOccurs="0"/&gt;
                         *         &lt;element name="Passengers" minOccurs="0"&gt;
                         *           &lt;complexType&gt;
                         *             &lt;complexContent&gt;
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *                 &lt;sequence&gt;
                         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
                         *                 &lt;/sequence&gt;
                         *               &lt;/restriction&gt;
                         *             &lt;/complexContent&gt;
                         *           &lt;/complexType&gt;
                         *         &lt;/element&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/extension&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "serviceID",
                            "passengers"
                        })
                        public static class AssociatedService
                            extends AssociatedObjectBaseType
                        {

                            @XmlElement(name = "ServiceID")
                            protected ServiceIDType serviceID;
                            @XmlElement(name = "Passengers")
                            protected ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService.Passengers passengers;

                            /**
                             * 获取serviceID属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link ServiceIDType }
                             *     
                             */
                            public ServiceIDType getServiceID() {
                                return serviceID;
                            }

                            /**
                             * 设置serviceID属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link ServiceIDType }
                             *     
                             */
                            public void setServiceID(ServiceIDType value) {
                                this.serviceID = value;
                            }

                            /**
                             * 获取passengers属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService.Passengers }
                             *     
                             */
                            public ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService.Passengers getPassengers() {
                                return passengers;
                            }

                            /**
                             * 设置passengers属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService.Passengers }
                             *     
                             */
                            public void setPassengers(ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService.Passengers value) {
                                this.passengers = value;
                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType&gt;
                             *   &lt;complexContent&gt;
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                             *       &lt;sequence&gt;
                             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
                             *       &lt;/sequence&gt;
                             *     &lt;/restriction&gt;
                             *   &lt;/complexContent&gt;
                             * &lt;/complexType&gt;
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "passengerReference"
                            })
                            public static class Passengers {

                                @XmlList
                                @XmlElement(name = "PassengerReference", required = true)
                                @XmlIDREF
                                @XmlSchemaType(name = "IDREFS")
                                protected List<Object> passengerReference;

                                /**
                                 * 
                                 *                                       Association to multiple Passenger instance(s). Example: pax1 pax2
                                 *                                     Gets the value of the passengerReference property.
                                 * 
                                 * <p>
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a <CODE>set</CODE> method for the passengerReference property.
                                 * 
                                 * <p>
                                 * For example, to add a new item, do as follows:
                                 * <pre>
                                 *    getPassengerReference().add(newItem);
                                 * </pre>
                                 * 
                                 * 
                                 * <p>
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link Object }
                                 * 
                                 * 
                                 */
                                public List<Object> getPassengerReference() {
                                    if (passengerReference == null) {
                                        passengerReference = new ArrayList<Object>();
                                    }
                                    return this.passengerReference;
                                }

                            }

                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *       &lt;attribute name="WaitListInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "inventoryGuarantee"
                    })
                    public static class Details {

                        @XmlElement(name = "InventoryGuarantee")
                        protected ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details.InventoryGuarantee inventoryGuarantee;
                        @XmlAttribute(name = "WaitListInd")
                        protected Boolean waitListInd;

                        /**
                         * 获取inventoryGuarantee属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details.InventoryGuarantee }
                         *     
                         */
                        public ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details.InventoryGuarantee getInventoryGuarantee() {
                            return inventoryGuarantee;
                        }

                        /**
                         * 设置inventoryGuarantee属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details.InventoryGuarantee }
                         *     
                         */
                        public void setInventoryGuarantee(ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.Details.InventoryGuarantee value) {
                            this.inventoryGuarantee = value;
                        }

                        /**
                         * 获取waitListInd属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Boolean }
                         *     
                         */
                        public Boolean isWaitListInd() {
                            return waitListInd;
                        }

                        /**
                         * 设置waitListInd属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Boolean }
                         *     
                         */
                        public void setWaitListInd(Boolean value) {
                            this.waitListInd = value;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "invGuaranteeID",
                            "invGuaranteeTimeLimits"
                        })
                        public static class InventoryGuarantee {

                            @XmlElement(name = "InvGuaranteeID", required = true)
                            protected String invGuaranteeID;
                            @XmlElement(name = "InvGuaranteeTimeLimits", required = true)
                            protected InvGuaranteeTimeLimits invGuaranteeTimeLimits;

                            /**
                             * 获取invGuaranteeID属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getInvGuaranteeID() {
                                return invGuaranteeID;
                            }

                            /**
                             * 设置invGuaranteeID属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setInvGuaranteeID(String value) {
                                this.invGuaranteeID = value;
                            }

                            /**
                             * Time limit by which guaranteed inventory for a specified product offer is guaranteed as available and by which the inventory held must be converted into a completed order before the time limit expires otherwise the guarantee is lost.
                             * 
                             * Examples: 2017-05-30T09:00:00,  2015-01-13T13:59:38Z
                             * 
                             * @return
                             *     possible object is
                             *     {@link InvGuaranteeTimeLimits }
                             *     
                             */
                            public InvGuaranteeTimeLimits getInvGuaranteeTimeLimits() {
                                return invGuaranteeTimeLimits;
                            }

                            /**
                             * 设置invGuaranteeTimeLimits属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link InvGuaranteeTimeLimits }
                             *     
                             */
                            public void setInvGuaranteeTimeLimits(InvGuaranteeTimeLimits value) {
                                this.invGuaranteeTimeLimits = value;
                            }

                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReference"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "passengerReference"
                    })
                    public static class Passengers {

                        @XmlList
                        @XmlElement(name = "PassengerReference", required = true)
                        @XmlIDREF
                        @XmlSchemaType(name = "IDREFS")
                        protected List<Object> passengerReference;

                        /**
                         * 
                         *                                       Association to multiple Passenger instance(s). Example: pax1 pax2
                         *                                     Gets the value of the passengerReference property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the passengerReference property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getPassengerReference().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link Object }
                         * 
                         * 
                         */
                        public List<Object> getPassengerReference() {
                            if (passengerReference == null) {
                                passengerReference = new ArrayList<Object>();
                            }
                            return this.passengerReference;
                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;choice&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
                     *       &lt;/choice&gt;
                     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "awardPricing",
                        "combinationPricing",
                        "simpleCurrencyPrice",
                        "detailCurrencyPrice",
                        "encodedCurrencyPrice"
                    })
                    public static class TotalPrice {

                        @XmlElement(name = "AwardPricing")
                        protected AwardPriceUnitType awardPricing;
                        @XmlElement(name = "CombinationPricing")
                        protected CombinationPriceType combinationPricing;
                        @XmlElement(name = "SimpleCurrencyPrice")
                        protected SimpleCurrencyPriceType simpleCurrencyPrice;
                        @XmlElement(name = "DetailCurrencyPrice")
                        protected DetailCurrencyPriceType detailCurrencyPrice;
                        @XmlElement(name = "EncodedCurrencyPrice")
                        protected EncodedPriceType encodedCurrencyPrice;
                        @XmlAttribute(name = "refs")
                        @XmlIDREF
                        protected List<Object> refs;

                        /**
                         * 获取awardPricing属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AwardPriceUnitType }
                         *     
                         */
                        public AwardPriceUnitType getAwardPricing() {
                            return awardPricing;
                        }

                        /**
                         * 设置awardPricing属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AwardPriceUnitType }
                         *     
                         */
                        public void setAwardPricing(AwardPriceUnitType value) {
                            this.awardPricing = value;
                        }

                        /**
                         * 获取combinationPricing属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link CombinationPriceType }
                         *     
                         */
                        public CombinationPriceType getCombinationPricing() {
                            return combinationPricing;
                        }

                        /**
                         * 设置combinationPricing属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link CombinationPriceType }
                         *     
                         */
                        public void setCombinationPricing(CombinationPriceType value) {
                            this.combinationPricing = value;
                        }

                        /**
                         * 获取simpleCurrencyPrice属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link SimpleCurrencyPriceType }
                         *     
                         */
                        public SimpleCurrencyPriceType getSimpleCurrencyPrice() {
                            return simpleCurrencyPrice;
                        }

                        /**
                         * 设置simpleCurrencyPrice属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link SimpleCurrencyPriceType }
                         *     
                         */
                        public void setSimpleCurrencyPrice(SimpleCurrencyPriceType value) {
                            this.simpleCurrencyPrice = value;
                        }

                        /**
                         * 获取detailCurrencyPrice属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link DetailCurrencyPriceType }
                         *     
                         */
                        public DetailCurrencyPriceType getDetailCurrencyPrice() {
                            return detailCurrencyPrice;
                        }

                        /**
                         * 设置detailCurrencyPrice属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link DetailCurrencyPriceType }
                         *     
                         */
                        public void setDetailCurrencyPrice(DetailCurrencyPriceType value) {
                            this.detailCurrencyPrice = value;
                        }

                        /**
                         * 获取encodedCurrencyPrice属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link EncodedPriceType }
                         *     
                         */
                        public EncodedPriceType getEncodedCurrencyPrice() {
                            return encodedCurrencyPrice;
                        }

                        /**
                         * 设置encodedCurrencyPrice属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link EncodedPriceType }
                         *     
                         */
                        public void setEncodedCurrencyPrice(EncodedPriceType value) {
                            this.encodedCurrencyPrice = value;
                        }

                        /**
                         * Gets the value of the refs property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the refs property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getRefs().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link Object }
                         * 
                         * 
                         */
                        public List<Object> getRefs() {
                            if (refs == null) {
                                refs = new ArrayList<Object>();
                            }
                            return this.refs;
                        }

                    }

                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;choice&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AwardPricing"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CombinationPricing"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPrice"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DetailCurrencyPrice"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}EncodedCurrencyPrice"/&gt;
             *       &lt;/choice&gt;
             *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "awardPricing",
                "combinationPricing",
                "simpleCurrencyPrice",
                "detailCurrencyPrice",
                "encodedCurrencyPrice"
            })
            public static class TotalPrice {

                @XmlElement(name = "AwardPricing")
                protected AwardPriceUnitType awardPricing;
                @XmlElement(name = "CombinationPricing")
                protected CombinationPriceType combinationPricing;
                @XmlElement(name = "SimpleCurrencyPrice")
                protected SimpleCurrencyPriceType simpleCurrencyPrice;
                @XmlElement(name = "DetailCurrencyPrice")
                protected DetailCurrencyPriceType detailCurrencyPrice;
                @XmlElement(name = "EncodedCurrencyPrice")
                protected EncodedPriceType encodedCurrencyPrice;
                @XmlAttribute(name = "refs")
                @XmlIDREF
                protected List<Object> refs;

                /**
                 * 获取awardPricing属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link AwardPriceUnitType }
                 *     
                 */
                public AwardPriceUnitType getAwardPricing() {
                    return awardPricing;
                }

                /**
                 * 设置awardPricing属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AwardPriceUnitType }
                 *     
                 */
                public void setAwardPricing(AwardPriceUnitType value) {
                    this.awardPricing = value;
                }

                /**
                 * 获取combinationPricing属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CombinationPriceType }
                 *     
                 */
                public CombinationPriceType getCombinationPricing() {
                    return combinationPricing;
                }

                /**
                 * 设置combinationPricing属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CombinationPriceType }
                 *     
                 */
                public void setCombinationPricing(CombinationPriceType value) {
                    this.combinationPricing = value;
                }

                /**
                 * 获取simpleCurrencyPrice属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link SimpleCurrencyPriceType }
                 *     
                 */
                public SimpleCurrencyPriceType getSimpleCurrencyPrice() {
                    return simpleCurrencyPrice;
                }

                /**
                 * 设置simpleCurrencyPrice属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SimpleCurrencyPriceType }
                 *     
                 */
                public void setSimpleCurrencyPrice(SimpleCurrencyPriceType value) {
                    this.simpleCurrencyPrice = value;
                }

                /**
                 * 获取detailCurrencyPrice属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link DetailCurrencyPriceType }
                 *     
                 */
                public DetailCurrencyPriceType getDetailCurrencyPrice() {
                    return detailCurrencyPrice;
                }

                /**
                 * 设置detailCurrencyPrice属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DetailCurrencyPriceType }
                 *     
                 */
                public void setDetailCurrencyPrice(DetailCurrencyPriceType value) {
                    this.detailCurrencyPrice = value;
                }

                /**
                 * 获取encodedCurrencyPrice属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link EncodedPriceType }
                 *     
                 */
                public EncodedPriceType getEncodedCurrencyPrice() {
                    return encodedCurrencyPrice;
                }

                /**
                 * 设置encodedCurrencyPrice属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link EncodedPriceType }
                 *     
                 */
                public void setEncodedCurrencyPrice(EncodedPriceType value) {
                    this.encodedCurrencyPrice = value;
                }

                /**
                 * Gets the value of the refs property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the refs property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getRefs().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Object }
                 * 
                 * 
                 */
                public List<Object> getRefs() {
                    if (refs == null) {
                        refs = new ArrayList<Object>();
                    }
                    return this.refs;
                }

            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;UniqueStringID_SimpleType"&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class ResponseID {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * A data type for a Unique String Identifier constraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }

}
