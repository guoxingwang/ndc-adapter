//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "qualifier"
})
@XmlRootElement(name = "Qualifiers")
public class Qualifiers {

    @XmlElement(name = "Qualifier", required = true)
    protected List<Qualifiers.Qualifier> qualifier;

    /**
     * Gets the value of the qualifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the qualifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQualifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Qualifiers.Qualifier }
     * 
     * 
     */
    public List<Qualifiers.Qualifier> getQualifier() {
        if (qualifier == null) {
            qualifier = new ArrayList<Qualifiers.Qualifier>();
        }
        return this.qualifier;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "baggagePricingQualifier",
        "existingOrderQualifier",
        "paymentCardQualifiers",
        "programQualifiers",
        "promotionQualifiers",
        "seatQualifier",
        "serviceQualifier",
        "socialMediaQualifiers",
        "specialFareQualifiers",
        "specialNeedQualifiers",
        "tripPurposeQualifier"
    })
    public static class Qualifier {

        @XmlElement(name = "BaggagePricingQualifier")
        protected BaggagePricingQualifierType baggagePricingQualifier;
        @XmlElement(name = "ExistingOrderQualifier")
        protected ExistingOrderQualifier existingOrderQualifier;
        @XmlElement(name = "PaymentCardQualifiers")
        protected CardQualifierType paymentCardQualifiers;
        @XmlElement(name = "ProgramQualifiers")
        protected ProgramQualifiers programQualifiers;
        @XmlElement(name = "PromotionQualifiers")
        protected PromotionQualifiers promotionQualifiers;
        @XmlElement(name = "SeatQualifier")
        protected SeatQualifier seatQualifier;
        @XmlElement(name = "ServiceQualifier")
        protected ServiceQualifierPriceType serviceQualifier;
        @XmlElement(name = "SocialMediaQualifiers")
        protected SocialQualiferType socialMediaQualifiers;
        @XmlElement(name = "SpecialFareQualifiers")
        protected FareQualifierType specialFareQualifiers;
        @XmlElement(name = "SpecialNeedQualifiers")
        protected SpecialQualiferType specialNeedQualifiers;
        @XmlElement(name = "TripPurposeQualifier")
        @XmlSchemaType(name = "string")
        protected TripPurposeListType tripPurposeQualifier;

        /**
         * 获取baggagePricingQualifier属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BaggagePricingQualifierType }
         *     
         */
        public BaggagePricingQualifierType getBaggagePricingQualifier() {
            return baggagePricingQualifier;
        }

        /**
         * 设置baggagePricingQualifier属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BaggagePricingQualifierType }
         *     
         */
        public void setBaggagePricingQualifier(BaggagePricingQualifierType value) {
            this.baggagePricingQualifier = value;
        }

        /**
         * 获取existingOrderQualifier属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ExistingOrderQualifier }
         *     
         */
        public ExistingOrderQualifier getExistingOrderQualifier() {
            return existingOrderQualifier;
        }

        /**
         * 设置existingOrderQualifier属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ExistingOrderQualifier }
         *     
         */
        public void setExistingOrderQualifier(ExistingOrderQualifier value) {
            this.existingOrderQualifier = value;
        }

        /**
         * Contains card type, and Issuer Identification Number. 
         * 
         * @return
         *     possible object is
         *     {@link CardQualifierType }
         *     
         */
        public CardQualifierType getPaymentCardQualifiers() {
            return paymentCardQualifiers;
        }

        /**
         * 设置paymentCardQualifiers属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CardQualifierType }
         *     
         */
        public void setPaymentCardQualifiers(CardQualifierType value) {
            this.paymentCardQualifiers = value;
        }

        /**
         * 获取programQualifiers属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ProgramQualifiers }
         *     
         */
        public ProgramQualifiers getProgramQualifiers() {
            return programQualifiers;
        }

        /**
         * 设置programQualifiers属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ProgramQualifiers }
         *     
         */
        public void setProgramQualifiers(ProgramQualifiers value) {
            this.programQualifiers = value;
        }

        /**
         * 获取promotionQualifiers属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PromotionQualifiers }
         *     
         */
        public PromotionQualifiers getPromotionQualifiers() {
            return promotionQualifiers;
        }

        /**
         * 设置promotionQualifiers属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PromotionQualifiers }
         *     
         */
        public void setPromotionQualifiers(PromotionQualifiers value) {
            this.promotionQualifiers = value;
        }

        /**
         * 获取seatQualifier属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SeatQualifier }
         *     
         */
        public SeatQualifier getSeatQualifier() {
            return seatQualifier;
        }

        /**
         * 设置seatQualifier属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SeatQualifier }
         *     
         */
        public void setSeatQualifier(SeatQualifier value) {
            this.seatQualifier = value;
        }

        /**
         * 获取serviceQualifier属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceQualifierPriceType }
         *     
         */
        public ServiceQualifierPriceType getServiceQualifier() {
            return serviceQualifier;
        }

        /**
         * 设置serviceQualifier属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceQualifierPriceType }
         *     
         */
        public void setServiceQualifier(ServiceQualifierPriceType value) {
            this.serviceQualifier = value;
        }

        /**
         * 获取socialMediaQualifiers属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SocialQualiferType }
         *     
         */
        public SocialQualiferType getSocialMediaQualifiers() {
            return socialMediaQualifiers;
        }

        /**
         * 设置socialMediaQualifiers属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SocialQualiferType }
         *     
         */
        public void setSocialMediaQualifiers(SocialQualiferType value) {
            this.socialMediaQualifiers = value;
        }

        /**
         * Contains elements identifying eligibility for special pricing, which may be private or negotiated.
         * 
         * Notes:
         *  1. Supports pricing discounts and/ or other benefits for Travelers affiliated with companies with private fare and/ or negotiated rate contracts.
         * 
         *  2. Private Fares apply to ALL Travelers.
         * 
         * @return
         *     possible object is
         *     {@link FareQualifierType }
         *     
         */
        public FareQualifierType getSpecialFareQualifiers() {
            return specialFareQualifiers;
        }

        /**
         * 设置specialFareQualifiers属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FareQualifierType }
         *     
         */
        public void setSpecialFareQualifiers(FareQualifierType value) {
            this.specialFareQualifiers = value;
        }

        /**
         * 获取specialNeedQualifiers属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SpecialQualiferType }
         *     
         */
        public SpecialQualiferType getSpecialNeedQualifiers() {
            return specialNeedQualifiers;
        }

        /**
         * 设置specialNeedQualifiers属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SpecialQualiferType }
         *     
         */
        public void setSpecialNeedQualifiers(SpecialQualiferType value) {
            this.specialNeedQualifiers = value;
        }

        /**
         * 获取tripPurposeQualifier属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TripPurposeListType }
         *     
         */
        public TripPurposeListType getTripPurposeQualifier() {
            return tripPurposeQualifier;
        }

        /**
         * 设置tripPurposeQualifier属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TripPurposeListType }
         *     
         */
        public void setTripPurposeQualifier(TripPurposeListType value) {
            this.tripPurposeQualifier = value;
        }

    }

}
