//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type definition for Augmentation Point Definition and List.
 * 
 * <p>AugPointInfoType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AugPointInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AugPoint" type="{http://www.iata.org/IATA/EDIST}AugPointType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Lists" type="{http://www.iata.org/IATA/EDIST}AugPointListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AugPointInfoType", propOrder = {
    "augPoint",
    "lists"
})
public class AugPointInfoType {

    @XmlElement(name = "AugPoint", required = true)
    protected List<AugPointType> augPoint;
    @XmlElement(name = "Lists")
    protected AugPointListType lists;

    /**
     * Gets the value of the augPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the augPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAugPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AugPointType }
     * 
     * 
     */
    public List<AugPointType> getAugPoint() {
        if (augPoint == null) {
            augPoint = new ArrayList<AugPointType>();
        }
        return this.augPoint;
    }

    /**
     * 获取lists属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AugPointListType }
     *     
     */
    public AugPointListType getLists() {
        return lists;
    }

    /**
     * 设置lists属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AugPointListType }
     *     
     */
    public void setLists(AugPointListType value) {
        this.lists = value;
    }

}
