//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice maxOccurs="unbounded"&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CheckedBagMetadatas"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CarryOnBagMetadatas"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageDisclosureMetadatas"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageDetailMetadata"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageQueryMetadata"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "checkedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas"
})
@XmlRootElement(name = "Baggage")
public class Baggage {

    @XmlElements({
        @XmlElement(name = "CheckedBagMetadatas", type = CheckedBagMetadatas.class),
        @XmlElement(name = "CarryOnBagMetadatas", type = CarryOnBagMetadatas.class),
        @XmlElement(name = "BaggageDisclosureMetadatas", type = BaggageDisclosureMetadatas.class),
        @XmlElement(name = "BaggageDetailMetadata", type = BaggageDetailMetadataType.class),
        @XmlElement(name = "BaggageQueryMetadata", type = BaggageQueryMetadataType.class)
    })
    protected List<Object> checkedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas;

    /**
     * Gets the value of the checkedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the checkedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCheckedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CheckedBagMetadatas }
     * {@link CarryOnBagMetadatas }
     * {@link BaggageDisclosureMetadatas }
     * {@link BaggageDetailMetadataType }
     * {@link BaggageQueryMetadataType }
     * 
     * 
     */
    public List<Object> getCheckedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas() {
        if (checkedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas == null) {
            checkedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas = new ArrayList<Object>();
        }
        return this.checkedBagMetadatasOrCarryOnBagMetadatasOrBaggageDisclosureMetadatas;
    }

}
