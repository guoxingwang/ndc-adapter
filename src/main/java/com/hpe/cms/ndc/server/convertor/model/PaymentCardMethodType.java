//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * PAYMENT CARD Payment Method definition.
 * 
 * <p>PaymentCardMethodType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PaymentCardMethodType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CardType" type="{http://www.iata.org/IATA/EDIST}PayCardTypeSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="CardCode" type="{http://www.iata.org/IATA/EDIST}PayCardCodeSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="MaskedCardNumber" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;MaskedCardNmbrSimpleType"&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TokenizedCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Contacts" minOccurs="0"/&gt;
 *         &lt;element name="CardHolderName" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CardIssuerName" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="BankID" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CardholderAddress" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleAddress"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}StructuredAddress"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentAddress"/&gt;
 *                 &lt;/choice&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="EffectiveExpireDate" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Effective" type="{http://www.iata.org/IATA/EDIST}PayCardDateSimpleType" minOccurs="0"/&gt;
 *                   &lt;element name="Expiration" type="{http://www.iata.org/IATA/EDIST}PayCardDateSimpleType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ApprovalType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentCardMethodType", propOrder = {
    "cardType",
    "cardCode",
    "maskedCardNumber",
    "tokenizedCardNumber",
    "contacts",
    "cardHolderName",
    "cardIssuerName",
    "cardholderAddress",
    "effectiveExpireDate",
    "approvalType"
})
public class PaymentCardMethodType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "CardType")
    protected String cardType;
    @XmlElement(name = "CardCode")
    protected String cardCode;
    @XmlElement(name = "MaskedCardNumber")
    protected PaymentCardMethodType.MaskedCardNumber maskedCardNumber;
    @XmlElement(name = "TokenizedCardNumber")
    protected String tokenizedCardNumber;
    @XmlElement(name = "Contacts")
    protected Contacts contacts;
    @XmlElement(name = "CardHolderName")
    protected PaymentCardMethodType.CardHolderName cardHolderName;
    @XmlElement(name = "CardIssuerName")
    protected PaymentCardMethodType.CardIssuerName cardIssuerName;
    @XmlElement(name = "CardholderAddress")
    protected PaymentCardMethodType.CardholderAddress cardholderAddress;
    @XmlElement(name = "EffectiveExpireDate")
    protected PaymentCardMethodType.EffectiveExpireDate effectiveExpireDate;
    @XmlElement(name = "ApprovalType")
    protected CodesetType approvalType;

    /**
     * 获取cardType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * 设置cardType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardType(String value) {
        this.cardType = value;
    }

    /**
     * 获取cardCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardCode() {
        return cardCode;
    }

    /**
     * 设置cardCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardCode(String value) {
        this.cardCode = value;
    }

    /**
     * 获取maskedCardNumber属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardMethodType.MaskedCardNumber }
     *     
     */
    public PaymentCardMethodType.MaskedCardNumber getMaskedCardNumber() {
        return maskedCardNumber;
    }

    /**
     * 设置maskedCardNumber属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardMethodType.MaskedCardNumber }
     *     
     */
    public void setMaskedCardNumber(PaymentCardMethodType.MaskedCardNumber value) {
        this.maskedCardNumber = value;
    }

    /**
     * 获取tokenizedCardNumber属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTokenizedCardNumber() {
        return tokenizedCardNumber;
    }

    /**
     * 设置tokenizedCardNumber属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTokenizedCardNumber(String value) {
        this.tokenizedCardNumber = value;
    }

    /**
     * 获取contacts属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Contacts }
     *     
     */
    public Contacts getContacts() {
        return contacts;
    }

    /**
     * 设置contacts属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Contacts }
     *     
     */
    public void setContacts(Contacts value) {
        this.contacts = value;
    }

    /**
     * 获取cardHolderName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardMethodType.CardHolderName }
     *     
     */
    public PaymentCardMethodType.CardHolderName getCardHolderName() {
        return cardHolderName;
    }

    /**
     * 设置cardHolderName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardMethodType.CardHolderName }
     *     
     */
    public void setCardHolderName(PaymentCardMethodType.CardHolderName value) {
        this.cardHolderName = value;
    }

    /**
     * 获取cardIssuerName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardMethodType.CardIssuerName }
     *     
     */
    public PaymentCardMethodType.CardIssuerName getCardIssuerName() {
        return cardIssuerName;
    }

    /**
     * 设置cardIssuerName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardMethodType.CardIssuerName }
     *     
     */
    public void setCardIssuerName(PaymentCardMethodType.CardIssuerName value) {
        this.cardIssuerName = value;
    }

    /**
     * 获取cardholderAddress属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardMethodType.CardholderAddress }
     *     
     */
    public PaymentCardMethodType.CardholderAddress getCardholderAddress() {
        return cardholderAddress;
    }

    /**
     * 设置cardholderAddress属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardMethodType.CardholderAddress }
     *     
     */
    public void setCardholderAddress(PaymentCardMethodType.CardholderAddress value) {
        this.cardholderAddress = value;
    }

    /**
     * 获取effectiveExpireDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardMethodType.EffectiveExpireDate }
     *     
     */
    public PaymentCardMethodType.EffectiveExpireDate getEffectiveExpireDate() {
        return effectiveExpireDate;
    }

    /**
     * 设置effectiveExpireDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardMethodType.EffectiveExpireDate }
     *     
     */
    public void setEffectiveExpireDate(PaymentCardMethodType.EffectiveExpireDate value) {
        this.effectiveExpireDate = value;
    }

    /**
     * 获取approvalType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getApprovalType() {
        return approvalType;
    }

    /**
     * 设置approvalType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setApprovalType(CodesetType value) {
        this.approvalType = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class CardHolderName {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * A data type for Proper Name size constraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="BankID" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bankID"
    })
    public static class CardIssuerName {

        @XmlElement(name = "BankID", required = true)
        protected String bankID;

        /**
         * 获取bankID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBankID() {
            return bankID;
        }

        /**
         * 设置bankID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBankID(String value) {
            this.bankID = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SimpleAddress"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}StructuredAddress"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentAddress"/&gt;
     *       &lt;/choice&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "simpleAddress",
        "structuredAddress",
        "paymentAddress"
    })
    public static class CardholderAddress {

        @XmlElement(name = "SimpleAddress")
        protected SimpleAddrType simpleAddress;
        @XmlElement(name = "StructuredAddress")
        protected StructuredAddrType structuredAddress;
        @XmlElement(name = "PaymentAddress")
        protected PaymentAddrType paymentAddress;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * 获取simpleAddress属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SimpleAddrType }
         *     
         */
        public SimpleAddrType getSimpleAddress() {
            return simpleAddress;
        }

        /**
         * 设置simpleAddress属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SimpleAddrType }
         *     
         */
        public void setSimpleAddress(SimpleAddrType value) {
            this.simpleAddress = value;
        }

        /**
         * 获取structuredAddress属性的值。
         * 
         * @return
         *     possible object is
         *     {@link StructuredAddrType }
         *     
         */
        public StructuredAddrType getStructuredAddress() {
            return structuredAddress;
        }

        /**
         * 设置structuredAddress属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link StructuredAddrType }
         *     
         */
        public void setStructuredAddress(StructuredAddrType value) {
            this.structuredAddress = value;
        }

        /**
         * 获取paymentAddress属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PaymentAddrType }
         *     
         */
        public PaymentAddrType getPaymentAddress() {
            return paymentAddress;
        }

        /**
         * 设置paymentAddress属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentAddrType }
         *     
         */
        public void setPaymentAddress(PaymentAddrType value) {
            this.paymentAddress = value;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Effective" type="{http://www.iata.org/IATA/EDIST}PayCardDateSimpleType" minOccurs="0"/&gt;
     *         &lt;element name="Expiration" type="{http://www.iata.org/IATA/EDIST}PayCardDateSimpleType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "effective",
        "expiration"
    })
    public static class EffectiveExpireDate {

        @XmlElement(name = "Effective")
        protected String effective;
        @XmlElement(name = "Expiration")
        protected String expiration;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * 获取effective属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffective() {
            return effective;
        }

        /**
         * 设置effective属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffective(String value) {
            this.effective = value;
        }

        /**
         * 获取expiration属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpiration() {
            return expiration;
        }

        /**
         * 设置expiration属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpiration(String value) {
            this.expiration = value;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;MaskedCardNmbrSimpleType"&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class MaskedCardNumber {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * A data type for a Masked Payment Card Number constraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }

}
