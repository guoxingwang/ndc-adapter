//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * COMMON NOTICES definition.
 * 
 * <p>NoticeBaseType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="NoticeBaseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Localization" type="{http://www.iata.org/IATA/EDIST}LocalizationNoticeType" minOccurs="0"/&gt;
 *         &lt;element name="InfoPolicies" type="{http://www.iata.org/IATA/EDIST}InfoPolicyNoticeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NoticeBaseType", propOrder = {
    "localization",
    "infoPolicies"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.ItinReshopParamsType.Notices.class,
    ItinReshopNoticesType.class,
    OrdCancelNoticesType.class,
    OrdChangeNoticesType.class,
    OrdCreateNoticesType.class,
    OrderHistoryNoticesType.class,
    OrderHistNoticesType.class,
    OrderListNoticesType.class,
    OrdRetrieveNoticesType.class,
    OrdRulesNoticesType.class,
    OrdViewNoticesType.class
})
public class NoticeBaseType {

    @XmlElement(name = "Localization")
    protected LocalizationNoticeType localization;
    @XmlElement(name = "InfoPolicies")
    protected InfoPolicyNoticeType infoPolicies;

    /**
     * 获取localization属性的值。
     * 
     * @return
     *     possible object is
     *     {@link LocalizationNoticeType }
     *     
     */
    public LocalizationNoticeType getLocalization() {
        return localization;
    }

    /**
     * 设置localization属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link LocalizationNoticeType }
     *     
     */
    public void setLocalization(LocalizationNoticeType value) {
        this.localization = value;
    }

    /**
     * 获取infoPolicies属性的值。
     * 
     * @return
     *     possible object is
     *     {@link InfoPolicyNoticeType }
     *     
     */
    public InfoPolicyNoticeType getInfoPolicies() {
        return infoPolicies;
    }

    /**
     * 设置infoPolicies属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link InfoPolicyNoticeType }
     *     
     */
    public void setInfoPolicies(InfoPolicyNoticeType value) {
        this.infoPolicies = value;
    }

}
