//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>PartnerTypeListType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="PartnerTypeListType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AirPassProgramHolder"/&gt;
 *     &lt;enumeration value="Merchandise"/&gt;
 *     &lt;enumeration value="ServiceFulfillment"/&gt;
 *     &lt;enumeration value="ServiceProvider"/&gt;
 *     &lt;enumeration value="Other"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PartnerTypeListType")
@XmlEnum
public enum PartnerTypeListType {

    @XmlEnumValue("AirPassProgramHolder")
    AIR_PASS_PROGRAM_HOLDER("AirPassProgramHolder"),
    @XmlEnumValue("Merchandise")
    MERCHANDISE("Merchandise"),
    @XmlEnumValue("ServiceFulfillment")
    SERVICE_FULFILLMENT("ServiceFulfillment"),
    @XmlEnumValue("ServiceProvider")
    SERVICE_PROVIDER("ServiceProvider"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    PartnerTypeListType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PartnerTypeListType fromValue(String v) {
        for (PartnerTypeListType c: PartnerTypeListType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
