package com.hpe.cms.ndc.server.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:36 2019/7/5
 * @ Description：不规范的xsd文件需要手动生成wsdl文件， 通过wsdl文件注册wsdl bean
 * @ Modified By：
 */
@Configuration
@Slf4j
public class OrderRetrieveWsServerConfigurer extends WsConfigurerAdapter {

    @Bean("OrderRetrieve_v16_2")
    public SimpleWsdl11Definition OrderRetrieveSimpleWsdl11Definition_v16_2() {
        SimpleWsdl11Definition definition = new SimpleWsdl11Definition();
        definition.setWsdl(new ClassPathResource("static/ndc/v16_2_wsdl/OrderRetrieve.wsdl"));
        return definition;
    }

    @Bean("IATA_OrderRetrieve_v18_2")
    public SimpleWsdl11Definition OrderRetrieveSimpleWsdl11Definition_v18_2() {
        SimpleWsdl11Definition definition = new SimpleWsdl11Definition();
        definition.setWsdl(new ClassPathResource("static/ndc/v18_2_wsdl/IATA_OrderRetrieve.wsdl"));
        return definition;
    }
}
