//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Authentication and Notification of NDC Actors participating in the message/ transaction. Note that information about parties is intended for business use, it is not intended for transaction authentication and routing of the message.
 * 
 * <p>MsgPartiesType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="MsgPartiesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Sender"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentUserSender"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AggregatorSender"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}EnabledSystemSender"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierSender"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ORA_Sender"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OperatingCarrierSender"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}POA_Sender"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}RetailPartnerSender"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelAgencySender"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Participants" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Participant" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AggregatorParticipant" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}EnabledSystemParticipant" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierParticipant" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ORA_Participant" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OperatingCarrierParticipant" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}POA_Participant" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}RetailSupplierParticipant" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelAgencyParticipant" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Recipient" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentUserRecipient"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AggregatorRecipient"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}EnabledSystemRecipient"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierRecipient"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ORA_Recipient"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OperatingCarrierRecipient"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}POA_Recipient"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}RetailPartnerRecipient"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelAgencyRecipient"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MsgPartiesType", propOrder = {
    "sender",
    "participants",
    "recipient"
})
public class MsgPartiesType {

    @XmlElement(name = "Sender", required = true)
    protected MsgPartiesType.Sender sender;
    @XmlElementRef(name = "Participants", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false)
    protected JAXBElement<MsgPartiesType.Participants> participants;
    @XmlElementRef(name = "Recipient", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false)
    protected JAXBElement<MsgPartiesType.Recipient> recipient;

    /**
     * 获取sender属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgPartiesType.Sender }
     *     
     */
    public MsgPartiesType.Sender getSender() {
        return sender;
    }

    /**
     * 设置sender属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgPartiesType.Sender }
     *     
     */
    public void setSender(MsgPartiesType.Sender value) {
        this.sender = value;
    }

    /**
     * 获取participants属性的值。
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MsgPartiesType.Participants }{@code >}
     *     
     */
    public JAXBElement<MsgPartiesType.Participants> getParticipants() {
        return participants;
    }

    /**
     * 设置participants属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MsgPartiesType.Participants }{@code >}
     *     
     */
    public void setParticipants(JAXBElement<MsgPartiesType.Participants> value) {
        this.participants = value;
    }

    /**
     * 获取recipient属性的值。
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MsgPartiesType.Recipient }{@code >}
     *     
     */
    public JAXBElement<MsgPartiesType.Recipient> getRecipient() {
        return recipient;
    }

    /**
     * 设置recipient属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MsgPartiesType.Recipient }{@code >}
     *     
     */
    public void setRecipient(JAXBElement<MsgPartiesType.Recipient> value) {
        this.recipient = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Participant" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AggregatorParticipant" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}EnabledSystemParticipant" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierParticipant" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ORA_Participant" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OperatingCarrierParticipant" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}POA_Participant" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}RetailSupplierParticipant" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelAgencyParticipant" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "participant"
    })
    public static class Participants {

        @XmlElement(name = "Participant", required = true)
        protected List<MsgPartiesType.Participants.Participant> participant;

        /**
         * Gets the value of the participant property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the participant property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getParticipant().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MsgPartiesType.Participants.Participant }
         * 
         * 
         */
        public List<MsgPartiesType.Participants.Participant> getParticipant() {
            if (participant == null) {
                participant = new ArrayList<MsgPartiesType.Participants.Participant>();
            }
            return this.participant;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AggregatorParticipant" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}EnabledSystemParticipant" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierParticipant" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ORA_Participant" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OperatingCarrierParticipant" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}POA_Participant" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}RetailSupplierParticipant" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelAgencyParticipant" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "aggregatorParticipant",
            "enabledSystemParticipant",
            "marketingCarrierParticipant",
            "oraParticipant",
            "operatingCarrierParticipant",
            "poaParticipant",
            "retailSupplierParticipant",
            "travelAgencyParticipant"
        })
        public static class Participant {

            @XmlElement(name = "AggregatorParticipant")
            protected AggregatorParticipantType aggregatorParticipant;
            @XmlElement(name = "EnabledSystemParticipant")
            protected EnabledSysParticipantType enabledSystemParticipant;
            @XmlElement(name = "MarketingCarrierParticipant")
            protected MarketingCarrierParticipantType marketingCarrierParticipant;
            @XmlElement(name = "ORA_Participant")
            protected ORAAirlineParticipantType oraParticipant;
            @XmlElement(name = "OperatingCarrierParticipant")
            protected OperatingCarrierParticipantType operatingCarrierParticipant;
            @XmlElement(name = "POA_Participant")
            protected POAAirlineParticipantType poaParticipant;
            @XmlElement(name = "RetailSupplierParticipant")
            protected RetailSupplierParticipantType retailSupplierParticipant;
            @XmlElement(name = "TravelAgencyParticipant")
            protected TravelAgencyParticipantType travelAgencyParticipant;

            /**
             * 获取aggregatorParticipant属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AggregatorParticipantType }
             *     
             */
            public AggregatorParticipantType getAggregatorParticipant() {
                return aggregatorParticipant;
            }

            /**
             * 设置aggregatorParticipant属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AggregatorParticipantType }
             *     
             */
            public void setAggregatorParticipant(AggregatorParticipantType value) {
                this.aggregatorParticipant = value;
            }

            /**
             * 获取enabledSystemParticipant属性的值。
             * 
             * @return
             *     possible object is
             *     {@link EnabledSysParticipantType }
             *     
             */
            public EnabledSysParticipantType getEnabledSystemParticipant() {
                return enabledSystemParticipant;
            }

            /**
             * 设置enabledSystemParticipant属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link EnabledSysParticipantType }
             *     
             */
            public void setEnabledSystemParticipant(EnabledSysParticipantType value) {
                this.enabledSystemParticipant = value;
            }

            /**
             * 获取marketingCarrierParticipant属性的值。
             * 
             * @return
             *     possible object is
             *     {@link MarketingCarrierParticipantType }
             *     
             */
            public MarketingCarrierParticipantType getMarketingCarrierParticipant() {
                return marketingCarrierParticipant;
            }

            /**
             * 设置marketingCarrierParticipant属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link MarketingCarrierParticipantType }
             *     
             */
            public void setMarketingCarrierParticipant(MarketingCarrierParticipantType value) {
                this.marketingCarrierParticipant = value;
            }

            /**
             * 获取oraParticipant属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ORAAirlineParticipantType }
             *     
             */
            public ORAAirlineParticipantType getORAParticipant() {
                return oraParticipant;
            }

            /**
             * 设置oraParticipant属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ORAAirlineParticipantType }
             *     
             */
            public void setORAParticipant(ORAAirlineParticipantType value) {
                this.oraParticipant = value;
            }

            /**
             * 获取operatingCarrierParticipant属性的值。
             * 
             * @return
             *     possible object is
             *     {@link OperatingCarrierParticipantType }
             *     
             */
            public OperatingCarrierParticipantType getOperatingCarrierParticipant() {
                return operatingCarrierParticipant;
            }

            /**
             * 设置operatingCarrierParticipant属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link OperatingCarrierParticipantType }
             *     
             */
            public void setOperatingCarrierParticipant(OperatingCarrierParticipantType value) {
                this.operatingCarrierParticipant = value;
            }

            /**
             * 获取poaParticipant属性的值。
             * 
             * @return
             *     possible object is
             *     {@link POAAirlineParticipantType }
             *     
             */
            public POAAirlineParticipantType getPOAParticipant() {
                return poaParticipant;
            }

            /**
             * 设置poaParticipant属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link POAAirlineParticipantType }
             *     
             */
            public void setPOAParticipant(POAAirlineParticipantType value) {
                this.poaParticipant = value;
            }

            /**
             * 获取retailSupplierParticipant属性的值。
             * 
             * @return
             *     possible object is
             *     {@link RetailSupplierParticipantType }
             *     
             */
            public RetailSupplierParticipantType getRetailSupplierParticipant() {
                return retailSupplierParticipant;
            }

            /**
             * 设置retailSupplierParticipant属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link RetailSupplierParticipantType }
             *     
             */
            public void setRetailSupplierParticipant(RetailSupplierParticipantType value) {
                this.retailSupplierParticipant = value;
            }

            /**
             * 获取travelAgencyParticipant属性的值。
             * 
             * @return
             *     possible object is
             *     {@link TravelAgencyParticipantType }
             *     
             */
            public TravelAgencyParticipantType getTravelAgencyParticipant() {
                return travelAgencyParticipant;
            }

            /**
             * 设置travelAgencyParticipant属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link TravelAgencyParticipantType }
             *     
             */
            public void setTravelAgencyParticipant(TravelAgencyParticipantType value) {
                this.travelAgencyParticipant = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentUserRecipient"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AggregatorRecipient"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}EnabledSystemRecipient"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierRecipient"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ORA_Recipient"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OperatingCarrierRecipient"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}POA_Recipient"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}RetailPartnerRecipient"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelAgencyRecipient"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "agentUserRecipient",
        "aggregatorRecipient",
        "enabledSystemRecipient",
        "marketingCarrierRecipient",
        "oraRecipient",
        "operatingCarrierRecipient",
        "poaRecipient",
        "retailPartnerRecipient",
        "travelAgencyRecipient"
    })
    public static class Recipient {

        @XmlElement(name = "AgentUserRecipient")
        protected AgentUserRecipientType agentUserRecipient;
        @XmlElement(name = "AggregatorRecipient")
        protected AggregatorRecipientType aggregatorRecipient;
        @XmlElement(name = "EnabledSystemRecipient")
        protected EnabledSysRecipientType enabledSystemRecipient;
        @XmlElement(name = "MarketingCarrierRecipient")
        protected MarketingCarrierRecipientType marketingCarrierRecipient;
        @XmlElement(name = "ORA_Recipient")
        protected ORAAirlineRecipientType oraRecipient;
        @XmlElement(name = "OperatingCarrierRecipient")
        protected OperatingCarrierRecipientType operatingCarrierRecipient;
        @XmlElement(name = "POA_Recipient")
        protected POAAirlineRecipientType poaRecipient;
        @XmlElement(name = "RetailPartnerRecipient")
        protected RetailSupplierRecipientType retailPartnerRecipient;
        @XmlElement(name = "TravelAgencyRecipient")
        protected TravelAgencyRecipientType travelAgencyRecipient;

        /**
         * 获取agentUserRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AgentUserRecipientType }
         *     
         */
        public AgentUserRecipientType getAgentUserRecipient() {
            return agentUserRecipient;
        }

        /**
         * 设置agentUserRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AgentUserRecipientType }
         *     
         */
        public void setAgentUserRecipient(AgentUserRecipientType value) {
            this.agentUserRecipient = value;
        }

        /**
         * 获取aggregatorRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AggregatorRecipientType }
         *     
         */
        public AggregatorRecipientType getAggregatorRecipient() {
            return aggregatorRecipient;
        }

        /**
         * 设置aggregatorRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AggregatorRecipientType }
         *     
         */
        public void setAggregatorRecipient(AggregatorRecipientType value) {
            this.aggregatorRecipient = value;
        }

        /**
         * 获取enabledSystemRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link EnabledSysRecipientType }
         *     
         */
        public EnabledSysRecipientType getEnabledSystemRecipient() {
            return enabledSystemRecipient;
        }

        /**
         * 设置enabledSystemRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link EnabledSysRecipientType }
         *     
         */
        public void setEnabledSystemRecipient(EnabledSysRecipientType value) {
            this.enabledSystemRecipient = value;
        }

        /**
         * 获取marketingCarrierRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link MarketingCarrierRecipientType }
         *     
         */
        public MarketingCarrierRecipientType getMarketingCarrierRecipient() {
            return marketingCarrierRecipient;
        }

        /**
         * 设置marketingCarrierRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link MarketingCarrierRecipientType }
         *     
         */
        public void setMarketingCarrierRecipient(MarketingCarrierRecipientType value) {
            this.marketingCarrierRecipient = value;
        }

        /**
         * 获取oraRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ORAAirlineRecipientType }
         *     
         */
        public ORAAirlineRecipientType getORARecipient() {
            return oraRecipient;
        }

        /**
         * 设置oraRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ORAAirlineRecipientType }
         *     
         */
        public void setORARecipient(ORAAirlineRecipientType value) {
            this.oraRecipient = value;
        }

        /**
         * 获取operatingCarrierRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OperatingCarrierRecipientType }
         *     
         */
        public OperatingCarrierRecipientType getOperatingCarrierRecipient() {
            return operatingCarrierRecipient;
        }

        /**
         * 设置operatingCarrierRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OperatingCarrierRecipientType }
         *     
         */
        public void setOperatingCarrierRecipient(OperatingCarrierRecipientType value) {
            this.operatingCarrierRecipient = value;
        }

        /**
         * 获取poaRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link POAAirlineRecipientType }
         *     
         */
        public POAAirlineRecipientType getPOARecipient() {
            return poaRecipient;
        }

        /**
         * 设置poaRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link POAAirlineRecipientType }
         *     
         */
        public void setPOARecipient(POAAirlineRecipientType value) {
            this.poaRecipient = value;
        }

        /**
         * 获取retailPartnerRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link RetailSupplierRecipientType }
         *     
         */
        public RetailSupplierRecipientType getRetailPartnerRecipient() {
            return retailPartnerRecipient;
        }

        /**
         * 设置retailPartnerRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link RetailSupplierRecipientType }
         *     
         */
        public void setRetailPartnerRecipient(RetailSupplierRecipientType value) {
            this.retailPartnerRecipient = value;
        }

        /**
         * 获取travelAgencyRecipient属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TravelAgencyRecipientType }
         *     
         */
        public TravelAgencyRecipientType getTravelAgencyRecipient() {
            return travelAgencyRecipient;
        }

        /**
         * 设置travelAgencyRecipient属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TravelAgencyRecipientType }
         *     
         */
        public void setTravelAgencyRecipient(TravelAgencyRecipientType value) {
            this.travelAgencyRecipient = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentUserSender"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AggregatorSender"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}EnabledSystemSender"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierSender"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ORA_Sender"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OperatingCarrierSender"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}POA_Sender"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}RetailPartnerSender"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TravelAgencySender"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "agentUserSender",
        "aggregatorSender",
        "enabledSystemSender",
        "marketingCarrierSender",
        "oraSender",
        "operatingCarrierSender",
        "poaSender",
        "retailPartnerSender",
        "travelAgencySender"
    })
    public static class Sender {

        @XmlElement(name = "AgentUserSender")
        protected AgentUserSenderType agentUserSender;
        @XmlElement(name = "AggregatorSender")
        protected AggregatorSenderType aggregatorSender;
        @XmlElement(name = "EnabledSystemSender")
        protected EnabledSysSenderType enabledSystemSender;
        @XmlElement(name = "MarketingCarrierSender")
        protected MarketingCarrierSenderType marketingCarrierSender;
        @XmlElement(name = "ORA_Sender")
        protected ORAAirlineSenderType oraSender;
        @XmlElement(name = "OperatingCarrierSender")
        protected OperatingCarrierSenderType operatingCarrierSender;
        @XmlElement(name = "POA_Sender")
        protected POAAirlineSenderType poaSender;
        @XmlElement(name = "RetailPartnerSender")
        protected RetailSupplierSenderType retailPartnerSender;
        @XmlElement(name = "TravelAgencySender")
        protected TravelAgencySenderType travelAgencySender;

        /**
         * 获取agentUserSender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AgentUserSenderType }
         *     
         */
        public AgentUserSenderType getAgentUserSender() {
            return agentUserSender;
        }

        /**
         * 设置agentUserSender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AgentUserSenderType }
         *     
         */
        public void setAgentUserSender(AgentUserSenderType value) {
            this.agentUserSender = value;
        }

        /**
         * 获取aggregatorSender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AggregatorSenderType }
         *     
         */
        public AggregatorSenderType getAggregatorSender() {
            return aggregatorSender;
        }

        /**
         * 设置aggregatorSender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AggregatorSenderType }
         *     
         */
        public void setAggregatorSender(AggregatorSenderType value) {
            this.aggregatorSender = value;
        }

        /**
         * 获取enabledSystemSender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link EnabledSysSenderType }
         *     
         */
        public EnabledSysSenderType getEnabledSystemSender() {
            return enabledSystemSender;
        }

        /**
         * 设置enabledSystemSender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link EnabledSysSenderType }
         *     
         */
        public void setEnabledSystemSender(EnabledSysSenderType value) {
            this.enabledSystemSender = value;
        }

        /**
         * 获取marketingCarrierSender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link MarketingCarrierSenderType }
         *     
         */
        public MarketingCarrierSenderType getMarketingCarrierSender() {
            return marketingCarrierSender;
        }

        /**
         * 设置marketingCarrierSender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link MarketingCarrierSenderType }
         *     
         */
        public void setMarketingCarrierSender(MarketingCarrierSenderType value) {
            this.marketingCarrierSender = value;
        }

        /**
         * 获取oraSender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ORAAirlineSenderType }
         *     
         */
        public ORAAirlineSenderType getORASender() {
            return oraSender;
        }

        /**
         * 设置oraSender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ORAAirlineSenderType }
         *     
         */
        public void setORASender(ORAAirlineSenderType value) {
            this.oraSender = value;
        }

        /**
         * 获取operatingCarrierSender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OperatingCarrierSenderType }
         *     
         */
        public OperatingCarrierSenderType getOperatingCarrierSender() {
            return operatingCarrierSender;
        }

        /**
         * 设置operatingCarrierSender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OperatingCarrierSenderType }
         *     
         */
        public void setOperatingCarrierSender(OperatingCarrierSenderType value) {
            this.operatingCarrierSender = value;
        }

        /**
         * 获取poaSender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link POAAirlineSenderType }
         *     
         */
        public POAAirlineSenderType getPOASender() {
            return poaSender;
        }

        /**
         * 设置poaSender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link POAAirlineSenderType }
         *     
         */
        public void setPOASender(POAAirlineSenderType value) {
            this.poaSender = value;
        }

        /**
         * 获取retailPartnerSender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link RetailSupplierSenderType }
         *     
         */
        public RetailSupplierSenderType getRetailPartnerSender() {
            return retailPartnerSender;
        }

        /**
         * 设置retailPartnerSender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link RetailSupplierSenderType }
         *     
         */
        public void setRetailPartnerSender(RetailSupplierSenderType value) {
            this.retailPartnerSender = value;
        }

        /**
         * 获取travelAgencySender属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TravelAgencySenderType }
         *     
         */
        public TravelAgencySenderType getTravelAgencySender() {
            return travelAgencySender;
        }

        /**
         * 设置travelAgencySender属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TravelAgencySenderType }
         *     
         */
        public void setTravelAgencySender(TravelAgencySenderType value) {
            this.travelAgencySender = value;
        }

    }

}
