//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * AirShopReq Business Object: FLIGHT SPECIFIC definition.
 * 
 * <p>AirShopFlightSpecificType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AirShopFlightSpecificType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="FlightSegment" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Departure"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Arrival"/&gt;
 *                   &lt;element name="MarketingAirline" type="{http://www.iata.org/IATA/EDIST}MarketingCarrierFlightType"/&gt;
 *                   &lt;element name="OperatingAirline" type="{http://www.iata.org/IATA/EDIST}OperatingCarrierFlightType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="SegmentKey" use="required" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Flight" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Journey" type="{http://www.iata.org/IATA/EDIST}TotalJourneyType" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SegmentReferences"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                 &lt;attribute name="FlightKey" type="{http://www.iata.org/IATA/EDIST}KeyIdentifier" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginDestination" maxOccurs="unbounded"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ShoppingResponseIDs" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirShopFlightSpecificType", propOrder = {
    "flightSegment",
    "flight",
    "originDestination",
    "shoppingResponseIDs"
})
public class AirShopFlightSpecificType {

    @XmlElement(name = "FlightSegment")
    protected List<AirShopFlightSpecificType.FlightSegment> flightSegment;
    @XmlElement(name = "Flight")
    protected List<AirShopFlightSpecificType.Flight> flight;
    @XmlElement(name = "OriginDestination")
    protected List<OriginDestination> originDestination;
    @XmlElement(name = "ShoppingResponseIDs")
    protected ShoppingResponseIDType shoppingResponseIDs;

    /**
     * Gets the value of the flightSegment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flightSegment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlightSegment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirShopFlightSpecificType.FlightSegment }
     * 
     * 
     */
    public List<AirShopFlightSpecificType.FlightSegment> getFlightSegment() {
        if (flightSegment == null) {
            flightSegment = new ArrayList<AirShopFlightSpecificType.FlightSegment>();
        }
        return this.flightSegment;
    }

    /**
     * Gets the value of the flight property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flight property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlight().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirShopFlightSpecificType.Flight }
     * 
     * 
     */
    public List<AirShopFlightSpecificType.Flight> getFlight() {
        if (flight == null) {
            flight = new ArrayList<AirShopFlightSpecificType.Flight>();
        }
        return this.flight;
    }

    /**
     * Gets the value of the originDestination property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the originDestination property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginDestination().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginDestination }
     * 
     * 
     */
    public List<OriginDestination> getOriginDestination() {
        if (originDestination == null) {
            originDestination = new ArrayList<OriginDestination>();
        }
        return this.originDestination;
    }

    /**
     * 获取shoppingResponseIDs属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShoppingResponseIDType }
     *     
     */
    public ShoppingResponseIDType getShoppingResponseIDs() {
        return shoppingResponseIDs;
    }

    /**
     * 设置shoppingResponseIDs属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShoppingResponseIDType }
     *     
     */
    public void setShoppingResponseIDs(ShoppingResponseIDType value) {
        this.shoppingResponseIDs = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Journey" type="{http://www.iata.org/IATA/EDIST}TotalJourneyType" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SegmentReferences"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *       &lt;attribute name="FlightKey" type="{http://www.iata.org/IATA/EDIST}KeyIdentifier" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "journey",
        "segmentReferences"
    })
    public static class Flight {

        @XmlElement(name = "Journey")
        protected TotalJourneyType journey;
        @XmlElement(name = "SegmentReferences", required = true)
        protected SegmentReferences segmentReferences;
        @XmlAttribute(name = "FlightKey")
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        protected String flightKey;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * 获取journey属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TotalJourneyType }
         *     
         */
        public TotalJourneyType getJourney() {
            return journey;
        }

        /**
         * 设置journey属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TotalJourneyType }
         *     
         */
        public void setJourney(TotalJourneyType value) {
            this.journey = value;
        }

        /**
         * 获取segmentReferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SegmentReferences }
         *     
         */
        public SegmentReferences getSegmentReferences() {
            return segmentReferences;
        }

        /**
         * 设置segmentReferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SegmentReferences }
         *     
         */
        public void setSegmentReferences(SegmentReferences value) {
            this.segmentReferences = value;
        }

        /**
         * 获取flightKey属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightKey() {
            return flightKey;
        }

        /**
         * 设置flightKey属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightKey(String value) {
            this.flightKey = value;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Departure"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Arrival"/&gt;
     *         &lt;element name="MarketingAirline" type="{http://www.iata.org/IATA/EDIST}MarketingCarrierFlightType"/&gt;
     *         &lt;element name="OperatingAirline" type="{http://www.iata.org/IATA/EDIST}OperatingCarrierFlightType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="SegmentKey" use="required" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "departure",
        "arrival",
        "marketingAirline",
        "operatingAirline"
    })
    public static class FlightSegment {

        @XmlElement(name = "Departure", required = true)
        protected Departure departure;
        @XmlElement(name = "Arrival", required = true)
        protected FlightArrivalType arrival;
        @XmlElement(name = "MarketingAirline", required = true)
        protected MarketingCarrierFlightType marketingAirline;
        @XmlElement(name = "OperatingAirline")
        protected OperatingCarrierFlightType operatingAirline;
        @XmlAttribute(name = "SegmentKey", required = true)
        protected int segmentKey;

        /**
         * 获取departure属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Departure }
         *     
         */
        public Departure getDeparture() {
            return departure;
        }

        /**
         * 设置departure属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Departure }
         *     
         */
        public void setDeparture(Departure value) {
            this.departure = value;
        }

        /**
         * 获取arrival属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FlightArrivalType }
         *     
         */
        public FlightArrivalType getArrival() {
            return arrival;
        }

        /**
         * 设置arrival属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FlightArrivalType }
         *     
         */
        public void setArrival(FlightArrivalType value) {
            this.arrival = value;
        }

        /**
         * 获取marketingAirline属性的值。
         * 
         * @return
         *     possible object is
         *     {@link MarketingCarrierFlightType }
         *     
         */
        public MarketingCarrierFlightType getMarketingAirline() {
            return marketingAirline;
        }

        /**
         * 设置marketingAirline属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link MarketingCarrierFlightType }
         *     
         */
        public void setMarketingAirline(MarketingCarrierFlightType value) {
            this.marketingAirline = value;
        }

        /**
         * 获取operatingAirline属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OperatingCarrierFlightType }
         *     
         */
        public OperatingCarrierFlightType getOperatingAirline() {
            return operatingAirline;
        }

        /**
         * 设置operatingAirline属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OperatingCarrierFlightType }
         *     
         */
        public void setOperatingAirline(OperatingCarrierFlightType value) {
            this.operatingAirline = value;
        }

        /**
         * 获取segmentKey属性的值。
         * 
         */
        public int getSegmentKey() {
            return segmentKey;
        }

        /**
         * 设置segmentKey属性的值。
         * 
         */
        public void setSegmentKey(int value) {
            this.segmentKey = value;
        }

    }

}
