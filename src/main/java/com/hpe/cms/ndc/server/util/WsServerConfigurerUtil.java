package com.hpe.cms.ndc.server.util;

import com.hpe.cms.config.WebServiceList;
import com.hpe.cms.ndc.server.config.NDCDefaultWsdl11Definition;
import com.hpe.cms.ndc.server.config.WsServerConfigurer;
import com.hpe.cms.ndc.server.constance.WsServerConstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import java.util.List;

@Slf4j
public class WsServerConfigurerUtil {

    /**
     * 注册XsdSchema bean
     *
     * @param version
     */
    public static void registerXsdSchemasByVersion(String version) {
        List xsdList = WebServiceList.getWebServiceXsdListByVersion(version);
        XsdSchema[] xsdSchemas = new XsdSchema[xsdList.size() * 2];
        int j = 0;
        for (int i = 0; i < xsdList.size(); i++) {
            String name = (String) xsdList.get(i);
            String beanName = name + "_" + version;
            log.debug("Register XsdSchemas Bean:{}", beanName);
            String schemaPath = WsServerConstance.BaseXsd.getResourcePath(version) + "/" + name + ".xsd";
            xsdSchemas[j++] = WsServerConfigurerUtil.registerBean(beanName, SimpleXsdSchema.class, new ClassPathResource(schemaPath));
        }
    }

    /**
     * 注册规范的wsdl bean
     *
     * @param version
     */
    public static void registerNDCDefaultWsdl11DefinitionBeanByVersion(String version) {
        List<String> wsList = WebServiceList.getWebServiceListByVersion(version);
        if (wsList == null) {
            log.warn("web servic config beanlist is null");
            return;
        }
        wsList.parallelStream().forEach(wsName -> {
            String beanName = wsName + "_" + version;
            String portType = wsName;
            String schemaRqName = wsName + "RQ" + "_" + version;
            String schemaRsName = wsName + "RS" + "_" + version;

            log.debug("Register Wsdl Bean:{}", beanName);

            registerBean(beanName,
                    NDCDefaultWsdl11Definition.class,
                    portType,
                    version,
                    ApplicationContextUtil.getApplicationContext().getBean(schemaRqName),
                    ApplicationContextUtil.getApplicationContext().getBean(schemaRsName)
            );
        });
    }

    /**
     * @param name  bean name
     * @param clazz Class
     * @param args  构造方法参数
     * @param <T>
     * @return
     */
    public static <T> T registerBean(String name, Class<T> clazz, Object... args) {
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(clazz);
        if (args.length > 0) {
            for (Object arg : args) {
                beanDefinitionBuilder.addConstructorArgValue(arg);
            }
        }
        BeanDefinition beanDefinition = beanDefinitionBuilder.getRawBeanDefinition();

        ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext) ApplicationContextUtil.getApplicationContext();
        BeanDefinitionRegistry beanFactory = (BeanDefinitionRegistry) configurableApplicationContext.getBeanFactory();
        beanFactory.registerBeanDefinition(name, beanDefinition);
        return configurableApplicationContext.getBean(name, clazz);
    }

    /**
     * 注册所有web service 为同一个 wsdl bean，同步注册xsd bean
     *
     * @param wsList 合法xsd文件列表（既有RS、又有RQ）
     */
    @Deprecated
    public static void registerWebServiceToOneWsdl(List<String> wsList) {
        if (wsList == null) {
            log.warn("web servic config beanlist is null");
            return;
        }
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(NDCDefaultWsdl11Definition.class);
        if (wsList.size() > 0) {
            wsList.stream().forEach(name -> {
                String schemaRqName = name + WsServerConfigurer.SCHEMA_RQ;
                String schemaRsName = name + WsServerConfigurer.SCHEMA_RS;
                log.info("registe RQ SimpleXsdSchema bean:{}", schemaRqName);
                log.info("registe RS SimpleXsdSchema bean:{}", schemaRsName);
                beanDefinitionBuilder.addConstructorArgValue(registerBean(schemaRqName, SimpleXsdSchema.class, new ClassPathResource(WsServerConstance.BaseXsd.getSchemaRqPath(name))));
            });
        }
        BeanDefinition beanDefinition = beanDefinitionBuilder.getRawBeanDefinition();

        ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext) ApplicationContextUtil.getApplicationContext();
        BeanDefinitionRegistry beanFactory = (BeanDefinitionRegistry) configurableApplicationContext.getBeanFactory();
        beanFactory.registerBeanDefinition("ndc", beanDefinition);
    }
}
