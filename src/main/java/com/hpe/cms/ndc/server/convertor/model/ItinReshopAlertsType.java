//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *  Alerts definition.
 * 
 * <p>ItinReshopAlertsType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ItinReshopAlertsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PIN_AuthRequest" type="{http://www.iata.org/IATA/EDIST}PinPhraseQuestionType" minOccurs="0"/&gt;
 *         &lt;element name="OrderItemExceptions" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Exception" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemID"/&gt;
 *                             &lt;element name="Code" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="ExpiredTimeLimits" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ExpiredTimeLimit" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}TimeLimitTypeSimpleType"/&gt;
 *                                                 &lt;element name="ExpiredDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                                                 &lt;element name="Associations" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="Passengers" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;choice&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
 *                                                                   &lt;/choice&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                           &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
 *                                                                   &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="AlternateOffers" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
 *                                                           &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
 *                                                           &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/extension&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                               &lt;attribute name="references" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
 *                                               &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="InvDiscrepency" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Associations" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Passengers" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;choice&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
 *                                                         &lt;/choice&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
 *                                                         &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="AlternateOffers" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
 *                                                 &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
 *                                                 &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/extension&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="policy" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
 *                                     &lt;attribute name="References" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
 *                                     &lt;attribute name="objectMetaReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
 *                                     &lt;attribute name="objectKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
 *                                     &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                     &lt;attribute name="NoInventoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="PriceVariances" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="PriceVariance" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="RuleID" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;simpleContent&gt;
 *                                                       &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
 *                                                         &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                                                         &lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *                                                       &lt;/extension&gt;
 *                                                     &lt;/simpleContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="Amount" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="Associations" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="Passengers" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                           &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
 *                                                                   &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                               &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="RuleExceptionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="Eligibility" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Promotions" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Promotion" maxOccurs="unbounded"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;extension base="{http://www.iata.org/IATA/EDIST}PromotionType"&gt;
 *                                                       &lt;/extension&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="Associations" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Passengers" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;choice&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
 *                                                         &lt;/choice&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
 *                                                         &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="AlternateOffers" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
 *                                                 &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
 *                                                 &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/extension&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="policy" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
 *                                     &lt;attribute name="References" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
 *                                     &lt;attribute name="objectMetaReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
 *                                     &lt;attribute name="objectKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
 *                                     &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                     &lt;attribute name="PaxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                     &lt;attribute name="PromotionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItinReshopAlertsType", propOrder = {
    "pinAuthRequest",
    "orderItemExceptions"
})
public class ItinReshopAlertsType
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "PIN_AuthRequest")
    protected PinPhraseQuestionType pinAuthRequest;
    @XmlElement(name = "OrderItemExceptions")
    protected ItinReshopAlertsType.OrderItemExceptions orderItemExceptions;

    /**
     * 获取pinAuthRequest属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PinPhraseQuestionType }
     *     
     */
    public PinPhraseQuestionType getPINAuthRequest() {
        return pinAuthRequest;
    }

    /**
     * 设置pinAuthRequest属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PinPhraseQuestionType }
     *     
     */
    public void setPINAuthRequest(PinPhraseQuestionType value) {
        this.pinAuthRequest = value;
    }

    /**
     * 获取orderItemExceptions属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ItinReshopAlertsType.OrderItemExceptions }
     *     
     */
    public ItinReshopAlertsType.OrderItemExceptions getOrderItemExceptions() {
        return orderItemExceptions;
    }

    /**
     * 设置orderItemExceptions属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ItinReshopAlertsType.OrderItemExceptions }
     *     
     */
    public void setOrderItemExceptions(ItinReshopAlertsType.OrderItemExceptions value) {
        this.orderItemExceptions = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Exception" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemID"/&gt;
     *                   &lt;element name="Code" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="ExpiredTimeLimits" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ExpiredTimeLimit" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}TimeLimitTypeSimpleType"/&gt;
     *                                       &lt;element name="ExpiredDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                                       &lt;element name="Associations" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="Passengers" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;choice&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
     *                                                         &lt;/choice&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                                 &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
     *                                                         &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="AlternateOffers" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
     *                                                 &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
     *                                                 &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/extension&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                     &lt;attribute name="references" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
     *                                     &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="InvDiscrepency" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Associations" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Passengers" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;choice&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
     *                                               &lt;/choice&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
     *                                               &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="AlternateOffers" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
     *                                       &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
     *                                       &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="policy" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
     *                           &lt;attribute name="References" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
     *                           &lt;attribute name="objectMetaReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
     *                           &lt;attribute name="objectKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
     *                           &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                           &lt;attribute name="NoInventoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="PriceVariances" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="PriceVariance" maxOccurs="unbounded" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="RuleID" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;simpleContent&gt;
     *                                             &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
     *                                               &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *                                               &lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/simpleContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="Amount" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="Associations" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="Passengers" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                                 &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
     *                                                         &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                     &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="RuleExceptionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="Eligibility" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Promotions" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Promotion" maxOccurs="unbounded"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}PromotionType"&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="Associations" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Passengers" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;choice&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
     *                                               &lt;/choice&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
     *                                               &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="AlternateOffers" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
     *                                       &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
     *                                       &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="policy" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
     *                           &lt;attribute name="References" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
     *                           &lt;attribute name="objectMetaReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
     *                           &lt;attribute name="objectKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
     *                           &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                           &lt;attribute name="PaxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                           &lt;attribute name="PromotionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "exception"
    })
    public static class OrderItemExceptions {

        @XmlElement(name = "Exception", required = true)
        protected List<ItinReshopAlertsType.OrderItemExceptions.Exception> exception;

        /**
         * Gets the value of the exception property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the exception property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getException().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ItinReshopAlertsType.OrderItemExceptions.Exception }
         * 
         * 
         */
        public List<ItinReshopAlertsType.OrderItemExceptions.Exception> getException() {
            if (exception == null) {
                exception = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception>();
            }
            return this.exception;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemID"/&gt;
         *         &lt;element name="Code" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
         *               &lt;/extension&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="ExpiredTimeLimits" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ExpiredTimeLimit" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}TimeLimitTypeSimpleType"/&gt;
         *                             &lt;element name="ExpiredDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *                             &lt;element name="Associations" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="Passengers" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;choice&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
         *                                               &lt;/choice&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                       &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
         *                                               &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="AlternateOffers" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
         *                                       &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
         *                                       &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/extension&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                           &lt;attribute name="references" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
         *                           &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="InvDiscrepency" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Associations" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Passengers" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;choice&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
         *                                     &lt;/choice&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
         *                                     &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="AlternateOffers" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
         *                             &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
         *                             &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="policy" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
         *                 &lt;attribute name="References" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
         *                 &lt;attribute name="objectMetaReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
         *                 &lt;attribute name="objectKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
         *                 &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *                 &lt;attribute name="NoInventoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="PriceVariances" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="PriceVariance" maxOccurs="unbounded" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="RuleID" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;simpleContent&gt;
         *                                   &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
         *                                     &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *                                     &lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/simpleContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="Amount" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="Associations" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="Passengers" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                       &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
         *                                               &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                           &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="RuleExceptionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="Eligibility" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Promotions" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Promotion" maxOccurs="unbounded"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}PromotionType"&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="Associations" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Passengers" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;choice&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
         *                                     &lt;/choice&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
         *                                     &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="AlternateOffers" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
         *                             &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
         *                             &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="policy" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
         *                 &lt;attribute name="References" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
         *                 &lt;attribute name="objectMetaReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
         *                 &lt;attribute name="objectKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
         *                 &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *                 &lt;attribute name="PaxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *                 &lt;attribute name="PromotionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "orderItemID",
            "code",
            "expiredTimeLimits",
            "invDiscrepency",
            "priceVariances",
            "eligibility"
        })
        public static class Exception {

            @XmlElement(name = "OrderItemID", required = true)
            protected ItemIDType orderItemID;
            @XmlElement(name = "Code")
            protected ItinReshopAlertsType.OrderItemExceptions.Exception.Code code;
            @XmlElement(name = "ExpiredTimeLimits")
            protected ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits expiredTimeLimits;
            @XmlElement(name = "InvDiscrepency")
            protected ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency invDiscrepency;
            @XmlElement(name = "PriceVariances")
            protected ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances priceVariances;
            @XmlElement(name = "Eligibility")
            protected ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility eligibility;

            /**
             * Associated Order Item ID(s).
             * Example: ORD7333-001
             * 
             * @return
             *     possible object is
             *     {@link ItemIDType }
             *     
             */
            public ItemIDType getOrderItemID() {
                return orderItemID;
            }

            /**
             * 设置orderItemID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ItemIDType }
             *     
             */
            public void setOrderItemID(ItemIDType value) {
                this.orderItemID = value;
            }

            /**
             * 获取code属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Code }
             *     
             */
            public ItinReshopAlertsType.OrderItemExceptions.Exception.Code getCode() {
                return code;
            }

            /**
             * 设置code属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Code }
             *     
             */
            public void setCode(ItinReshopAlertsType.OrderItemExceptions.Exception.Code value) {
                this.code = value;
            }

            /**
             * 获取expiredTimeLimits属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits }
             *     
             */
            public ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits getExpiredTimeLimits() {
                return expiredTimeLimits;
            }

            /**
             * 设置expiredTimeLimits属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits }
             *     
             */
            public void setExpiredTimeLimits(ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits value) {
                this.expiredTimeLimits = value;
            }

            /**
             * 获取invDiscrepency属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency }
             *     
             */
            public ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency getInvDiscrepency() {
                return invDiscrepency;
            }

            /**
             * 设置invDiscrepency属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency }
             *     
             */
            public void setInvDiscrepency(ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency value) {
                this.invDiscrepency = value;
            }

            /**
             * 获取priceVariances属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances }
             *     
             */
            public ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances getPriceVariances() {
                return priceVariances;
            }

            /**
             * 设置priceVariances属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances }
             *     
             */
            public void setPriceVariances(ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances value) {
                this.priceVariances = value;
            }

            /**
             * 获取eligibility属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility }
             *     
             */
            public ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility getEligibility() {
                return eligibility;
            }

            /**
             * 设置eligibility属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility }
             *     
             */
            public void setEligibility(ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility value) {
                this.eligibility = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
             *     &lt;/extension&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Code
                extends CodesetType
            {


            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Promotions" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Promotion" maxOccurs="unbounded"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}PromotionType"&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="Associations" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Passengers" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;choice&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
             *                           &lt;/choice&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
             *                           &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="AlternateOffers" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
             *                   &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
             *                   &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="policy" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
             *       &lt;attribute name="References" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
             *       &lt;attribute name="objectMetaReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
             *       &lt;attribute name="objectKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
             *       &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *       &lt;attribute name="PaxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *       &lt;attribute name="PromotionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "promotions",
                "associations",
                "alternateOffers",
                "remarks"
            })
            public static class Eligibility {

                @XmlElement(name = "Promotions")
                protected ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions promotions;
                @XmlElement(name = "Associations")
                protected ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations associations;
                @XmlElement(name = "AlternateOffers")
                protected ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers alternateOffers;
                @XmlElement(name = "Remarks")
                protected RemarkType remarks;
                @XmlAttribute(name = "policy")
                @XmlIDREF
                protected List<Object> policy;
                @XmlAttribute(name = "References")
                @XmlIDREF
                protected List<Object> references;
                @XmlAttribute(name = "objectMetaReferences")
                @XmlIDREF
                @XmlSchemaType(name = "IDREFS")
                protected List<Object> objectMetaReferences;
                @XmlAttribute(name = "objectKey")
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlID
                @XmlSchemaType(name = "ID")
                protected String objectKey;
                @XmlAttribute(name = "AltOfferInd")
                protected Boolean altOfferInd;
                @XmlAttribute(name = "PaxInd")
                protected Boolean paxInd;
                @XmlAttribute(name = "PromotionInd")
                protected Boolean promotionInd;

                /**
                 * 获取promotions属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions }
                 *     
                 */
                public ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions getPromotions() {
                    return promotions;
                }

                /**
                 * 设置promotions属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions }
                 *     
                 */
                public void setPromotions(ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions value) {
                    this.promotions = value;
                }

                /**
                 * 获取associations属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations }
                 *     
                 */
                public ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations getAssociations() {
                    return associations;
                }

                /**
                 * 设置associations属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations }
                 *     
                 */
                public void setAssociations(ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations value) {
                    this.associations = value;
                }

                /**
                 * 获取alternateOffers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers }
                 *     
                 */
                public ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers getAlternateOffers() {
                    return alternateOffers;
                }

                /**
                 * 设置alternateOffers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers }
                 *     
                 */
                public void setAlternateOffers(ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers value) {
                    this.alternateOffers = value;
                }

                /**
                 * 获取remarks属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link RemarkType }
                 *     
                 */
                public RemarkType getRemarks() {
                    return remarks;
                }

                /**
                 * 设置remarks属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RemarkType }
                 *     
                 */
                public void setRemarks(RemarkType value) {
                    this.remarks = value;
                }

                /**
                 * Gets the value of the policy property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the policy property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPolicy().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Object }
                 * 
                 * 
                 */
                public List<Object> getPolicy() {
                    if (policy == null) {
                        policy = new ArrayList<Object>();
                    }
                    return this.policy;
                }

                /**
                 * Gets the value of the references property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the references property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getReferences().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Object }
                 * 
                 * 
                 */
                public List<Object> getReferences() {
                    if (references == null) {
                        references = new ArrayList<Object>();
                    }
                    return this.references;
                }

                /**
                 * Gets the value of the objectMetaReferences property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the objectMetaReferences property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getObjectMetaReferences().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Object }
                 * 
                 * 
                 */
                public List<Object> getObjectMetaReferences() {
                    if (objectMetaReferences == null) {
                        objectMetaReferences = new ArrayList<Object>();
                    }
                    return this.objectMetaReferences;
                }

                /**
                 * 获取objectKey属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getObjectKey() {
                    return objectKey;
                }

                /**
                 * 设置objectKey属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setObjectKey(String value) {
                    this.objectKey = value;
                }

                /**
                 * 获取altOfferInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAltOfferInd() {
                    return altOfferInd;
                }

                /**
                 * 设置altOfferInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAltOfferInd(Boolean value) {
                    this.altOfferInd = value;
                }

                /**
                 * 获取paxInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isPaxInd() {
                    return paxInd;
                }

                /**
                 * 设置paxInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setPaxInd(Boolean value) {
                    this.paxInd = value;
                }

                /**
                 * 获取promotionInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isPromotionInd() {
                    return promotionInd;
                }

                /**
                 * 设置promotionInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setPromotionInd(Boolean value) {
                    this.promotionInd = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
                 *         &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
                 *         &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "totalOfferQuantity",
                    "owner",
                    "alternateOffer"
                })
                public static class AlternateOffers {

                    @XmlElement(name = "TotalOfferQuantity")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger totalOfferQuantity;
                    @XmlElement(name = "Owner", required = true)
                    protected AirlineIDType owner;
                    @XmlElement(name = "AlternateOffer", required = true)
                    protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers.AlternateOffer> alternateOffer;

                    /**
                     * 获取totalOfferQuantity属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getTotalOfferQuantity() {
                        return totalOfferQuantity;
                    }

                    /**
                     * 设置totalOfferQuantity属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setTotalOfferQuantity(BigInteger value) {
                        this.totalOfferQuantity = value;
                    }

                    /**
                     * 获取owner属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link AirlineIDType }
                     *     
                     */
                    public AirlineIDType getOwner() {
                        return owner;
                    }

                    /**
                     * 设置owner属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link AirlineIDType }
                     *     
                     */
                    public void setOwner(AirlineIDType value) {
                        this.owner = value;
                    }

                    /**
                     * Gets the value of the alternateOffer property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the alternateOffer property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAlternateOffer().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers.AlternateOffer }
                     * 
                     * 
                     */
                    public List<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers.AlternateOffer> getAlternateOffer() {
                        if (alternateOffer == null) {
                            alternateOffer = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers.AlternateOffer>();
                        }
                        return this.alternateOffer;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "altBaggageOffer",
                        "altPricedFlightOffer",
                        "altOtherOffer",
                        "altSeatOffer"
                    })
                    public static class AlternateOffer
                        extends OfferType
                    {

                        @XmlElement(name = "AltBaggageOffer")
                        protected AltBaggageOfferType altBaggageOffer;
                        @XmlElement(name = "AltPricedFlightOffer")
                        protected AltPricedFlightOfferType altPricedFlightOffer;
                        @XmlElement(name = "AltOtherOffer")
                        protected AltOtherOfferType altOtherOffer;
                        @XmlElement(name = "AltSeatOffer")
                        protected AltSeatOfferType altSeatOffer;

                        /**
                         * 获取altBaggageOffer属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AltBaggageOfferType }
                         *     
                         */
                        public AltBaggageOfferType getAltBaggageOffer() {
                            return altBaggageOffer;
                        }

                        /**
                         * 设置altBaggageOffer属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AltBaggageOfferType }
                         *     
                         */
                        public void setAltBaggageOffer(AltBaggageOfferType value) {
                            this.altBaggageOffer = value;
                        }

                        /**
                         * 获取altPricedFlightOffer属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AltPricedFlightOfferType }
                         *     
                         */
                        public AltPricedFlightOfferType getAltPricedFlightOffer() {
                            return altPricedFlightOffer;
                        }

                        /**
                         * 设置altPricedFlightOffer属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AltPricedFlightOfferType }
                         *     
                         */
                        public void setAltPricedFlightOffer(AltPricedFlightOfferType value) {
                            this.altPricedFlightOffer = value;
                        }

                        /**
                         * 获取altOtherOffer属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AltOtherOfferType }
                         *     
                         */
                        public AltOtherOfferType getAltOtherOffer() {
                            return altOtherOffer;
                        }

                        /**
                         * 设置altOtherOffer属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AltOtherOfferType }
                         *     
                         */
                        public void setAltOtherOffer(AltOtherOfferType value) {
                            this.altOtherOffer = value;
                        }

                        /**
                         * 获取altSeatOffer属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AltSeatOfferType }
                         *     
                         */
                        public AltSeatOfferType getAltSeatOffer() {
                            return altSeatOffer;
                        }

                        /**
                         * 设置altSeatOffer属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AltSeatOfferType }
                         *     
                         */
                        public void setAltSeatOffer(AltSeatOfferType value) {
                            this.altSeatOffer = value;
                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Passengers" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;choice&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                 *                 &lt;/choice&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                 *                 &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "passengers",
                    "otherAssociation"
                })
                public static class Associations {

                    @XmlElement(name = "Passengers")
                    protected ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.Passengers passengers;
                    @XmlElement(name = "OtherAssociation")
                    protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.OtherAssociation> otherAssociation;

                    /**
                     * 获取passengers属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.Passengers }
                     *     
                     */
                    public ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.Passengers getPassengers() {
                        return passengers;
                    }

                    /**
                     * 设置passengers属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.Passengers }
                     *     
                     */
                    public void setPassengers(ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.Passengers value) {
                        this.passengers = value;
                    }

                    /**
                     * Gets the value of the otherAssociation property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the otherAssociation property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getOtherAssociation().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.OtherAssociation }
                     * 
                     * 
                     */
                    public List<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.OtherAssociation> getOtherAssociation() {
                        if (otherAssociation == null) {
                            otherAssociation = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Associations.OtherAssociation>();
                        }
                        return this.otherAssociation;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                     *       &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class OtherAssociation {

                        @XmlAttribute(name = "Type", required = true)
                        protected String type;
                        @XmlAttribute(name = "ReferenceValue", required = true)
                        @XmlIDREF
                        @XmlSchemaType(name = "IDREF")
                        protected Object referenceValue;

                        /**
                         * 获取type属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * 设置type属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                        /**
                         * 获取referenceValue属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getReferenceValue() {
                            return referenceValue;
                        }

                        /**
                         * 设置referenceValue属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setReferenceValue(Object value) {
                            this.referenceValue = value;
                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;choice&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                     *       &lt;/choice&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "passengerReferences",
                        "groupReference"
                    })
                    public static class Passengers {

                        @XmlList
                        @XmlElement(name = "PassengerReferences")
                        @XmlIDREF
                        @XmlSchemaType(name = "IDREFS")
                        protected List<Object> passengerReferences;
                        @XmlElement(name = "GroupReference")
                        @XmlIDREF
                        @XmlSchemaType(name = "IDREF")
                        protected Object groupReference;

                        /**
                         * Association to multiple Passenger instance(s). Example: pax1 pax2 Gets the value of the passengerReferences property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the passengerReferences property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getPassengerReferences().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link Object }
                         * 
                         * 
                         */
                        public List<Object> getPassengerReferences() {
                            if (passengerReferences == null) {
                                passengerReferences = new ArrayList<Object>();
                            }
                            return this.passengerReferences;
                        }

                        /**
                         * 获取groupReference属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getGroupReference() {
                            return groupReference;
                        }

                        /**
                         * 设置groupReference属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setGroupReference(Object value) {
                            this.groupReference = value;
                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Promotion" maxOccurs="unbounded"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}PromotionType"&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "promotion"
                })
                public static class Promotions {

                    @XmlElement(name = "Promotion", required = true)
                    protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions.Promotion> promotion;

                    /**
                     * Gets the value of the promotion property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the promotion property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getPromotion().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions.Promotion }
                     * 
                     * 
                     */
                    public List<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions.Promotion> getPromotion() {
                        if (promotion == null) {
                            promotion = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.Promotions.Promotion>();
                        }
                        return this.promotion;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}PromotionType"&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Promotion
                        extends PromotionType
                    {


                    }

                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ExpiredTimeLimit" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}TimeLimitTypeSimpleType"/&gt;
             *                   &lt;element name="ExpiredDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
             *                   &lt;element name="Associations" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="Passengers" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;choice&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
             *                                     &lt;/choice&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                             &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
             *                                     &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="AlternateOffers" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
             *                             &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
             *                             &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/extension&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *                 &lt;attribute name="references" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
             *                 &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "expiredTimeLimit"
            })
            public static class ExpiredTimeLimits {

                @XmlElement(name = "ExpiredTimeLimit", required = true)
                protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit> expiredTimeLimit;
                @XmlAttribute(name = "AltOfferInd")
                protected Boolean altOfferInd;

                /**
                 * Gets the value of the expiredTimeLimit property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the expiredTimeLimit property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getExpiredTimeLimit().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit }
                 * 
                 * 
                 */
                public List<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit> getExpiredTimeLimit() {
                    if (expiredTimeLimit == null) {
                        expiredTimeLimit = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit>();
                    }
                    return this.expiredTimeLimit;
                }

                /**
                 * 获取altOfferInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAltOfferInd() {
                    return altOfferInd;
                }

                /**
                 * 设置altOfferInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAltOfferInd(Boolean value) {
                    this.altOfferInd = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}TimeLimitTypeSimpleType"/&gt;
                 *         &lt;element name="ExpiredDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
                 *         &lt;element name="Associations" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="Passengers" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;choice&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                 *                           &lt;/choice&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                   &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                 *                           &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="AlternateOffers" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
                 *                   &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
                 *                   &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/extension&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *       &lt;attribute name="references" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
                 *       &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "type",
                    "expiredDate",
                    "associations",
                    "alternateOffers"
                })
                public static class ExpiredTimeLimit {

                    @XmlElement(name = "Type", required = true)
                    protected String type;
                    @XmlElement(name = "ExpiredDate")
                    @XmlSchemaType(name = "date")
                    protected XMLGregorianCalendar expiredDate;
                    @XmlElement(name = "Associations")
                    protected ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations associations;
                    @XmlElement(name = "AlternateOffers")
                    protected ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers alternateOffers;
                    @XmlAttribute(name = "references")
                    @XmlIDREF
                    @XmlSchemaType(name = "IDREFS")
                    protected List<Object> references;
                    @XmlAttribute(name = "trxItemKey")
                    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                    @XmlID
                    @XmlSchemaType(name = "ID")
                    protected String trxItemKey;

                    /**
                     * 获取type属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getType() {
                        return type;
                    }

                    /**
                     * 设置type属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setType(String value) {
                        this.type = value;
                    }

                    /**
                     * 获取expiredDate属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public XMLGregorianCalendar getExpiredDate() {
                        return expiredDate;
                    }

                    /**
                     * 设置expiredDate属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public void setExpiredDate(XMLGregorianCalendar value) {
                        this.expiredDate = value;
                    }

                    /**
                     * 获取associations属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations }
                     *     
                     */
                    public ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations getAssociations() {
                        return associations;
                    }

                    /**
                     * 设置associations属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations }
                     *     
                     */
                    public void setAssociations(ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations value) {
                        this.associations = value;
                    }

                    /**
                     * 获取alternateOffers属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers }
                     *     
                     */
                    public ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers getAlternateOffers() {
                        return alternateOffers;
                    }

                    /**
                     * 设置alternateOffers属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers }
                     *     
                     */
                    public void setAlternateOffers(ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers value) {
                        this.alternateOffers = value;
                    }

                    /**
                     * Gets the value of the references property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the references property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getReferences().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link Object }
                     * 
                     * 
                     */
                    public List<Object> getReferences() {
                        if (references == null) {
                            references = new ArrayList<Object>();
                        }
                        return this.references;
                    }

                    /**
                     * 获取trxItemKey属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTrxItemKey() {
                        return trxItemKey;
                    }

                    /**
                     * 设置trxItemKey属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTrxItemKey(String value) {
                        this.trxItemKey = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
                     *         &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
                     *         &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/extension&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "totalOfferQuantity",
                        "owner",
                        "alternateOffer"
                    })
                    public static class AlternateOffers {

                        @XmlElement(name = "TotalOfferQuantity")
                        @XmlSchemaType(name = "positiveInteger")
                        protected BigInteger totalOfferQuantity;
                        @XmlElement(name = "Owner", required = true)
                        protected AirlineIDType owner;
                        @XmlElement(name = "AlternateOffer", required = true)
                        protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers.AlternateOffer> alternateOffer;

                        /**
                         * 获取totalOfferQuantity属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getTotalOfferQuantity() {
                            return totalOfferQuantity;
                        }

                        /**
                         * 设置totalOfferQuantity属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setTotalOfferQuantity(BigInteger value) {
                            this.totalOfferQuantity = value;
                        }

                        /**
                         * 获取owner属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AirlineIDType }
                         *     
                         */
                        public AirlineIDType getOwner() {
                            return owner;
                        }

                        /**
                         * 设置owner属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AirlineIDType }
                         *     
                         */
                        public void setOwner(AirlineIDType value) {
                            this.owner = value;
                        }

                        /**
                         * Gets the value of the alternateOffer property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the alternateOffer property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getAlternateOffer().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers.AlternateOffer }
                         * 
                         * 
                         */
                        public List<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers.AlternateOffer> getAlternateOffer() {
                            if (alternateOffer == null) {
                                alternateOffer = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers.AlternateOffer>();
                            }
                            return this.alternateOffer;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/extension&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "altBaggageOffer",
                            "altPricedFlightOffer",
                            "altOtherOffer",
                            "altSeatOffer"
                        })
                        public static class AlternateOffer
                            extends OfferType
                        {

                            @XmlElement(name = "AltBaggageOffer")
                            protected AltBaggageOfferType altBaggageOffer;
                            @XmlElement(name = "AltPricedFlightOffer")
                            protected AltPricedFlightOfferType altPricedFlightOffer;
                            @XmlElement(name = "AltOtherOffer")
                            protected AltOtherOfferType altOtherOffer;
                            @XmlElement(name = "AltSeatOffer")
                            protected AltSeatOfferType altSeatOffer;

                            /**
                             * 获取altBaggageOffer属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link AltBaggageOfferType }
                             *     
                             */
                            public AltBaggageOfferType getAltBaggageOffer() {
                                return altBaggageOffer;
                            }

                            /**
                             * 设置altBaggageOffer属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link AltBaggageOfferType }
                             *     
                             */
                            public void setAltBaggageOffer(AltBaggageOfferType value) {
                                this.altBaggageOffer = value;
                            }

                            /**
                             * 获取altPricedFlightOffer属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link AltPricedFlightOfferType }
                             *     
                             */
                            public AltPricedFlightOfferType getAltPricedFlightOffer() {
                                return altPricedFlightOffer;
                            }

                            /**
                             * 设置altPricedFlightOffer属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link AltPricedFlightOfferType }
                             *     
                             */
                            public void setAltPricedFlightOffer(AltPricedFlightOfferType value) {
                                this.altPricedFlightOffer = value;
                            }

                            /**
                             * 获取altOtherOffer属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link AltOtherOfferType }
                             *     
                             */
                            public AltOtherOfferType getAltOtherOffer() {
                                return altOtherOffer;
                            }

                            /**
                             * 设置altOtherOffer属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link AltOtherOfferType }
                             *     
                             */
                            public void setAltOtherOffer(AltOtherOfferType value) {
                                this.altOtherOffer = value;
                            }

                            /**
                             * 获取altSeatOffer属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link AltSeatOfferType }
                             *     
                             */
                            public AltSeatOfferType getAltSeatOffer() {
                                return altSeatOffer;
                            }

                            /**
                             * 设置altSeatOffer属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link AltSeatOfferType }
                             *     
                             */
                            public void setAltSeatOffer(AltSeatOfferType value) {
                                this.altSeatOffer = value;
                            }

                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="Passengers" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;choice&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                     *                 &lt;/choice&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *         &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                     *                 &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "passengers",
                        "otherAssociation"
                    })
                    public static class Associations {

                        @XmlElement(name = "Passengers")
                        protected ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.Passengers passengers;
                        @XmlElement(name = "OtherAssociation")
                        protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.OtherAssociation> otherAssociation;

                        /**
                         * 获取passengers属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.Passengers }
                         *     
                         */
                        public ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.Passengers getPassengers() {
                            return passengers;
                        }

                        /**
                         * 设置passengers属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.Passengers }
                         *     
                         */
                        public void setPassengers(ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.Passengers value) {
                            this.passengers = value;
                        }

                        /**
                         * Gets the value of the otherAssociation property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the otherAssociation property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getOtherAssociation().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.OtherAssociation }
                         * 
                         * 
                         */
                        public List<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.OtherAssociation> getOtherAssociation() {
                            if (otherAssociation == null) {
                                otherAssociation = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.Associations.OtherAssociation>();
                            }
                            return this.otherAssociation;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                         *       &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class OtherAssociation {

                            @XmlAttribute(name = "Type", required = true)
                            protected String type;
                            @XmlAttribute(name = "ReferenceValue", required = true)
                            @XmlIDREF
                            @XmlSchemaType(name = "IDREF")
                            protected Object referenceValue;

                            /**
                             * 获取type属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getType() {
                                return type;
                            }

                            /**
                             * 设置type属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setType(String value) {
                                this.type = value;
                            }

                            /**
                             * 获取referenceValue属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getReferenceValue() {
                                return referenceValue;
                            }

                            /**
                             * 设置referenceValue属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setReferenceValue(Object value) {
                                this.referenceValue = value;
                            }

                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;choice&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                         *       &lt;/choice&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "passengerReferences",
                            "groupReference"
                        })
                        public static class Passengers {

                            @XmlList
                            @XmlElement(name = "PassengerReferences")
                            @XmlIDREF
                            @XmlSchemaType(name = "IDREFS")
                            protected List<Object> passengerReferences;
                            @XmlElement(name = "GroupReference")
                            @XmlIDREF
                            @XmlSchemaType(name = "IDREF")
                            protected Object groupReference;

                            /**
                             * Association to multiple Passenger instance(s). Example: pax1 pax2 Gets the value of the passengerReferences property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the passengerReferences property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getPassengerReferences().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link Object }
                             * 
                             * 
                             */
                            public List<Object> getPassengerReferences() {
                                if (passengerReferences == null) {
                                    passengerReferences = new ArrayList<Object>();
                                }
                                return this.passengerReferences;
                            }

                            /**
                             * 获取groupReference属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getGroupReference() {
                                return groupReference;
                            }

                            /**
                             * 设置groupReference属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setGroupReference(Object value) {
                                this.groupReference = value;
                            }

                        }

                    }

                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Associations" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Passengers" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;choice&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
             *                           &lt;/choice&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
             *                           &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="AlternateOffers" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
             *                   &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
             *                   &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="policy" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
             *       &lt;attribute name="References" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
             *       &lt;attribute name="objectMetaReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" /&gt;
             *       &lt;attribute name="objectKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
             *       &lt;attribute name="AltOfferInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *       &lt;attribute name="NoInventoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "associations",
                "alternateOffers",
                "remarks"
            })
            public static class InvDiscrepency {

                @XmlElement(name = "Associations")
                protected ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations associations;
                @XmlElement(name = "AlternateOffers")
                protected ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers alternateOffers;
                @XmlElement(name = "Remarks")
                protected RemarkType remarks;
                @XmlAttribute(name = "policy")
                @XmlIDREF
                protected List<Object> policy;
                @XmlAttribute(name = "References")
                @XmlIDREF
                protected List<Object> references;
                @XmlAttribute(name = "objectMetaReferences")
                @XmlIDREF
                @XmlSchemaType(name = "IDREFS")
                protected List<Object> objectMetaReferences;
                @XmlAttribute(name = "objectKey")
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlID
                @XmlSchemaType(name = "ID")
                protected String objectKey;
                @XmlAttribute(name = "AltOfferInd")
                protected Boolean altOfferInd;
                @XmlAttribute(name = "NoInventoryInd")
                protected Boolean noInventoryInd;

                /**
                 * 获取associations属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations }
                 *     
                 */
                public ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations getAssociations() {
                    return associations;
                }

                /**
                 * 设置associations属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations }
                 *     
                 */
                public void setAssociations(ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations value) {
                    this.associations = value;
                }

                /**
                 * 获取alternateOffers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers }
                 *     
                 */
                public ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers getAlternateOffers() {
                    return alternateOffers;
                }

                /**
                 * 设置alternateOffers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers }
                 *     
                 */
                public void setAlternateOffers(ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers value) {
                    this.alternateOffers = value;
                }

                /**
                 * 获取remarks属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link RemarkType }
                 *     
                 */
                public RemarkType getRemarks() {
                    return remarks;
                }

                /**
                 * 设置remarks属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RemarkType }
                 *     
                 */
                public void setRemarks(RemarkType value) {
                    this.remarks = value;
                }

                /**
                 * Gets the value of the policy property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the policy property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPolicy().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Object }
                 * 
                 * 
                 */
                public List<Object> getPolicy() {
                    if (policy == null) {
                        policy = new ArrayList<Object>();
                    }
                    return this.policy;
                }

                /**
                 * Gets the value of the references property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the references property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getReferences().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Object }
                 * 
                 * 
                 */
                public List<Object> getReferences() {
                    if (references == null) {
                        references = new ArrayList<Object>();
                    }
                    return this.references;
                }

                /**
                 * Gets the value of the objectMetaReferences property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the objectMetaReferences property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getObjectMetaReferences().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Object }
                 * 
                 * 
                 */
                public List<Object> getObjectMetaReferences() {
                    if (objectMetaReferences == null) {
                        objectMetaReferences = new ArrayList<Object>();
                    }
                    return this.objectMetaReferences;
                }

                /**
                 * 获取objectKey属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getObjectKey() {
                    return objectKey;
                }

                /**
                 * 设置objectKey属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setObjectKey(String value) {
                    this.objectKey = value;
                }

                /**
                 * 获取altOfferInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isAltOfferInd() {
                    return altOfferInd;
                }

                /**
                 * 设置altOfferInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setAltOfferInd(Boolean value) {
                    this.altOfferInd = value;
                }

                /**
                 * 获取noInventoryInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isNoInventoryInd() {
                    return noInventoryInd;
                }

                /**
                 * 设置noInventoryInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setNoInventoryInd(Boolean value) {
                    this.noInventoryInd = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
                 *         &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
                 *         &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "totalOfferQuantity",
                    "owner",
                    "alternateOffer"
                })
                public static class AlternateOffers {

                    @XmlElement(name = "TotalOfferQuantity")
                    @XmlSchemaType(name = "positiveInteger")
                    protected BigInteger totalOfferQuantity;
                    @XmlElement(name = "Owner", required = true)
                    protected AirlineIDType owner;
                    @XmlElement(name = "AlternateOffer", required = true)
                    protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers.AlternateOffer> alternateOffer;

                    /**
                     * 获取totalOfferQuantity属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getTotalOfferQuantity() {
                        return totalOfferQuantity;
                    }

                    /**
                     * 设置totalOfferQuantity属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setTotalOfferQuantity(BigInteger value) {
                        this.totalOfferQuantity = value;
                    }

                    /**
                     * 获取owner属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link AirlineIDType }
                     *     
                     */
                    public AirlineIDType getOwner() {
                        return owner;
                    }

                    /**
                     * 设置owner属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link AirlineIDType }
                     *     
                     */
                    public void setOwner(AirlineIDType value) {
                        this.owner = value;
                    }

                    /**
                     * Gets the value of the alternateOffer property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the alternateOffer property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAlternateOffer().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers.AlternateOffer }
                     * 
                     * 
                     */
                    public List<ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers.AlternateOffer> getAlternateOffer() {
                        if (alternateOffer == null) {
                            alternateOffer = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers.AlternateOffer>();
                        }
                        return this.alternateOffer;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer" minOccurs="0"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer" minOccurs="0"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer" minOccurs="0"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer" minOccurs="0"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "altBaggageOffer",
                        "altPricedFlightOffer",
                        "altOtherOffer",
                        "altSeatOffer"
                    })
                    public static class AlternateOffer
                        extends OfferType
                    {

                        @XmlElement(name = "AltBaggageOffer")
                        protected AltBaggageOfferType altBaggageOffer;
                        @XmlElement(name = "AltPricedFlightOffer")
                        protected AltPricedFlightOfferType altPricedFlightOffer;
                        @XmlElement(name = "AltOtherOffer")
                        protected AltOtherOfferType altOtherOffer;
                        @XmlElement(name = "AltSeatOffer")
                        protected AltSeatOfferType altSeatOffer;

                        /**
                         * 获取altBaggageOffer属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AltBaggageOfferType }
                         *     
                         */
                        public AltBaggageOfferType getAltBaggageOffer() {
                            return altBaggageOffer;
                        }

                        /**
                         * 设置altBaggageOffer属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AltBaggageOfferType }
                         *     
                         */
                        public void setAltBaggageOffer(AltBaggageOfferType value) {
                            this.altBaggageOffer = value;
                        }

                        /**
                         * 获取altPricedFlightOffer属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AltPricedFlightOfferType }
                         *     
                         */
                        public AltPricedFlightOfferType getAltPricedFlightOffer() {
                            return altPricedFlightOffer;
                        }

                        /**
                         * 设置altPricedFlightOffer属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AltPricedFlightOfferType }
                         *     
                         */
                        public void setAltPricedFlightOffer(AltPricedFlightOfferType value) {
                            this.altPricedFlightOffer = value;
                        }

                        /**
                         * 获取altOtherOffer属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AltOtherOfferType }
                         *     
                         */
                        public AltOtherOfferType getAltOtherOffer() {
                            return altOtherOffer;
                        }

                        /**
                         * 设置altOtherOffer属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AltOtherOfferType }
                         *     
                         */
                        public void setAltOtherOffer(AltOtherOfferType value) {
                            this.altOtherOffer = value;
                        }

                        /**
                         * 获取altSeatOffer属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AltSeatOfferType }
                         *     
                         */
                        public AltSeatOfferType getAltSeatOffer() {
                            return altSeatOffer;
                        }

                        /**
                         * 设置altSeatOffer属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AltSeatOfferType }
                         *     
                         */
                        public void setAltSeatOffer(AltSeatOfferType value) {
                            this.altSeatOffer = value;
                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Passengers" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;choice&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                 *                 &lt;/choice&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                 *                 &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "passengers",
                    "otherAssociation"
                })
                public static class Associations {

                    @XmlElement(name = "Passengers")
                    protected ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.Passengers passengers;
                    @XmlElement(name = "OtherAssociation")
                    protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.OtherAssociation> otherAssociation;

                    /**
                     * 获取passengers属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.Passengers }
                     *     
                     */
                    public ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.Passengers getPassengers() {
                        return passengers;
                    }

                    /**
                     * 设置passengers属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.Passengers }
                     *     
                     */
                    public void setPassengers(ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.Passengers value) {
                        this.passengers = value;
                    }

                    /**
                     * Gets the value of the otherAssociation property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the otherAssociation property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getOtherAssociation().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.OtherAssociation }
                     * 
                     * 
                     */
                    public List<ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.OtherAssociation> getOtherAssociation() {
                        if (otherAssociation == null) {
                            otherAssociation = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.Associations.OtherAssociation>();
                        }
                        return this.otherAssociation;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                     *       &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class OtherAssociation {

                        @XmlAttribute(name = "Type", required = true)
                        protected String type;
                        @XmlAttribute(name = "ReferenceValue", required = true)
                        @XmlIDREF
                        @XmlSchemaType(name = "IDREF")
                        protected Object referenceValue;

                        /**
                         * 获取type属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getType() {
                            return type;
                        }

                        /**
                         * 设置type属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setType(String value) {
                            this.type = value;
                        }

                        /**
                         * 获取referenceValue属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getReferenceValue() {
                            return referenceValue;
                        }

                        /**
                         * 设置referenceValue属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setReferenceValue(Object value) {
                            this.referenceValue = value;
                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;choice&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                     *       &lt;/choice&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "passengerReferences",
                        "groupReference"
                    })
                    public static class Passengers {

                        @XmlList
                        @XmlElement(name = "PassengerReferences")
                        @XmlIDREF
                        @XmlSchemaType(name = "IDREFS")
                        protected List<Object> passengerReferences;
                        @XmlElement(name = "GroupReference")
                        @XmlIDREF
                        @XmlSchemaType(name = "IDREF")
                        protected Object groupReference;

                        /**
                         * Association to multiple Passenger instance(s). Example: pax1 pax2 Gets the value of the passengerReferences property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the passengerReferences property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getPassengerReferences().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link Object }
                         * 
                         * 
                         */
                        public List<Object> getPassengerReferences() {
                            if (passengerReferences == null) {
                                passengerReferences = new ArrayList<Object>();
                            }
                            return this.passengerReferences;
                        }

                        /**
                         * 获取groupReference属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link Object }
                         *     
                         */
                        public Object getGroupReference() {
                            return groupReference;
                        }

                        /**
                         * 设置groupReference属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Object }
                         *     
                         */
                        public void setGroupReference(Object value) {
                            this.groupReference = value;
                        }

                    }

                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="PriceVariance" maxOccurs="unbounded" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="RuleID" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;simpleContent&gt;
             *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
             *                           &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
             *                           &lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/simpleContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="Amount" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="Associations" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="Passengers" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                             &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
             *                                     &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *                 &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="RuleExceptionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "priceVariance"
            })
            public static class PriceVariances {

                @XmlElement(name = "PriceVariance")
                protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance> priceVariance;
                @XmlAttribute(name = "RuleExceptionInd")
                protected Boolean ruleExceptionInd;

                /**
                 * Gets the value of the priceVariance property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the priceVariance property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPriceVariance().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance }
                 * 
                 * 
                 */
                public List<ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance> getPriceVariance() {
                    if (priceVariance == null) {
                        priceVariance = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance>();
                    }
                    return this.priceVariance;
                }

                /**
                 * 获取ruleExceptionInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isRuleExceptionInd() {
                    return ruleExceptionInd;
                }

                /**
                 * 设置ruleExceptionInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setRuleExceptionInd(Boolean value) {
                    this.ruleExceptionInd = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="RuleID" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;simpleContent&gt;
                 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                 *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
                 *                 &lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/simpleContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="Amount" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="Associations" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="Passengers" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                   &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                 *                           &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *       &lt;attribute name="trxItemKey" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "ruleID",
                    "amount",
                    "associations"
                })
                public static class PriceVariance {

                    @XmlElement(name = "RuleID")
                    protected ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.RuleID ruleID;
                    @XmlElement(name = "Amount")
                    protected ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Amount amount;
                    @XmlElement(name = "Associations")
                    protected ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations associations;
                    @XmlAttribute(name = "trxItemKey")
                    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                    @XmlID
                    @XmlSchemaType(name = "ID")
                    protected String trxItemKey;

                    /**
                     * 获取ruleID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.RuleID }
                     *     
                     */
                    public ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.RuleID getRuleID() {
                        return ruleID;
                    }

                    /**
                     * 设置ruleID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.RuleID }
                     *     
                     */
                    public void setRuleID(ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.RuleID value) {
                        this.ruleID = value;
                    }

                    /**
                     * 获取amount属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Amount }
                     *     
                     */
                    public ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Amount getAmount() {
                        return amount;
                    }

                    /**
                     * 设置amount属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Amount }
                     *     
                     */
                    public void setAmount(ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Amount value) {
                        this.amount = value;
                    }

                    /**
                     * 获取associations属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations }
                     *     
                     */
                    public ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations getAssociations() {
                        return associations;
                    }

                    /**
                     * 设置associations属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations }
                     *     
                     */
                    public void setAssociations(ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations value) {
                        this.associations = value;
                    }

                    /**
                     * 获取trxItemKey属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTrxItemKey() {
                        return trxItemKey;
                    }

                    /**
                     * 设置trxItemKey属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTrxItemKey(String value) {
                        this.trxItemKey = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "currencyAmountValue",
                        "percentageValue"
                    })
                    public static class Amount {

                        @XmlElement(name = "CurrencyAmountValue", required = true)
                        protected CurrencyAmountOptType currencyAmountValue;
                        @XmlElement(name = "PercentageValue", required = true)
                        protected BigDecimal percentageValue;

                        /**
                         * 获取currencyAmountValue属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link CurrencyAmountOptType }
                         *     
                         */
                        public CurrencyAmountOptType getCurrencyAmountValue() {
                            return currencyAmountValue;
                        }

                        /**
                         * 设置currencyAmountValue属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link CurrencyAmountOptType }
                         *     
                         */
                        public void setCurrencyAmountValue(CurrencyAmountOptType value) {
                            this.currencyAmountValue = value;
                        }

                        /**
                         * 获取percentageValue属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public BigDecimal getPercentageValue() {
                            return percentageValue;
                        }

                        /**
                         * 设置percentageValue属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigDecimal }
                         *     
                         */
                        public void setPercentageValue(BigDecimal value) {
                            this.percentageValue = value;
                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="Passengers" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *         &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                     *                 &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "passengers",
                        "otherAssociation"
                    })
                    public static class Associations {

                        @XmlElement(name = "Passengers")
                        protected ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.Passengers passengers;
                        @XmlElement(name = "OtherAssociation")
                        protected List<ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.OtherAssociation> otherAssociation;

                        /**
                         * 获取passengers属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.Passengers }
                         *     
                         */
                        public ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.Passengers getPassengers() {
                            return passengers;
                        }

                        /**
                         * 设置passengers属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.Passengers }
                         *     
                         */
                        public void setPassengers(ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.Passengers value) {
                            this.passengers = value;
                        }

                        /**
                         * Gets the value of the otherAssociation property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the otherAssociation property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getOtherAssociation().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.OtherAssociation }
                         * 
                         * 
                         */
                        public List<ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.OtherAssociation> getOtherAssociation() {
                            if (otherAssociation == null) {
                                otherAssociation = new ArrayList<ItinReshopAlertsType.OrderItemExceptions.Exception.PriceVariances.PriceVariance.Associations.OtherAssociation>();
                            }
                            return this.otherAssociation;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                         *       &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class OtherAssociation {

                            @XmlAttribute(name = "Type", required = true)
                            protected String type;
                            @XmlAttribute(name = "ReferenceValue", required = true)
                            @XmlIDREF
                            @XmlSchemaType(name = "IDREF")
                            protected Object referenceValue;

                            /**
                             * 获取type属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getType() {
                                return type;
                            }

                            /**
                             * 设置type属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setType(String value) {
                                this.type = value;
                            }

                            /**
                             * 获取referenceValue属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getReferenceValue() {
                                return referenceValue;
                            }

                            /**
                             * 设置referenceValue属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setReferenceValue(Object value) {
                                this.referenceValue = value;
                            }

                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "passengerReferences",
                            "groupReference"
                        })
                        public static class Passengers {

                            @XmlList
                            @XmlElement(name = "PassengerReferences", required = true)
                            @XmlIDREF
                            @XmlSchemaType(name = "IDREFS")
                            protected List<Object> passengerReferences;
                            @XmlElement(name = "GroupReference", required = true)
                            @XmlIDREF
                            @XmlSchemaType(name = "IDREF")
                            protected Object groupReference;

                            /**
                             * Association to multiple Passenger instance(s). Example: pax1 pax2 Gets the value of the passengerReferences property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the passengerReferences property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getPassengerReferences().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link Object }
                             * 
                             * 
                             */
                            public List<Object> getPassengerReferences() {
                                if (passengerReferences == null) {
                                    passengerReferences = new ArrayList<Object>();
                                }
                                return this.passengerReferences;
                            }

                            /**
                             * 获取groupReference属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getGroupReference() {
                                return groupReference;
                            }

                            /**
                             * 设置groupReference属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setGroupReference(Object value) {
                                this.groupReference = value;
                            }

                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;simpleContent&gt;
                     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                     *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
                     *       &lt;attribute name="Name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/simpleContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "value"
                    })
                    public static class RuleID {

                        @XmlValue
                        protected String value;
                        @XmlAttribute(name = "Context")
                        protected String context;
                        @XmlAttribute(name = "Name")
                        @XmlSchemaType(name = "anySimpleType")
                        protected String name;

                        /**
                         * A data type for Proper Name size constraint.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getValue() {
                            return value;
                        }

                        /**
                         * 设置value属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setValue(String value) {
                            this.value = value;
                        }

                        /**
                         * 获取context属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getContext() {
                            return context;
                        }

                        /**
                         * 设置context属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setContext(String value) {
                            this.context = value;
                        }

                        /**
                         * 获取name属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * 设置name属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                    }

                }

            }

        }

    }

}
