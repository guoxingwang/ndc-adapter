//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * A base type for an Object with Instance Associations.
 * 
 * <p>AssociatedObjectBaseType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AssociatedObjectBaseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *       &lt;attribute name="ApproxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="RefundAllInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ExemptAllInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AllGuaranteeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="DisclosureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="BrowserInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssociatedObjectBaseType")
@XmlSeeAlso({
    PaymentAddrType.class,
    SimpleAddrType.class,
    com.hpe.cms.ndc.server.convertor.model.AirPassCoreType.Certificate.class,
    AttributesType.class,
    BagAllowanceDescType.class,
    BagAllowanceDimensionType.class,
    MediaAttachmentType.class,
    AllianceProgramType.class,
    AllianceAirlinePartyType.class,
    StructuredAddrType.class,
    com.hpe.cms.ndc.server.convertor.model.CombinationPriceType.Partial.class,
    ShoppingResponseIDType.class,
    com.hpe.cms.ndc.server.convertor.model.BilateralTimeLimitsType.BilateralTimeLimit.class,
    com.hpe.cms.ndc.server.convertor.model.StopLocationType.StopLocation.class,
    FlightDistanceType.class,
    FlightDurationType.class,
    TotalJourneyType.class,
    InvGuaranteeType.class,
    PTCQuantityPricedType.class,
    TicketByTimeLimitType.class,
    AdditionalReferenceType.class,
    CouponEffectiveType.class,
    com.hpe.cms.ndc.server.convertor.model.ExitPositionType.Rows.class,
    com.hpe.cms.ndc.server.convertor.model.WingPositionType.Rows.class,
    com.hpe.cms.ndc.server.convertor.model.SeatComponentType.Locations.Location.class,
    com.hpe.cms.ndc.server.convertor.model.SeatComponentType.Dimensions.class,
    SrvcLocationAirportType.class,
    LeadDatePeriodType.class,
    LeadTimePeriodType.class,
    AirlinePreferencesType.class,
    AlliancePreferencesType.class,
    CabinPreferencesType.class,
    FlightPreferencesType.class,
    FarePreferencesType.class,
    TravelTimePreferencesType.class,
    TravelDistancePreferencesType.class,
    TransferPreferencesType.class,
    FltSegmentMaxTimePreferencesType.class,
    AirDocCancelResProcessType.class,
    com.hpe.cms.ndc.server.convertor.model.AirShopReqParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.AirShopReqParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.AirShopReqAffinityQueryType.OriginDestination.class,
    com.hpe.cms.ndc.server.convertor.model.AirShopReqAttributeQueryType.OriginDestination.class,
    com.hpe.cms.ndc.server.convertor.model.BagAllowReqParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.BagAllowReqParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.BagAllowResParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.BagChargeReqParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.BagChargeReqParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.BagChargeResParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.BagChargeResParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.BagListReqParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.BagListReqParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.BagListResParamsType.Pricing.FeeExemption.class,
    CustInputResProcessType.class,
    MediaMarkupDescType.class,
    com.hpe.cms.ndc.server.convertor.model.FltPriceReqParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.FltPriceReqParamsType.Pricing.FeeExemption.class,
    TicketAutoExchangeType.class,
    com.hpe.cms.ndc.server.convertor.model.FltPriceResParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.InvGuaranteeRQ.Query.Qualifiers.Locations.class,
    InvGuarResProcessType.class,
    CommissionType.class,
    com.hpe.cms.ndc.server.convertor.model.OrderChangeNotif.Query.Amendments.Amendment.OfferItem.class,
    com.hpe.cms.ndc.server.convertor.model.OrderViewRS.Response.Amendments.Amendment.OfferItem.class,
    com.hpe.cms.ndc.server.convertor.model.SeatAvailReqParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.SeatAvailReqParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.SeatAvailResParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.SrvListReqParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.SrvListReqParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.SrvListResParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.SrvPriceReqParamsType.ServiceFilters.class,
    com.hpe.cms.ndc.server.convertor.model.SrvPriceReqParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.SrvPriceResParamsType.Pricing.FeeExemption.class,
    com.hpe.cms.ndc.server.convertor.model.ShopProductRQ.Query.Locations.class,
    com.hpe.cms.ndc.server.convertor.model.ShopProductRQ.Query.class,
    ShopProductResProcessType.class,
    S5Type.class,
    S7Type.class,
    ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.AssociatedServices.AssociatedService.class,
    ShoppingResponseOrderType.Offers.Offer.OfferItems.OfferItem.class,
    ShoppingResponseOrderType.class,
    MarketingCarrierFlightType.class,
    MarketMsgAssocType.class,
    OfferPriceLeadType.class,
    com.hpe.cms.ndc.server.convertor.model.OrderCoreChangeType.Details.ShoppingResponse.OfferItems.OfferItem.class,
    com.hpe.cms.ndc.server.convertor.model.OrderCoreChangeType.Details.ShoppingResponse.class,
    com.hpe.cms.ndc.server.convertor.model.OrderCoreChangeType.Details.Amendments.Amendment.OrderItem.OfferItem.class,
    com.hpe.cms.ndc.server.convertor.model.OrderCoreChangeType.Details.Amendments.Amendment.OrderItem.class,
    com.hpe.cms.ndc.server.convertor.model.DetailCurrencyPriceType.Details.class,
    FeeSurchargeType.class,
    GroupFarePreferencesType.class,
    OperatingCarrierFlightType.class,
    com.hpe.cms.ndc.server.convertor.model.AirShopResParamsType.Pricing.FeeExemption.class,
    Base64MediaType.class,
    HexMediaType.class,
    ItinReshopAlertsType.class,
    OrdChangeAlertsType.class,
    OrdCreateAlertsType.class,
    OrderListAlertsType.class,
    OrdRetrieveAlertsType.class,
    com.hpe.cms.ndc.server.convertor.model.ShopProductResParamsType.Pricing.FeeExemption.class,
    MessageParamsBaseType.class,
    ProgramQualifiers.class,
    ProgramStatusQualifier.class,
    PrePaidProgramQualifier.class,
    IncentiveProgramType.class,
    SecurePaymentPaxInfoType.class,
    BagAllowanceWeightType.class,
    BagAllowancePieceType.class
})
public class AssociatedObjectBaseType {

    @XmlAttribute(name = "ApproxInd")
    protected Boolean approxInd;
    @XmlAttribute(name = "RefundAllInd")
    protected Boolean refundAllInd;
    @XmlAttribute(name = "ExemptAllInd")
    protected Boolean exemptAllInd;
    @XmlAttribute(name = "AllGuaranteeInd")
    protected Boolean allGuaranteeInd;
    @XmlAttribute(name = "DisclosureInd")
    protected Boolean disclosureInd;
    @XmlAttribute(name = "BrowserInd")
    protected Boolean browserInd;
    @XmlAttribute(name = "refs")
    @XmlIDREF
    protected List<Object> refs;

    /**
     * 获取approxInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApproxInd() {
        return approxInd;
    }

    /**
     * 设置approxInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApproxInd(Boolean value) {
        this.approxInd = value;
    }

    /**
     * 获取refundAllInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRefundAllInd() {
        return refundAllInd;
    }

    /**
     * 设置refundAllInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRefundAllInd(Boolean value) {
        this.refundAllInd = value;
    }

    /**
     * 获取exemptAllInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExemptAllInd() {
        return exemptAllInd;
    }

    /**
     * 设置exemptAllInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExemptAllInd(Boolean value) {
        this.exemptAllInd = value;
    }

    /**
     * 获取allGuaranteeInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllGuaranteeInd() {
        return allGuaranteeInd;
    }

    /**
     * 设置allGuaranteeInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllGuaranteeInd(Boolean value) {
        this.allGuaranteeInd = value;
    }

    /**
     * 获取disclosureInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisclosureInd() {
        return disclosureInd;
    }

    /**
     * 设置disclosureInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisclosureInd(Boolean value) {
        this.disclosureInd = value;
    }

    /**
     * 获取browserInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBrowserInd() {
        return browserInd;
    }

    /**
     * 设置browserInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBrowserInd(Boolean value) {
        this.browserInd = value;
    }

    /**
     * Gets the value of the refs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getRefs() {
        if (refs == null) {
            refs = new ArrayList<Object>();
        }
        return this.refs;
    }

}
