//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Shopping Offer Association(s) definition.
 * 
 * <p>OfferAssociationsType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="OfferAssociationsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Shopper" type="{http://www.iata.org/IATA/EDIST}ShopperInfoAssocType" minOccurs="0"/&gt;
 *         &lt;element name="Flight" type="{http://www.iata.org/IATA/EDIST}FlightInfoAssocType" minOccurs="0"/&gt;
 *         &lt;element name="PriceClass" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="BagDetails" type="{http://www.iata.org/IATA/EDIST}BagDetailAssocType" minOccurs="0"/&gt;
 *         &lt;element name="OfferDetails" type="{http://www.iata.org/IATA/EDIST}OfferDetailInfoAssocType" minOccurs="0"/&gt;
 *         &lt;element name="Services" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Service" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}BundleReference" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceReferences"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Media" type="{http://www.iata.org/IATA/EDIST}MediaAssocType" minOccurs="0"/&gt;
 *         &lt;element name="Other" type="{http://www.iata.org/IATA/EDIST}OtherOfferAssocType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferAssociationsType", propOrder = {
    "shopper",
    "flight",
    "priceClass",
    "bagDetails",
    "offerDetails",
    "services",
    "media",
    "other"
})
@XmlSeeAlso({
    OfferAssociations.class
})
public class OfferAssociationsType {

    @XmlElement(name = "Shopper")
    protected ShopperInfoAssocType shopper;
    @XmlElement(name = "Flight")
    protected FlightInfoAssocType flight;
    @XmlElement(name = "PriceClass")
    protected OfferAssociationsType.PriceClass priceClass;
    @XmlElement(name = "BagDetails")
    protected BagDetailAssocType bagDetails;
    @XmlElement(name = "OfferDetails")
    protected OfferDetailInfoAssocType offerDetails;
    @XmlElement(name = "Services")
    protected OfferAssociationsType.Services services;
    @XmlElement(name = "Media")
    protected MediaAssocType media;
    @XmlElement(name = "Other")
    protected OtherOfferAssocType other;

    /**
     * 获取shopper属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShopperInfoAssocType }
     *     
     */
    public ShopperInfoAssocType getShopper() {
        return shopper;
    }

    /**
     * 设置shopper属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShopperInfoAssocType }
     *     
     */
    public void setShopper(ShopperInfoAssocType value) {
        this.shopper = value;
    }

    /**
     * 获取flight属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FlightInfoAssocType }
     *     
     */
    public FlightInfoAssocType getFlight() {
        return flight;
    }

    /**
     * 设置flight属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FlightInfoAssocType }
     *     
     */
    public void setFlight(FlightInfoAssocType value) {
        this.flight = value;
    }

    /**
     * 获取priceClass属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OfferAssociationsType.PriceClass }
     *     
     */
    public OfferAssociationsType.PriceClass getPriceClass() {
        return priceClass;
    }

    /**
     * 设置priceClass属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OfferAssociationsType.PriceClass }
     *     
     */
    public void setPriceClass(OfferAssociationsType.PriceClass value) {
        this.priceClass = value;
    }

    /**
     * 获取bagDetails属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BagDetailAssocType }
     *     
     */
    public BagDetailAssocType getBagDetails() {
        return bagDetails;
    }

    /**
     * 设置bagDetails属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BagDetailAssocType }
     *     
     */
    public void setBagDetails(BagDetailAssocType value) {
        this.bagDetails = value;
    }

    /**
     * 获取offerDetails属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OfferDetailInfoAssocType }
     *     
     */
    public OfferDetailInfoAssocType getOfferDetails() {
        return offerDetails;
    }

    /**
     * 设置offerDetails属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OfferDetailInfoAssocType }
     *     
     */
    public void setOfferDetails(OfferDetailInfoAssocType value) {
        this.offerDetails = value;
    }

    /**
     * 获取services属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OfferAssociationsType.Services }
     *     
     */
    public OfferAssociationsType.Services getServices() {
        return services;
    }

    /**
     * 设置services属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OfferAssociationsType.Services }
     *     
     */
    public void setServices(OfferAssociationsType.Services value) {
        this.services = value;
    }

    /**
     * 获取media属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MediaAssocType }
     *     
     */
    public MediaAssocType getMedia() {
        return media;
    }

    /**
     * 设置media属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MediaAssocType }
     *     
     */
    public void setMedia(MediaAssocType value) {
        this.media = value;
    }

    /**
     * 获取other属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OtherOfferAssocType }
     *     
     */
    public OtherOfferAssocType getOther() {
        return other;
    }

    /**
     * 设置other属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OtherOfferAssocType }
     *     
     */
    public void setOther(OtherOfferAssocType value) {
        this.other = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "priceClassReference"
    })
    public static class PriceClass {

        @XmlElement(name = "PriceClassReference", required = true)
        @XmlIDREF
        @XmlSchemaType(name = "IDREF")
        protected Object priceClassReference;

        /**
         * 获取priceClassReference属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getPriceClassReference() {
            return priceClassReference;
        }

        /**
         * 设置priceClassReference属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setPriceClassReference(Object value) {
            this.priceClassReference = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Service" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BundleReference" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceReferences"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service"
    })
    public static class Services {

        @XmlElement(name = "Service", required = true)
        protected List<OfferAssociationsType.Services.Service> service;

        /**
         * Gets the value of the service property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the service property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getService().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link OfferAssociationsType.Services.Service }
         * 
         * 
         */
        public List<OfferAssociationsType.Services.Service> getService() {
            if (service == null) {
                service = new ArrayList<OfferAssociationsType.Services.Service>();
            }
            return this.service;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BundleReference" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceReferences"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "bundleReference",
            "serviceReferences"
        })
        public static class Service {

            @XmlElement(name = "BundleReference")
            @XmlIDREF
            @XmlSchemaType(name = "IDREF")
            protected Object bundleReference;
            @XmlList
            @XmlElement(name = "ServiceReferences", required = true)
            @XmlIDREF
            @XmlSchemaType(name = "IDREFS")
            protected List<Object> serviceReferences;

            /**
             * 获取bundleReference属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getBundleReference() {
                return bundleReference;
            }

            /**
             * 设置bundleReference属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setBundleReference(Object value) {
                this.bundleReference = value;
            }

            /**
             * Gets the value of the serviceReferences property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the serviceReferences property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getServiceReferences().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public List<Object> getServiceReferences() {
                if (serviceReferences == null) {
                    serviceReferences = new ArrayList<Object>();
                }
                return this.serviceReferences;
            }

        }

    }

}
