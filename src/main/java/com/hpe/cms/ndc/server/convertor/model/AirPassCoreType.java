//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * AIR PASS CORE representation.
 * 
 * <p>AirPassCoreType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AirPassCoreType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlineID"/&gt;
 *         &lt;element name="Certificate" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Number"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
 *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Application" type="{http://www.iata.org/IATA/EDIST}ContactAppSimpleType" minOccurs="0"/&gt;
 *                   &lt;element name="EffectivePeriod" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}YearPeriod"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}YearMonthPeriod"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}TimePeriod"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}QuarterPeriod"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}MonthPeriod"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DayPeriod"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DateTimePeriod"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DatePeriod"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirPassCoreType", propOrder = {
    "airlineID",
    "certificate"
})
@XmlSeeAlso({
    AirPassDetailType.class
})
public class AirPassCoreType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "AirlineID", required = true)
    protected AirlineID airlineID;
    @XmlElement(name = "Certificate", required = true)
    protected List<AirPassCoreType.Certificate> certificate;

    /**
     * Air Pass Program issuing airline designator code. Example: BA
     * 
     * Encoding Scheme: IATA/ A4A (two to three character) Airline Designator Code
     * 
     * @return
     *     possible object is
     *     {@link AirlineID }
     *     
     */
    public AirlineID getAirlineID() {
        return airlineID;
    }

    /**
     * 设置airlineID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AirlineID }
     *     
     */
    public void setAirlineID(AirlineID value) {
        this.airlineID = value;
    }

    /**
     * Gets the value of the certificate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the certificate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCertificate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirPassCoreType.Certificate }
     * 
     * 
     */
    public List<AirPassCoreType.Certificate> getCertificate() {
        if (certificate == null) {
            certificate = new ArrayList<AirPassCoreType.Certificate>();
        }
        return this.certificate;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Number"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
     *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Application" type="{http://www.iata.org/IATA/EDIST}ContactAppSimpleType" minOccurs="0"/&gt;
     *         &lt;element name="EffectivePeriod" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}YearPeriod"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}YearMonthPeriod"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TimePeriod"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}QuarterPeriod"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}MonthPeriod"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DayPeriod"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DateTimePeriod"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DatePeriod"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "number",
        "application",
        "effectivePeriod"
    })
    public static class Certificate
        extends AssociatedObjectBaseType
    {

        @XmlElement(name = "Number", required = true)
        protected AirPassCoreType.Certificate.Number number;
        @XmlElement(name = "Application")
        protected String application;
        @XmlElement(name = "EffectivePeriod")
        protected AirPassCoreType.Certificate.EffectivePeriod effectivePeriod;

        /**
         * 获取number属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirPassCoreType.Certificate.Number }
         *     
         */
        public AirPassCoreType.Certificate.Number getNumber() {
            return number;
        }

        /**
         * 设置number属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirPassCoreType.Certificate.Number }
         *     
         */
        public void setNumber(AirPassCoreType.Certificate.Number value) {
            this.number = value;
        }

        /**
         * 获取application属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApplication() {
            return application;
        }

        /**
         * 设置application属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApplication(String value) {
            this.application = value;
        }

        /**
         * 获取effectivePeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirPassCoreType.Certificate.EffectivePeriod }
         *     
         */
        public AirPassCoreType.Certificate.EffectivePeriod getEffectivePeriod() {
            return effectivePeriod;
        }

        /**
         * 设置effectivePeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirPassCoreType.Certificate.EffectivePeriod }
         *     
         */
        public void setEffectivePeriod(AirPassCoreType.Certificate.EffectivePeriod value) {
            this.effectivePeriod = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}YearPeriod"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}YearMonthPeriod"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TimePeriod"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}QuarterPeriod"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MonthPeriod"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DayPeriod"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DateTimePeriod"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DatePeriod"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "yearPeriod",
            "yearMonthPeriod",
            "timePeriod",
            "quarterPeriod",
            "monthPeriod",
            "dayPeriod",
            "dateTimePeriod",
            "datePeriod"
        })
        public static class EffectivePeriod {

            @XmlElement(name = "YearPeriod", required = true)
            protected YearPeriodRepType yearPeriod;
            @XmlElement(name = "YearMonthPeriod", required = true)
            protected YearMonthPeriodRepType yearMonthPeriod;
            @XmlElement(name = "TimePeriod", required = true)
            protected TimePeriodRepType timePeriod;
            @XmlElement(name = "QuarterPeriod", required = true)
            protected QuarterPeriodRepType quarterPeriod;
            @XmlElement(name = "MonthPeriod", required = true)
            protected MonthPeriodRepType monthPeriod;
            @XmlElement(name = "DayPeriod", required = true)
            protected DayPeriodRepType dayPeriod;
            @XmlElement(name = "DateTimePeriod", required = true)
            protected DateTimePeriodRepType dateTimePeriod;
            @XmlElement(name = "DatePeriod", required = true)
            protected DatePeriodRepType datePeriod;

            /**
             * 获取yearPeriod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link YearPeriodRepType }
             *     
             */
            public YearPeriodRepType getYearPeriod() {
                return yearPeriod;
            }

            /**
             * 设置yearPeriod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link YearPeriodRepType }
             *     
             */
            public void setYearPeriod(YearPeriodRepType value) {
                this.yearPeriod = value;
            }

            /**
             * 获取yearMonthPeriod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link YearMonthPeriodRepType }
             *     
             */
            public YearMonthPeriodRepType getYearMonthPeriod() {
                return yearMonthPeriod;
            }

            /**
             * 设置yearMonthPeriod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link YearMonthPeriodRepType }
             *     
             */
            public void setYearMonthPeriod(YearMonthPeriodRepType value) {
                this.yearMonthPeriod = value;
            }

            /**
             * 获取timePeriod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link TimePeriodRepType }
             *     
             */
            public TimePeriodRepType getTimePeriod() {
                return timePeriod;
            }

            /**
             * 设置timePeriod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link TimePeriodRepType }
             *     
             */
            public void setTimePeriod(TimePeriodRepType value) {
                this.timePeriod = value;
            }

            /**
             * 获取quarterPeriod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link QuarterPeriodRepType }
             *     
             */
            public QuarterPeriodRepType getQuarterPeriod() {
                return quarterPeriod;
            }

            /**
             * 设置quarterPeriod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link QuarterPeriodRepType }
             *     
             */
            public void setQuarterPeriod(QuarterPeriodRepType value) {
                this.quarterPeriod = value;
            }

            /**
             * 获取monthPeriod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link MonthPeriodRepType }
             *     
             */
            public MonthPeriodRepType getMonthPeriod() {
                return monthPeriod;
            }

            /**
             * 设置monthPeriod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link MonthPeriodRepType }
             *     
             */
            public void setMonthPeriod(MonthPeriodRepType value) {
                this.monthPeriod = value;
            }

            /**
             * 获取dayPeriod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link DayPeriodRepType }
             *     
             */
            public DayPeriodRepType getDayPeriod() {
                return dayPeriod;
            }

            /**
             * 设置dayPeriod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link DayPeriodRepType }
             *     
             */
            public void setDayPeriod(DayPeriodRepType value) {
                this.dayPeriod = value;
            }

            /**
             * 获取dateTimePeriod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link DateTimePeriodRepType }
             *     
             */
            public DateTimePeriodRepType getDateTimePeriod() {
                return dateTimePeriod;
            }

            /**
             * 设置dateTimePeriod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link DateTimePeriodRepType }
             *     
             */
            public void setDateTimePeriod(DateTimePeriodRepType value) {
                this.dateTimePeriod = value;
            }

            /**
             * 获取datePeriod属性的值。
             * 
             * @return
             *     possible object is
             *     {@link DatePeriodRepType }
             *     
             */
            public DatePeriodRepType getDatePeriod() {
                return datePeriod;
            }

            /**
             * 设置datePeriod属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link DatePeriodRepType }
             *     
             */
            public void setDatePeriod(DatePeriodRepType value) {
                this.datePeriod = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
         *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Number {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "refs")
            @XmlIDREF
            protected List<Object> refs;

            /**
             * A data type for Proper Name size constraint.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the refs property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the refs property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRefs().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public List<Object> getRefs() {
                if (refs == null) {
                    refs = new ArrayList<Object>();
                }
                return this.refs;
            }

        }

    }

}
