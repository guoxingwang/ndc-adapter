//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * ORDER CREATION PRICE VARIANCE RULE definition.
 * 
 * <p>PriceVarianceRuleType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PriceVarianceRuleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RuleID" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType"/&gt;
 *         &lt;element name="AcceptableVariance"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Name" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="EffectivePeriod" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DatePeriod"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DateTimePeriod"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}QuarterPeriod"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DayPeriod"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}MonthPeriod"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}YearPeriod"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}YearMonthPeriod"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TimePeriod"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectKeyAttrGroup"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceVarianceRuleType", propOrder = {
    "ruleID",
    "acceptableVariance",
    "name",
    "owner",
    "effectivePeriod",
    "remarks"
})
public class PriceVarianceRuleType {

    @XmlElement(name = "RuleID", required = true)
    protected String ruleID;
    @XmlElement(name = "AcceptableVariance", required = true)
    protected PriceVarianceRuleType.AcceptableVariance acceptableVariance;
    @XmlElement(name = "Name")
    protected PriceVarianceRuleType.Name name;
    @XmlElement(name = "Owner")
    protected String owner;
    @XmlElement(name = "EffectivePeriod")
    protected PriceVarianceRuleType.EffectivePeriod effectivePeriod;
    @XmlElement(name = "Remarks")
    protected RemarkType remarks;
    @XmlAttribute(name = "refs")
    @XmlIDREF
    protected List<Object> refs;
    @XmlAttribute(name = "ObjectKey")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String objectKey;

    /**
     * 获取ruleID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleID() {
        return ruleID;
    }

    /**
     * 设置ruleID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleID(String value) {
        this.ruleID = value;
    }

    /**
     * 获取acceptableVariance属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PriceVarianceRuleType.AcceptableVariance }
     *     
     */
    public PriceVarianceRuleType.AcceptableVariance getAcceptableVariance() {
        return acceptableVariance;
    }

    /**
     * 设置acceptableVariance属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PriceVarianceRuleType.AcceptableVariance }
     *     
     */
    public void setAcceptableVariance(PriceVarianceRuleType.AcceptableVariance value) {
        this.acceptableVariance = value;
    }

    /**
     * 获取name属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PriceVarianceRuleType.Name }
     *     
     */
    public PriceVarianceRuleType.Name getName() {
        return name;
    }

    /**
     * 设置name属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PriceVarianceRuleType.Name }
     *     
     */
    public void setName(PriceVarianceRuleType.Name value) {
        this.name = value;
    }

    /**
     * 获取owner属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * 设置owner属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * 获取effectivePeriod属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PriceVarianceRuleType.EffectivePeriod }
     *     
     */
    public PriceVarianceRuleType.EffectivePeriod getEffectivePeriod() {
        return effectivePeriod;
    }

    /**
     * 设置effectivePeriod属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PriceVarianceRuleType.EffectivePeriod }
     *     
     */
    public void setEffectivePeriod(PriceVarianceRuleType.EffectivePeriod value) {
        this.effectivePeriod = value;
    }

    /**
     * 获取remarks属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RemarkType }
     *     
     */
    public RemarkType getRemarks() {
        return remarks;
    }

    /**
     * 设置remarks属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RemarkType }
     *     
     */
    public void setRemarks(RemarkType value) {
        this.remarks = value;
    }

    /**
     * Gets the value of the refs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getRefs() {
        if (refs == null) {
            refs = new ArrayList<Object>();
        }
        return this.refs;
    }

    /**
     * 获取objectKey属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectKey() {
        return objectKey;
    }

    /**
     * 设置objectKey属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectKey(String value) {
        this.objectKey = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "currencyAmountValue",
        "percentageValue"
    })
    public static class AcceptableVariance {

        @XmlElement(name = "CurrencyAmountValue", required = true)
        protected CurrencyAmountOptType currencyAmountValue;
        @XmlElement(name = "PercentageValue")
        protected BigDecimal percentageValue;

        /**
         * 获取currencyAmountValue属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CurrencyAmountOptType }
         *     
         */
        public CurrencyAmountOptType getCurrencyAmountValue() {
            return currencyAmountValue;
        }

        /**
         * 设置currencyAmountValue属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CurrencyAmountOptType }
         *     
         */
        public void setCurrencyAmountValue(CurrencyAmountOptType value) {
            this.currencyAmountValue = value;
        }

        /**
         * 获取percentageValue属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPercentageValue() {
            return percentageValue;
        }

        /**
         * 设置percentageValue属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPercentageValue(BigDecimal value) {
            this.percentageValue = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DatePeriod"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DateTimePeriod"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}QuarterPeriod"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DayPeriod"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MonthPeriod"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}YearPeriod"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}YearMonthPeriod"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TimePeriod"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "datePeriod",
        "dateTimePeriod",
        "quarterPeriod",
        "dayPeriod",
        "monthPeriod",
        "yearPeriod",
        "yearMonthPeriod",
        "timePeriod"
    })
    public static class EffectivePeriod {

        @XmlElement(name = "DatePeriod")
        protected DatePeriodRepType datePeriod;
        @XmlElement(name = "DateTimePeriod")
        protected DateTimePeriodRepType dateTimePeriod;
        @XmlElement(name = "QuarterPeriod")
        protected QuarterPeriodRepType quarterPeriod;
        @XmlElement(name = "DayPeriod")
        protected DayPeriodRepType dayPeriod;
        @XmlElement(name = "MonthPeriod")
        protected MonthPeriodRepType monthPeriod;
        @XmlElement(name = "YearPeriod")
        protected YearPeriodRepType yearPeriod;
        @XmlElement(name = "YearMonthPeriod")
        protected YearMonthPeriodRepType yearMonthPeriod;
        @XmlElement(name = "TimePeriod")
        protected TimePeriodRepType timePeriod;

        /**
         * 获取datePeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link DatePeriodRepType }
         *     
         */
        public DatePeriodRepType getDatePeriod() {
            return datePeriod;
        }

        /**
         * 设置datePeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link DatePeriodRepType }
         *     
         */
        public void setDatePeriod(DatePeriodRepType value) {
            this.datePeriod = value;
        }

        /**
         * 获取dateTimePeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link DateTimePeriodRepType }
         *     
         */
        public DateTimePeriodRepType getDateTimePeriod() {
            return dateTimePeriod;
        }

        /**
         * 设置dateTimePeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link DateTimePeriodRepType }
         *     
         */
        public void setDateTimePeriod(DateTimePeriodRepType value) {
            this.dateTimePeriod = value;
        }

        /**
         * 获取quarterPeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link QuarterPeriodRepType }
         *     
         */
        public QuarterPeriodRepType getQuarterPeriod() {
            return quarterPeriod;
        }

        /**
         * 设置quarterPeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link QuarterPeriodRepType }
         *     
         */
        public void setQuarterPeriod(QuarterPeriodRepType value) {
            this.quarterPeriod = value;
        }

        /**
         * 获取dayPeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link DayPeriodRepType }
         *     
         */
        public DayPeriodRepType getDayPeriod() {
            return dayPeriod;
        }

        /**
         * 设置dayPeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link DayPeriodRepType }
         *     
         */
        public void setDayPeriod(DayPeriodRepType value) {
            this.dayPeriod = value;
        }

        /**
         * 获取monthPeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link MonthPeriodRepType }
         *     
         */
        public MonthPeriodRepType getMonthPeriod() {
            return monthPeriod;
        }

        /**
         * 设置monthPeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link MonthPeriodRepType }
         *     
         */
        public void setMonthPeriod(MonthPeriodRepType value) {
            this.monthPeriod = value;
        }

        /**
         * 获取yearPeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link YearPeriodRepType }
         *     
         */
        public YearPeriodRepType getYearPeriod() {
            return yearPeriod;
        }

        /**
         * 设置yearPeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link YearPeriodRepType }
         *     
         */
        public void setYearPeriod(YearPeriodRepType value) {
            this.yearPeriod = value;
        }

        /**
         * 获取yearMonthPeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link YearMonthPeriodRepType }
         *     
         */
        public YearMonthPeriodRepType getYearMonthPeriod() {
            return yearMonthPeriod;
        }

        /**
         * 设置yearMonthPeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link YearMonthPeriodRepType }
         *     
         */
        public void setYearMonthPeriod(YearMonthPeriodRepType value) {
            this.yearMonthPeriod = value;
        }

        /**
         * 获取timePeriod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TimePeriodRepType }
         *     
         */
        public TimePeriodRepType getTimePeriod() {
            return timePeriod;
        }

        /**
         * 设置timePeriod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TimePeriodRepType }
         *     
         */
        public void setTimePeriod(TimePeriodRepType value) {
            this.timePeriod = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Name {

        @XmlValue
        protected String value;

        /**
         * A data type for Proper Name size constraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }

}
