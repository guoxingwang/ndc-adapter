//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a>
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * PHONE NUMBER helper object.
 *
 * <p>PhoneType complex type的 Java 类。
 *
 * <p>以下模式片段指定包含在此类中的预期内容。
 *
 * <pre>
 * &lt;complexType name="PhoneType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Application" type="{http://www.iata.org/IATA/EDIST}ContactAppSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="Number" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                 &lt;attribute name="CountryCode" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" /&gt;
 *                 &lt;attribute name="AreaCode" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" /&gt;
 *                 &lt;attribute name="Extension" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhoneType", propOrder = {
    "application",
    "number"
})
@XmlSeeAlso({
    PhoneContactType.class
})
public class PhoneType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "Application")
    protected String application;
    @XmlElement(name = "Number")
    protected List<PhoneType.Number> number;

    /**
     * 获取application属性的值。
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getApplication() {
        return application;
    }

    /**
     * 设置application属性的值。
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setApplication(String value) {
        this.application = value;
    }

    /**
     * Gets the value of the number property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the number property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNumber().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PhoneType.Number }
     *
     *
     */
    public List<PhoneType.Number> getNumber() {
        if (number == null) {
            number = new ArrayList<PhoneType.Number>();
        }
        return this.number;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     *
     * <p>以下模式片段指定包含在此类中的预期内容。
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *       &lt;attribute name="CountryCode" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" /&gt;
     *       &lt;attribute name="AreaCode" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" /&gt;
     *       &lt;attribute name="Extension" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class Number {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "CountryCode")
        protected String countryCode;
        @XmlAttribute(name = "AreaCode")
        protected String areaCode;
        @XmlAttribute(name = "Extension")
        protected String extension;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * 获取value属性的值。
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * 获取countryCode属性的值。
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * 设置countryCode属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCountryCode(String value) {
            this.countryCode = value;
        }

        /**
         * 获取areaCode属性的值。
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getAreaCode() {
            return areaCode;
        }

        /**
         * 设置areaCode属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAreaCode(String value) {
            this.areaCode = value;
        }

        /**
         * 获取extension属性的值。
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getExtension() {
            return extension;
        }

        /**
         * 设置extension属性的值。
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setExtension(String value) {
            this.extension = value;
        }

        /**
         * Gets the value of the refs property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         *
         *
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }

}
