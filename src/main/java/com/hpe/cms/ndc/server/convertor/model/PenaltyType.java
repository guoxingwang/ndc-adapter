//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * NDC Capability Model: PENALTY
 * 
 *  ============
 * Representation(s)
 *  ============
 * • Core Penalty
 * • Detail Penalty
 * 
 *  ============
 * Feature(s)
 *  ============
 * • Penalty Indicators (Cancellation fees apply; Change of Itinerary fees apply; Refundable ticket and/ or fare; Reusable ticket and/ or fare; Upgrade fee applies)
 * • Applicable Fee Remarks
 * • Penalty Type (Cancellation, No Show, Upgrade, etc.)
 * • Penalty Application (IATA Padis Codeset FAT - Fare or Application Penalty Type)
 * • Penalty Amount, represented as percentage or fixed value, with Amount Application (MinimumPenaltyAmount, MaximumPenaltyAmount, Other) and Applicable Fee Remarks
 * 
 *  ============
 * Metadata
 *  ============
 * • PenaltyMetadata
 * 
 * <p>PenaltyType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PenaltyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ApplicableFeeRemarks" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}RemarkType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Details" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Detail" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}PenaltySimpleType"/&gt;
 *                             &lt;element name="Application" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                             &lt;element name="Amounts" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Amount" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;choice&gt;
 *                                                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
 *                                                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
 *                                                 &lt;/choice&gt;
 *                                                 &lt;element name="AmountApplication" type="{http://www.iata.org/IATA/EDIST}PenaltyAppSimpleType" minOccurs="0"/&gt;
 *                                                 &lt;element name="ApplicableFeeRemarks" minOccurs="0"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;extension base="{http://www.iata.org/IATA/EDIST}RemarkType"&gt;
 *                                                       &lt;/extension&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="CancelFeeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ChangeFeeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="RefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ReuseInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="UpgradeFeeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PenaltyType", propOrder = {
    "applicableFeeRemarks",
    "details"
})
@XmlSeeAlso({
    PenaltyDetailType.class,
    OfferPenaltyType.class,
    OrderPenaltyType.class
})
public class PenaltyType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "ApplicableFeeRemarks")
    protected PenaltyType.ApplicableFeeRemarks applicableFeeRemarks;
    @XmlElement(name = "Details")
    protected PenaltyType.Details details;
    @XmlAttribute(name = "CancelFeeInd")
    protected Boolean cancelFeeInd;
    @XmlAttribute(name = "ChangeFeeInd")
    protected Boolean changeFeeInd;
    @XmlAttribute(name = "RefundableInd")
    protected Boolean refundableInd;
    @XmlAttribute(name = "ReuseInd")
    protected Boolean reuseInd;
    @XmlAttribute(name = "UpgradeFeeInd")
    protected Boolean upgradeFeeInd;

    /**
     * 获取applicableFeeRemarks属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PenaltyType.ApplicableFeeRemarks }
     *     
     */
    public PenaltyType.ApplicableFeeRemarks getApplicableFeeRemarks() {
        return applicableFeeRemarks;
    }

    /**
     * 设置applicableFeeRemarks属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PenaltyType.ApplicableFeeRemarks }
     *     
     */
    public void setApplicableFeeRemarks(PenaltyType.ApplicableFeeRemarks value) {
        this.applicableFeeRemarks = value;
    }

    /**
     * 获取details属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PenaltyType.Details }
     *     
     */
    public PenaltyType.Details getDetails() {
        return details;
    }

    /**
     * 设置details属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PenaltyType.Details }
     *     
     */
    public void setDetails(PenaltyType.Details value) {
        this.details = value;
    }

    /**
     * 获取cancelFeeInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancelFeeInd() {
        return cancelFeeInd;
    }

    /**
     * 设置cancelFeeInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancelFeeInd(Boolean value) {
        this.cancelFeeInd = value;
    }

    /**
     * 获取changeFeeInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChangeFeeInd() {
        return changeFeeInd;
    }

    /**
     * 设置changeFeeInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeFeeInd(Boolean value) {
        this.changeFeeInd = value;
    }

    /**
     * 获取refundableInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRefundableInd() {
        return refundableInd;
    }

    /**
     * 设置refundableInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRefundableInd(Boolean value) {
        this.refundableInd = value;
    }

    /**
     * 获取reuseInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReuseInd() {
        return reuseInd;
    }

    /**
     * 设置reuseInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReuseInd(Boolean value) {
        this.reuseInd = value;
    }

    /**
     * 获取upgradeFeeInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUpgradeFeeInd() {
        return upgradeFeeInd;
    }

    /**
     * 设置upgradeFeeInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUpgradeFeeInd(Boolean value) {
        this.upgradeFeeInd = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}RemarkType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ApplicableFeeRemarks
        extends RemarkType
    {


    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Detail" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}PenaltySimpleType"/&gt;
     *                   &lt;element name="Application" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *                   &lt;element name="Amounts" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Amount" maxOccurs="unbounded"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;choice&gt;
     *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
     *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
     *                                       &lt;/choice&gt;
     *                                       &lt;element name="AmountApplication" type="{http://www.iata.org/IATA/EDIST}PenaltyAppSimpleType" minOccurs="0"/&gt;
     *                                       &lt;element name="ApplicableFeeRemarks" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}RemarkType"&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "detail"
    })
    public static class Details {

        @XmlElement(name = "Detail", required = true)
        protected List<PenaltyType.Details.Detail> detail;

        /**
         * Gets the value of the detail property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the detail property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDetail().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PenaltyType.Details.Detail }
         * 
         * 
         */
        public List<PenaltyType.Details.Detail> getDetail() {
            if (detail == null) {
                detail = new ArrayList<PenaltyType.Details.Detail>();
            }
            return this.detail;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}PenaltySimpleType"/&gt;
         *         &lt;element name="Application" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
         *         &lt;element name="Amounts" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Amount" maxOccurs="unbounded"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;choice&gt;
         *                               &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
         *                               &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
         *                             &lt;/choice&gt;
         *                             &lt;element name="AmountApplication" type="{http://www.iata.org/IATA/EDIST}PenaltyAppSimpleType" minOccurs="0"/&gt;
         *                             &lt;element name="ApplicableFeeRemarks" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}RemarkType"&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type",
            "application",
            "amounts"
        })
        public static class Detail
            extends KeyWithMetaObjectBaseType
        {

            @XmlElement(name = "Type", required = true)
            protected String type;
            @XmlElement(name = "Application")
            protected CodesetType application;
            @XmlElement(name = "Amounts")
            protected PenaltyType.Details.Detail.Amounts amounts;

            /**
             * 获取type属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * 设置type属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * 获取application属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CodesetType }
             *     
             */
            public CodesetType getApplication() {
                return application;
            }

            /**
             * 设置application属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CodesetType }
             *     
             */
            public void setApplication(CodesetType value) {
                this.application = value;
            }

            /**
             * 获取amounts属性的值。
             * 
             * @return
             *     possible object is
             *     {@link PenaltyType.Details.Detail.Amounts }
             *     
             */
            public PenaltyType.Details.Detail.Amounts getAmounts() {
                return amounts;
            }

            /**
             * 设置amounts属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link PenaltyType.Details.Detail.Amounts }
             *     
             */
            public void setAmounts(PenaltyType.Details.Detail.Amounts value) {
                this.amounts = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Amount" maxOccurs="unbounded"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;choice&gt;
             *                     &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
             *                     &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
             *                   &lt;/choice&gt;
             *                   &lt;element name="AmountApplication" type="{http://www.iata.org/IATA/EDIST}PenaltyAppSimpleType" minOccurs="0"/&gt;
             *                   &lt;element name="ApplicableFeeRemarks" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}RemarkType"&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amount"
            })
            public static class Amounts {

                @XmlElement(name = "Amount", required = true)
                protected List<PenaltyType.Details.Detail.Amounts.Amount> amount;

                /**
                 * Gets the value of the amount property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the amount property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getAmount().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link PenaltyType.Details.Detail.Amounts.Amount }
                 * 
                 * 
                 */
                public List<PenaltyType.Details.Detail.Amounts.Amount> getAmount() {
                    if (amount == null) {
                        amount = new ArrayList<PenaltyType.Details.Detail.Amounts.Amount>();
                    }
                    return this.amount;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;choice&gt;
                 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmountValue"/&gt;
                 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}PercentageValue"/&gt;
                 *         &lt;/choice&gt;
                 *         &lt;element name="AmountApplication" type="{http://www.iata.org/IATA/EDIST}PenaltyAppSimpleType" minOccurs="0"/&gt;
                 *         &lt;element name="ApplicableFeeRemarks" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}RemarkType"&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "currencyAmountValue",
                    "percentageValue",
                    "amountApplication",
                    "applicableFeeRemarks"
                })
                public static class Amount {

                    @XmlElement(name = "CurrencyAmountValue")
                    protected CurrencyAmountOptType currencyAmountValue;
                    @XmlElement(name = "PercentageValue")
                    protected BigDecimal percentageValue;
                    @XmlElement(name = "AmountApplication")
                    protected String amountApplication;
                    @XmlElement(name = "ApplicableFeeRemarks")
                    protected PenaltyType.Details.Detail.Amounts.Amount.ApplicableFeeRemarks applicableFeeRemarks;

                    /**
                     * Penalty monetary value amount and currency code.
                     * 
                     * @return
                     *     possible object is
                     *     {@link CurrencyAmountOptType }
                     *     
                     */
                    public CurrencyAmountOptType getCurrencyAmountValue() {
                        return currencyAmountValue;
                    }

                    /**
                     * 设置currencyAmountValue属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link CurrencyAmountOptType }
                     *     
                     */
                    public void setCurrencyAmountValue(CurrencyAmountOptType value) {
                        this.currencyAmountValue = value;
                    }

                    /**
                     * 获取percentageValue属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public BigDecimal getPercentageValue() {
                        return percentageValue;
                    }

                    /**
                     * 设置percentageValue属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigDecimal }
                     *     
                     */
                    public void setPercentageValue(BigDecimal value) {
                        this.percentageValue = value;
                    }

                    /**
                     * 获取amountApplication属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAmountApplication() {
                        return amountApplication;
                    }

                    /**
                     * 设置amountApplication属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAmountApplication(String value) {
                        this.amountApplication = value;
                    }

                    /**
                     * 获取applicableFeeRemarks属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link PenaltyType.Details.Detail.Amounts.Amount.ApplicableFeeRemarks }
                     *     
                     */
                    public PenaltyType.Details.Detail.Amounts.Amount.ApplicableFeeRemarks getApplicableFeeRemarks() {
                        return applicableFeeRemarks;
                    }

                    /**
                     * 设置applicableFeeRemarks属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PenaltyType.Details.Detail.Amounts.Amount.ApplicableFeeRemarks }
                     *     
                     */
                    public void setApplicableFeeRemarks(PenaltyType.Details.Detail.Amounts.Amount.ApplicableFeeRemarks value) {
                        this.applicableFeeRemarks = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}RemarkType"&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class ApplicableFeeRemarks
                        extends RemarkType
                    {


                    }

                }

            }

        }

    }

}
