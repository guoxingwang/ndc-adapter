//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * OPTIONAL SERVICE Data List definition.
 * 
 * <p>ListOfServiceType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfServiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Service" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}DataListObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID"/&gt;
 *                   &lt;element name="Name"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Encoding" type="{http://www.iata.org/IATA/EDIST}ServiceEncodingType"/&gt;
 *                   &lt;element name="FeeMethod" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="SVCDescriptions" type="{http://www.iata.org/IATA/EDIST}ServiceDescriptionType"/&gt;
 *                   &lt;element name="Settlement" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Method" type="{http://www.iata.org/IATA/EDIST}CodesetType"/&gt;
 *                             &lt;element name="InterlineSettlementValue" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Price" type="{http://www.iata.org/IATA/EDIST}ServicePriceType" minOccurs="0"/&gt;
 *                   &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}ServiceAssocType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfServiceType", propOrder = {
    "service"
})
public class ListOfServiceType {

    @XmlElement(name = "Service", required = true)
    protected List<ListOfServiceType.Service> service;

    /**
     * Gets the value of the service property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the service property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListOfServiceType.Service }
     * 
     * 
     */
    public List<ListOfServiceType.Service> getService() {
        if (service == null) {
            service = new ArrayList<ListOfServiceType.Service>();
        }
        return this.service;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}DataListObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID"/&gt;
     *         &lt;element name="Name"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Encoding" type="{http://www.iata.org/IATA/EDIST}ServiceEncodingType"/&gt;
     *         &lt;element name="FeeMethod" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="SVCDescriptions" type="{http://www.iata.org/IATA/EDIST}ServiceDescriptionType"/&gt;
     *         &lt;element name="Settlement" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Method" type="{http://www.iata.org/IATA/EDIST}CodesetType"/&gt;
     *                   &lt;element name="InterlineSettlementValue" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Price" type="{http://www.iata.org/IATA/EDIST}ServicePriceType" minOccurs="0"/&gt;
     *         &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}ServiceAssocType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceID",
        "name",
        "encoding",
        "feeMethod",
        "svcDescriptions",
        "settlement",
        "price",
        "associations"
    })
    public static class Service
        extends DataListObjectBaseType
    {

        @XmlElement(name = "ServiceID", required = true)
        protected ServiceIDType serviceID;
        @XmlElement(name = "Name", required = true)
        protected ListOfServiceType.Service.Name name;
        @XmlElement(name = "Encoding", required = true)
        protected ServiceEncodingType encoding;
        @XmlElement(name = "FeeMethod")
        protected ListOfServiceType.Service.FeeMethod feeMethod;
        @XmlElement(name = "SVCDescriptions", required = true)
        protected ServiceDescriptionType svcDescriptions;
        @XmlElement(name = "Settlement")
        protected ListOfServiceType.Service.Settlement settlement;
        @XmlElement(name = "Price")
        protected ServicePriceType price;
        @XmlElement(name = "Associations")
        protected ServiceAssocType associations;

        /**
         * 获取serviceID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceIDType }
         *     
         */
        public ServiceIDType getServiceID() {
            return serviceID;
        }

        /**
         * 设置serviceID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceIDType }
         *     
         */
        public void setServiceID(ServiceIDType value) {
            this.serviceID = value;
        }

        /**
         * 获取name属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ListOfServiceType.Service.Name }
         *     
         */
        public ListOfServiceType.Service.Name getName() {
            return name;
        }

        /**
         * 设置name属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfServiceType.Service.Name }
         *     
         */
        public void setName(ListOfServiceType.Service.Name value) {
            this.name = value;
        }

        /**
         * 获取encoding属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceEncodingType }
         *     
         */
        public ServiceEncodingType getEncoding() {
            return encoding;
        }

        /**
         * 设置encoding属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceEncodingType }
         *     
         */
        public void setEncoding(ServiceEncodingType value) {
            this.encoding = value;
        }

        /**
         * 获取feeMethod属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ListOfServiceType.Service.FeeMethod }
         *     
         */
        public ListOfServiceType.Service.FeeMethod getFeeMethod() {
            return feeMethod;
        }

        /**
         * 设置feeMethod属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfServiceType.Service.FeeMethod }
         *     
         */
        public void setFeeMethod(ListOfServiceType.Service.FeeMethod value) {
            this.feeMethod = value;
        }

        /**
         * 获取svcDescriptions属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceDescriptionType }
         *     
         */
        public ServiceDescriptionType getSVCDescriptions() {
            return svcDescriptions;
        }

        /**
         * 设置svcDescriptions属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceDescriptionType }
         *     
         */
        public void setSVCDescriptions(ServiceDescriptionType value) {
            this.svcDescriptions = value;
        }

        /**
         * 获取settlement属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ListOfServiceType.Service.Settlement }
         *     
         */
        public ListOfServiceType.Service.Settlement getSettlement() {
            return settlement;
        }

        /**
         * 设置settlement属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfServiceType.Service.Settlement }
         *     
         */
        public void setSettlement(ListOfServiceType.Service.Settlement value) {
            this.settlement = value;
        }

        /**
         * 获取price属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServicePriceType }
         *     
         */
        public ServicePriceType getPrice() {
            return price;
        }

        /**
         * 设置price属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServicePriceType }
         *     
         */
        public void setPrice(ServicePriceType value) {
            this.price = value;
        }

        /**
         * 获取associations属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceAssocType }
         *     
         */
        public ServiceAssocType getAssociations() {
            return associations;
        }

        /**
         * 设置associations属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceAssocType }
         *     
         */
        public void setAssociations(ServiceAssocType value) {
            this.associations = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class FeeMethod {

            @XmlValue
            protected String value;

            /**
             * 获取value属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Name {

            @XmlValue
            protected String value;

            /**
             * A data type for Proper Name size constraint.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Method" type="{http://www.iata.org/IATA/EDIST}CodesetType"/&gt;
         *         &lt;element name="InterlineSettlementValue" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "method",
            "interlineSettlementValue"
        })
        public static class Settlement {

            @XmlElement(name = "Method", required = true)
            protected CodesetType method;
            @XmlElement(name = "InterlineSettlementValue")
            protected CurrencyAmountOptType interlineSettlementValue;

            /**
             * 获取method属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CodesetType }
             *     
             */
            public CodesetType getMethod() {
                return method;
            }

            /**
             * 设置method属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CodesetType }
             *     
             */
            public void setMethod(CodesetType value) {
                this.method = value;
            }

            /**
             * 获取interlineSettlementValue属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public CurrencyAmountOptType getInterlineSettlementValue() {
                return interlineSettlementValue;
            }

            /**
             * 设置interlineSettlementValue属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public void setInterlineSettlementValue(CurrencyAmountOptType value) {
                this.interlineSettlementValue = value;
            }

        }

    }

}
