//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * PROCESSING ALERT definition.
 * 
 * <p>AlertType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AlertType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}InventoryDiscrepancyAlert"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PIN_AuthenticationAlert"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SecurePaymentAlert"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertType", propOrder = {
    "inventoryDiscrepancyAlert",
    "pinAuthenticationAlert",
    "securePaymentAlert"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.AlertsType.Alert.class
})
public class AlertType {

    @XmlElement(name = "InventoryDiscrepancyAlert")
    protected InvDiscrepencyAlertType inventoryDiscrepancyAlert;
    @XmlElement(name = "PIN_AuthenticationAlert")
    protected PINAuthTravelerType pinAuthenticationAlert;
    @XmlElement(name = "SecurePaymentAlert")
    protected SecurePaymentAlertType securePaymentAlert;

    /**
     * 获取inventoryDiscrepancyAlert属性的值。
     * 
     * @return
     *     possible object is
     *     {@link InvDiscrepencyAlertType }
     *     
     */
    public InvDiscrepencyAlertType getInventoryDiscrepancyAlert() {
        return inventoryDiscrepancyAlert;
    }

    /**
     * 设置inventoryDiscrepancyAlert属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link InvDiscrepencyAlertType }
     *     
     */
    public void setInventoryDiscrepancyAlert(InvDiscrepencyAlertType value) {
        this.inventoryDiscrepancyAlert = value;
    }

    /**
     * 获取pinAuthenticationAlert属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PINAuthTravelerType }
     *     
     */
    public PINAuthTravelerType getPINAuthenticationAlert() {
        return pinAuthenticationAlert;
    }

    /**
     * 设置pinAuthenticationAlert属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PINAuthTravelerType }
     *     
     */
    public void setPINAuthenticationAlert(PINAuthTravelerType value) {
        this.pinAuthenticationAlert = value;
    }

    /**
     * 获取securePaymentAlert属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType }
     *     
     */
    public SecurePaymentAlertType getSecurePaymentAlert() {
        return securePaymentAlert;
    }

    /**
     * 设置securePaymentAlert属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType }
     *     
     */
    public void setSecurePaymentAlert(SecurePaymentAlertType value) {
        this.securePaymentAlert = value;
    }

}
