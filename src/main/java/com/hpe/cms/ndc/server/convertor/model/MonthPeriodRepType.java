//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Effective/ Expiration Month Date Period Representation.
 * 
 * <p>MonthPeriodRepType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="MonthPeriodRepType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Effective" type="{http://www.iata.org/IATA/EDIST}MonthRepType" minOccurs="0"/&gt;
 *         &lt;element name="Expiration" type="{http://www.iata.org/IATA/EDIST}MonthRepType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MonthPeriodRepType", propOrder = {
    "effective",
    "expiration"
})
public class MonthPeriodRepType {

    @XmlElement(name = "Effective")
    protected MonthRepType effective;
    @XmlElement(name = "Expiration")
    protected MonthRepType expiration;

    /**
     * 获取effective属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MonthRepType }
     *     
     */
    public MonthRepType getEffective() {
        return effective;
    }

    /**
     * 设置effective属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MonthRepType }
     *     
     */
    public void setEffective(MonthRepType value) {
        this.effective = value;
    }

    /**
     * 获取expiration属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MonthRepType }
     *     
     */
    public MonthRepType getExpiration() {
        return expiration;
    }

    /**
     * 设置expiration属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MonthRepType }
     *     
     */
    public void setExpiration(MonthRepType value) {
        this.expiration = value;
    }

}
