//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Inventory Discrepency Alert definition.
 * 
 * Note: This processing condition occurs when there is a discrepency between offered inventory and available inventory. It may include references to Alternate Offers.
 * 
 * <p>InvDiscrepencyAlertType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="InvDiscrepencyAlertType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Code" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}MultiAssociationType" minOccurs="0"/&gt;
 *         &lt;element name="AlternateOffers" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
 *                   &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
 *                   &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="NoInventoryInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvDiscrepencyAlertType", propOrder = {
    "code",
    "associations",
    "alternateOffers",
    "remarks"
})
public class InvDiscrepencyAlertType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "Code")
    protected InvDiscrepencyAlertType.Code code;
    @XmlElement(name = "Associations")
    protected MultiAssociationType associations;
    @XmlElement(name = "AlternateOffers")
    protected InvDiscrepencyAlertType.AlternateOffers alternateOffers;
    @XmlElement(name = "Remarks")
    protected RemarkType remarks;
    @XmlAttribute(name = "NoInventoryInd")
    protected Boolean noInventoryInd;

    /**
     * 获取code属性的值。
     * 
     * @return
     *     possible object is
     *     {@link InvDiscrepencyAlertType.Code }
     *     
     */
    public InvDiscrepencyAlertType.Code getCode() {
        return code;
    }

    /**
     * 设置code属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link InvDiscrepencyAlertType.Code }
     *     
     */
    public void setCode(InvDiscrepencyAlertType.Code value) {
        this.code = value;
    }

    /**
     * 获取associations属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MultiAssociationType }
     *     
     */
    public MultiAssociationType getAssociations() {
        return associations;
    }

    /**
     * 设置associations属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MultiAssociationType }
     *     
     */
    public void setAssociations(MultiAssociationType value) {
        this.associations = value;
    }

    /**
     * 获取alternateOffers属性的值。
     * 
     * @return
     *     possible object is
     *     {@link InvDiscrepencyAlertType.AlternateOffers }
     *     
     */
    public InvDiscrepencyAlertType.AlternateOffers getAlternateOffers() {
        return alternateOffers;
    }

    /**
     * 设置alternateOffers属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link InvDiscrepencyAlertType.AlternateOffers }
     *     
     */
    public void setAlternateOffers(InvDiscrepencyAlertType.AlternateOffers value) {
        this.alternateOffers = value;
    }

    /**
     * 获取remarks属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RemarkType }
     *     
     */
    public RemarkType getRemarks() {
        return remarks;
    }

    /**
     * 设置remarks属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RemarkType }
     *     
     */
    public void setRemarks(RemarkType value) {
        this.remarks = value;
    }

    /**
     * 获取noInventoryInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoInventoryInd() {
        return noInventoryInd;
    }

    /**
     * 设置noInventoryInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoInventoryInd(Boolean value) {
        this.noInventoryInd = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="TotalOfferQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
     *         &lt;element name="Owner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type"/&gt;
     *         &lt;element name="AlternateOffer" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "totalOfferQuantity",
        "owner",
        "alternateOffer"
    })
    public static class AlternateOffers {

        @XmlElement(name = "TotalOfferQuantity")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger totalOfferQuantity;
        @XmlElement(name = "Owner", required = true)
        protected AirlineIDType owner;
        @XmlElement(name = "AlternateOffer", required = true)
        protected List<InvDiscrepencyAlertType.AlternateOffers.AlternateOffer> alternateOffer;

        /**
         * 获取totalOfferQuantity属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotalOfferQuantity() {
            return totalOfferQuantity;
        }

        /**
         * 设置totalOfferQuantity属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotalOfferQuantity(BigInteger value) {
            this.totalOfferQuantity = value;
        }

        /**
         * 获取owner属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirlineIDType }
         *     
         */
        public AirlineIDType getOwner() {
            return owner;
        }

        /**
         * 设置owner属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirlineIDType }
         *     
         */
        public void setOwner(AirlineIDType value) {
            this.owner = value;
        }

        /**
         * Gets the value of the alternateOffer property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alternateOffer property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlternateOffer().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link InvDiscrepencyAlertType.AlternateOffers.AlternateOffer }
         * 
         * 
         */
        public List<InvDiscrepencyAlertType.AlternateOffers.AlternateOffer> getAlternateOffer() {
            if (alternateOffer == null) {
                alternateOffer = new ArrayList<InvDiscrepencyAlertType.AlternateOffers.AlternateOffer>();
            }
            return this.alternateOffer;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltBaggageOffer"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltOtherOffer"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltPricedFlightOffer"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AltSeatOffer"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "altBaggageOffer",
            "altOtherOffer",
            "altPricedFlightOffer",
            "altSeatOffer"
        })
        public static class AlternateOffer
            extends OfferType
        {

            @XmlElement(name = "AltBaggageOffer", required = true)
            protected AltBaggageOfferType altBaggageOffer;
            @XmlElement(name = "AltOtherOffer", required = true)
            protected AltOtherOfferType altOtherOffer;
            @XmlElement(name = "AltPricedFlightOffer", required = true)
            protected AltPricedFlightOfferType altPricedFlightOffer;
            @XmlElement(name = "AltSeatOffer", required = true)
            protected AltSeatOfferType altSeatOffer;

            /**
             * 获取altBaggageOffer属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AltBaggageOfferType }
             *     
             */
            public AltBaggageOfferType getAltBaggageOffer() {
                return altBaggageOffer;
            }

            /**
             * 设置altBaggageOffer属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AltBaggageOfferType }
             *     
             */
            public void setAltBaggageOffer(AltBaggageOfferType value) {
                this.altBaggageOffer = value;
            }

            /**
             * 获取altOtherOffer属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AltOtherOfferType }
             *     
             */
            public AltOtherOfferType getAltOtherOffer() {
                return altOtherOffer;
            }

            /**
             * 设置altOtherOffer属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AltOtherOfferType }
             *     
             */
            public void setAltOtherOffer(AltOtherOfferType value) {
                this.altOtherOffer = value;
            }

            /**
             * 获取altPricedFlightOffer属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AltPricedFlightOfferType }
             *     
             */
            public AltPricedFlightOfferType getAltPricedFlightOffer() {
                return altPricedFlightOffer;
            }

            /**
             * 设置altPricedFlightOffer属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AltPricedFlightOfferType }
             *     
             */
            public void setAltPricedFlightOffer(AltPricedFlightOfferType value) {
                this.altPricedFlightOffer = value;
            }

            /**
             * 获取altSeatOffer属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AltSeatOfferType }
             *     
             */
            public AltSeatOfferType getAltSeatOffer() {
                return altSeatOffer;
            }

            /**
             * 设置altSeatOffer属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AltSeatOfferType }
             *     
             */
            public void setAltSeatOffer(AltSeatOfferType value) {
                this.altSeatOffer = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Code
        extends CodesetType
    {


    }

}
