//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * OFFER TERMS definition.
 * 
 * <p>OfferTermsType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="OfferTermsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="AvailablePeriod" type="{http://www.iata.org/IATA/EDIST}OfferValidPeriodType"/&gt;
 *         &lt;element name="Descriptions" type="{http://www.iata.org/IATA/EDIST}Descriptions"/&gt;
 *         &lt;element name="GroupSize" type="{http://www.iata.org/IATA/EDIST}OfferGroupSizeType"/&gt;
 *         &lt;element name="OrderingQuantity" type="{http://www.iata.org/IATA/EDIST}OfferOrderQuantityType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferTermsType", propOrder = {
    "availablePeriod",
    "descriptions",
    "groupSize",
    "orderingQuantity"
})
public class OfferTermsType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "AvailablePeriod")
    protected OfferValidPeriodType availablePeriod;
    @XmlElement(name = "Descriptions")
    protected Descriptions descriptions;
    @XmlElement(name = "GroupSize")
    protected OfferGroupSizeType groupSize;
    @XmlElement(name = "OrderingQuantity")
    protected OfferOrderQuantityType orderingQuantity;

    /**
     * 获取availablePeriod属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OfferValidPeriodType }
     *     
     */
    public OfferValidPeriodType getAvailablePeriod() {
        return availablePeriod;
    }

    /**
     * 设置availablePeriod属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OfferValidPeriodType }
     *     
     */
    public void setAvailablePeriod(OfferValidPeriodType value) {
        this.availablePeriod = value;
    }

    /**
     * 获取descriptions属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Descriptions }
     *     
     */
    public Descriptions getDescriptions() {
        return descriptions;
    }

    /**
     * 设置descriptions属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Descriptions }
     *     
     */
    public void setDescriptions(Descriptions value) {
        this.descriptions = value;
    }

    /**
     * 获取groupSize属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OfferGroupSizeType }
     *     
     */
    public OfferGroupSizeType getGroupSize() {
        return groupSize;
    }

    /**
     * 设置groupSize属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OfferGroupSizeType }
     *     
     */
    public void setGroupSize(OfferGroupSizeType value) {
        this.groupSize = value;
    }

    /**
     * 获取orderingQuantity属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OfferOrderQuantityType }
     *     
     */
    public OfferOrderQuantityType getOrderingQuantity() {
        return orderingQuantity;
    }

    /**
     * 设置orderingQuantity属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OfferOrderQuantityType }
     *     
     */
    public void setOrderingQuantity(OfferOrderQuantityType value) {
        this.orderingQuantity = value;
    }

}
