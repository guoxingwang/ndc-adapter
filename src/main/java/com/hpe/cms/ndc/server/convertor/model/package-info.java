//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.iata.org/IATA/EDIST", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.hpe.cms.ndc.server.convertor.model;
