//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * CONTENT SOURCE data list definition.
 * 
 * <p>ListOfContentSourceType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfContentSourceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ContentSource" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}DataListObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="NodePath" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;choice&gt;
 *                     &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlineID"/&gt;
 *                     &lt;element name="OtherID"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;simpleContent&gt;
 *                           &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                             &lt;attribute name="name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" /&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/simpleContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfContentSourceType", propOrder = {
    "contentSource"
})
public class ListOfContentSourceType {

    @XmlElement(name = "ContentSource", required = true)
    protected List<ListOfContentSourceType.ContentSource> contentSource;

    /**
     * Gets the value of the contentSource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contentSource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContentSource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListOfContentSourceType.ContentSource }
     * 
     * 
     */
    public List<ListOfContentSourceType.ContentSource> getContentSource() {
        if (contentSource == null) {
            contentSource = new ArrayList<ListOfContentSourceType.ContentSource>();
        }
        return this.contentSource;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}DataListObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="NodePath" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;choice&gt;
     *           &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlineID"/&gt;
     *           &lt;element name="OtherID"&gt;
     *             &lt;complexType&gt;
     *               &lt;simpleContent&gt;
     *                 &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                   &lt;attribute name="name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" /&gt;
     *                 &lt;/extension&gt;
     *               &lt;/simpleContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nodePath",
        "airlineID",
        "otherID"
    })
    public static class ContentSource
        extends DataListObjectBaseType
    {

        @XmlElement(name = "NodePath", required = true)
        protected String nodePath;
        @XmlElement(name = "AirlineID")
        protected AirlineID airlineID;
        @XmlElement(name = "OtherID")
        protected ListOfContentSourceType.ContentSource.OtherID otherID;

        /**
         * 获取nodePath属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNodePath() {
            return nodePath;
        }

        /**
         * 设置nodePath属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNodePath(String value) {
            this.nodePath = value;
        }

        /**
         * 获取airlineID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirlineID }
         *     
         */
        public AirlineID getAirlineID() {
            return airlineID;
        }

        /**
         * 设置airlineID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirlineID }
         *     
         */
        public void setAirlineID(AirlineID value) {
            this.airlineID = value;
        }

        /**
         * 获取otherID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ListOfContentSourceType.ContentSource.OtherID }
         *     
         */
        public ListOfContentSourceType.ContentSource.OtherID getOtherID() {
            return otherID;
        }

        /**
         * 设置otherID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfContentSourceType.ContentSource.OtherID }
         *     
         */
        public void setOtherID(ListOfContentSourceType.ContentSource.OtherID value) {
            this.otherID = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *       &lt;attribute name="name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class OtherID {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "name")
            protected String name;

            /**
             * 获取value属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * 获取name属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * 设置name属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

        }

    }

}
