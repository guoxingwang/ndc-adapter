//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * FLIGHT Metadata definition.
 * 
 * <p>FlightMetadataType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="FlightMetadataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}MetadataObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ActionCode" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="BindingKey" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightStatus" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Frequency" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}InstantPurchase" minOccurs="0"/&gt;
 *         &lt;element name="Meals" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Meal" type="{http://www.iata.org/IATA/EDIST}FlightMealsType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OnTimePerformance" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Remarks" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightMetadataType", propOrder = {
    "actionCode",
    "bindingKey",
    "flightStatus",
    "frequency",
    "instantPurchase",
    "meals",
    "onTimePerformance",
    "remarks"
})
@XmlSeeAlso({
    FlightMetadata.class
})
public class FlightMetadataType
    extends MetadataObjectBaseType
{

    @XmlElement(name = "ActionCode")
    protected String actionCode;
    @XmlElement(name = "BindingKey")
    protected String bindingKey;
    @XmlElement(name = "FlightStatus")
    protected FlightStatusType flightStatus;
    @XmlElement(name = "Frequency")
    protected FlightFrequencyType frequency;
    @XmlElement(name = "InstantPurchase")
    protected InstantPurchaseType instantPurchase;
    @XmlElement(name = "Meals")
    protected FlightMetadataType.Meals meals;
    @XmlElement(name = "OnTimePerformance")
    protected OnTimePerformance onTimePerformance;
    @XmlElement(name = "Remarks")
    protected RemarkType remarks;

    /**
     * 获取actionCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * 设置actionCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * 获取bindingKey属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBindingKey() {
        return bindingKey;
    }

    /**
     * 设置bindingKey属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBindingKey(String value) {
        this.bindingKey = value;
    }

    /**
     * 获取flightStatus属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FlightStatusType }
     *     
     */
    public FlightStatusType getFlightStatus() {
        return flightStatus;
    }

    /**
     * 设置flightStatus属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FlightStatusType }
     *     
     */
    public void setFlightStatus(FlightStatusType value) {
        this.flightStatus = value;
    }

    /**
     * 获取frequency属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FlightFrequencyType }
     *     
     */
    public FlightFrequencyType getFrequency() {
        return frequency;
    }

    /**
     * 设置frequency属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FlightFrequencyType }
     *     
     */
    public void setFrequency(FlightFrequencyType value) {
        this.frequency = value;
    }

    /**
     * 获取instantPurchase属性的值。
     * 
     * @return
     *     possible object is
     *     {@link InstantPurchaseType }
     *     
     */
    public InstantPurchaseType getInstantPurchase() {
        return instantPurchase;
    }

    /**
     * 设置instantPurchase属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link InstantPurchaseType }
     *     
     */
    public void setInstantPurchase(InstantPurchaseType value) {
        this.instantPurchase = value;
    }

    /**
     * 获取meals属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FlightMetadataType.Meals }
     *     
     */
    public FlightMetadataType.Meals getMeals() {
        return meals;
    }

    /**
     * 设置meals属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FlightMetadataType.Meals }
     *     
     */
    public void setMeals(FlightMetadataType.Meals value) {
        this.meals = value;
    }

    /**
     * 获取onTimePerformance属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OnTimePerformance }
     *     
     */
    public OnTimePerformance getOnTimePerformance() {
        return onTimePerformance;
    }

    /**
     * 设置onTimePerformance属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OnTimePerformance }
     *     
     */
    public void setOnTimePerformance(OnTimePerformance value) {
        this.onTimePerformance = value;
    }

    /**
     * 获取remarks属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RemarkType }
     *     
     */
    public RemarkType getRemarks() {
        return remarks;
    }

    /**
     * 设置remarks属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RemarkType }
     *     
     */
    public void setRemarks(RemarkType value) {
        this.remarks = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Meal" type="{http://www.iata.org/IATA/EDIST}FlightMealsType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "meal"
    })
    public static class Meals {

        @XmlElement(name = "Meal", required = true)
        protected List<FlightMealsType> meal;

        /**
         * Gets the value of the meal property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the meal property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMeal().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FlightMealsType }
         * 
         * 
         */
        public List<FlightMealsType> getMeal() {
            if (meal == null) {
                meal = new ArrayList<FlightMealsType>();
            }
            return this.meal;
        }

    }

}
