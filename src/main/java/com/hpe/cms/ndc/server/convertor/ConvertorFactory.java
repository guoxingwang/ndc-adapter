package com.hpe.cms.ndc.server.convertor;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:09 2019/7/27
 * @ Description：
 * @ Modified By：
 */
public class ConvertorFactory {

    /**
     * 创建相应接口，不同版本间的Convertor
     *
     * @param c
     * @param sourceVersion
     * @param targetVersion
     * @return
     */
    public static AbstractConvertor createConvertor(String beanName, String sourceVersion, String targetVersion) {
        switch (beanName) {
            case "com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRQ":
                return new AirShoppingRqConvertor(sourceVersion, targetVersion);
            case "com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRS":
                return new AirShoppingRqConvertor(sourceVersion, targetVersion);
            case "com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ":
                return new AirShoppingRqConvertor(sourceVersion, targetVersion);
            case "com.hpe.cms.ndc.server.v15_2.model.AirShoppingRS":
                return new AirShoppingRqConvertor(sourceVersion, targetVersion);
            case "other interfase": //todo 其它接口需要在此处扩展
                return null;
            default:
                break;
        }
        return null;
    }
}
