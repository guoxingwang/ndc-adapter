//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * The reissued flown flight coupon information.
 * 
 * <p>ReissuedFlownType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ReissuedFlownType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FlightCouponData" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CouponFlightSegmentType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="IntermediateStop" maxOccurs="9" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="AiportCode"&gt;
 *                             &lt;simpleType&gt;
 *                               &lt;restriction base="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"&gt;
 *                               &lt;/restriction&gt;
 *                             &lt;/simpleType&gt;
 *                           &lt;/attribute&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="StopoverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="InvoluntaryIndCode" type="{http://www.iata.org/IATA/EDIST}IATA_CodeType" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Number" type="{http://www.iata.org/IATA/EDIST}Numeric1to4" /&gt;
 *       &lt;attribute name="CouponItinerarySeqNbr" type="{http://www.iata.org/IATA/EDIST}Numeric0to99" /&gt;
 *       &lt;attribute name="FareBasisCode" type="{http://www.iata.org/IATA/EDIST}StringLength1to16" /&gt;
 *       &lt;attribute name="TicketDocumentNbr" type="{http://www.iata.org/IATA/EDIST}StringLength1to16" /&gt;
 *       &lt;attribute name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="WaiverCode" type="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to19" /&gt;
 *       &lt;attribute name="TicketDesignatorCode" type="{http://www.iata.org/IATA/EDIST}StringLength1to16" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReissuedFlownType", propOrder = {
    "flightCouponData"
})
public class ReissuedFlownType {

    @XmlElement(name = "FlightCouponData")
    protected ReissuedFlownType.FlightCouponData flightCouponData;
    @XmlAttribute(name = "Number")
    protected Integer number;
    @XmlAttribute(name = "CouponItinerarySeqNbr")
    protected Integer couponItinerarySeqNbr;
    @XmlAttribute(name = "FareBasisCode")
    protected String fareBasisCode;
    @XmlAttribute(name = "TicketDocumentNbr")
    protected String ticketDocumentNbr;
    @XmlAttribute(name = "DateOfIssue")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfIssue;
    @XmlAttribute(name = "WaiverCode")
    protected String waiverCode;
    @XmlAttribute(name = "TicketDesignatorCode")
    protected String ticketDesignatorCode;

    /**
     * 获取flightCouponData属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ReissuedFlownType.FlightCouponData }
     *     
     */
    public ReissuedFlownType.FlightCouponData getFlightCouponData() {
        return flightCouponData;
    }

    /**
     * 设置flightCouponData属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ReissuedFlownType.FlightCouponData }
     *     
     */
    public void setFlightCouponData(ReissuedFlownType.FlightCouponData value) {
        this.flightCouponData = value;
    }

    /**
     * 获取number属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * 设置number属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumber(Integer value) {
        this.number = value;
    }

    /**
     * 获取couponItinerarySeqNbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCouponItinerarySeqNbr() {
        return couponItinerarySeqNbr;
    }

    /**
     * 设置couponItinerarySeqNbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCouponItinerarySeqNbr(Integer value) {
        this.couponItinerarySeqNbr = value;
    }

    /**
     * 获取fareBasisCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFareBasisCode() {
        return fareBasisCode;
    }

    /**
     * 设置fareBasisCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFareBasisCode(String value) {
        this.fareBasisCode = value;
    }

    /**
     * 获取ticketDocumentNbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketDocumentNbr() {
        return ticketDocumentNbr;
    }

    /**
     * 设置ticketDocumentNbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketDocumentNbr(String value) {
        this.ticketDocumentNbr = value;
    }

    /**
     * 获取dateOfIssue属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfIssue() {
        return dateOfIssue;
    }

    /**
     * 设置dateOfIssue属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfIssue(XMLGregorianCalendar value) {
        this.dateOfIssue = value;
    }

    /**
     * 获取waiverCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaiverCode() {
        return waiverCode;
    }

    /**
     * 设置waiverCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaiverCode(String value) {
        this.waiverCode = value;
    }

    /**
     * 获取ticketDesignatorCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketDesignatorCode() {
        return ticketDesignatorCode;
    }

    /**
     * 设置ticketDesignatorCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketDesignatorCode(String value) {
        this.ticketDesignatorCode = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CouponFlightSegmentType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="IntermediateStop" maxOccurs="9" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="AiportCode"&gt;
     *                   &lt;simpleType&gt;
     *                     &lt;restriction base="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"&gt;
     *                     &lt;/restriction&gt;
     *                   &lt;/simpleType&gt;
     *                 &lt;/attribute&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="StopoverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="InvoluntaryIndCode" type="{http://www.iata.org/IATA/EDIST}IATA_CodeType" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "intermediateStop"
    })
    public static class FlightCouponData
        extends CouponFlightSegmentType
    {

        @XmlElement(name = "IntermediateStop")
        protected List<ReissuedFlownType.FlightCouponData.IntermediateStop> intermediateStop;
        @XmlAttribute(name = "StopoverInd")
        protected Boolean stopoverInd;
        @XmlAttribute(name = "InvoluntaryIndCode")
        protected String involuntaryIndCode;

        /**
         * Gets the value of the intermediateStop property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the intermediateStop property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getIntermediateStop().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReissuedFlownType.FlightCouponData.IntermediateStop }
         * 
         * 
         */
        public List<ReissuedFlownType.FlightCouponData.IntermediateStop> getIntermediateStop() {
            if (intermediateStop == null) {
                intermediateStop = new ArrayList<ReissuedFlownType.FlightCouponData.IntermediateStop>();
            }
            return this.intermediateStop;
        }

        /**
         * 获取stopoverInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isStopoverInd() {
            return stopoverInd;
        }

        /**
         * 设置stopoverInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setStopoverInd(Boolean value) {
            this.stopoverInd = value;
        }

        /**
         * 获取involuntaryIndCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvoluntaryIndCode() {
            return involuntaryIndCode;
        }

        /**
         * 设置involuntaryIndCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvoluntaryIndCode(String value) {
            this.involuntaryIndCode = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="AiportCode"&gt;
         *         &lt;simpleType&gt;
         *           &lt;restriction base="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"&gt;
         *           &lt;/restriction&gt;
         *         &lt;/simpleType&gt;
         *       &lt;/attribute&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class IntermediateStop {

            @XmlAttribute(name = "AiportCode")
            protected String aiportCode;

            /**
             * 获取aiportCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAiportCode() {
                return aiportCode;
            }

            /**
             * 设置aiportCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAiportCode(String value) {
                this.aiportCode = value;
            }

        }

    }

}
