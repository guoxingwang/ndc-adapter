//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Policy Notification and Text, e.g. for sensitive data notification and/or handling.
 * 
 * <p>PolicyDefaultType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PolicyDefaultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Policy" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="DataPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *                   &lt;element name="PolicyID" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" minOccurs="0"/&gt;
 *                   &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="Description" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType"/&gt;
 *                     &lt;element ref="{http://www.iata.org/IATA/EDIST}PolicyLink"/&gt;
 *                   &lt;/choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}NodePath" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="refs" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyDefaultType", propOrder = {
    "policy"
})
public class PolicyDefaultType {

    @XmlElement(name = "Policy", required = true)
    protected List<PolicyDefaultType.Policy> policy;

    /**
     * Gets the value of the policy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyDefaultType.Policy }
     * 
     * 
     */
    public List<PolicyDefaultType.Policy> getPolicy() {
        if (policy == null) {
            policy = new ArrayList<PolicyDefaultType.Policy>();
        }
        return this.policy;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="DataPolicyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
     *         &lt;element name="PolicyID" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" minOccurs="0"/&gt;
     *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
     *         &lt;choice&gt;
     *           &lt;element name="Description" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType"/&gt;
     *           &lt;element ref="{http://www.iata.org/IATA/EDIST}PolicyLink"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}NodePath" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="refs" type="{http://www.iata.org/IATA/EDIST}InstanceClassRefSimpleType" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dataPolicyInd",
        "policyID",
        "type",
        "description",
        "policyLink",
        "nodePath"
    })
    public static class Policy {

        @XmlElement(name = "DataPolicyInd")
        protected Boolean dataPolicyInd;
        @XmlElement(name = "PolicyID")
        protected String policyID;
        @XmlElement(name = "Type")
        protected Object type;
        @XmlElement(name = "Description")
        protected String description;
        @XmlElement(name = "PolicyLink")
        @XmlSchemaType(name = "anyURI")
        protected String policyLink;
        @XmlElement(name = "NodePath")
        protected NodePathType nodePath;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * 获取dataPolicyInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDataPolicyInd() {
            return dataPolicyInd;
        }

        /**
         * 设置dataPolicyInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDataPolicyInd(Boolean value) {
            this.dataPolicyInd = value;
        }

        /**
         * 获取policyID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolicyID() {
            return policyID;
        }

        /**
         * 设置policyID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolicyID(String value) {
            this.policyID = value;
        }

        /**
         * 获取type属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getType() {
            return type;
        }

        /**
         * 设置type属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setType(Object value) {
            this.type = value;
        }

        /**
         * 获取description属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * 设置description属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * 获取policyLink属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolicyLink() {
            return policyLink;
        }

        /**
         * 设置policyLink属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolicyLink(String value) {
            this.policyLink = value;
        }

        /**
         * Message Property Path information for this policy-associated element.
         * 
         * @return
         *     possible object is
         *     {@link NodePathType }
         *     
         */
        public NodePathType getNodePath() {
            return nodePath;
        }

        /**
         * 设置nodePath属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link NodePathType }
         *     
         */
        public void setNodePath(NodePathType value) {
            this.nodePath = value;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }

}
