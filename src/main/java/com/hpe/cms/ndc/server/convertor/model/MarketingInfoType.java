//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Marketing Messages in a response message.
 * 
 * <p>MarketingInfoType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="MarketingInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Message" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}DescriptionType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}MarketMsgAssocType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingInfoType", propOrder = {
    "message"
})
public class MarketingInfoType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "Message", required = true)
    protected List<MarketingInfoType.Message> message;

    /**
     * Gets the value of the message property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the message property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MarketingInfoType.Message }
     * 
     * 
     */
    public List<MarketingInfoType.Message> getMessage() {
        if (message == null) {
            message = new ArrayList<MarketingInfoType.Message>();
        }
        return this.message;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}DescriptionType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}MarketMsgAssocType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "associations"
    })
    public static class Message
        extends DescriptionType
    {

        @XmlElement(name = "Associations")
        protected MarketMsgAssocType associations;

        /**
         * 获取associations属性的值。
         * 
         * @return
         *     possible object is
         *     {@link MarketMsgAssocType }
         *     
         */
        public MarketMsgAssocType getAssociations() {
            return associations;
        }

        /**
         * 设置associations属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link MarketMsgAssocType }
         *     
         */
        public void setAssociations(MarketMsgAssocType value) {
            this.associations = value;
        }

    }

}
