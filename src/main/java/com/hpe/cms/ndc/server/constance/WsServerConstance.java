package com.hpe.cms.ndc.server.constance;

import com.hpe.cms.ndc.server.config.NDC;
import com.hpe.cms.ndc.server.util.ApplicationContextUtil;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 12:23 2019/7/6
 * @ Description：
 * @ Modified By：
 */
public class WsServerConstance {

    public static final String REQUEST_SUFFIX = "RQ";
    public static final String RESPONSE_SUFFIX = "RS";
    public static final String V15_2_NAMESPACE = "http://www.iata.org/IATA/EDIST";
    public static final String V16_1_NAMESPACE = "http://www.iata.org/IATA/EDIST";
    public static final String V16_2_NAMESPACE = "http://www.iata.org/IATA/EDIST";
    public static final String V17_1_NAMESPACE = "http://www.iata.org/IATA/EDIST";
    public static final String V17_2_NAMESPACE = "http://www.iata.org/IATA/EDIST";
    public static final String V18_1_NAMESPACE = "http://www.iata.org/IATA/2015/00/2018.1";
    public static final String V18_2_NAMESPACE = "http://www.iata.org/IATA/2015/00/2018.2";
    public static final String V15_2 = "v15_2";
    public static final String V16_1 = "v16_1";
    public static final String V16_2 = "v16_2";
    public static final String V17_1 = "v17_1";
    public static final String V17_2 = "v17_2";
    public static final String V18_1 = "v18_1";
    public static final String V18_2 = "v18_2";
    public static final String WSDL_PATH = "/ndc-adapter/ws/";
    public static NDC ndc = ApplicationContextUtil.getApplicationContext().getBean(NDC.class);
    public static String VERSION = ndc.getVersion();
    public static final String LOCATION = WSDL_PATH + VERSION;
    public static final String RESOURCE_PATH = "static/ndc/" + VERSION + "_schemas";
    public static String WSDL_NAME = ndc.getWsdlName();


    public static String getLocalPartByInterfaceNameAndVersion(String interfaceName, String version){
        return interfaceName + "_" + version;
    }

    public static class BaseXsd {

        public static String getLocationUri(String name) {
            return LOCATION + "/" + name;
        }

        public static String getSchemaRqPath(String name) {
            return RESOURCE_PATH + "/" + name + "RQ.xsd";
        }

        public static String getResourcePath(String version) {
            return "static/ndc/" + version + "_schemas";
        }
    }
}
