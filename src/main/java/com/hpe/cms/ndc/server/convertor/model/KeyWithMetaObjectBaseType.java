//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * A base type for a Keyed Object with Metadata Reference..
 * 
 * <p>KeyWithMetaObjectBaseType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="KeyWithMetaObjectBaseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectKeyMetaAttrGroup"/&gt;
 *       &lt;attribute name="DisplayAllInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AllExemptInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PricingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="E_TicketInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TicketlessInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AssociateInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TaxOnEMD_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="StopOverInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TaxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="OtherChargeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PoolingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="SpecialItemsInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TaxesInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NetFareInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ReissueOnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AutoExchangeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AlertInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AuthenticationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MarketingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="MultiPayFormInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NoticeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PartialInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="WarningInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FailedPaymentInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PartialPaymentInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="VerificationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PriceVarianceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="InvGuaranteedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="WaitlistInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="BundleInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="InvGuarRequestedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ExitRowInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PremiumInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="UpperDeckInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TaxIncludedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FeeIncludedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="LeadPricingInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="ShortDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="Time" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType" /&gt;
 *       &lt;attribute name="MetadataToken" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyWithMetaObjectBaseType")
@XmlSeeAlso({
    AddressCoreType.class,
    AirPassCoreType.class,
    com.hpe.cms.ndc.server.convertor.model.CarryOnAllowanceType.CarryOnBag.class,
    com.hpe.cms.ndc.server.convertor.model.CheckedBagAllowanceType.CheckedBag.class,
    PhoneType.class,
    DirectionsType.class,
    FQTVProgramCoreType.class,
    FQTVAllianceType.class,
    TravelerFQTVType.class,
    PaymentCardType.class,
    com.hpe.cms.ndc.server.convertor.model.PaymentForms.PaymentForm.class,
    AwardRedemptionType.class,
    TaxExemptionType.class,
    PaymentTimeLimitType.class,
    PriceGuaranteeTimeLimitType.class,
    com.hpe.cms.ndc.server.convertor.model.PassengerSummaryType.FQTVs.class,
    ProcessingResultType.class,
    InvDiscrepencyAlertType.class,
    com.hpe.cms.ndc.server.convertor.model.Warnings.Warning.class,
    InvGuaranteeNoticeType.class,
    FareType.class,
    FareDetailType.class,
    FareRulesType.class,
    FareComponentType.class,
    com.hpe.cms.ndc.server.convertor.model.FareRulesRemarks.FareRulesRemark.class,
    FareFilingType.class,
    FlightArrivalType.class,
    AircraftCoreType.class,
    AircraftTailNmbrQueryType.class,
    AircraftFeaturesQueryType.class,
    ItineraryType.class,
    OfferMatchType.class,
    AllOffersSnapshotType.class,
    AirlineOffersSnapshotType.class,
    OrderKeysType.class,
    BaggageItemType.class,
    FlightItemType.class,
    OtherItemType.class,
    PaymentCardMethodType.class,
    ETFareInfoType.class,
    AcceptedPaymentFormType.class,
    ExitPositionType.class,
    WingPositionType.class,
    SeatComponentType.class,
    ServicePriceType.class,
    ServiceFulfillmentType.class,
    FltDepartQualifiedQueryType.class,
    FltArriveQualifiedQueryType.class,
    DiscountProgramType.class,
    StayPeriodDateRangeType.class,
    StayPeriodSeasonType.class,
    BaggagePricingQualifierType.class,
    CardQualifierType.class,
    ServiceQualifierPriceType.Fulfillment.class,
    ServiceQualifierPriceType.class,
    FareQualifierType.class,
    AircraftCodeQueryType.class,
    com.hpe.cms.ndc.server.convertor.model.BaggageChargesRQ.TicketIdentification.class,
    OfferTimeLimitSetType.class,
    com.hpe.cms.ndc.server.convertor.model.ItinReshopRQ.Query.Reshop.Actions.OfferItemSets.OfferItemSet.class,
    com.hpe.cms.ndc.server.convertor.model.PaymentMethodType.Method.class,
    com.hpe.cms.ndc.server.convertor.model.OrderListRQ.Query.Filters.Passengers.FOID.class,
    com.hpe.cms.ndc.server.convertor.model.OrderListRQ.Query.Filters.Payments.Method.class,
    com.hpe.cms.ndc.server.convertor.model.OrderRetrieveRQ.Query.Filters.Passengers.PassengerIDInfo.FOID.class,
    com.hpe.cms.ndc.server.convertor.model.OrderRetrieveRQ.Query.Filters.Payments.Method.class,
    com.hpe.cms.ndc.server.convertor.model.QualifiersType.Qualifier.class,
    ServiceCoreType.class,
    BagDisclosureType.class,
    CountryQueryType.class,
    DiscountType.class,
    RemarkType.class,
    PenaltyType.Details.Detail.class,
    PenaltyType.class,
    RuleSetType.class,
    SpecialType.class,
    LocationProximityType.class,
    TravelerFOIDType.class,
    MarketingInfoType.class,
    com.hpe.cms.ndc.server.convertor.model.ProcessingErrorType.Error.class,
    OrderFareDetailType.class,
    FlightDetailType.class,
    FlightOnlyType.class,
    PricedFlightOfferCoreType.class,
    com.hpe.cms.ndc.server.convertor.model.OfferIncentiveType.Incentive.class,
    OfferTermsType.class,
    com.hpe.cms.ndc.server.convertor.model.OfferItemTimeLimitSetType.TimeLimit.class,
    OrderCoreType.class,
    OrderCoreChangeType.class,
    com.hpe.cms.ndc.server.convertor.model.OrderItemCoreType.OrderItem.class,
    com.hpe.cms.ndc.server.convertor.model.OrderItemRepriceType.OrderItem.class,
    FlightItemOfferType.class,
    OrderOfferItemType.class,
    PaymentProcessType.class,
    FlightPriceType.class,
    OrderProcessResultType.class,
    OrderPaymentFormType.class,
    com.hpe.cms.ndc.server.convertor.model.OrderPaymentMethodType.Method.class,
    PriceClassType.class,
    SeatCoreType.class,
    SeatMapMessageType.class,
    ServiceEncodingType.class,
    ServiceDescriptionType.class,
    ServiceFilterType.class,
    SocialMediaQualifierType.class,
    FareBasisCodeType.class,
    TaxCoreType.class,
    CouponFlightSegmentType.class,
    OfferItemCoreType.class,
    com.hpe.cms.ndc.server.convertor.model.OrdChangeNoticesType.PriceVarianceRules.PriceVarianceRule.class,
    com.hpe.cms.ndc.server.convertor.model.OrdCreateNoticesType.PriceVarianceRules.PriceVarianceRule.class,
    PricedFlightOffer.class,
    PromotionType.class,
    SeatItemType.class,
    OrderInstructionType.class,
    PriceGuarTimeLimitType.class,
    CodesetType.class,
    FlightDepartureType.class,
    TicketingTimeLimitType.class,
    NamingTimeLimitType.class,
    DepositTimeLimitType.class,
    PositionType.class,
    LoyaltyRedemption.class,
    DisclosureType.class,
    DescriptionType.class,
    BookingReferenceType.class
})
public class KeyWithMetaObjectBaseType {

    @XmlAttribute(name = "DisplayAllInd")
    protected Boolean displayAllInd;
    @XmlAttribute(name = "AllExemptInd")
    protected Boolean allExemptInd;
    @XmlAttribute(name = "PricingInd")
    protected Boolean pricingInd;
    @XmlAttribute(name = "ConnectionInd")
    protected Boolean connectionInd;
    @XmlAttribute(name = "E_TicketInd")
    protected Boolean eTicketInd;
    @XmlAttribute(name = "TicketlessInd")
    protected Boolean ticketlessInd;
    @XmlAttribute(name = "AssociateInd")
    protected Boolean associateInd;
    @XmlAttribute(name = "TaxOnEMD_Ind")
    protected Boolean taxOnEMDInd;
    @XmlAttribute(name = "StopOverInd")
    protected Boolean stopOverInd;
    @XmlAttribute(name = "TaxInd")
    protected Boolean taxInd;
    @XmlAttribute(name = "OtherChargeInd")
    protected Boolean otherChargeInd;
    @XmlAttribute(name = "PoolingInd")
    protected Boolean poolingInd;
    @XmlAttribute(name = "SpecialItemsInd")
    protected Boolean specialItemsInd;
    @XmlAttribute(name = "TaxesInd")
    protected Boolean taxesInd;
    @XmlAttribute(name = "NetFareInd")
    protected Boolean netFareInd;
    @XmlAttribute(name = "ReissueOnlyInd")
    protected Boolean reissueOnlyInd;
    @XmlAttribute(name = "AutoExchangeInd")
    protected Boolean autoExchangeInd;
    @XmlAttribute(name = "AlertInd")
    protected Boolean alertInd;
    @XmlAttribute(name = "AuthenticationInd")
    protected Boolean authenticationInd;
    @XmlAttribute(name = "MarketingInd")
    protected Boolean marketingInd;
    @XmlAttribute(name = "MultiPayFormInd")
    protected Boolean multiPayFormInd;
    @XmlAttribute(name = "NoticeInd")
    protected Boolean noticeInd;
    @XmlAttribute(name = "PartialInd")
    protected Boolean partialInd;
    @XmlAttribute(name = "WarningInd")
    protected Boolean warningInd;
    @XmlAttribute(name = "FailedPaymentInd")
    protected Boolean failedPaymentInd;
    @XmlAttribute(name = "PartialPaymentInd")
    protected Boolean partialPaymentInd;
    @XmlAttribute(name = "VerificationInd")
    protected Boolean verificationInd;
    @XmlAttribute(name = "PriceVarianceInd")
    protected Boolean priceVarianceInd;
    @XmlAttribute(name = "InvGuaranteedInd")
    protected Boolean invGuaranteedInd;
    @XmlAttribute(name = "WaitlistInd")
    protected Boolean waitlistInd;
    @XmlAttribute(name = "BundleInd")
    protected Boolean bundleInd;
    @XmlAttribute(name = "InvGuarRequestedInd")
    protected Boolean invGuarRequestedInd;
    @XmlAttribute(name = "ExitRowInd")
    protected Boolean exitRowInd;
    @XmlAttribute(name = "PremiumInd")
    protected Boolean premiumInd;
    @XmlAttribute(name = "UpperDeckInd")
    protected Boolean upperDeckInd;
    @XmlAttribute(name = "TaxIncludedInd")
    protected Boolean taxIncludedInd;
    @XmlAttribute(name = "FeeIncludedInd")
    protected Boolean feeIncludedInd;
    @XmlAttribute(name = "LeadPricingInd")
    protected Boolean leadPricingInd;
    @XmlAttribute(name = "DateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTime;
    @XmlAttribute(name = "ShortDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar shortDate;
    @XmlAttribute(name = "Timestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlAttribute(name = "Time")
    protected String time;
    @XmlAttribute(name = "MetadataToken")
    protected String metadataToken;
    @XmlAttribute(name = "refs")
    @XmlIDREF
    protected List<Object> refs;
    @XmlAttribute(name = "ObjectKey")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String objectKey;

    /**
     * 获取displayAllInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisplayAllInd() {
        return displayAllInd;
    }

    /**
     * 设置displayAllInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayAllInd(Boolean value) {
        this.displayAllInd = value;
    }

    /**
     * 获取allExemptInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllExemptInd() {
        return allExemptInd;
    }

    /**
     * 设置allExemptInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllExemptInd(Boolean value) {
        this.allExemptInd = value;
    }

    /**
     * 获取pricingInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPricingInd() {
        return pricingInd;
    }

    /**
     * 设置pricingInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPricingInd(Boolean value) {
        this.pricingInd = value;
    }

    /**
     * 获取connectionInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConnectionInd() {
        return connectionInd;
    }

    /**
     * 设置connectionInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConnectionInd(Boolean value) {
        this.connectionInd = value;
    }

    /**
     * 获取eTicketInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isETicketInd() {
        return eTicketInd;
    }

    /**
     * 设置eTicketInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setETicketInd(Boolean value) {
        this.eTicketInd = value;
    }

    /**
     * 获取ticketlessInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTicketlessInd() {
        return ticketlessInd;
    }

    /**
     * 设置ticketlessInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTicketlessInd(Boolean value) {
        this.ticketlessInd = value;
    }

    /**
     * 获取associateInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAssociateInd() {
        return associateInd;
    }

    /**
     * 设置associateInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAssociateInd(Boolean value) {
        this.associateInd = value;
    }

    /**
     * 获取taxOnEMDInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxOnEMDInd() {
        return taxOnEMDInd;
    }

    /**
     * 设置taxOnEMDInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxOnEMDInd(Boolean value) {
        this.taxOnEMDInd = value;
    }

    /**
     * 获取stopOverInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStopOverInd() {
        return stopOverInd;
    }

    /**
     * 设置stopOverInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStopOverInd(Boolean value) {
        this.stopOverInd = value;
    }

    /**
     * 获取taxInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxInd() {
        return taxInd;
    }

    /**
     * 设置taxInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxInd(Boolean value) {
        this.taxInd = value;
    }

    /**
     * 获取otherChargeInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOtherChargeInd() {
        return otherChargeInd;
    }

    /**
     * 设置otherChargeInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOtherChargeInd(Boolean value) {
        this.otherChargeInd = value;
    }

    /**
     * 获取poolingInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPoolingInd() {
        return poolingInd;
    }

    /**
     * 设置poolingInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPoolingInd(Boolean value) {
        this.poolingInd = value;
    }

    /**
     * 获取specialItemsInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSpecialItemsInd() {
        return specialItemsInd;
    }

    /**
     * 设置specialItemsInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSpecialItemsInd(Boolean value) {
        this.specialItemsInd = value;
    }

    /**
     * 获取taxesInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxesInd() {
        return taxesInd;
    }

    /**
     * 设置taxesInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxesInd(Boolean value) {
        this.taxesInd = value;
    }

    /**
     * 获取netFareInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNetFareInd() {
        return netFareInd;
    }

    /**
     * 设置netFareInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNetFareInd(Boolean value) {
        this.netFareInd = value;
    }

    /**
     * 获取reissueOnlyInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReissueOnlyInd() {
        return reissueOnlyInd;
    }

    /**
     * 设置reissueOnlyInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReissueOnlyInd(Boolean value) {
        this.reissueOnlyInd = value;
    }

    /**
     * 获取autoExchangeInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoExchangeInd() {
        return autoExchangeInd;
    }

    /**
     * 设置autoExchangeInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoExchangeInd(Boolean value) {
        this.autoExchangeInd = value;
    }

    /**
     * 获取alertInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAlertInd() {
        return alertInd;
    }

    /**
     * 设置alertInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAlertInd(Boolean value) {
        this.alertInd = value;
    }

    /**
     * 获取authenticationInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAuthenticationInd() {
        return authenticationInd;
    }

    /**
     * 设置authenticationInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAuthenticationInd(Boolean value) {
        this.authenticationInd = value;
    }

    /**
     * 获取marketingInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingInd() {
        return marketingInd;
    }

    /**
     * 设置marketingInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingInd(Boolean value) {
        this.marketingInd = value;
    }

    /**
     * 获取multiPayFormInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMultiPayFormInd() {
        return multiPayFormInd;
    }

    /**
     * 设置multiPayFormInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMultiPayFormInd(Boolean value) {
        this.multiPayFormInd = value;
    }

    /**
     * 获取noticeInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoticeInd() {
        return noticeInd;
    }

    /**
     * 设置noticeInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoticeInd(Boolean value) {
        this.noticeInd = value;
    }

    /**
     * 获取partialInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPartialInd() {
        return partialInd;
    }

    /**
     * 设置partialInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartialInd(Boolean value) {
        this.partialInd = value;
    }

    /**
     * 获取warningInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWarningInd() {
        return warningInd;
    }

    /**
     * 设置warningInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWarningInd(Boolean value) {
        this.warningInd = value;
    }

    /**
     * 获取failedPaymentInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFailedPaymentInd() {
        return failedPaymentInd;
    }

    /**
     * 设置failedPaymentInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFailedPaymentInd(Boolean value) {
        this.failedPaymentInd = value;
    }

    /**
     * 获取partialPaymentInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPartialPaymentInd() {
        return partialPaymentInd;
    }

    /**
     * 设置partialPaymentInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartialPaymentInd(Boolean value) {
        this.partialPaymentInd = value;
    }

    /**
     * 获取verificationInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVerificationInd() {
        return verificationInd;
    }

    /**
     * 设置verificationInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVerificationInd(Boolean value) {
        this.verificationInd = value;
    }

    /**
     * 获取priceVarianceInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPriceVarianceInd() {
        return priceVarianceInd;
    }

    /**
     * 设置priceVarianceInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPriceVarianceInd(Boolean value) {
        this.priceVarianceInd = value;
    }

    /**
     * 获取invGuaranteedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInvGuaranteedInd() {
        return invGuaranteedInd;
    }

    /**
     * 设置invGuaranteedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInvGuaranteedInd(Boolean value) {
        this.invGuaranteedInd = value;
    }

    /**
     * 获取waitlistInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWaitlistInd() {
        return waitlistInd;
    }

    /**
     * 设置waitlistInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWaitlistInd(Boolean value) {
        this.waitlistInd = value;
    }

    /**
     * 获取bundleInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBundleInd() {
        return bundleInd;
    }

    /**
     * 设置bundleInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBundleInd(Boolean value) {
        this.bundleInd = value;
    }

    /**
     * 获取invGuarRequestedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInvGuarRequestedInd() {
        return invGuarRequestedInd;
    }

    /**
     * 设置invGuarRequestedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInvGuarRequestedInd(Boolean value) {
        this.invGuarRequestedInd = value;
    }

    /**
     * 获取exitRowInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExitRowInd() {
        return exitRowInd;
    }

    /**
     * 设置exitRowInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExitRowInd(Boolean value) {
        this.exitRowInd = value;
    }

    /**
     * 获取premiumInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPremiumInd() {
        return premiumInd;
    }

    /**
     * 设置premiumInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPremiumInd(Boolean value) {
        this.premiumInd = value;
    }

    /**
     * 获取upperDeckInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUpperDeckInd() {
        return upperDeckInd;
    }

    /**
     * 设置upperDeckInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUpperDeckInd(Boolean value) {
        this.upperDeckInd = value;
    }

    /**
     * 获取taxIncludedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxIncludedInd() {
        return taxIncludedInd;
    }

    /**
     * 设置taxIncludedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxIncludedInd(Boolean value) {
        this.taxIncludedInd = value;
    }

    /**
     * 获取feeIncludedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFeeIncludedInd() {
        return feeIncludedInd;
    }

    /**
     * 设置feeIncludedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFeeIncludedInd(Boolean value) {
        this.feeIncludedInd = value;
    }

    /**
     * 获取leadPricingInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLeadPricingInd() {
        return leadPricingInd;
    }

    /**
     * 设置leadPricingInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLeadPricingInd(Boolean value) {
        this.leadPricingInd = value;
    }

    /**
     * 获取dateTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTime() {
        return dateTime;
    }

    /**
     * 设置dateTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTime(XMLGregorianCalendar value) {
        this.dateTime = value;
    }

    /**
     * 获取shortDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getShortDate() {
        return shortDate;
    }

    /**
     * 设置shortDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setShortDate(XMLGregorianCalendar value) {
        this.shortDate = value;
    }

    /**
     * 获取timestamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * 设置timestamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * 获取time属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTime() {
        return time;
    }

    /**
     * 设置time属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTime(String value) {
        this.time = value;
    }

    /**
     * 获取metadataToken属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetadataToken() {
        return metadataToken;
    }

    /**
     * 设置metadataToken属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetadataToken(String value) {
        this.metadataToken = value;
    }

    /**
     * Gets the value of the refs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getRefs() {
        if (refs == null) {
            refs = new ArrayList<Object>();
        }
        return this.refs;
    }

    /**
     * 获取objectKey属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectKey() {
        return objectKey;
    }

    /**
     * 设置objectKey属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectKey(String value) {
        this.objectKey = value;
    }

}
