//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Inventory Guarantee Notice definition.
 * 
 * <p>InvGuaranteeNoticeType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="InvGuaranteeNoticeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="Query"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="GuaranteeRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Results"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
 *                             &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}MultiAssociationType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="NoGuaranteeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvGuaranteeNoticeType", propOrder = {
    "query",
    "results"
})
public class InvGuaranteeNoticeType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "Query")
    protected InvGuaranteeNoticeType.Query query;
    @XmlElement(name = "Results")
    protected InvGuaranteeNoticeType.Results results;

    /**
     * 获取query属性的值。
     * 
     * @return
     *     possible object is
     *     {@link InvGuaranteeNoticeType.Query }
     *     
     */
    public InvGuaranteeNoticeType.Query getQuery() {
        return query;
    }

    /**
     * 设置query属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link InvGuaranteeNoticeType.Query }
     *     
     */
    public void setQuery(InvGuaranteeNoticeType.Query value) {
        this.query = value;
    }

    /**
     * 获取results属性的值。
     * 
     * @return
     *     possible object is
     *     {@link InvGuaranteeNoticeType.Results }
     *     
     */
    public InvGuaranteeNoticeType.Results getResults() {
        return results;
    }

    /**
     * 设置results属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link InvGuaranteeNoticeType.Results }
     *     
     */
    public void setResults(InvGuaranteeNoticeType.Results value) {
        this.results = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="GuaranteeRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Query {

        @XmlAttribute(name = "GuaranteeRequestInd")
        protected Boolean guaranteeRequestInd;

        /**
         * 获取guaranteeRequestInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isGuaranteeRequestInd() {
            return guaranteeRequestInd;
        }

        /**
         * 设置guaranteeRequestInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setGuaranteeRequestInd(Boolean value) {
            this.guaranteeRequestInd = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
     *                   &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}MultiAssociationType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="NoGuaranteeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "inventoryGuarantee"
    })
    public static class Results {

        @XmlElement(name = "InventoryGuarantee")
        protected InvGuaranteeNoticeType.Results.InventoryGuarantee inventoryGuarantee;
        @XmlAttribute(name = "NoGuaranteeInd")
        protected Boolean noGuaranteeInd;

        /**
         * 获取inventoryGuarantee属性的值。
         * 
         * @return
         *     possible object is
         *     {@link InvGuaranteeNoticeType.Results.InventoryGuarantee }
         *     
         */
        public InvGuaranteeNoticeType.Results.InventoryGuarantee getInventoryGuarantee() {
            return inventoryGuarantee;
        }

        /**
         * 设置inventoryGuarantee属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link InvGuaranteeNoticeType.Results.InventoryGuarantee }
         *     
         */
        public void setInventoryGuarantee(InvGuaranteeNoticeType.Results.InventoryGuarantee value) {
            this.inventoryGuarantee = value;
        }

        /**
         * 获取noGuaranteeInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isNoGuaranteeInd() {
            return noGuaranteeInd;
        }

        /**
         * 设置noGuaranteeInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setNoGuaranteeInd(Boolean value) {
            this.noGuaranteeInd = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
         *         &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}MultiAssociationType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "invGuaranteeID",
            "invGuaranteeTimeLimits",
            "associations"
        })
        public static class InventoryGuarantee {

            @XmlElement(name = "InvGuaranteeID")
            protected String invGuaranteeID;
            @XmlElement(name = "InvGuaranteeTimeLimits", required = true)
            protected InvGuaranteeTimeLimits invGuaranteeTimeLimits;
            @XmlElement(name = "Associations")
            protected MultiAssociationType associations;

            /**
             * 获取invGuaranteeID属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvGuaranteeID() {
                return invGuaranteeID;
            }

            /**
             * 设置invGuaranteeID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvGuaranteeID(String value) {
                this.invGuaranteeID = value;
            }

            /**
             * 获取invGuaranteeTimeLimits属性的值。
             * 
             * @return
             *     possible object is
             *     {@link InvGuaranteeTimeLimits }
             *     
             */
            public InvGuaranteeTimeLimits getInvGuaranteeTimeLimits() {
                return invGuaranteeTimeLimits;
            }

            /**
             * 设置invGuaranteeTimeLimits属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link InvGuaranteeTimeLimits }
             *     
             */
            public void setInvGuaranteeTimeLimits(InvGuaranteeTimeLimits value) {
                this.invGuaranteeTimeLimits = value;
            }

            /**
             * 获取associations属性的值。
             * 
             * @return
             *     possible object is
             *     {@link MultiAssociationType }
             *     
             */
            public MultiAssociationType getAssociations() {
                return associations;
            }

            /**
             * 设置associations属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link MultiAssociationType }
             *     
             */
            public void setAssociations(MultiAssociationType value) {
                this.associations = value;
            }

        }

    }

}
