//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * SECURE PAYMENT URL definition.
 * A data type for Airline and 3DS Merchant URL(s).
 * 
 * <p>SecurePaymentUrlType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="SecurePaymentUrlType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ACS_URL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *         &lt;element name="FailURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *         &lt;element name="MerchantURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *         &lt;element name="TermURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecurePaymentUrlType", propOrder = {
    "acsurl",
    "failURL",
    "merchantURL",
    "termURL"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.SecurePaymentAlertType.URLs.class
})
public class SecurePaymentUrlType {

    @XmlElement(name = "ACS_URL")
    @XmlSchemaType(name = "anyURI")
    protected String acsurl;
    @XmlElement(name = "FailURL")
    @XmlSchemaType(name = "anyURI")
    protected String failURL;
    @XmlElement(name = "MerchantURL")
    @XmlSchemaType(name = "anyURI")
    protected String merchantURL;
    @XmlElement(name = "TermURL")
    @XmlSchemaType(name = "anyURI")
    protected String termURL;

    /**
     * 获取acsurl属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACSURL() {
        return acsurl;
    }

    /**
     * 设置acsurl属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACSURL(String value) {
        this.acsurl = value;
    }

    /**
     * 获取failURL属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFailURL() {
        return failURL;
    }

    /**
     * 设置failURL属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFailURL(String value) {
        this.failURL = value;
    }

    /**
     * 获取merchantURL属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantURL() {
        return merchantURL;
    }

    /**
     * 设置merchantURL属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantURL(String value) {
        this.merchantURL = value;
    }

    /**
     * 获取termURL属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermURL() {
        return termURL;
    }

    /**
     * 设置termURL属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermURL(String value) {
        this.termURL = value;
    }

}
