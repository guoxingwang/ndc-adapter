//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * LEAD PRICING CALENDAR Pricing definition.
 * 
 * Price information for alternate dates.
 * 
 * Notes:
 *  1. This is based on lead pricing specified in the AirShoppingRQ/ AttributeQuery/Flight node Arrival and/ or Departure elements.
 * 
 *  2. This capability may not be supported by all NDC-enabled systems or may be supported by bilateral agreement only.The technical implementation of this capability is not dictated by this specification.
 * 
 *  3. If implemented, the client application will typically send new a new shopping request if any of these window shopping dates are selected.
 * 
 * <p>OfferPriceLeadType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="OfferPriceLeadType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RequestedDate"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferPriceLeadDetailType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}PricedFlightOfferAssocType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FareDetail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="OfferItemID" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferPriceLeadType", propOrder = {
    "requestedDate",
    "fareDetail"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.PricedFlightOfferType.OfferPrice.class,
    com.hpe.cms.ndc.server.convertor.model.PricedFlightOfferCoreType.OfferPrice.class
})
public class OfferPriceLeadType
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "RequestedDate", required = true)
    protected OfferPriceLeadType.RequestedDate requestedDate;
    @XmlElement(name = "FareDetail")
    protected FareDetailType fareDetail;
    @XmlAttribute(name = "OfferItemID")
    @XmlSchemaType(name = "anySimpleType")
    protected String offerItemID;

    /**
     * 获取requestedDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OfferPriceLeadType.RequestedDate }
     *     
     */
    public OfferPriceLeadType.RequestedDate getRequestedDate() {
        return requestedDate;
    }

    /**
     * 设置requestedDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OfferPriceLeadType.RequestedDate }
     *     
     */
    public void setRequestedDate(OfferPriceLeadType.RequestedDate value) {
        this.requestedDate = value;
    }

    /**
     * 获取fareDetail属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FareDetailType }
     *     
     */
    public FareDetailType getFareDetail() {
        return fareDetail;
    }

    /**
     * 设置fareDetail属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FareDetailType }
     *     
     */
    public void setFareDetail(FareDetailType value) {
        this.fareDetail = value;
    }

    /**
     * 获取offerItemID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferItemID() {
        return offerItemID;
    }

    /**
     * 设置offerItemID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferItemID(String value) {
        this.offerItemID = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferPriceLeadDetailType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}PricedFlightOfferAssocType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "associations"
    })
    public static class RequestedDate
        extends OfferPriceLeadDetailType
    {

        @XmlElement(name = "Associations")
        protected List<PricedFlightOfferAssocType> associations;

        /**
         * Gets the value of the associations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the associations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAssociations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PricedFlightOfferAssocType }
         * 
         * 
         */
        public List<PricedFlightOfferAssocType> getAssociations() {
            if (associations == null) {
                associations = new ArrayList<PricedFlightOfferAssocType>();
            }
            return this.associations;
        }

    }

}
