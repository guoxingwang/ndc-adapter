//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for AirShopReq Business Object: ATTRIBUTE QUERY definition.
 * 
 * <p>AirShopReqAttributeQueryType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AirShopReqAttributeQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OriginDestination" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Departure"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Arrival"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ShoppingResponseIDs" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierAirline" minOccurs="0"/&gt;
 *                   &lt;element name="CalendarDates" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="DaysBefore" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                           &lt;attribute name="DaysAfter" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="OriginDestinationKey" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirShopReqAttributeQueryType", propOrder = {
    "originDestination"
})
public class AirShopReqAttributeQueryType {

    @XmlElement(name = "OriginDestination", required = true)
    protected List<AirShopReqAttributeQueryType.OriginDestination> originDestination;

    /**
     * Gets the value of the originDestination property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the originDestination property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginDestination().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirShopReqAttributeQueryType.OriginDestination }
     * 
     * 
     */
    public List<AirShopReqAttributeQueryType.OriginDestination> getOriginDestination() {
        if (originDestination == null) {
            originDestination = new ArrayList<AirShopReqAttributeQueryType.OriginDestination>();
        }
        return this.originDestination;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Departure"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Arrival"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ShoppingResponseIDs" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MarketingCarrierAirline" minOccurs="0"/&gt;
     *         &lt;element name="CalendarDates" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="DaysBefore" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *                 &lt;attribute name="DaysAfter" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="OriginDestinationKey" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "departure",
        "arrival",
        "segMaxTimePreferences",
        "shoppingResponseIDs",
        "marketingCarrierAirline",
        "calendarDates"
    })
    public static class OriginDestination
        extends AssociatedObjectBaseType
    {

        @XmlElement(name = "Departure", required = true)
        protected Departure departure;
        @XmlElement(name = "Arrival", required = true)
        protected FlightArrivalType arrival;
        @XmlElement(name = "SegMaxTimePreferences")
        protected FltSegmentMaxTimePreferencesType segMaxTimePreferences;
        @XmlElement(name = "ShoppingResponseIDs")
        protected ShoppingResponseIDType shoppingResponseIDs;
        @XmlElement(name = "MarketingCarrierAirline")
        protected MarketingCarrierType marketingCarrierAirline;
        @XmlElement(name = "CalendarDates")
        protected AirShopReqAttributeQueryType.OriginDestination.CalendarDates calendarDates;
        @XmlAttribute(name = "OriginDestinationKey")
        @XmlSchemaType(name = "anySimpleType")
        protected String originDestinationKey;

        /**
         * FLIGHT DEPARTURE (ORIGIN).
         * 
         * @return
         *     possible object is
         *     {@link Departure }
         *     
         */
        public Departure getDeparture() {
            return departure;
        }

        /**
         * 设置departure属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Departure }
         *     
         */
        public void setDeparture(Departure value) {
            this.departure = value;
        }

        /**
         * FLIGHT ARRIVAL (DESTINATION).
         * 
         * @return
         *     possible object is
         *     {@link FlightArrivalType }
         *     
         */
        public FlightArrivalType getArrival() {
            return arrival;
        }

        /**
         * 设置arrival属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FlightArrivalType }
         *     
         */
        public void setArrival(FlightArrivalType value) {
            this.arrival = value;
        }

        /**
         * 获取segMaxTimePreferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FltSegmentMaxTimePreferencesType }
         *     
         */
        public FltSegmentMaxTimePreferencesType getSegMaxTimePreferences() {
            return segMaxTimePreferences;
        }

        /**
         * 设置segMaxTimePreferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FltSegmentMaxTimePreferencesType }
         *     
         */
        public void setSegMaxTimePreferences(FltSegmentMaxTimePreferencesType value) {
            this.segMaxTimePreferences = value;
        }

        /**
         * Flight-related shopping response IDs, including the unique (master) Shopping Session ID, Offer IDs, Offer Item IDs and Service IDs.
         * 
         * @return
         *     possible object is
         *     {@link ShoppingResponseIDType }
         *     
         */
        public ShoppingResponseIDType getShoppingResponseIDs() {
            return shoppingResponseIDs;
        }

        /**
         * 设置shoppingResponseIDs属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ShoppingResponseIDType }
         *     
         */
        public void setShoppingResponseIDs(ShoppingResponseIDType value) {
            this.shoppingResponseIDs = value;
        }

        /**
         * 获取marketingCarrierAirline属性的值。
         * 
         * @return
         *     possible object is
         *     {@link MarketingCarrierType }
         *     
         */
        public MarketingCarrierType getMarketingCarrierAirline() {
            return marketingCarrierAirline;
        }

        /**
         * 设置marketingCarrierAirline属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link MarketingCarrierType }
         *     
         */
        public void setMarketingCarrierAirline(MarketingCarrierType value) {
            this.marketingCarrierAirline = value;
        }

        /**
         * 获取calendarDates属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirShopReqAttributeQueryType.OriginDestination.CalendarDates }
         *     
         */
        public AirShopReqAttributeQueryType.OriginDestination.CalendarDates getCalendarDates() {
            return calendarDates;
        }

        /**
         * 设置calendarDates属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirShopReqAttributeQueryType.OriginDestination.CalendarDates }
         *     
         */
        public void setCalendarDates(AirShopReqAttributeQueryType.OriginDestination.CalendarDates value) {
            this.calendarDates = value;
        }

        /**
         * 获取originDestinationKey属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOriginDestinationKey() {
            return originDestinationKey;
        }

        /**
         * 设置originDestinationKey属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOriginDestinationKey(String value) {
            this.originDestinationKey = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="DaysBefore" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *       &lt;attribute name="DaysAfter" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class CalendarDates {

            @XmlAttribute(name = "DaysBefore")
            protected Integer daysBefore;
            @XmlAttribute(name = "DaysAfter")
            protected Integer daysAfter;

            /**
             * 获取daysBefore属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getDaysBefore() {
                return daysBefore;
            }

            /**
             * 设置daysBefore属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setDaysBefore(Integer value) {
                this.daysBefore = value;
            }

            /**
             * 获取daysAfter属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getDaysAfter() {
                return daysAfter;
            }

            /**
             * 设置daysAfter属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setDaysAfter(Integer value) {
                this.daysAfter = value;
            }

        }

    }

}
