//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Document"/&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="Success" type="{http://www.iata.org/IATA/EDIST}SuccessType"/&gt;
 *             &lt;element name="Warnings" type="{http://www.iata.org/IATA/EDIST}WarningsType" minOccurs="0"/&gt;
 *             &lt;element name="Response"&gt;
 *               &lt;complexType&gt;
 *                 &lt;complexContent&gt;
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                     &lt;sequence&gt;
 *                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AirDocDisplayProcessing" minOccurs="0"/&gt;
 *                       &lt;element name="TicketDocInfos" minOccurs="0"&gt;
 *                         &lt;complexType&gt;
 *                           &lt;complexContent&gt;
 *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
 *                                   &lt;complexType&gt;
 *                                     &lt;complexContent&gt;
 *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                         &lt;sequence&gt;
 *                                           &lt;element name="Traveler" type="{http://www.iata.org/IATA/EDIST}CouponTravelerDetailType"/&gt;
 *                                           &lt;element name="AgentIDs" minOccurs="0"&gt;
 *                                             &lt;complexType&gt;
 *                                               &lt;complexContent&gt;
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                   &lt;sequence&gt;
 *                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
 *                                                   &lt;/sequence&gt;
 *                                                 &lt;/restriction&gt;
 *                                               &lt;/complexContent&gt;
 *                                             &lt;/complexType&gt;
 *                                           &lt;/element&gt;
 *                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo"/&gt;
 *                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences"/&gt;
 *                                           &lt;element name="Payments"&gt;
 *                                             &lt;complexType&gt;
 *                                               &lt;complexContent&gt;
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                   &lt;sequence&gt;
 *                                                     &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentFormType" maxOccurs="unbounded"/&gt;
 *                                                   &lt;/sequence&gt;
 *                                                 &lt;/restriction&gt;
 *                                               &lt;/complexContent&gt;
 *                                             &lt;/complexType&gt;
 *                                           &lt;/element&gt;
 *                                           &lt;element name="OriginDestination" minOccurs="0"&gt;
 *                                             &lt;complexType&gt;
 *                                               &lt;complexContent&gt;
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                   &lt;sequence&gt;
 *                                                     &lt;element name="Origin" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
 *                                                     &lt;element name="Destination" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
 *                                                   &lt;/sequence&gt;
 *                                                 &lt;/restriction&gt;
 *                                               &lt;/complexContent&gt;
 *                                             &lt;/complexType&gt;
 *                                           &lt;/element&gt;
 *                                           &lt;element name="EndorsementText" minOccurs="0"&gt;
 *                                             &lt;simpleType&gt;
 *                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                                 &lt;pattern value="[0-9a-zA-Z]{1,70} "/&gt;
 *                                               &lt;/restriction&gt;
 *                                             &lt;/simpleType&gt;
 *                                           &lt;/element&gt;
 *                                           &lt;element name="AddlReferenceIDs" minOccurs="0"&gt;
 *                                             &lt;complexType&gt;
 *                                               &lt;complexContent&gt;
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                   &lt;sequence&gt;
 *                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}AddlReferenceID" maxOccurs="5"/&gt;
 *                                                   &lt;/sequence&gt;
 *                                                 &lt;/restriction&gt;
 *                                               &lt;/complexContent&gt;
 *                                             &lt;/complexType&gt;
 *                                           &lt;/element&gt;
 *                                           &lt;element name="FareInfo" minOccurs="0"&gt;
 *                                             &lt;complexType&gt;
 *                                               &lt;complexContent&gt;
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                   &lt;sequence&gt;
 *                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}BaseFare" maxOccurs="3" minOccurs="0"/&gt;
 *                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}EquivFare" maxOccurs="3" minOccurs="0"/&gt;
 *                                                     &lt;element name="Total" type="{http://www.iata.org/IATA/EDIST}TotalFareTransactionType" maxOccurs="3" minOccurs="0"/&gt;
 *                                                     &lt;element name="Taxes" minOccurs="0"&gt;
 *                                                       &lt;complexType&gt;
 *                                                         &lt;complexContent&gt;
 *                                                           &lt;extension base="{http://www.iata.org/IATA/EDIST}TaxDetailType"&gt;
 *                                                           &lt;/extension&gt;
 *                                                         &lt;/complexContent&gt;
 *                                                       &lt;/complexType&gt;
 *                                                     &lt;/element&gt;
 *                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}UnstructuredFareCalcInfo" maxOccurs="3" minOccurs="0"/&gt;
 *                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}FareInfo" minOccurs="0"/&gt;
 *                                                   &lt;/sequence&gt;
 *                                                 &lt;/restriction&gt;
 *                                               &lt;/complexContent&gt;
 *                                             &lt;/complexType&gt;
 *                                           &lt;/element&gt;
 *                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
 *                                           &lt;element name="Price" type="{http://www.iata.org/IATA/EDIST}ServicePriceType" minOccurs="0"/&gt;
 *                                           &lt;element name="OrderReference" type="{http://www.iata.org/IATA/EDIST}CouponOrderKeyType" minOccurs="0"/&gt;
 *                                           &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
 *                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
 *                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
 *                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingInfo" minOccurs="0"/&gt;
 *                                           &lt;element name="PassengerSpecificData" type="{http://www.iata.org/IATA/EDIST}StringLength1to32" minOccurs="0"/&gt;
 *                                           &lt;element name="TaxCouponInfo" type="{http://www.iata.org/IATA/EDIST}TaxCouponInfoType" minOccurs="0"/&gt;
 *                                           &lt;element name="ReissuedFlown" type="{http://www.iata.org/IATA/EDIST}ReissuedFlownType" minOccurs="0"/&gt;
 *                                         &lt;/sequence&gt;
 *                                       &lt;/restriction&gt;
 *                                     &lt;/complexContent&gt;
 *                                   &lt;/complexType&gt;
 *                                 &lt;/element&gt;
 *                               &lt;/sequence&gt;
 *                             &lt;/restriction&gt;
 *                           &lt;/complexContent&gt;
 *                         &lt;/complexType&gt;
 *                       &lt;/element&gt;
 *                     &lt;/sequence&gt;
 *                   &lt;/restriction&gt;
 *                 &lt;/complexContent&gt;
 *               &lt;/complexType&gt;
 *             &lt;/element&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}Errors"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}IATA_PayloadStdAttributes"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "document",
    "success",
    "warnings",
    "response",
    "errors"
})
@XmlRootElement(name = "AirDocDisplayRS")
public class AirDocDisplayRS {

    @XmlElement(name = "Document", required = true)
    protected MsgDocumentType document;
    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "Response")
    protected AirDocDisplayRS.Response response;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String target;
    @XmlAttribute(name = "Version", required = true)
    protected String version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "AsynchronousAllowedInd")
    protected Boolean asynchronousAllowedInd;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;

    /**
     * 获取document属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgDocumentType }
     *     
     */
    public MsgDocumentType getDocument() {
        return document;
    }

    /**
     * 设置document属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgDocumentType }
     *     
     */
    public void setDocument(MsgDocumentType value) {
        this.document = value;
    }

    /**
     * 获取success属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * 设置success属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * 获取warnings属性的值。
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * 设置warnings属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * 获取response属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AirDocDisplayRS.Response }
     *     
     */
    public AirDocDisplayRS.Response getResponse() {
        return response;
    }

    /**
     * 设置response属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AirDocDisplayRS.Response }
     *     
     */
    public void setResponse(AirDocDisplayRS.Response value) {
        this.response = value;
    }

    /**
     * 获取errors属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * 设置errors属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * 获取echoToken属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * 设置echoToken属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * 获取target属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        if (target == null) {
            return "Production";
        } else {
            return target;
        }
    }

    /**
     * 设置target属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取transactionIdentifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * 设置transactionIdentifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * 获取sequenceNmbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * 设置sequenceNmbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * 获取transactionStatusCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * 设置transactionStatusCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * 获取retransmissionIndicator属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * 设置retransmissionIndicator属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * 获取correlationID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * 设置correlationID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * 获取asynchronousAllowedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAsynchronousAllowedInd() {
        return asynchronousAllowedInd;
    }

    /**
     * 设置asynchronousAllowedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAsynchronousAllowedInd(Boolean value) {
        this.asynchronousAllowedInd = value;
    }

    /**
     * 获取altLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * 设置altLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * 获取primaryLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * 设置primaryLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirDocDisplayProcessing" minOccurs="0"/&gt;
     *         &lt;element name="TicketDocInfos" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Traveler" type="{http://www.iata.org/IATA/EDIST}CouponTravelerDetailType"/&gt;
     *                             &lt;element name="AgentIDs" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences"/&gt;
     *                             &lt;element name="Payments"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentFormType" maxOccurs="unbounded"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="OriginDestination" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Origin" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
     *                                       &lt;element name="Destination" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="EndorsementText" minOccurs="0"&gt;
     *                               &lt;simpleType&gt;
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                                   &lt;pattern value="[0-9a-zA-Z]{1,70} "/&gt;
     *                                 &lt;/restriction&gt;
     *                               &lt;/simpleType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="AddlReferenceIDs" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AddlReferenceID" maxOccurs="5"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="FareInfo" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}BaseFare" maxOccurs="3" minOccurs="0"/&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}EquivFare" maxOccurs="3" minOccurs="0"/&gt;
     *                                       &lt;element name="Total" type="{http://www.iata.org/IATA/EDIST}TotalFareTransactionType" maxOccurs="3" minOccurs="0"/&gt;
     *                                       &lt;element name="Taxes" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}TaxDetailType"&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}UnstructuredFareCalcInfo" maxOccurs="3" minOccurs="0"/&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}FareInfo" minOccurs="0"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
     *                             &lt;element name="Price" type="{http://www.iata.org/IATA/EDIST}ServicePriceType" minOccurs="0"/&gt;
     *                             &lt;element name="OrderReference" type="{http://www.iata.org/IATA/EDIST}CouponOrderKeyType" minOccurs="0"/&gt;
     *                             &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingInfo" minOccurs="0"/&gt;
     *                             &lt;element name="PassengerSpecificData" type="{http://www.iata.org/IATA/EDIST}StringLength1to32" minOccurs="0"/&gt;
     *                             &lt;element name="TaxCouponInfo" type="{http://www.iata.org/IATA/EDIST}TaxCouponInfoType" minOccurs="0"/&gt;
     *                             &lt;element name="ReissuedFlown" type="{http://www.iata.org/IATA/EDIST}ReissuedFlownType" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airDocDisplayProcessing",
        "ticketDocInfos"
    })
    public static class Response {

        @XmlElement(name = "AirDocDisplayProcessing")
        protected AirDocDisplayResProcessType airDocDisplayProcessing;
        @XmlElement(name = "TicketDocInfos")
        protected AirDocDisplayRS.Response.TicketDocInfos ticketDocInfos;

        /**
         * 获取airDocDisplayProcessing属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirDocDisplayResProcessType }
         *     
         */
        public AirDocDisplayResProcessType getAirDocDisplayProcessing() {
            return airDocDisplayProcessing;
        }

        /**
         * 设置airDocDisplayProcessing属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirDocDisplayResProcessType }
         *     
         */
        public void setAirDocDisplayProcessing(AirDocDisplayResProcessType value) {
            this.airDocDisplayProcessing = value;
        }

        /**
         * 获取ticketDocInfos属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirDocDisplayRS.Response.TicketDocInfos }
         *     
         */
        public AirDocDisplayRS.Response.TicketDocInfos getTicketDocInfos() {
            return ticketDocInfos;
        }

        /**
         * 设置ticketDocInfos属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirDocDisplayRS.Response.TicketDocInfos }
         *     
         */
        public void setTicketDocInfos(AirDocDisplayRS.Response.TicketDocInfos value) {
            this.ticketDocInfos = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Traveler" type="{http://www.iata.org/IATA/EDIST}CouponTravelerDetailType"/&gt;
         *                   &lt;element name="AgentIDs" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences"/&gt;
         *                   &lt;element name="Payments"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentFormType" maxOccurs="unbounded"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="OriginDestination" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Origin" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
         *                             &lt;element name="Destination" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="EndorsementText" minOccurs="0"&gt;
         *                     &lt;simpleType&gt;
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *                         &lt;pattern value="[0-9a-zA-Z]{1,70} "/&gt;
         *                       &lt;/restriction&gt;
         *                     &lt;/simpleType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="AddlReferenceIDs" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AddlReferenceID" maxOccurs="5"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="FareInfo" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}BaseFare" maxOccurs="3" minOccurs="0"/&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}EquivFare" maxOccurs="3" minOccurs="0"/&gt;
         *                             &lt;element name="Total" type="{http://www.iata.org/IATA/EDIST}TotalFareTransactionType" maxOccurs="3" minOccurs="0"/&gt;
         *                             &lt;element name="Taxes" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}TaxDetailType"&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}UnstructuredFareCalcInfo" maxOccurs="3" minOccurs="0"/&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}FareInfo" minOccurs="0"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
         *                   &lt;element name="Price" type="{http://www.iata.org/IATA/EDIST}ServicePriceType" minOccurs="0"/&gt;
         *                   &lt;element name="OrderReference" type="{http://www.iata.org/IATA/EDIST}CouponOrderKeyType" minOccurs="0"/&gt;
         *                   &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingInfo" minOccurs="0"/&gt;
         *                   &lt;element name="PassengerSpecificData" type="{http://www.iata.org/IATA/EDIST}StringLength1to32" minOccurs="0"/&gt;
         *                   &lt;element name="TaxCouponInfo" type="{http://www.iata.org/IATA/EDIST}TaxCouponInfoType" minOccurs="0"/&gt;
         *                   &lt;element name="ReissuedFlown" type="{http://www.iata.org/IATA/EDIST}ReissuedFlownType" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ticketDocInfo"
        })
        public static class TicketDocInfos {

            @XmlElement(name = "TicketDocInfo", required = true)
            protected List<AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo> ticketDocInfo;

            /**
             * Gets the value of the ticketDocInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the ticketDocInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTicketDocInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo }
             * 
             * 
             */
            public List<AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo> getTicketDocInfo() {
                if (ticketDocInfo == null) {
                    ticketDocInfo = new ArrayList<AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo>();
                }
                return this.ticketDocInfo;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Traveler" type="{http://www.iata.org/IATA/EDIST}CouponTravelerDetailType"/&gt;
             *         &lt;element name="AgentIDs" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences"/&gt;
             *         &lt;element name="Payments"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentFormType" maxOccurs="unbounded"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="OriginDestination" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Origin" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
             *                   &lt;element name="Destination" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="EndorsementText" minOccurs="0"&gt;
             *           &lt;simpleType&gt;
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
             *               &lt;pattern value="[0-9a-zA-Z]{1,70} "/&gt;
             *             &lt;/restriction&gt;
             *           &lt;/simpleType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="AddlReferenceIDs" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AddlReferenceID" maxOccurs="5"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="FareInfo" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BaseFare" maxOccurs="3" minOccurs="0"/&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}EquivFare" maxOccurs="3" minOccurs="0"/&gt;
             *                   &lt;element name="Total" type="{http://www.iata.org/IATA/EDIST}TotalFareTransactionType" maxOccurs="3" minOccurs="0"/&gt;
             *                   &lt;element name="Taxes" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}TaxDetailType"&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}UnstructuredFareCalcInfo" maxOccurs="3" minOccurs="0"/&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FareInfo" minOccurs="0"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
             *         &lt;element name="Price" type="{http://www.iata.org/IATA/EDIST}ServicePriceType" minOccurs="0"/&gt;
             *         &lt;element name="OrderReference" type="{http://www.iata.org/IATA/EDIST}CouponOrderKeyType" minOccurs="0"/&gt;
             *         &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingInfo" minOccurs="0"/&gt;
             *         &lt;element name="PassengerSpecificData" type="{http://www.iata.org/IATA/EDIST}StringLength1to32" minOccurs="0"/&gt;
             *         &lt;element name="TaxCouponInfo" type="{http://www.iata.org/IATA/EDIST}TaxCouponInfoType" minOccurs="0"/&gt;
             *         &lt;element name="ReissuedFlown" type="{http://www.iata.org/IATA/EDIST}ReissuedFlownType" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "traveler",
                "agentIDs",
                "issuingAirlineInfo",
                "bookingReferences",
                "payments",
                "originDestination",
                "endorsementText",
                "addlReferenceIDs",
                "fareInfo",
                "ticketDocument",
                "price",
                "orderReference",
                "commission",
                "carrierFeeInfo",
                "originalIssueInfo",
                "pricingInfo",
                "passengerSpecificData",
                "taxCouponInfo",
                "reissuedFlown"
            })
            public static class TicketDocInfo {

                @XmlElement(name = "Traveler", required = true)
                protected CouponTravelerDetailType traveler;
                @XmlElement(name = "AgentIDs")
                protected AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AgentIDs agentIDs;
                @XmlElement(name = "IssuingAirlineInfo", required = true)
                protected IssuingAirlineInfo issuingAirlineInfo;
                @XmlElement(name = "BookingReferences", required = true)
                protected BookingReferences bookingReferences;
                @XmlElement(name = "Payments", required = true)
                protected AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.Payments payments;
                @XmlElement(name = "OriginDestination")
                protected AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.OriginDestination originDestination;
                @XmlElement(name = "EndorsementText")
                protected String endorsementText;
                @XmlElement(name = "AddlReferenceIDs")
                protected AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AddlReferenceIDs addlReferenceIDs;
                @XmlElement(name = "FareInfo")
                protected AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo fareInfo;
                @XmlElement(name = "TicketDocument", required = true)
                protected List<TicketDocument> ticketDocument;
                @XmlElement(name = "Price")
                protected ServicePriceType price;
                @XmlElement(name = "OrderReference")
                protected CouponOrderKeyType orderReference;
                @XmlElement(name = "Commission")
                protected CommissionType commission;
                @XmlElement(name = "CarrierFeeInfo")
                protected CarrierFeeInfoType carrierFeeInfo;
                @XmlElement(name = "OriginalIssueInfo")
                protected OriginalIssueInfo originalIssueInfo;
                @XmlElement(name = "PricingInfo")
                protected PricingInfo pricingInfo;
                @XmlElement(name = "PassengerSpecificData")
                protected String passengerSpecificData;
                @XmlElement(name = "TaxCouponInfo")
                protected TaxCouponInfoType taxCouponInfo;
                @XmlElement(name = "ReissuedFlown")
                protected ReissuedFlownType reissuedFlown;

                /**
                 * 获取traveler属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CouponTravelerDetailType }
                 *     
                 */
                public CouponTravelerDetailType getTraveler() {
                    return traveler;
                }

                /**
                 * 设置traveler属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CouponTravelerDetailType }
                 *     
                 */
                public void setTraveler(CouponTravelerDetailType value) {
                    this.traveler = value;
                }

                /**
                 * 获取agentIDs属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AgentIDs }
                 *     
                 */
                public AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AgentIDs getAgentIDs() {
                    return agentIDs;
                }

                /**
                 * 设置agentIDs属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AgentIDs }
                 *     
                 */
                public void setAgentIDs(AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AgentIDs value) {
                    this.agentIDs = value;
                }

                /**
                 * 获取issuingAirlineInfo属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link IssuingAirlineInfo }
                 *     
                 */
                public IssuingAirlineInfo getIssuingAirlineInfo() {
                    return issuingAirlineInfo;
                }

                /**
                 * 设置issuingAirlineInfo属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IssuingAirlineInfo }
                 *     
                 */
                public void setIssuingAirlineInfo(IssuingAirlineInfo value) {
                    this.issuingAirlineInfo = value;
                }

                /**
                 * 获取bookingReferences属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link BookingReferences }
                 *     
                 */
                public BookingReferences getBookingReferences() {
                    return bookingReferences;
                }

                /**
                 * 设置bookingReferences属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BookingReferences }
                 *     
                 */
                public void setBookingReferences(BookingReferences value) {
                    this.bookingReferences = value;
                }

                /**
                 * 获取payments属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.Payments }
                 *     
                 */
                public AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.Payments getPayments() {
                    return payments;
                }

                /**
                 * 设置payments属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.Payments }
                 *     
                 */
                public void setPayments(AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.Payments value) {
                    this.payments = value;
                }

                /**
                 * 获取originDestination属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.OriginDestination }
                 *     
                 */
                public AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.OriginDestination getOriginDestination() {
                    return originDestination;
                }

                /**
                 * 设置originDestination属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.OriginDestination }
                 *     
                 */
                public void setOriginDestination(AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.OriginDestination value) {
                    this.originDestination = value;
                }

                /**
                 * 获取endorsementText属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEndorsementText() {
                    return endorsementText;
                }

                /**
                 * 设置endorsementText属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEndorsementText(String value) {
                    this.endorsementText = value;
                }

                /**
                 * 获取addlReferenceIDs属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AddlReferenceIDs }
                 *     
                 */
                public AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AddlReferenceIDs getAddlReferenceIDs() {
                    return addlReferenceIDs;
                }

                /**
                 * 设置addlReferenceIDs属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AddlReferenceIDs }
                 *     
                 */
                public void setAddlReferenceIDs(AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.AddlReferenceIDs value) {
                    this.addlReferenceIDs = value;
                }

                /**
                 * 获取fareInfo属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo }
                 *     
                 */
                public AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo getFareInfo() {
                    return fareInfo;
                }

                /**
                 * 设置fareInfo属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo }
                 *     
                 */
                public void setFareInfo(AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo value) {
                    this.fareInfo = value;
                }

                /**
                 * Gets the value of the ticketDocument property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the ticketDocument property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getTicketDocument().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TicketDocument }
                 * 
                 * 
                 */
                public List<TicketDocument> getTicketDocument() {
                    if (ticketDocument == null) {
                        ticketDocument = new ArrayList<TicketDocument>();
                    }
                    return this.ticketDocument;
                }

                /**
                 * 获取price属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ServicePriceType }
                 *     
                 */
                public ServicePriceType getPrice() {
                    return price;
                }

                /**
                 * 设置price属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ServicePriceType }
                 *     
                 */
                public void setPrice(ServicePriceType value) {
                    this.price = value;
                }

                /**
                 * 获取orderReference属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CouponOrderKeyType }
                 *     
                 */
                public CouponOrderKeyType getOrderReference() {
                    return orderReference;
                }

                /**
                 * 设置orderReference属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CouponOrderKeyType }
                 *     
                 */
                public void setOrderReference(CouponOrderKeyType value) {
                    this.orderReference = value;
                }

                /**
                 * 获取commission属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CommissionType }
                 *     
                 */
                public CommissionType getCommission() {
                    return commission;
                }

                /**
                 * 设置commission属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CommissionType }
                 *     
                 */
                public void setCommission(CommissionType value) {
                    this.commission = value;
                }

                /**
                 * 获取carrierFeeInfo属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CarrierFeeInfoType }
                 *     
                 */
                public CarrierFeeInfoType getCarrierFeeInfo() {
                    return carrierFeeInfo;
                }

                /**
                 * 设置carrierFeeInfo属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CarrierFeeInfoType }
                 *     
                 */
                public void setCarrierFeeInfo(CarrierFeeInfoType value) {
                    this.carrierFeeInfo = value;
                }

                /**
                 * 获取originalIssueInfo属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link OriginalIssueInfo }
                 *     
                 */
                public OriginalIssueInfo getOriginalIssueInfo() {
                    return originalIssueInfo;
                }

                /**
                 * 设置originalIssueInfo属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OriginalIssueInfo }
                 *     
                 */
                public void setOriginalIssueInfo(OriginalIssueInfo value) {
                    this.originalIssueInfo = value;
                }

                /**
                 * 获取pricingInfo属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link PricingInfo }
                 *     
                 */
                public PricingInfo getPricingInfo() {
                    return pricingInfo;
                }

                /**
                 * 设置pricingInfo属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PricingInfo }
                 *     
                 */
                public void setPricingInfo(PricingInfo value) {
                    this.pricingInfo = value;
                }

                /**
                 * 获取passengerSpecificData属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPassengerSpecificData() {
                    return passengerSpecificData;
                }

                /**
                 * 设置passengerSpecificData属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPassengerSpecificData(String value) {
                    this.passengerSpecificData = value;
                }

                /**
                 * 获取taxCouponInfo属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link TaxCouponInfoType }
                 *     
                 */
                public TaxCouponInfoType getTaxCouponInfo() {
                    return taxCouponInfo;
                }

                /**
                 * 设置taxCouponInfo属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TaxCouponInfoType }
                 *     
                 */
                public void setTaxCouponInfo(TaxCouponInfoType value) {
                    this.taxCouponInfo = value;
                }

                /**
                 * 获取reissuedFlown属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ReissuedFlownType }
                 *     
                 */
                public ReissuedFlownType getReissuedFlown() {
                    return reissuedFlown;
                }

                /**
                 * 设置reissuedFlown属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ReissuedFlownType }
                 *     
                 */
                public void setReissuedFlown(ReissuedFlownType value) {
                    this.reissuedFlown = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AddlReferenceID" maxOccurs="5"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "addlReferenceID"
                })
                public static class AddlReferenceIDs {

                    @XmlElement(name = "AddlReferenceID", required = true)
                    protected List<AdditionalReferenceType> addlReferenceID;

                    /**
                     * Gets the value of the addlReferenceID property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the addlReferenceID property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAddlReferenceID().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link AdditionalReferenceType }
                     * 
                     * 
                     */
                    public List<AdditionalReferenceType> getAddlReferenceID() {
                        if (addlReferenceID == null) {
                            addlReferenceID = new ArrayList<AdditionalReferenceType>();
                        }
                        return this.addlReferenceID;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "agentID"
                })
                public static class AgentIDs {

                    @XmlElement(name = "AgentID", required = true)
                    protected List<AgentID> agentID;

                    /**
                     * Gets the value of the agentID property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the agentID property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAgentID().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link AgentID }
                     * 
                     * 
                     */
                    public List<AgentID> getAgentID() {
                        if (agentID == null) {
                            agentID = new ArrayList<AgentID>();
                        }
                        return this.agentID;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaseFare" maxOccurs="3" minOccurs="0"/&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}EquivFare" maxOccurs="3" minOccurs="0"/&gt;
                 *         &lt;element name="Total" type="{http://www.iata.org/IATA/EDIST}TotalFareTransactionType" maxOccurs="3" minOccurs="0"/&gt;
                 *         &lt;element name="Taxes" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}TaxDetailType"&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}UnstructuredFareCalcInfo" maxOccurs="3" minOccurs="0"/&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FareInfo" minOccurs="0"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "baseFare",
                    "equivFare",
                    "total",
                    "taxes",
                    "unstructuredFareCalcInfo",
                    "fareInfo"
                })
                public static class FareInfo {

                    @XmlElement(name = "BaseFare")
                    protected List<BaseFareTransactionType> baseFare;
                    @XmlElement(name = "EquivFare")
                    protected List<EquivFareTransactionType> equivFare;
                    @XmlElement(name = "Total")
                    protected List<TotalFareTransactionType> total;
                    @XmlElement(name = "Taxes")
                    protected AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo.Taxes taxes;
                    @XmlElement(name = "UnstructuredFareCalcInfo")
                    protected List<UnstructuredFareCalcType> unstructuredFareCalcInfo;
                    @XmlElement(name = "FareInfo")
                    protected ETFareInfoType fareInfo;

                    /**
                     * Gets the value of the baseFare property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the baseFare property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getBaseFare().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link BaseFareTransactionType }
                     * 
                     * 
                     */
                    public List<BaseFareTransactionType> getBaseFare() {
                        if (baseFare == null) {
                            baseFare = new ArrayList<BaseFareTransactionType>();
                        }
                        return this.baseFare;
                    }

                    /**
                     * Gets the value of the equivFare property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the equivFare property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getEquivFare().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link EquivFareTransactionType }
                     * 
                     * 
                     */
                    public List<EquivFareTransactionType> getEquivFare() {
                        if (equivFare == null) {
                            equivFare = new ArrayList<EquivFareTransactionType>();
                        }
                        return this.equivFare;
                    }

                    /**
                     * Gets the value of the total property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the total property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getTotal().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link TotalFareTransactionType }
                     * 
                     * 
                     */
                    public List<TotalFareTransactionType> getTotal() {
                        if (total == null) {
                            total = new ArrayList<TotalFareTransactionType>();
                        }
                        return this.total;
                    }

                    /**
                     * 获取taxes属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo.Taxes }
                     *     
                     */
                    public AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo.Taxes getTaxes() {
                        return taxes;
                    }

                    /**
                     * 设置taxes属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo.Taxes }
                     *     
                     */
                    public void setTaxes(AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo.Taxes value) {
                        this.taxes = value;
                    }

                    /**
                     * Gets the value of the unstructuredFareCalcInfo property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the unstructuredFareCalcInfo property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getUnstructuredFareCalcInfo().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link UnstructuredFareCalcType }
                     * 
                     * 
                     */
                    public List<UnstructuredFareCalcType> getUnstructuredFareCalcInfo() {
                        if (unstructuredFareCalcInfo == null) {
                            unstructuredFareCalcInfo = new ArrayList<UnstructuredFareCalcType>();
                        }
                        return this.unstructuredFareCalcInfo;
                    }

                    /**
                     * 获取fareInfo属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ETFareInfoType }
                     *     
                     */
                    public ETFareInfoType getFareInfo() {
                        return fareInfo;
                    }

                    /**
                     * 设置fareInfo属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ETFareInfoType }
                     *     
                     */
                    public void setFareInfo(ETFareInfoType value) {
                        this.fareInfo = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}TaxDetailType"&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Taxes
                        extends TaxDetailType
                    {


                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Origin" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
                 *         &lt;element name="Destination" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "origin",
                    "destination"
                })
                public static class OriginDestination {

                    @XmlElement(name = "Origin", required = true)
                    protected String origin;
                    @XmlElement(name = "Destination", required = true)
                    protected String destination;

                    /**
                     * 获取origin属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOrigin() {
                        return origin;
                    }

                    /**
                     * 设置origin属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOrigin(String value) {
                        this.origin = value;
                    }

                    /**
                     * 获取destination属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDestination() {
                        return destination;
                    }

                    /**
                     * 设置destination属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDestination(String value) {
                        this.destination = value;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentFormType" maxOccurs="unbounded"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "payment"
                })
                public static class Payments {

                    @XmlElement(name = "Payment", required = true)
                    protected List<OrderPaymentFormType> payment;

                    /**
                     * Gets the value of the payment property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the payment property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getPayment().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link OrderPaymentFormType }
                     * 
                     * 
                     */
                    public List<OrderPaymentFormType> getPayment() {
                        if (payment == null) {
                            payment = new ArrayList<OrderPaymentFormType>();
                        }
                        return this.payment;
                    }

                }

            }

        }

    }

}
