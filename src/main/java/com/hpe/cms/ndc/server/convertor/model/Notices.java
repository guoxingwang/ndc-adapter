//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Notice" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TaxExemptionNotice"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceFilterNotice"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingParametersNotice"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceVarianceNotice"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentRulesNotice"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}InventoryGuaranteeNotif"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "notice"
})
@XmlRootElement(name = "Notices")
public class Notices {

    @XmlElement(name = "Notice", required = true)
    protected List<Notices.Notice> notice;

    /**
     * Gets the value of the notice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Notices.Notice }
     * 
     * 
     */
    public List<Notices.Notice> getNotice() {
        if (notice == null) {
            notice = new ArrayList<Notices.Notice>();
        }
        return this.notice;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TaxExemptionNotice"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceFilterNotice"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingParametersNotice"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceVarianceNotice"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentRulesNotice"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}InventoryGuaranteeNotif"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "taxExemptionNotice",
        "serviceFilterNotice",
        "pricingParametersNotice",
        "priceVarianceNotice",
        "paymentRulesNotice",
        "inventoryGuaranteeNotif"
    })
    public static class Notice {

        @XmlElement(name = "TaxExemptionNotice")
        protected TaxExemptionNoticeType taxExemptionNotice;
        @XmlElement(name = "ServiceFilterNotice")
        protected ServiceFilterNoticeType serviceFilterNotice;
        @XmlElement(name = "PricingParametersNotice")
        protected PricingParametersNoticeType pricingParametersNotice;
        @XmlElement(name = "PriceVarianceNotice")
        protected PriceVarianceNotice priceVarianceNotice;
        @XmlElement(name = "PaymentRulesNotice")
        protected PaymentRulesNoticeType paymentRulesNotice;
        @XmlElement(name = "InventoryGuaranteeNotif")
        protected InvGuaranteeNoticeType inventoryGuaranteeNotif;

        /**
         * 获取taxExemptionNotice属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TaxExemptionNoticeType }
         *     
         */
        public TaxExemptionNoticeType getTaxExemptionNotice() {
            return taxExemptionNotice;
        }

        /**
         * 设置taxExemptionNotice属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TaxExemptionNoticeType }
         *     
         */
        public void setTaxExemptionNotice(TaxExemptionNoticeType value) {
            this.taxExemptionNotice = value;
        }

        /**
         * 获取serviceFilterNotice属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceFilterNoticeType }
         *     
         */
        public ServiceFilterNoticeType getServiceFilterNotice() {
            return serviceFilterNotice;
        }

        /**
         * 设置serviceFilterNotice属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceFilterNoticeType }
         *     
         */
        public void setServiceFilterNotice(ServiceFilterNoticeType value) {
            this.serviceFilterNotice = value;
        }

        /**
         * 获取pricingParametersNotice属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PricingParametersNoticeType }
         *     
         */
        public PricingParametersNoticeType getPricingParametersNotice() {
            return pricingParametersNotice;
        }

        /**
         * 设置pricingParametersNotice属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PricingParametersNoticeType }
         *     
         */
        public void setPricingParametersNotice(PricingParametersNoticeType value) {
            this.pricingParametersNotice = value;
        }

        /**
         * Price Variance Rule(s) Notice. Note: Supports specifying Payment Processing Rules, including payment failure. Request messages may include defined payment processing rules and an indicator requesting that the rules be applied during payment processing where relevant. Response messages may additionally include notice about if and how payment rule(s) were applied during transaction processing.
         *       
         * 
         * @return
         *     possible object is
         *     {@link PriceVarianceNotice }
         *     
         */
        public PriceVarianceNotice getPriceVarianceNotice() {
            return priceVarianceNotice;
        }

        /**
         * 设置priceVarianceNotice属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PriceVarianceNotice }
         *     
         */
        public void setPriceVarianceNotice(PriceVarianceNotice value) {
            this.priceVarianceNotice = value;
        }

        /**
         * 获取paymentRulesNotice属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PaymentRulesNoticeType }
         *     
         */
        public PaymentRulesNoticeType getPaymentRulesNotice() {
            return paymentRulesNotice;
        }

        /**
         * 设置paymentRulesNotice属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentRulesNoticeType }
         *     
         */
        public void setPaymentRulesNotice(PaymentRulesNoticeType value) {
            this.paymentRulesNotice = value;
        }

        /**
         * 获取inventoryGuaranteeNotif属性的值。
         * 
         * @return
         *     possible object is
         *     {@link InvGuaranteeNoticeType }
         *     
         */
        public InvGuaranteeNoticeType getInventoryGuaranteeNotif() {
            return inventoryGuaranteeNotif;
        }

        /**
         * 设置inventoryGuaranteeNotif属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link InvGuaranteeNoticeType }
         *     
         */
        public void setInventoryGuaranteeNotif(InvGuaranteeNoticeType value) {
            this.inventoryGuaranteeNotif = value;
        }

    }

}
