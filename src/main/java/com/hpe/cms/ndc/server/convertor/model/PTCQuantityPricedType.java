//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Requested and Priced Passenger Type Code(s) and Quantity.
 * 
 * Passenger type code and Traveler quantity.  Example: ADT
 * 
 * Encoding scheme:  IATA (three character) Passenger Type Code
 * 
 * <p>PTC_QuantityPricedType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PTC_QuantityPricedType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Requested" type="{http://www.iata.org/IATA/EDIST}PTC_QuantityType" minOccurs="0"/&gt;
 *         &lt;element name="Priced" type="{http://www.iata.org/IATA/EDIST}PTC_QuantityType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PTC_QuantityPricedType", propOrder = {
    "requested",
    "priced"
})
public class PTCQuantityPricedType
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "Requested")
    protected PTCQuantityType requested;
    @XmlElement(name = "Priced")
    protected PTCQuantityType priced;

    /**
     * 获取requested属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PTCQuantityType }
     *     
     */
    public PTCQuantityType getRequested() {
        return requested;
    }

    /**
     * 设置requested属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PTCQuantityType }
     *     
     */
    public void setRequested(PTCQuantityType value) {
        this.requested = value;
    }

    /**
     * 获取priced属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PTCQuantityType }
     *     
     */
    public PTCQuantityType getPriced() {
        return priced;
    }

    /**
     * 设置priced属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PTCQuantityType }
     *     
     */
    public void setPriced(PTCQuantityType value) {
        this.priced = value;
    }

}
