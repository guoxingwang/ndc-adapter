//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CouponTicketDocType"&gt;
 *       &lt;attribute name="PresentCreditCardInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PenaltyRestrictionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NonCommissionableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NonInterlineableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NonReissuableNonExchangInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="NonRefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TaxOnEMD_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ExchTicketNbrInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="PrimaryDocInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "TicketDocument")
public class TicketDocument
    extends CouponTicketDocType
{

    @XmlAttribute(name = "PresentCreditCardInd")
    protected Boolean presentCreditCardInd;
    @XmlAttribute(name = "PenaltyRestrictionInd")
    protected Boolean penaltyRestrictionInd;
    @XmlAttribute(name = "NonCommissionableInd")
    protected Boolean nonCommissionableInd;
    @XmlAttribute(name = "NonInterlineableInd")
    protected Boolean nonInterlineableInd;
    @XmlAttribute(name = "NonReissuableNonExchangInd")
    protected Boolean nonReissuableNonExchangInd;
    @XmlAttribute(name = "NonRefundableInd")
    protected Boolean nonRefundableInd;
    @XmlAttribute(name = "TaxOnEMD_Ind")
    protected Boolean taxOnEMDInd;
    @XmlAttribute(name = "ExchTicketNbrInd")
    protected Boolean exchTicketNbrInd;
    @XmlAttribute(name = "PrimaryDocInd")
    protected Boolean primaryDocInd;

    /**
     * 获取presentCreditCardInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPresentCreditCardInd() {
        return presentCreditCardInd;
    }

    /**
     * 设置presentCreditCardInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPresentCreditCardInd(Boolean value) {
        this.presentCreditCardInd = value;
    }

    /**
     * 获取penaltyRestrictionInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPenaltyRestrictionInd() {
        return penaltyRestrictionInd;
    }

    /**
     * 设置penaltyRestrictionInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPenaltyRestrictionInd(Boolean value) {
        this.penaltyRestrictionInd = value;
    }

    /**
     * 获取nonCommissionableInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonCommissionableInd() {
        return nonCommissionableInd;
    }

    /**
     * 设置nonCommissionableInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonCommissionableInd(Boolean value) {
        this.nonCommissionableInd = value;
    }

    /**
     * 获取nonInterlineableInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonInterlineableInd() {
        return nonInterlineableInd;
    }

    /**
     * 设置nonInterlineableInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonInterlineableInd(Boolean value) {
        this.nonInterlineableInd = value;
    }

    /**
     * 获取nonReissuableNonExchangInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonReissuableNonExchangInd() {
        return nonReissuableNonExchangInd;
    }

    /**
     * 设置nonReissuableNonExchangInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonReissuableNonExchangInd(Boolean value) {
        this.nonReissuableNonExchangInd = value;
    }

    /**
     * 获取nonRefundableInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonRefundableInd() {
        return nonRefundableInd;
    }

    /**
     * 设置nonRefundableInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonRefundableInd(Boolean value) {
        this.nonRefundableInd = value;
    }

    /**
     * 获取taxOnEMDInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxOnEMDInd() {
        return taxOnEMDInd;
    }

    /**
     * 设置taxOnEMDInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxOnEMDInd(Boolean value) {
        this.taxOnEMDInd = value;
    }

    /**
     * 获取exchTicketNbrInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExchTicketNbrInd() {
        return exchTicketNbrInd;
    }

    /**
     * 设置exchTicketNbrInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExchTicketNbrInd(Boolean value) {
        this.exchTicketNbrInd = value;
    }

    /**
     * 获取primaryDocInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrimaryDocInd() {
        return primaryDocInd;
    }

    /**
     * 设置primaryDocInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimaryDocInd(Boolean value) {
        this.primaryDocInd = value;
    }

}
