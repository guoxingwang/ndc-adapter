//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * File Retrieve Response Metadata definition.
 * 
 * <p>FileRetrieveResMetadataType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="FileRetrieveResMetadataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}MediaMetadataType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MediaMetadatas" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileRetrieveResMetadataType", propOrder = {
    "mediaMetadatas"
})
public class FileRetrieveResMetadataType
    extends MediaMetadataType
{

    @XmlElement(name = "MediaMetadatas")
    protected MediaMetadatas mediaMetadatas;

    /**
     * 获取mediaMetadatas属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MediaMetadatas }
     *     
     */
    public MediaMetadatas getMediaMetadatas() {
        return mediaMetadatas;
    }

    /**
     * 设置mediaMetadatas属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MediaMetadatas }
     *     
     */
    public void setMediaMetadatas(MediaMetadatas value) {
        this.mediaMetadatas = value;
    }

}
