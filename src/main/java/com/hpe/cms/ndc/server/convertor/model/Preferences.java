//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Preference" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlinePreferences"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AlliancePreferences"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FarePreferences"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightPreferences"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingMethodPreference"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServicePricingOnlyPreference"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TransferPreferences"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "preference"
})
@XmlRootElement(name = "Preferences")
public class Preferences {

    @XmlElement(name = "Preference", required = true)
    protected List<Preferences.Preference> preference;

    /**
     * Gets the value of the preference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the preference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPreference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Preferences.Preference }
     * 
     * 
     */
    public List<Preferences.Preference> getPreference() {
        if (preference == null) {
            preference = new ArrayList<Preferences.Preference>();
        }
        return this.preference;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlinePreferences"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AlliancePreferences"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FarePreferences"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightPreferences"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingMethodPreference"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServicePricingOnlyPreference"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TransferPreferences"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airlinePreferences",
        "alliancePreferences",
        "farePreferences",
        "flightPreferences",
        "pricingMethodPreference",
        "segMaxTimePreferences",
        "servicePricingOnlyPreference",
        "transferPreferences"
    })
    public static class Preference {

        @XmlElement(name = "AirlinePreferences")
        protected AirlinePreferencesType airlinePreferences;
        @XmlElement(name = "AlliancePreferences")
        protected AlliancePreferencesType alliancePreferences;
        @XmlElement(name = "FarePreferences")
        protected FarePreferencesType farePreferences;
        @XmlElement(name = "FlightPreferences")
        protected FlightPreferencesType flightPreferences;
        @XmlElement(name = "PricingMethodPreference")
        protected BestPricingPreferencesType pricingMethodPreference;
        @XmlElement(name = "SegMaxTimePreferences")
        protected FltSegmentMaxTimePreferencesType segMaxTimePreferences;
        @XmlElement(name = "ServicePricingOnlyPreference")
        protected ServicePricingOnlyPreference servicePricingOnlyPreference;
        @XmlElement(name = "TransferPreferences")
        protected TransferPreferencesType transferPreferences;

        /**
         * 获取airlinePreferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirlinePreferencesType }
         *     
         */
        public AirlinePreferencesType getAirlinePreferences() {
            return airlinePreferences;
        }

        /**
         * 设置airlinePreferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirlinePreferencesType }
         *     
         */
        public void setAirlinePreferences(AirlinePreferencesType value) {
            this.airlinePreferences = value;
        }

        /**
         * 获取alliancePreferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AlliancePreferencesType }
         *     
         */
        public AlliancePreferencesType getAlliancePreferences() {
            return alliancePreferences;
        }

        /**
         * 设置alliancePreferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AlliancePreferencesType }
         *     
         */
        public void setAlliancePreferences(AlliancePreferencesType value) {
            this.alliancePreferences = value;
        }

        /**
         * 获取farePreferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FarePreferencesType }
         *     
         */
        public FarePreferencesType getFarePreferences() {
            return farePreferences;
        }

        /**
         * 设置farePreferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FarePreferencesType }
         *     
         */
        public void setFarePreferences(FarePreferencesType value) {
            this.farePreferences = value;
        }

        /**
         * 获取flightPreferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FlightPreferencesType }
         *     
         */
        public FlightPreferencesType getFlightPreferences() {
            return flightPreferences;
        }

        /**
         * 设置flightPreferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FlightPreferencesType }
         *     
         */
        public void setFlightPreferences(FlightPreferencesType value) {
            this.flightPreferences = value;
        }

        /**
         * Contains information to identify how reservation was re-priced to obtain best published fare (Eg. best fare in same cabin, best fare in all cabins, other parameters to select best fare).
         * 
         * @return
         *     possible object is
         *     {@link BestPricingPreferencesType }
         *     
         */
        public BestPricingPreferencesType getPricingMethodPreference() {
            return pricingMethodPreference;
        }

        /**
         * 设置pricingMethodPreference属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BestPricingPreferencesType }
         *     
         */
        public void setPricingMethodPreference(BestPricingPreferencesType value) {
            this.pricingMethodPreference = value;
        }

        /**
         * 获取segMaxTimePreferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FltSegmentMaxTimePreferencesType }
         *     
         */
        public FltSegmentMaxTimePreferencesType getSegMaxTimePreferences() {
            return segMaxTimePreferences;
        }

        /**
         * 设置segMaxTimePreferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FltSegmentMaxTimePreferencesType }
         *     
         */
        public void setSegMaxTimePreferences(FltSegmentMaxTimePreferencesType value) {
            this.segMaxTimePreferences = value;
        }

        /**
         * 获取servicePricingOnlyPreference属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServicePricingOnlyPreference }
         *     
         */
        public ServicePricingOnlyPreference getServicePricingOnlyPreference() {
            return servicePricingOnlyPreference;
        }

        /**
         * 设置servicePricingOnlyPreference属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServicePricingOnlyPreference }
         *     
         */
        public void setServicePricingOnlyPreference(ServicePricingOnlyPreference value) {
            this.servicePricingOnlyPreference = value;
        }

        /**
         * 获取transferPreferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TransferPreferencesType }
         *     
         */
        public TransferPreferencesType getTransferPreferences() {
            return transferPreferences;
        }

        /**
         * 设置transferPreferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TransferPreferencesType }
         *     
         */
        public void setTransferPreferences(TransferPreferencesType value) {
            this.transferPreferences = value;
        }

    }

}
