//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PointOfSale" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Document"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Party"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ShopProductParameters" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Travelers"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Query"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="Qualifiers" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;choice&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
 *                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
 *                                       &lt;/choice&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="Locations" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="LocationCode" maxOccurs="unbounded"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;simpleContent&gt;
 *                                     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirportCitySimpleType"&gt;
 *                                       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}RadiusAttrGroup"/&gt;
 *                                       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
 *                                     &lt;/extension&gt;
 *                                   &lt;/simpleContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/extension&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="Preferences" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="Alliances" type="{http://www.iata.org/IATA/EDIST}AlliancePreferencesType" minOccurs="0"/&gt;
 *                               &lt;element name="Airlines" type="{http://www.iata.org/IATA/EDIST}AirlinePreferencesType" minOccurs="0"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="Metadata" type="{http://www.iata.org/IATA/EDIST}ShopProductMetadataType" minOccurs="0"/&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/extension&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}ShoppingResponseIDs"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}IATA_PayloadStdAttributes"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pointOfSale",
    "document",
    "party",
    "shopProductParameters",
    "travelers",
    "query",
    "shoppingResponseIDs"
})
@XmlRootElement(name = "ShopProductRQ")
public class ShopProductRQ {

    @XmlElement(name = "PointOfSale")
    protected PointOfSaleType pointOfSale;
    @XmlElement(name = "Document", required = true)
    protected MsgDocumentType document;
    @XmlElement(name = "Party", required = true)
    protected MsgPartiesType party;
    @XmlElement(name = "ShopProductParameters")
    protected ShopProductReqParamsType shopProductParameters;
    @XmlElement(name = "Travelers", required = true)
    protected Travelers travelers;
    @XmlElement(name = "Query")
    protected ShopProductRQ.Query query;
    @XmlElement(name = "ShoppingResponseIDs")
    protected ShoppingResponseIDType shoppingResponseIDs;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String target;
    @XmlAttribute(name = "Version", required = true)
    protected String version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "AsynchronousAllowedInd")
    protected Boolean asynchronousAllowedInd;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;

    /**
     * 获取pointOfSale属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PointOfSaleType }
     *     
     */
    public PointOfSaleType getPointOfSale() {
        return pointOfSale;
    }

    /**
     * 设置pointOfSale属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PointOfSaleType }
     *     
     */
    public void setPointOfSale(PointOfSaleType value) {
        this.pointOfSale = value;
    }

    /**
     * 获取document属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgDocumentType }
     *     
     */
    public MsgDocumentType getDocument() {
        return document;
    }

    /**
     * 设置document属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgDocumentType }
     *     
     */
    public void setDocument(MsgDocumentType value) {
        this.document = value;
    }

    /**
     * 获取party属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgPartiesType }
     *     
     */
    public MsgPartiesType getParty() {
        return party;
    }

    /**
     * 设置party属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgPartiesType }
     *     
     */
    public void setParty(MsgPartiesType value) {
        this.party = value;
    }

    /**
     * 获取shopProductParameters属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShopProductReqParamsType }
     *     
     */
    public ShopProductReqParamsType getShopProductParameters() {
        return shopProductParameters;
    }

    /**
     * 设置shopProductParameters属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShopProductReqParamsType }
     *     
     */
    public void setShopProductParameters(ShopProductReqParamsType value) {
        this.shopProductParameters = value;
    }

    /**
     * 获取travelers属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Travelers }
     *     
     */
    public Travelers getTravelers() {
        return travelers;
    }

    /**
     * 设置travelers属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Travelers }
     *     
     */
    public void setTravelers(Travelers value) {
        this.travelers = value;
    }

    /**
     * 获取query属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShopProductRQ.Query }
     *     
     */
    public ShopProductRQ.Query getQuery() {
        return query;
    }

    /**
     * 设置query属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShopProductRQ.Query }
     *     
     */
    public void setQuery(ShopProductRQ.Query value) {
        this.query = value;
    }

    /**
     * 获取shoppingResponseIDs属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShoppingResponseIDType }
     *     
     */
    public ShoppingResponseIDType getShoppingResponseIDs() {
        return shoppingResponseIDs;
    }

    /**
     * 设置shoppingResponseIDs属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShoppingResponseIDType }
     *     
     */
    public void setShoppingResponseIDs(ShoppingResponseIDType value) {
        this.shoppingResponseIDs = value;
    }

    /**
     * 获取echoToken属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * 设置echoToken属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * 获取target属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        if (target == null) {
            return "Production";
        } else {
            return target;
        }
    }

    /**
     * 设置target属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取transactionIdentifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * 设置transactionIdentifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * 获取sequenceNmbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * 设置sequenceNmbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * 获取transactionStatusCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * 设置transactionStatusCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * 获取retransmissionIndicator属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * 设置retransmissionIndicator属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * 获取correlationID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * 设置correlationID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * 获取asynchronousAllowedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAsynchronousAllowedInd() {
        return asynchronousAllowedInd;
    }

    /**
     * 设置asynchronousAllowedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAsynchronousAllowedInd(Boolean value) {
        this.asynchronousAllowedInd = value;
    }

    /**
     * 获取altLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * 设置altLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * 获取primaryLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * 设置primaryLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Qualifiers" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;choice&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
     *                           &lt;/choice&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Locations" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="LocationCode" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirportCitySimpleType"&gt;
     *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}RadiusAttrGroup"/&gt;
     *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Preferences" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Alliances" type="{http://www.iata.org/IATA/EDIST}AlliancePreferencesType" minOccurs="0"/&gt;
     *                   &lt;element name="Airlines" type="{http://www.iata.org/IATA/EDIST}AirlinePreferencesType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Metadata" type="{http://www.iata.org/IATA/EDIST}ShopProductMetadataType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "qualifiers",
        "locations",
        "preferences",
        "metadata"
    })
    public static class Query
        extends AssociatedObjectBaseType
    {

        @XmlElement(name = "Qualifiers")
        protected ShopProductRQ.Query.Qualifiers qualifiers;
        @XmlElement(name = "Locations")
        protected ShopProductRQ.Query.Locations locations;
        @XmlElement(name = "Preferences")
        protected ShopProductRQ.Query.Preferences preferences;
        @XmlElement(name = "Metadata")
        protected ShopProductMetadataType metadata;

        /**
         * 获取qualifiers属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ShopProductRQ.Query.Qualifiers }
         *     
         */
        public ShopProductRQ.Query.Qualifiers getQualifiers() {
            return qualifiers;
        }

        /**
         * 设置qualifiers属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ShopProductRQ.Query.Qualifiers }
         *     
         */
        public void setQualifiers(ShopProductRQ.Query.Qualifiers value) {
            this.qualifiers = value;
        }

        /**
         * 获取locations属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ShopProductRQ.Query.Locations }
         *     
         */
        public ShopProductRQ.Query.Locations getLocations() {
            return locations;
        }

        /**
         * 设置locations属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ShopProductRQ.Query.Locations }
         *     
         */
        public void setLocations(ShopProductRQ.Query.Locations value) {
            this.locations = value;
        }

        /**
         * 获取preferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ShopProductRQ.Query.Preferences }
         *     
         */
        public ShopProductRQ.Query.Preferences getPreferences() {
            return preferences;
        }

        /**
         * 设置preferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ShopProductRQ.Query.Preferences }
         *     
         */
        public void setPreferences(ShopProductRQ.Query.Preferences value) {
            this.preferences = value;
        }

        /**
         * 获取metadata属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ShopProductMetadataType }
         *     
         */
        public ShopProductMetadataType getMetadata() {
            return metadata;
        }

        /**
         * 设置metadata属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ShopProductMetadataType }
         *     
         */
        public void setMetadata(ShopProductMetadataType value) {
            this.metadata = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="LocationCode" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirportCitySimpleType"&gt;
         *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}RadiusAttrGroup"/&gt;
         *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "locationCode"
        })
        public static class Locations
            extends AssociatedObjectBaseType
        {

            @XmlElement(name = "LocationCode", required = true)
            protected List<ShopProductRQ.Query.Locations.LocationCode> locationCode;

            /**
             * Gets the value of the locationCode property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the locationCode property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getLocationCode().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ShopProductRQ.Query.Locations.LocationCode }
             * 
             * 
             */
            public List<ShopProductRQ.Query.Locations.LocationCode> getLocationCode() {
                if (locationCode == null) {
                    locationCode = new ArrayList<ShopProductRQ.Query.Locations.LocationCode>();
                }
                return this.locationCode;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirportCitySimpleType"&gt;
             *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}RadiusAttrGroup"/&gt;
             *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class LocationCode {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "Application")
                protected String application;
                @XmlAttribute(name = "Area")
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger area;
                @XmlAttribute(name = "UOM")
                protected DistanceUnitListType uom;
                @XmlAttribute(name = "PreferencesLevel")
                protected String preferencesLevel;
                @XmlAttribute(name = "PreferencesContext")
                protected String preferencesContext;

                /**
                 * Examples: JFK, NYC
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * 设置value属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * 获取application属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getApplication() {
                    return application;
                }

                /**
                 * 设置application属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setApplication(String value) {
                    this.application = value;
                }

                /**
                 * 获取area属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getArea() {
                    return area;
                }

                /**
                 * 设置area属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setArea(BigInteger value) {
                    this.area = value;
                }

                /**
                 * 获取uom属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link DistanceUnitListType }
                 *     
                 */
                public DistanceUnitListType getUOM() {
                    return uom;
                }

                /**
                 * 设置uom属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link DistanceUnitListType }
                 *     
                 */
                public void setUOM(DistanceUnitListType value) {
                    this.uom = value;
                }

                /**
                 * 获取preferencesLevel属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPreferencesLevel() {
                    return preferencesLevel;
                }

                /**
                 * 设置preferencesLevel属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPreferencesLevel(String value) {
                    this.preferencesLevel = value;
                }

                /**
                 * 获取preferencesContext属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPreferencesContext() {
                    return preferencesContext;
                }

                /**
                 * 设置preferencesContext属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPreferencesContext(String value) {
                    this.preferencesContext = value;
                }

            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Alliances" type="{http://www.iata.org/IATA/EDIST}AlliancePreferencesType" minOccurs="0"/&gt;
         *         &lt;element name="Airlines" type="{http://www.iata.org/IATA/EDIST}AirlinePreferencesType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "alliances",
            "airlines"
        })
        public static class Preferences {

            @XmlElement(name = "Alliances")
            protected AlliancePreferencesType alliances;
            @XmlElement(name = "Airlines")
            protected AirlinePreferencesType airlines;

            /**
             * 获取alliances属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AlliancePreferencesType }
             *     
             */
            public AlliancePreferencesType getAlliances() {
                return alliances;
            }

            /**
             * 设置alliances属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AlliancePreferencesType }
             *     
             */
            public void setAlliances(AlliancePreferencesType value) {
                this.alliances = value;
            }

            /**
             * 获取airlines属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AirlinePreferencesType }
             *     
             */
            public AirlinePreferencesType getAirlines() {
                return airlines;
            }

            /**
             * 设置airlines属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AirlinePreferencesType }
             *     
             */
            public void setAirlines(AirlinePreferencesType value) {
                this.airlines = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;choice&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
         *                 &lt;/choice&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "qualifier"
        })
        public static class Qualifiers {

            @XmlElement(name = "Qualifier", required = true)
            protected List<ShopProductRQ.Query.Qualifiers.Qualifier> qualifier;

            /**
             * Gets the value of the qualifier property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the qualifier property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getQualifier().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ShopProductRQ.Query.Qualifiers.Qualifier }
             * 
             * 
             */
            public List<ShopProductRQ.Query.Qualifiers.Qualifier> getQualifier() {
                if (qualifier == null) {
                    qualifier = new ArrayList<ShopProductRQ.Query.Qualifiers.Qualifier>();
                }
                return this.qualifier;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;choice&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
             *       &lt;/choice&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "baggagePricingQualifier",
                "existingOrderQualifier",
                "paymentCardQualifiers",
                "programQualifiers",
                "promotionQualifiers",
                "seatQualifier",
                "serviceQualifier",
                "socialMediaQualifiers",
                "specialFareQualifiers",
                "specialNeedQualifiers",
                "tripPurposeQualifier"
            })
            public static class Qualifier {

                @XmlElement(name = "BaggagePricingQualifier")
                protected BaggagePricingQualifierType baggagePricingQualifier;
                @XmlElement(name = "ExistingOrderQualifier")
                protected ExistingOrderQualifier existingOrderQualifier;
                @XmlElement(name = "PaymentCardQualifiers")
                protected CardQualifierType paymentCardQualifiers;
                @XmlElement(name = "ProgramQualifiers")
                protected ProgramQualifiers programQualifiers;
                @XmlElement(name = "PromotionQualifiers")
                protected PromotionQualifiers promotionQualifiers;
                @XmlElement(name = "SeatQualifier")
                protected SeatQualifier seatQualifier;
                @XmlElement(name = "ServiceQualifier")
                protected ServiceQualifierPriceType serviceQualifier;
                @XmlElement(name = "SocialMediaQualifiers")
                protected SocialQualiferType socialMediaQualifiers;
                @XmlElement(name = "SpecialFareQualifiers")
                protected FareQualifierType specialFareQualifiers;
                @XmlElement(name = "SpecialNeedQualifiers")
                protected SpecialQualiferType specialNeedQualifiers;
                @XmlElement(name = "TripPurposeQualifier")
                @XmlSchemaType(name = "string")
                protected TripPurposeListType tripPurposeQualifier;

                /**
                 * 获取baggagePricingQualifier属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link BaggagePricingQualifierType }
                 *     
                 */
                public BaggagePricingQualifierType getBaggagePricingQualifier() {
                    return baggagePricingQualifier;
                }

                /**
                 * 设置baggagePricingQualifier属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BaggagePricingQualifierType }
                 *     
                 */
                public void setBaggagePricingQualifier(BaggagePricingQualifierType value) {
                    this.baggagePricingQualifier = value;
                }

                /**
                 * 获取existingOrderQualifier属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ExistingOrderQualifier }
                 *     
                 */
                public ExistingOrderQualifier getExistingOrderQualifier() {
                    return existingOrderQualifier;
                }

                /**
                 * 设置existingOrderQualifier属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ExistingOrderQualifier }
                 *     
                 */
                public void setExistingOrderQualifier(ExistingOrderQualifier value) {
                    this.existingOrderQualifier = value;
                }

                /**
                 * 获取paymentCardQualifiers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CardQualifierType }
                 *     
                 */
                public CardQualifierType getPaymentCardQualifiers() {
                    return paymentCardQualifiers;
                }

                /**
                 * 设置paymentCardQualifiers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CardQualifierType }
                 *     
                 */
                public void setPaymentCardQualifiers(CardQualifierType value) {
                    this.paymentCardQualifiers = value;
                }

                /**
                 * 获取programQualifiers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ProgramQualifiers }
                 *     
                 */
                public ProgramQualifiers getProgramQualifiers() {
                    return programQualifiers;
                }

                /**
                 * 设置programQualifiers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ProgramQualifiers }
                 *     
                 */
                public void setProgramQualifiers(ProgramQualifiers value) {
                    this.programQualifiers = value;
                }

                /**
                 * 获取promotionQualifiers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link PromotionQualifiers }
                 *     
                 */
                public PromotionQualifiers getPromotionQualifiers() {
                    return promotionQualifiers;
                }

                /**
                 * 设置promotionQualifiers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PromotionQualifiers }
                 *     
                 */
                public void setPromotionQualifiers(PromotionQualifiers value) {
                    this.promotionQualifiers = value;
                }

                /**
                 * 获取seatQualifier属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link SeatQualifier }
                 *     
                 */
                public SeatQualifier getSeatQualifier() {
                    return seatQualifier;
                }

                /**
                 * 设置seatQualifier属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SeatQualifier }
                 *     
                 */
                public void setSeatQualifier(SeatQualifier value) {
                    this.seatQualifier = value;
                }

                /**
                 * 获取serviceQualifier属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ServiceQualifierPriceType }
                 *     
                 */
                public ServiceQualifierPriceType getServiceQualifier() {
                    return serviceQualifier;
                }

                /**
                 * 设置serviceQualifier属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ServiceQualifierPriceType }
                 *     
                 */
                public void setServiceQualifier(ServiceQualifierPriceType value) {
                    this.serviceQualifier = value;
                }

                /**
                 * 获取socialMediaQualifiers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link SocialQualiferType }
                 *     
                 */
                public SocialQualiferType getSocialMediaQualifiers() {
                    return socialMediaQualifiers;
                }

                /**
                 * 设置socialMediaQualifiers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SocialQualiferType }
                 *     
                 */
                public void setSocialMediaQualifiers(SocialQualiferType value) {
                    this.socialMediaQualifiers = value;
                }

                /**
                 * 获取specialFareQualifiers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link FareQualifierType }
                 *     
                 */
                public FareQualifierType getSpecialFareQualifiers() {
                    return specialFareQualifiers;
                }

                /**
                 * 设置specialFareQualifiers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link FareQualifierType }
                 *     
                 */
                public void setSpecialFareQualifiers(FareQualifierType value) {
                    this.specialFareQualifiers = value;
                }

                /**
                 * 获取specialNeedQualifiers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link SpecialQualiferType }
                 *     
                 */
                public SpecialQualiferType getSpecialNeedQualifiers() {
                    return specialNeedQualifiers;
                }

                /**
                 * 设置specialNeedQualifiers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SpecialQualiferType }
                 *     
                 */
                public void setSpecialNeedQualifiers(SpecialQualiferType value) {
                    this.specialNeedQualifiers = value;
                }

                /**
                 * 获取tripPurposeQualifier属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link TripPurposeListType }
                 *     
                 */
                public TripPurposeListType getTripPurposeQualifier() {
                    return tripPurposeQualifier;
                }

                /**
                 * 设置tripPurposeQualifier属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link TripPurposeListType }
                 *     
                 */
                public void setTripPurposeQualifier(TripPurposeListType value) {
                    this.tripPurposeQualifier = value;
                }

            }

        }

    }

}
