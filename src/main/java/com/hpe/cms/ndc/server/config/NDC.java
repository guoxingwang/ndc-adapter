package com.hpe.cms.ndc.server.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 12:23 2019/7/6
 * @ Description：
 * @ Modified By：
 */
@Component
@ConfigurationProperties(prefix = "ndc")
@Data
public class NDC {
    private String version;
    private String wsdlName = "NdcAdapter";   //统一wsdl的名称
}
