//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PointOfSale" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Document"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Party"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ItinReshopParameters" minOccurs="0"/&gt;
 *         &lt;element name="Query"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element name="RepriceOnly"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="OrderItems" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemCoreType"&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="RepriceAllInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Reshop"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Actions" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ActionType"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;simpleContent&gt;
 *                                             &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
 *                                               &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                                             &lt;/extension&gt;
 *                                           &lt;/simpleContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences" minOccurs="0"/&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderID" minOccurs="0"/&gt;
 *                                       &lt;element name="OfferItemSets" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="OfferItemSet" maxOccurs="unbounded"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="OfferItem" type="{http://www.iata.org/IATA/EDIST}OrderOfferItemType" maxOccurs="unbounded"/&gt;
 *                                                         &lt;/sequence&gt;
 *                                                         &lt;attribute name="SetItemQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *                                                       &lt;/extension&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="OrderItems" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemDetailType"&gt;
 *                                             &lt;/extension&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="TicketDocuments" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="TicketDocument" type="{http://www.iata.org/IATA/EDIST}TicketDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="Passengers" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}Passenger" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                                                 &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="Qualifiers" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;choice&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
 *                                                         &lt;/choice&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="Preferences" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="Preference" maxOccurs="unbounded"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;choice&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlinePreferences"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AlliancePreferences"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightPreferences"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}FarePreferences"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingMethodPreference"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}ServicePricingOnlyPreference"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}TransferPreferences"/&gt;
 *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}CabinPreferences"/&gt;
 *                                                         &lt;/choice&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}IATA_PayloadStdAttributes"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pointOfSale",
    "document",
    "party",
    "itinReshopParameters",
    "query"
})
@XmlRootElement(name = "ItinReshopRQ")
public class ItinReshopRQ {

    @XmlElement(name = "PointOfSale")
    protected PointOfSaleType pointOfSale;
    @XmlElement(name = "Document", required = true)
    protected MsgDocumentType document;
    @XmlElement(name = "Party", required = true)
    protected MsgPartiesType party;
    @XmlElement(name = "ItinReshopParameters")
    protected ItinReshopParamsType itinReshopParameters;
    @XmlElement(name = "Query", required = true)
    protected ItinReshopRQ.Query query;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String target;
    @XmlAttribute(name = "Version", required = true)
    protected String version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "AsynchronousAllowedInd")
    protected Boolean asynchronousAllowedInd;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;

    /**
     * 获取pointOfSale属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PointOfSaleType }
     *     
     */
    public PointOfSaleType getPointOfSale() {
        return pointOfSale;
    }

    /**
     * 设置pointOfSale属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PointOfSaleType }
     *     
     */
    public void setPointOfSale(PointOfSaleType value) {
        this.pointOfSale = value;
    }

    /**
     * 获取document属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgDocumentType }
     *     
     */
    public MsgDocumentType getDocument() {
        return document;
    }

    /**
     * 设置document属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgDocumentType }
     *     
     */
    public void setDocument(MsgDocumentType value) {
        this.document = value;
    }

    /**
     * 获取party属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgPartiesType }
     *     
     */
    public MsgPartiesType getParty() {
        return party;
    }

    /**
     * 设置party属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgPartiesType }
     *     
     */
    public void setParty(MsgPartiesType value) {
        this.party = value;
    }

    /**
     * 获取itinReshopParameters属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ItinReshopParamsType }
     *     
     */
    public ItinReshopParamsType getItinReshopParameters() {
        return itinReshopParameters;
    }

    /**
     * 设置itinReshopParameters属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ItinReshopParamsType }
     *     
     */
    public void setItinReshopParameters(ItinReshopParamsType value) {
        this.itinReshopParameters = value;
    }

    /**
     * 获取query属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ItinReshopRQ.Query }
     *     
     */
    public ItinReshopRQ.Query getQuery() {
        return query;
    }

    /**
     * 设置query属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ItinReshopRQ.Query }
     *     
     */
    public void setQuery(ItinReshopRQ.Query value) {
        this.query = value;
    }

    /**
     * 获取echoToken属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * 设置echoToken属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * 获取target属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        if (target == null) {
            return "Production";
        } else {
            return target;
        }
    }

    /**
     * 设置target属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取transactionIdentifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * 设置transactionIdentifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * 获取sequenceNmbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * 设置sequenceNmbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * 获取transactionStatusCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * 设置transactionStatusCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * 获取retransmissionIndicator属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * 设置retransmissionIndicator属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * 获取correlationID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * 设置correlationID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * 获取asynchronousAllowedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAsynchronousAllowedInd() {
        return asynchronousAllowedInd;
    }

    /**
     * 设置asynchronousAllowedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAsynchronousAllowedInd(Boolean value) {
        this.asynchronousAllowedInd = value;
    }

    /**
     * 获取altLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * 设置altLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * 获取primaryLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * 设置primaryLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="RepriceOnly"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="OrderItems" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemCoreType"&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="RepriceAllInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Reshop"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Actions" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ActionType"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;simpleContent&gt;
     *                                   &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
     *                                     &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *                                   &lt;/extension&gt;
     *                                 &lt;/simpleContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences" minOccurs="0"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderID" minOccurs="0"/&gt;
     *                             &lt;element name="OfferItemSets" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="OfferItemSet" maxOccurs="unbounded"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="OfferItem" type="{http://www.iata.org/IATA/EDIST}OrderOfferItemType" maxOccurs="unbounded"/&gt;
     *                                               &lt;/sequence&gt;
     *                                               &lt;attribute name="SetItemQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
     *                                             &lt;/extension&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="OrderItems" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemDetailType"&gt;
     *                                   &lt;/extension&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="TicketDocuments" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="TicketDocument" type="{http://www.iata.org/IATA/EDIST}TicketDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="Passengers" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}Passenger" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                                       &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="Qualifiers" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;choice&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
     *                                               &lt;/choice&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="Preferences" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Preference" maxOccurs="unbounded"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;choice&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlinePreferences"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AlliancePreferences"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightPreferences"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}FarePreferences"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingMethodPreference"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}ServicePricingOnlyPreference"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}TransferPreferences"/&gt;
     *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}CabinPreferences"/&gt;
     *                                               &lt;/choice&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "repriceOnly",
        "reshop"
    })
    public static class Query {

        @XmlElement(name = "RepriceOnly")
        protected ItinReshopRQ.Query.RepriceOnly repriceOnly;
        @XmlElement(name = "Reshop")
        protected ItinReshopRQ.Query.Reshop reshop;

        /**
         * 获取repriceOnly属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ItinReshopRQ.Query.RepriceOnly }
         *     
         */
        public ItinReshopRQ.Query.RepriceOnly getRepriceOnly() {
            return repriceOnly;
        }

        /**
         * 设置repriceOnly属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ItinReshopRQ.Query.RepriceOnly }
         *     
         */
        public void setRepriceOnly(ItinReshopRQ.Query.RepriceOnly value) {
            this.repriceOnly = value;
        }

        /**
         * 获取reshop属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ItinReshopRQ.Query.Reshop }
         *     
         */
        public ItinReshopRQ.Query.Reshop getReshop() {
            return reshop;
        }

        /**
         * 设置reshop属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ItinReshopRQ.Query.Reshop }
         *     
         */
        public void setReshop(ItinReshopRQ.Query.Reshop value) {
            this.reshop = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="OrderItems" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemCoreType"&gt;
         *               &lt;/extension&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="RepriceAllInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "orderItems"
        })
        public static class RepriceOnly {

            @XmlElement(name = "OrderItems")
            protected ItinReshopRQ.Query.RepriceOnly.OrderItems orderItems;
            @XmlAttribute(name = "RepriceAllInd")
            protected Boolean repriceAllInd;

            /**
             * 获取orderItems属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ItinReshopRQ.Query.RepriceOnly.OrderItems }
             *     
             */
            public ItinReshopRQ.Query.RepriceOnly.OrderItems getOrderItems() {
                return orderItems;
            }

            /**
             * 设置orderItems属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ItinReshopRQ.Query.RepriceOnly.OrderItems }
             *     
             */
            public void setOrderItems(ItinReshopRQ.Query.RepriceOnly.OrderItems value) {
                this.orderItems = value;
            }

            /**
             * 获取repriceAllInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRepriceAllInd() {
                return repriceAllInd;
            }

            /**
             * 设置repriceAllInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRepriceAllInd(Boolean value) {
                this.repriceAllInd = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemCoreType"&gt;
             *     &lt;/extension&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class OrderItems
                extends OrderItemCoreType
            {


            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Actions" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ActionType"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;simpleContent&gt;
         *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
         *                           &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *                         &lt;/extension&gt;
         *                       &lt;/simpleContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences" minOccurs="0"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderID" minOccurs="0"/&gt;
         *                   &lt;element name="OfferItemSets" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="OfferItemSet" maxOccurs="unbounded"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="OfferItem" type="{http://www.iata.org/IATA/EDIST}OrderOfferItemType" maxOccurs="unbounded"/&gt;
         *                                     &lt;/sequence&gt;
         *                                     &lt;attribute name="SetItemQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
         *                                   &lt;/extension&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="OrderItems" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemDetailType"&gt;
         *                         &lt;/extension&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="TicketDocuments" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="TicketDocument" type="{http://www.iata.org/IATA/EDIST}TicketDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="Passengers" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}Passenger" maxOccurs="unbounded" minOccurs="0"/&gt;
         *                             &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="Qualifiers" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;choice&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
         *                                     &lt;/choice&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="Preferences" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Preference" maxOccurs="unbounded"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;choice&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlinePreferences"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AlliancePreferences"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightPreferences"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}FarePreferences"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingMethodPreference"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}ServicePricingOnlyPreference"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}TransferPreferences"/&gt;
         *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}CabinPreferences"/&gt;
         *                                     &lt;/choice&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "actions"
        })
        public static class Reshop {

            @XmlElement(name = "Actions", required = true)
            protected List<ItinReshopRQ.Query.Reshop.Actions> actions;

            /**
             * Gets the value of the actions property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the actions property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getActions().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ItinReshopRQ.Query.Reshop.Actions }
             * 
             * 
             */
            public List<ItinReshopRQ.Query.Reshop.Actions> getActions() {
                if (actions == null) {
                    actions = new ArrayList<ItinReshopRQ.Query.Reshop.Actions>();
                }
                return this.actions;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ActionType"&gt;
             *           &lt;complexType&gt;
             *             &lt;simpleContent&gt;
             *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
             *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
             *               &lt;/extension&gt;
             *             &lt;/simpleContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences" minOccurs="0"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderID" minOccurs="0"/&gt;
             *         &lt;element name="OfferItemSets" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="OfferItemSet" maxOccurs="unbounded"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="OfferItem" type="{http://www.iata.org/IATA/EDIST}OrderOfferItemType" maxOccurs="unbounded"/&gt;
             *                           &lt;/sequence&gt;
             *                           &lt;attribute name="SetItemQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
             *                         &lt;/extension&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="OrderItems" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemDetailType"&gt;
             *               &lt;/extension&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="TicketDocuments" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="TicketDocument" type="{http://www.iata.org/IATA/EDIST}TicketDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="Passengers" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Passenger" maxOccurs="unbounded" minOccurs="0"/&gt;
             *                   &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="Qualifiers" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;choice&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
             *                           &lt;/choice&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="Preferences" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Preference" maxOccurs="unbounded"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;choice&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlinePreferences"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AlliancePreferences"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightPreferences"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}FarePreferences"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingMethodPreference"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ServicePricingOnlyPreference"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}TransferPreferences"/&gt;
             *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CabinPreferences"/&gt;
             *                           &lt;/choice&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "actionType",
                "bookingReferences",
                "orderID",
                "offerItemSets",
                "orderItems",
                "ticketDocuments",
                "passengers",
                "qualifiers",
                "preferences"
            })
            public static class Actions {

                @XmlElement(name = "ActionType", required = true)
                protected ItinReshopRQ.Query.Reshop.Actions.ActionType actionType;
                @XmlElement(name = "BookingReferences")
                protected BookingReferences bookingReferences;
                @XmlElement(name = "OrderID")
                protected OrderIDType orderID;
                @XmlElement(name = "OfferItemSets")
                protected ItinReshopRQ.Query.Reshop.Actions.OfferItemSets offerItemSets;
                @XmlElement(name = "OrderItems")
                protected ItinReshopRQ.Query.Reshop.Actions.OrderItems orderItems;
                @XmlElement(name = "TicketDocuments")
                protected ItinReshopRQ.Query.Reshop.Actions.TicketDocuments ticketDocuments;
                @XmlElement(name = "Passengers")
                protected ItinReshopRQ.Query.Reshop.Actions.Passengers passengers;
                @XmlElement(name = "Qualifiers")
                protected ItinReshopRQ.Query.Reshop.Actions.Qualifiers qualifiers;
                @XmlElement(name = "Preferences")
                protected ItinReshopRQ.Query.Reshop.Actions.Preferences preferences;

                /**
                 * 获取actionType属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.ActionType }
                 *     
                 */
                public ItinReshopRQ.Query.Reshop.Actions.ActionType getActionType() {
                    return actionType;
                }

                /**
                 * 设置actionType属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.ActionType }
                 *     
                 */
                public void setActionType(ItinReshopRQ.Query.Reshop.Actions.ActionType value) {
                    this.actionType = value;
                }

                /**
                 * Existing Booking Reference (e.g. PNR Record Locator). Example: R1A3SK
                 * 
                 * @return
                 *     possible object is
                 *     {@link BookingReferences }
                 *     
                 */
                public BookingReferences getBookingReferences() {
                    return bookingReferences;
                }

                /**
                 * 设置bookingReferences属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BookingReferences }
                 *     
                 */
                public void setBookingReferences(BookingReferences value) {
                    this.bookingReferences = value;
                }

                /**
                 * 获取orderID属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrderIDType }
                 *     
                 */
                public OrderIDType getOrderID() {
                    return orderID;
                }

                /**
                 * 设置orderID属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrderIDType }
                 *     
                 */
                public void setOrderID(OrderIDType value) {
                    this.orderID = value;
                }

                /**
                 * 获取offerItemSets属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.OfferItemSets }
                 *     
                 */
                public ItinReshopRQ.Query.Reshop.Actions.OfferItemSets getOfferItemSets() {
                    return offerItemSets;
                }

                /**
                 * 设置offerItemSets属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.OfferItemSets }
                 *     
                 */
                public void setOfferItemSets(ItinReshopRQ.Query.Reshop.Actions.OfferItemSets value) {
                    this.offerItemSets = value;
                }

                /**
                 * 获取orderItems属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.OrderItems }
                 *     
                 */
                public ItinReshopRQ.Query.Reshop.Actions.OrderItems getOrderItems() {
                    return orderItems;
                }

                /**
                 * 设置orderItems属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.OrderItems }
                 *     
                 */
                public void setOrderItems(ItinReshopRQ.Query.Reshop.Actions.OrderItems value) {
                    this.orderItems = value;
                }

                /**
                 * 获取ticketDocuments属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.TicketDocuments }
                 *     
                 */
                public ItinReshopRQ.Query.Reshop.Actions.TicketDocuments getTicketDocuments() {
                    return ticketDocuments;
                }

                /**
                 * 设置ticketDocuments属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.TicketDocuments }
                 *     
                 */
                public void setTicketDocuments(ItinReshopRQ.Query.Reshop.Actions.TicketDocuments value) {
                    this.ticketDocuments = value;
                }

                /**
                 * 获取passengers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.Passengers }
                 *     
                 */
                public ItinReshopRQ.Query.Reshop.Actions.Passengers getPassengers() {
                    return passengers;
                }

                /**
                 * 设置passengers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.Passengers }
                 *     
                 */
                public void setPassengers(ItinReshopRQ.Query.Reshop.Actions.Passengers value) {
                    this.passengers = value;
                }

                /**
                 * 获取qualifiers属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.Qualifiers }
                 *     
                 */
                public ItinReshopRQ.Query.Reshop.Actions.Qualifiers getQualifiers() {
                    return qualifiers;
                }

                /**
                 * 设置qualifiers属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.Qualifiers }
                 *     
                 */
                public void setQualifiers(ItinReshopRQ.Query.Reshop.Actions.Qualifiers value) {
                    this.qualifiers = value;
                }

                /**
                 * 获取preferences属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.Preferences }
                 *     
                 */
                public ItinReshopRQ.Query.Reshop.Actions.Preferences getPreferences() {
                    return preferences;
                }

                /**
                 * 设置preferences属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ItinReshopRQ.Query.Reshop.Actions.Preferences }
                 *     
                 */
                public void setPreferences(ItinReshopRQ.Query.Reshop.Actions.Preferences value) {
                    this.preferences = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;simpleContent&gt;
                 *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
                 *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
                 *     &lt;/extension&gt;
                 *   &lt;/simpleContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class ActionType {

                    @XmlValue
                    protected String value;
                    @XmlAttribute(name = "Context")
                    protected String context;

                    /**
                     * 
                     *         A data type for an Order Action. Examples: Create, Update, Delete, Reissue, Reprice
                     *       
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getValue() {
                        return value;
                    }

                    /**
                     * 设置value属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setValue(String value) {
                        this.value = value;
                    }

                    /**
                     * 获取context属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getContext() {
                        return context;
                    }

                    /**
                     * 设置context属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setContext(String value) {
                        this.context = value;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="OfferItemSet" maxOccurs="unbounded"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="OfferItem" type="{http://www.iata.org/IATA/EDIST}OrderOfferItemType" maxOccurs="unbounded"/&gt;
                 *                 &lt;/sequence&gt;
                 *                 &lt;attribute name="SetItemQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
                 *               &lt;/extension&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "offerItemSet"
                })
                public static class OfferItemSets {

                    @XmlElement(name = "OfferItemSet", required = true)
                    protected List<ItinReshopRQ.Query.Reshop.Actions.OfferItemSets.OfferItemSet> offerItemSet;

                    /**
                     * Gets the value of the offerItemSet property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the offerItemSet property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getOfferItemSet().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItinReshopRQ.Query.Reshop.Actions.OfferItemSets.OfferItemSet }
                     * 
                     * 
                     */
                    public List<ItinReshopRQ.Query.Reshop.Actions.OfferItemSets.OfferItemSet> getOfferItemSet() {
                        if (offerItemSet == null) {
                            offerItemSet = new ArrayList<ItinReshopRQ.Query.Reshop.Actions.OfferItemSets.OfferItemSet>();
                        }
                        return this.offerItemSet;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="OfferItem" type="{http://www.iata.org/IATA/EDIST}OrderOfferItemType" maxOccurs="unbounded"/&gt;
                     *       &lt;/sequence&gt;
                     *       &lt;attribute name="SetItemQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
                     *     &lt;/extension&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "offerItem"
                    })
                    public static class OfferItemSet
                        extends KeyWithMetaObjectBaseType
                    {

                        @XmlElement(name = "OfferItem", required = true)
                        protected List<OrderOfferItemType> offerItem;
                        @XmlAttribute(name = "SetItemQuantity")
                        @XmlSchemaType(name = "positiveInteger")
                        protected BigInteger setItemQuantity;

                        /**
                         * Gets the value of the offerItem property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the offerItem property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getOfferItem().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link OrderOfferItemType }
                         * 
                         * 
                         */
                        public List<OrderOfferItemType> getOfferItem() {
                            if (offerItem == null) {
                                offerItem = new ArrayList<OrderOfferItemType>();
                            }
                            return this.offerItem;
                        }

                        /**
                         * 获取setItemQuantity属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getSetItemQuantity() {
                            return setItemQuantity;
                        }

                        /**
                         * 设置setItemQuantity属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setSetItemQuantity(BigInteger value) {
                            this.setItemQuantity = value;
                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}OrderItemDetailType"&gt;
                 *     &lt;/extension&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class OrderItems
                    extends OrderItemDetailType
                {


                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Passenger" maxOccurs="unbounded" minOccurs="0"/&gt;
                 *         &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "passenger",
                    "group"
                })
                public static class Passengers {

                    @XmlElement(name = "Passenger")
                    protected List<Passenger> passenger;
                    @XmlElement(name = "Group")
                    protected GroupType group;

                    /**
                     * Gets the value of the passenger property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the passenger property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getPassenger().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link Passenger }
                     * 
                     * 
                     */
                    public List<Passenger> getPassenger() {
                        if (passenger == null) {
                            passenger = new ArrayList<Passenger>();
                        }
                        return this.passenger;
                    }

                    /**
                     * 获取group属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link GroupType }
                     *     
                     */
                    public GroupType getGroup() {
                        return group;
                    }

                    /**
                     * 设置group属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link GroupType }
                     *     
                     */
                    public void setGroup(GroupType value) {
                        this.group = value;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Preference" maxOccurs="unbounded"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;choice&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlinePreferences"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AlliancePreferences"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightPreferences"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FarePreferences"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingMethodPreference"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServicePricingOnlyPreference"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TransferPreferences"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CabinPreferences"/&gt;
                 *                 &lt;/choice&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "preference"
                })
                public static class Preferences {

                    @XmlElement(name = "Preference", required = true)
                    protected List<ItinReshopRQ.Query.Reshop.Actions.Preferences.Preference> preference;

                    /**
                     * Gets the value of the preference property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the preference property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getPreference().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItinReshopRQ.Query.Reshop.Actions.Preferences.Preference }
                     * 
                     * 
                     */
                    public List<ItinReshopRQ.Query.Reshop.Actions.Preferences.Preference> getPreference() {
                        if (preference == null) {
                            preference = new ArrayList<ItinReshopRQ.Query.Reshop.Actions.Preferences.Preference>();
                        }
                        return this.preference;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;choice&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlinePreferences"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AlliancePreferences"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightPreferences"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FarePreferences"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PricingMethodPreference"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServicePricingOnlyPreference"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TransferPreferences"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CabinPreferences"/&gt;
                     *       &lt;/choice&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "airlinePreferences",
                        "alliancePreferences",
                        "flightPreferences",
                        "farePreferences",
                        "pricingMethodPreference",
                        "segMaxTimePreferences",
                        "servicePricingOnlyPreference",
                        "transferPreferences",
                        "cabinPreferences"
                    })
                    public static class Preference {

                        @XmlElement(name = "AirlinePreferences")
                        protected AirlinePreferencesType airlinePreferences;
                        @XmlElement(name = "AlliancePreferences")
                        protected AlliancePreferencesType alliancePreferences;
                        @XmlElement(name = "FlightPreferences")
                        protected FlightPreferencesType flightPreferences;
                        @XmlElement(name = "FarePreferences")
                        protected FarePreferencesType farePreferences;
                        @XmlElement(name = "PricingMethodPreference")
                        protected BestPricingPreferencesType pricingMethodPreference;
                        @XmlElement(name = "SegMaxTimePreferences")
                        protected FltSegmentMaxTimePreferencesType segMaxTimePreferences;
                        @XmlElement(name = "ServicePricingOnlyPreference")
                        protected ServicePricingOnlyPreference servicePricingOnlyPreference;
                        @XmlElement(name = "TransferPreferences")
                        protected TransferPreferencesType transferPreferences;
                        @XmlElement(name = "CabinPreferences")
                        protected CabinPreferencesType cabinPreferences;

                        /**
                         * 获取airlinePreferences属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AirlinePreferencesType }
                         *     
                         */
                        public AirlinePreferencesType getAirlinePreferences() {
                            return airlinePreferences;
                        }

                        /**
                         * 设置airlinePreferences属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AirlinePreferencesType }
                         *     
                         */
                        public void setAirlinePreferences(AirlinePreferencesType value) {
                            this.airlinePreferences = value;
                        }

                        /**
                         * 获取alliancePreferences属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link AlliancePreferencesType }
                         *     
                         */
                        public AlliancePreferencesType getAlliancePreferences() {
                            return alliancePreferences;
                        }

                        /**
                         * 设置alliancePreferences属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link AlliancePreferencesType }
                         *     
                         */
                        public void setAlliancePreferences(AlliancePreferencesType value) {
                            this.alliancePreferences = value;
                        }

                        /**
                         * 获取flightPreferences属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link FlightPreferencesType }
                         *     
                         */
                        public FlightPreferencesType getFlightPreferences() {
                            return flightPreferences;
                        }

                        /**
                         * 设置flightPreferences属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link FlightPreferencesType }
                         *     
                         */
                        public void setFlightPreferences(FlightPreferencesType value) {
                            this.flightPreferences = value;
                        }

                        /**
                         * 获取farePreferences属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link FarePreferencesType }
                         *     
                         */
                        public FarePreferencesType getFarePreferences() {
                            return farePreferences;
                        }

                        /**
                         * 设置farePreferences属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link FarePreferencesType }
                         *     
                         */
                        public void setFarePreferences(FarePreferencesType value) {
                            this.farePreferences = value;
                        }

                        /**
                         * 获取pricingMethodPreference属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link BestPricingPreferencesType }
                         *     
                         */
                        public BestPricingPreferencesType getPricingMethodPreference() {
                            return pricingMethodPreference;
                        }

                        /**
                         * 设置pricingMethodPreference属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BestPricingPreferencesType }
                         *     
                         */
                        public void setPricingMethodPreference(BestPricingPreferencesType value) {
                            this.pricingMethodPreference = value;
                        }

                        /**
                         * 获取segMaxTimePreferences属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link FltSegmentMaxTimePreferencesType }
                         *     
                         */
                        public FltSegmentMaxTimePreferencesType getSegMaxTimePreferences() {
                            return segMaxTimePreferences;
                        }

                        /**
                         * 设置segMaxTimePreferences属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link FltSegmentMaxTimePreferencesType }
                         *     
                         */
                        public void setSegMaxTimePreferences(FltSegmentMaxTimePreferencesType value) {
                            this.segMaxTimePreferences = value;
                        }

                        /**
                         * 获取servicePricingOnlyPreference属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ServicePricingOnlyPreference }
                         *     
                         */
                        public ServicePricingOnlyPreference getServicePricingOnlyPreference() {
                            return servicePricingOnlyPreference;
                        }

                        /**
                         * 设置servicePricingOnlyPreference属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ServicePricingOnlyPreference }
                         *     
                         */
                        public void setServicePricingOnlyPreference(ServicePricingOnlyPreference value) {
                            this.servicePricingOnlyPreference = value;
                        }

                        /**
                         * 获取transferPreferences属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link TransferPreferencesType }
                         *     
                         */
                        public TransferPreferencesType getTransferPreferences() {
                            return transferPreferences;
                        }

                        /**
                         * 设置transferPreferences属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link TransferPreferencesType }
                         *     
                         */
                        public void setTransferPreferences(TransferPreferencesType value) {
                            this.transferPreferences = value;
                        }

                        /**
                         * 获取cabinPreferences属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link CabinPreferencesType }
                         *     
                         */
                        public CabinPreferencesType getCabinPreferences() {
                            return cabinPreferences;
                        }

                        /**
                         * 设置cabinPreferences属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link CabinPreferencesType }
                         *     
                         */
                        public void setCabinPreferences(CabinPreferencesType value) {
                            this.cabinPreferences = value;
                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Qualifier" maxOccurs="unbounded"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;choice&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
                 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
                 *                 &lt;/choice&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "qualifier"
                })
                public static class Qualifiers {

                    @XmlElement(name = "Qualifier", required = true)
                    protected List<ItinReshopRQ.Query.Reshop.Actions.Qualifiers.Qualifier> qualifier;

                    /**
                     * Gets the value of the qualifier property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the qualifier property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getQualifier().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ItinReshopRQ.Query.Reshop.Actions.Qualifiers.Qualifier }
                     * 
                     * 
                     */
                    public List<ItinReshopRQ.Query.Reshop.Actions.Qualifiers.Qualifier> getQualifier() {
                        if (qualifier == null) {
                            qualifier = new ArrayList<ItinReshopRQ.Query.Reshop.Actions.Qualifiers.Qualifier>();
                        }
                        return this.qualifier;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;choice&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggagePricingQualifier"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ExistingOrderQualifier"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PaymentCardQualifiers"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramQualifiers"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PromotionQualifiers"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatQualifier"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceQualifier"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SocialMediaQualifiers"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialFareQualifiers"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SpecialNeedQualifiers"/&gt;
                     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TripPurposeQualifier"/&gt;
                     *       &lt;/choice&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "baggagePricingQualifier",
                        "existingOrderQualifier",
                        "paymentCardQualifiers",
                        "programQualifiers",
                        "promotionQualifiers",
                        "seatQualifier",
                        "serviceQualifier",
                        "socialMediaQualifiers",
                        "specialFareQualifiers",
                        "specialNeedQualifiers",
                        "tripPurposeQualifier"
                    })
                    public static class Qualifier {

                        @XmlElement(name = "BaggagePricingQualifier")
                        protected BaggagePricingQualifierType baggagePricingQualifier;
                        @XmlElement(name = "ExistingOrderQualifier")
                        protected ExistingOrderQualifier existingOrderQualifier;
                        @XmlElement(name = "PaymentCardQualifiers")
                        protected CardQualifierType paymentCardQualifiers;
                        @XmlElement(name = "ProgramQualifiers")
                        protected ProgramQualifiers programQualifiers;
                        @XmlElement(name = "PromotionQualifiers")
                        protected PromotionQualifiers promotionQualifiers;
                        @XmlElement(name = "SeatQualifier")
                        protected SeatQualifier seatQualifier;
                        @XmlElement(name = "ServiceQualifier")
                        protected ServiceQualifierPriceType serviceQualifier;
                        @XmlElement(name = "SocialMediaQualifiers")
                        protected SocialQualiferType socialMediaQualifiers;
                        @XmlElement(name = "SpecialFareQualifiers")
                        protected FareQualifierType specialFareQualifiers;
                        @XmlElement(name = "SpecialNeedQualifiers")
                        protected SpecialQualiferType specialNeedQualifiers;
                        @XmlElement(name = "TripPurposeQualifier")
                        @XmlSchemaType(name = "string")
                        protected TripPurposeListType tripPurposeQualifier;

                        /**
                         * 获取baggagePricingQualifier属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link BaggagePricingQualifierType }
                         *     
                         */
                        public BaggagePricingQualifierType getBaggagePricingQualifier() {
                            return baggagePricingQualifier;
                        }

                        /**
                         * 设置baggagePricingQualifier属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BaggagePricingQualifierType }
                         *     
                         */
                        public void setBaggagePricingQualifier(BaggagePricingQualifierType value) {
                            this.baggagePricingQualifier = value;
                        }

                        /**
                         * 获取existingOrderQualifier属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ExistingOrderQualifier }
                         *     
                         */
                        public ExistingOrderQualifier getExistingOrderQualifier() {
                            return existingOrderQualifier;
                        }

                        /**
                         * 设置existingOrderQualifier属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ExistingOrderQualifier }
                         *     
                         */
                        public void setExistingOrderQualifier(ExistingOrderQualifier value) {
                            this.existingOrderQualifier = value;
                        }

                        /**
                         * 获取paymentCardQualifiers属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link CardQualifierType }
                         *     
                         */
                        public CardQualifierType getPaymentCardQualifiers() {
                            return paymentCardQualifiers;
                        }

                        /**
                         * 设置paymentCardQualifiers属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link CardQualifierType }
                         *     
                         */
                        public void setPaymentCardQualifiers(CardQualifierType value) {
                            this.paymentCardQualifiers = value;
                        }

                        /**
                         * 获取programQualifiers属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ProgramQualifiers }
                         *     
                         */
                        public ProgramQualifiers getProgramQualifiers() {
                            return programQualifiers;
                        }

                        /**
                         * 设置programQualifiers属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ProgramQualifiers }
                         *     
                         */
                        public void setProgramQualifiers(ProgramQualifiers value) {
                            this.programQualifiers = value;
                        }

                        /**
                         * 获取promotionQualifiers属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link PromotionQualifiers }
                         *     
                         */
                        public PromotionQualifiers getPromotionQualifiers() {
                            return promotionQualifiers;
                        }

                        /**
                         * 设置promotionQualifiers属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link PromotionQualifiers }
                         *     
                         */
                        public void setPromotionQualifiers(PromotionQualifiers value) {
                            this.promotionQualifiers = value;
                        }

                        /**
                         * 获取seatQualifier属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link SeatQualifier }
                         *     
                         */
                        public SeatQualifier getSeatQualifier() {
                            return seatQualifier;
                        }

                        /**
                         * 设置seatQualifier属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link SeatQualifier }
                         *     
                         */
                        public void setSeatQualifier(SeatQualifier value) {
                            this.seatQualifier = value;
                        }

                        /**
                         * 获取serviceQualifier属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ServiceQualifierPriceType }
                         *     
                         */
                        public ServiceQualifierPriceType getServiceQualifier() {
                            return serviceQualifier;
                        }

                        /**
                         * 设置serviceQualifier属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ServiceQualifierPriceType }
                         *     
                         */
                        public void setServiceQualifier(ServiceQualifierPriceType value) {
                            this.serviceQualifier = value;
                        }

                        /**
                         * 获取socialMediaQualifiers属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link SocialQualiferType }
                         *     
                         */
                        public SocialQualiferType getSocialMediaQualifiers() {
                            return socialMediaQualifiers;
                        }

                        /**
                         * 设置socialMediaQualifiers属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link SocialQualiferType }
                         *     
                         */
                        public void setSocialMediaQualifiers(SocialQualiferType value) {
                            this.socialMediaQualifiers = value;
                        }

                        /**
                         * 获取specialFareQualifiers属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link FareQualifierType }
                         *     
                         */
                        public FareQualifierType getSpecialFareQualifiers() {
                            return specialFareQualifiers;
                        }

                        /**
                         * 设置specialFareQualifiers属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link FareQualifierType }
                         *     
                         */
                        public void setSpecialFareQualifiers(FareQualifierType value) {
                            this.specialFareQualifiers = value;
                        }

                        /**
                         * 获取specialNeedQualifiers属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link SpecialQualiferType }
                         *     
                         */
                        public SpecialQualiferType getSpecialNeedQualifiers() {
                            return specialNeedQualifiers;
                        }

                        /**
                         * 设置specialNeedQualifiers属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link SpecialQualiferType }
                         *     
                         */
                        public void setSpecialNeedQualifiers(SpecialQualiferType value) {
                            this.specialNeedQualifiers = value;
                        }

                        /**
                         * 获取tripPurposeQualifier属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link TripPurposeListType }
                         *     
                         */
                        public TripPurposeListType getTripPurposeQualifier() {
                            return tripPurposeQualifier;
                        }

                        /**
                         * 设置tripPurposeQualifier属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link TripPurposeListType }
                         *     
                         */
                        public void setTripPurposeQualifier(TripPurposeListType value) {
                            this.tripPurposeQualifier = value;
                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="TicketDocument" type="{http://www.iata.org/IATA/EDIST}TicketDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "ticketDocument"
                })
                public static class TicketDocuments {

                    @XmlElement(name = "TicketDocument")
                    protected List<TicketDocumentType> ticketDocument;

                    /**
                     * Gets the value of the ticketDocument property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the ticketDocument property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getTicketDocument().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link TicketDocumentType }
                     * 
                     * 
                     */
                    public List<TicketDocumentType> getTicketDocument() {
                        if (ticketDocument == null) {
                            ticketDocument = new ArrayList<TicketDocumentType>();
                        }
                        return this.ticketDocument;
                    }

                }

            }

        }

    }

}
