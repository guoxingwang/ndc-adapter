//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Common NDC Data List choices.
 * 
 * <p>DataListType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="DataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AnonymousTravelerList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}RecognizedTravelerList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BagDisclosureList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CarryOnAllowanceList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CheckedBagAllowanceList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ClassOfServiceList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ContentSourceList" minOccurs="0"/&gt;
 *         &lt;element name="DescriptionList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Description"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DisclosureList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FareList" minOccurs="0"/&gt;
 *         &lt;element name="FlightSegmentList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="FlightSegment" type="{http://www.iata.org/IATA/EDIST}ListOfFlightSegmentType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FlightList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Flight" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Journey" type="{http://www.iata.org/IATA/EDIST}TotalJourneyType" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SegmentReferences"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                           &lt;attribute name="FlightKey" type="{http://www.iata.org/IATA/EDIST}KeyIdentifier" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="OriginDestinationList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginDestination" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}InstructionsList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MediaList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PenaltyList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceBundleList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TermsList" minOccurs="0"/&gt;
 *         &lt;element name="SeatList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Seats" type="{http://www.iata.org/IATA/EDIST}ListOfSeatType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataListType", propOrder = {
    "anonymousTravelerList",
    "recognizedTravelerList",
    "bagDisclosureList",
    "carryOnAllowanceList",
    "checkedBagAllowanceList",
    "classOfServiceList",
    "contentSourceList",
    "descriptionList",
    "disclosureList",
    "fareList",
    "flightSegmentList",
    "flightList",
    "originDestinationList",
    "instructionsList",
    "mediaList",
    "penaltyList",
    "priceClassList",
    "serviceBundleList",
    "serviceList",
    "termsList",
    "seatList"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.AirShoppingRS.DataLists.class,
    com.hpe.cms.ndc.server.convertor.model.FlightPriceRQ.DataLists.class,
    com.hpe.cms.ndc.server.convertor.model.InvGuaranteeRQ.DataLists.class
})
public class DataListType {

    @XmlElement(name = "AnonymousTravelerList")
    protected AnonymousTravelerList anonymousTravelerList;
    @XmlElement(name = "RecognizedTravelerList")
    protected RecognizedTravelerList recognizedTravelerList;
    @XmlElement(name = "BagDisclosureList")
    protected ListOfBagDisclosureType bagDisclosureList;
    @XmlElement(name = "CarryOnAllowanceList")
    protected ListOfCarryOnAllowanceType carryOnAllowanceList;
    @XmlElement(name = "CheckedBagAllowanceList")
    protected ListOfBagAllowanceType checkedBagAllowanceList;
    @XmlElement(name = "ClassOfServiceList")
    protected ListOfClassOfServiceType classOfServiceList;
    @XmlElement(name = "ContentSourceList")
    protected ListOfContentSourceType contentSourceList;
    @XmlElement(name = "DescriptionList")
    protected DataListType.DescriptionList descriptionList;
    @XmlElement(name = "DisclosureList")
    protected ListOfDisclosureType disclosureList;
    @XmlElement(name = "FareList")
    protected FareList fareList;
    @XmlElement(name = "FlightSegmentList")
    protected DataListType.FlightSegmentList flightSegmentList;
    @XmlElement(name = "FlightList")
    protected DataListType.FlightList flightList;
    @XmlElement(name = "OriginDestinationList")
    protected DataListType.OriginDestinationList originDestinationList;
    @XmlElement(name = "InstructionsList")
    protected ListOfOfferInstructionsType instructionsList;
    @XmlElement(name = "MediaList")
    protected ListOfMediaType mediaList;
    @XmlElement(name = "PenaltyList")
    protected ListOfOfferPenaltyType penaltyList;
    @XmlElement(name = "PriceClassList")
    protected ListOfPriceClassType priceClassList;
    @XmlElement(name = "ServiceBundleList")
    protected ListOfServiceBundleType serviceBundleList;
    @XmlElement(name = "ServiceList")
    protected ServiceList serviceList;
    @XmlElement(name = "TermsList")
    protected ListOfOfferTermsType termsList;
    @XmlElement(name = "SeatList")
    protected DataListType.SeatList seatList;

    /**
     * 获取anonymousTravelerList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AnonymousTravelerList }
     *     
     */
    public AnonymousTravelerList getAnonymousTravelerList() {
        return anonymousTravelerList;
    }

    /**
     * 设置anonymousTravelerList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AnonymousTravelerList }
     *     
     */
    public void setAnonymousTravelerList(AnonymousTravelerList value) {
        this.anonymousTravelerList = value;
    }

    /**
     * 获取recognizedTravelerList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link RecognizedTravelerList }
     *     
     */
    public RecognizedTravelerList getRecognizedTravelerList() {
        return recognizedTravelerList;
    }

    /**
     * 设置recognizedTravelerList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link RecognizedTravelerList }
     *     
     */
    public void setRecognizedTravelerList(RecognizedTravelerList value) {
        this.recognizedTravelerList = value;
    }

    /**
     * 获取bagDisclosureList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfBagDisclosureType }
     *     
     */
    public ListOfBagDisclosureType getBagDisclosureList() {
        return bagDisclosureList;
    }

    /**
     * 设置bagDisclosureList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfBagDisclosureType }
     *     
     */
    public void setBagDisclosureList(ListOfBagDisclosureType value) {
        this.bagDisclosureList = value;
    }

    /**
     * 获取carryOnAllowanceList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfCarryOnAllowanceType }
     *     
     */
    public ListOfCarryOnAllowanceType getCarryOnAllowanceList() {
        return carryOnAllowanceList;
    }

    /**
     * 设置carryOnAllowanceList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfCarryOnAllowanceType }
     *     
     */
    public void setCarryOnAllowanceList(ListOfCarryOnAllowanceType value) {
        this.carryOnAllowanceList = value;
    }

    /**
     * 获取checkedBagAllowanceList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfBagAllowanceType }
     *     
     */
    public ListOfBagAllowanceType getCheckedBagAllowanceList() {
        return checkedBagAllowanceList;
    }

    /**
     * 设置checkedBagAllowanceList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfBagAllowanceType }
     *     
     */
    public void setCheckedBagAllowanceList(ListOfBagAllowanceType value) {
        this.checkedBagAllowanceList = value;
    }

    /**
     * 获取classOfServiceList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfClassOfServiceType }
     *     
     */
    public ListOfClassOfServiceType getClassOfServiceList() {
        return classOfServiceList;
    }

    /**
     * 设置classOfServiceList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfClassOfServiceType }
     *     
     */
    public void setClassOfServiceList(ListOfClassOfServiceType value) {
        this.classOfServiceList = value;
    }

    /**
     * 获取contentSourceList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfContentSourceType }
     *     
     */
    public ListOfContentSourceType getContentSourceList() {
        return contentSourceList;
    }

    /**
     * 设置contentSourceList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfContentSourceType }
     *     
     */
    public void setContentSourceList(ListOfContentSourceType value) {
        this.contentSourceList = value;
    }

    /**
     * 获取descriptionList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DataListType.DescriptionList }
     *     
     */
    public DataListType.DescriptionList getDescriptionList() {
        return descriptionList;
    }

    /**
     * 设置descriptionList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DataListType.DescriptionList }
     *     
     */
    public void setDescriptionList(DataListType.DescriptionList value) {
        this.descriptionList = value;
    }

    /**
     * 获取disclosureList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfDisclosureType }
     *     
     */
    public ListOfDisclosureType getDisclosureList() {
        return disclosureList;
    }

    /**
     * 设置disclosureList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfDisclosureType }
     *     
     */
    public void setDisclosureList(ListOfDisclosureType value) {
        this.disclosureList = value;
    }

    /**
     * 获取fareList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FareList }
     *     
     */
    public FareList getFareList() {
        return fareList;
    }

    /**
     * 设置fareList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FareList }
     *     
     */
    public void setFareList(FareList value) {
        this.fareList = value;
    }

    /**
     * 获取flightSegmentList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DataListType.FlightSegmentList }
     *     
     */
    public DataListType.FlightSegmentList getFlightSegmentList() {
        return flightSegmentList;
    }

    /**
     * 设置flightSegmentList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DataListType.FlightSegmentList }
     *     
     */
    public void setFlightSegmentList(DataListType.FlightSegmentList value) {
        this.flightSegmentList = value;
    }

    /**
     * 获取flightList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DataListType.FlightList }
     *     
     */
    public DataListType.FlightList getFlightList() {
        return flightList;
    }

    /**
     * 设置flightList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DataListType.FlightList }
     *     
     */
    public void setFlightList(DataListType.FlightList value) {
        this.flightList = value;
    }

    /**
     * 获取originDestinationList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DataListType.OriginDestinationList }
     *     
     */
    public DataListType.OriginDestinationList getOriginDestinationList() {
        return originDestinationList;
    }

    /**
     * 设置originDestinationList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DataListType.OriginDestinationList }
     *     
     */
    public void setOriginDestinationList(DataListType.OriginDestinationList value) {
        this.originDestinationList = value;
    }

    /**
     * 获取instructionsList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfOfferInstructionsType }
     *     
     */
    public ListOfOfferInstructionsType getInstructionsList() {
        return instructionsList;
    }

    /**
     * 设置instructionsList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfOfferInstructionsType }
     *     
     */
    public void setInstructionsList(ListOfOfferInstructionsType value) {
        this.instructionsList = value;
    }

    /**
     * 获取mediaList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfMediaType }
     *     
     */
    public ListOfMediaType getMediaList() {
        return mediaList;
    }

    /**
     * 设置mediaList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfMediaType }
     *     
     */
    public void setMediaList(ListOfMediaType value) {
        this.mediaList = value;
    }

    /**
     * 获取penaltyList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfOfferPenaltyType }
     *     
     */
    public ListOfOfferPenaltyType getPenaltyList() {
        return penaltyList;
    }

    /**
     * 设置penaltyList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfOfferPenaltyType }
     *     
     */
    public void setPenaltyList(ListOfOfferPenaltyType value) {
        this.penaltyList = value;
    }

    /**
     * 获取priceClassList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfPriceClassType }
     *     
     */
    public ListOfPriceClassType getPriceClassList() {
        return priceClassList;
    }

    /**
     * 设置priceClassList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfPriceClassType }
     *     
     */
    public void setPriceClassList(ListOfPriceClassType value) {
        this.priceClassList = value;
    }

    /**
     * 获取serviceBundleList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfServiceBundleType }
     *     
     */
    public ListOfServiceBundleType getServiceBundleList() {
        return serviceBundleList;
    }

    /**
     * 设置serviceBundleList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfServiceBundleType }
     *     
     */
    public void setServiceBundleList(ListOfServiceBundleType value) {
        this.serviceBundleList = value;
    }

    /**
     * 获取serviceList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ServiceList }
     *     
     */
    public ServiceList getServiceList() {
        return serviceList;
    }

    /**
     * 设置serviceList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceList }
     *     
     */
    public void setServiceList(ServiceList value) {
        this.serviceList = value;
    }

    /**
     * 获取termsList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ListOfOfferTermsType }
     *     
     */
    public ListOfOfferTermsType getTermsList() {
        return termsList;
    }

    /**
     * 设置termsList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfOfferTermsType }
     *     
     */
    public void setTermsList(ListOfOfferTermsType value) {
        this.termsList = value;
    }

    /**
     * 获取seatList属性的值。
     * 
     * @return
     *     possible object is
     *     {@link DataListType.SeatList }
     *     
     */
    public DataListType.SeatList getSeatList() {
        return seatList;
    }

    /**
     * 设置seatList属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DataListType.SeatList }
     *     
     */
    public void setSeatList(DataListType.SeatList value) {
        this.seatList = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Description"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "description"
    })
    public static class DescriptionList {

        @XmlElement(name = "Description", required = true)
        protected Description description;

        /**
         * 获取description属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Description }
         *     
         */
        public Description getDescription() {
            return description;
        }

        /**
         * 设置description属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Description }
         *     
         */
        public void setDescription(Description value) {
            this.description = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Flight" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Journey" type="{http://www.iata.org/IATA/EDIST}TotalJourneyType" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SegmentReferences"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *                 &lt;attribute name="FlightKey" type="{http://www.iata.org/IATA/EDIST}KeyIdentifier" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "flight"
    })
    public static class FlightList {

        @XmlElement(name = "Flight", required = true)
        protected List<DataListType.FlightList.Flight> flight;

        /**
         * Gets the value of the flight property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the flight property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFlight().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DataListType.FlightList.Flight }
         * 
         * 
         */
        public List<DataListType.FlightList.Flight> getFlight() {
            if (flight == null) {
                flight = new ArrayList<DataListType.FlightList.Flight>();
            }
            return this.flight;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Journey" type="{http://www.iata.org/IATA/EDIST}TotalJourneyType" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SegmentReferences"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *       &lt;attribute name="FlightKey" type="{http://www.iata.org/IATA/EDIST}KeyIdentifier" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "journey",
            "segmentReferences"
        })
        public static class Flight {

            @XmlElement(name = "Journey")
            protected TotalJourneyType journey;
            @XmlElement(name = "SegmentReferences", required = true)
            protected SegmentReferences segmentReferences;
            @XmlAttribute(name = "FlightKey")
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            @XmlID
            protected String flightKey;
            @XmlAttribute(name = "refs")
            @XmlIDREF
            protected List<Object> refs;

            /**
             * 获取journey属性的值。
             * 
             * @return
             *     possible object is
             *     {@link TotalJourneyType }
             *     
             */
            public TotalJourneyType getJourney() {
                return journey;
            }

            /**
             * 设置journey属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link TotalJourneyType }
             *     
             */
            public void setJourney(TotalJourneyType value) {
                this.journey = value;
            }

            /**
             * 获取segmentReferences属性的值。
             * 
             * @return
             *     possible object is
             *     {@link SegmentReferences }
             *     
             */
            public SegmentReferences getSegmentReferences() {
                return segmentReferences;
            }

            /**
             * 设置segmentReferences属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link SegmentReferences }
             *     
             */
            public void setSegmentReferences(SegmentReferences value) {
                this.segmentReferences = value;
            }

            /**
             * 获取flightKey属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFlightKey() {
                return flightKey;
            }

            /**
             * 设置flightKey属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFlightKey(String value) {
                this.flightKey = value;
            }

            /**
             * Gets the value of the refs property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the refs property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRefs().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public List<Object> getRefs() {
                if (refs == null) {
                    refs = new ArrayList<Object>();
                }
                return this.refs;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="FlightSegment" type="{http://www.iata.org/IATA/EDIST}ListOfFlightSegmentType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "flightSegment"
    })
    public static class FlightSegmentList {

        @XmlElement(name = "FlightSegment", required = true)
        protected List<ListOfFlightSegmentType> flightSegment;

        /**
         * Gets the value of the flightSegment property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the flightSegment property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFlightSegment().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListOfFlightSegmentType }
         * 
         * 
         */
        public List<ListOfFlightSegmentType> getFlightSegment() {
            if (flightSegment == null) {
                flightSegment = new ArrayList<ListOfFlightSegmentType>();
            }
            return this.flightSegment;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginDestination" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "originDestination"
    })
    public static class OriginDestinationList {

        @XmlElement(name = "OriginDestination", required = true)
        protected List<OriginDestination> originDestination;

        /**
         * Gets the value of the originDestination property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the originDestination property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOriginDestination().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link OriginDestination }
         * 
         * 
         */
        public List<OriginDestination> getOriginDestination() {
            if (originDestination == null) {
                originDestination = new ArrayList<OriginDestination>();
            }
            return this.originDestination;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Seats" type="{http://www.iata.org/IATA/EDIST}ListOfSeatType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "seats"
    })
    public static class SeatList {

        @XmlElement(name = "Seats", required = true)
        protected List<ListOfSeatType> seats;

        /**
         * Gets the value of the seats property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the seats property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSeats().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ListOfSeatType }
         * 
         * 
         */
        public List<ListOfSeatType> getSeats() {
            if (seats == null) {
                seats = new ArrayList<ListOfSeatType>();
            }
            return this.seats;
        }

    }

}
