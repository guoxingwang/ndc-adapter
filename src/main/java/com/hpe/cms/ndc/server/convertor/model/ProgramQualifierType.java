//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Shopping Request Business Object: PROGRAM Qualifiers.
 * 
 * <p>ProgramQualifierType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ProgramQualifierType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DiscountProgramQualifier" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}IncentiveProgramQualifier" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PrePaidProgramQualifier" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ProgramStatusQualifier"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProgramQualifierType", propOrder = {
    "discountProgramQualifier",
    "incentiveProgramQualifier",
    "prePaidProgramQualifier",
    "programStatusQualifier"
})
public class ProgramQualifierType {

    @XmlElement(name = "DiscountProgramQualifier")
    protected DiscountProgramType discountProgramQualifier;
    @XmlElement(name = "IncentiveProgramQualifier")
    protected IncentiveProgramQualifier incentiveProgramQualifier;
    @XmlElement(name = "PrePaidProgramQualifier")
    protected PrePaidProgramQualifier prePaidProgramQualifier;
    @XmlElement(name = "ProgramStatusQualifier", required = true)
    protected ProgramStatusQualifier programStatusQualifier;

    /**
     * Discount Program Qualifiers; fare type for this PTC.
     * Notes:
     *  1. Supports pricing discounts and/ or other benefits from membership in Travel Discount Programs, such as AARP and AAA.
     *  2. Discount Programs may be associated to individual Travellers but will apply to ALL Travellers if no associations are specified.
     * 
     * @return
     *     possible object is
     *     {@link DiscountProgramType }
     *     
     */
    public DiscountProgramType getDiscountProgramQualifier() {
        return discountProgramQualifier;
    }

    /**
     * 设置discountProgramQualifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link DiscountProgramType }
     *     
     */
    public void setDiscountProgramQualifier(DiscountProgramType value) {
        this.discountProgramQualifier = value;
    }

    /**
     * 获取incentiveProgramQualifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link IncentiveProgramQualifier }
     *     
     */
    public IncentiveProgramQualifier getIncentiveProgramQualifier() {
        return incentiveProgramQualifier;
    }

    /**
     * 设置incentiveProgramQualifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link IncentiveProgramQualifier }
     *     
     */
    public void setIncentiveProgramQualifier(IncentiveProgramQualifier value) {
        this.incentiveProgramQualifier = value;
    }

    /**
     * 获取prePaidProgramQualifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PrePaidProgramQualifier }
     *     
     */
    public PrePaidProgramQualifier getPrePaidProgramQualifier() {
        return prePaidProgramQualifier;
    }

    /**
     * 设置prePaidProgramQualifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PrePaidProgramQualifier }
     *     
     */
    public void setPrePaidProgramQualifier(PrePaidProgramQualifier value) {
        this.prePaidProgramQualifier = value;
    }

    /**
     * 获取programStatusQualifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ProgramStatusQualifier }
     *     
     */
    public ProgramStatusQualifier getProgramStatusQualifier() {
        return programStatusQualifier;
    }

    /**
     * 设置programStatusQualifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ProgramStatusQualifier }
     *     
     */
    public void setProgramStatusQualifier(ProgramStatusQualifier value) {
        this.programStatusQualifier = value;
    }

}
