//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * CORE ORDER ITEM definition.
 * 
 * <p>OrderItemCoreType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="OrderItemCoreType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OrderItem" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemID" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem" minOccurs="0"/&gt;
 *                   &lt;element name="TimeLimits" type="{http://www.iata.org/IATA/EDIST}OrderItemTimeLimitsType" minOccurs="0"/&gt;
 *                   &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Associations" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderID" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemID" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                             &lt;element name="Passengers" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;choice&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
 *                                     &lt;/choice&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}IncludedService" minOccurs="0"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AssociatedService" minOccurs="0"/&gt;
 *                             &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
 *                                     &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Disclosures" minOccurs="0"/&gt;
 *                   &lt;element name="Penalty" type="{http://www.iata.org/IATA/EDIST}OrderPenaltyType" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemDetails" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderItemCoreType", propOrder = {
    "orderItem"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.ItinReshopRQ.Query.RepriceOnly.OrderItems.class,
    OrderItemDetailType.class
})
public class OrderItemCoreType {

    @XmlElement(name = "OrderItem", required = true)
    protected List<OrderItemCoreType.OrderItem> orderItem;

    /**
     * Gets the value of the orderItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderItemCoreType.OrderItem }
     * 
     * 
     */
    public List<OrderItemCoreType.OrderItem> getOrderItem() {
        if (orderItem == null) {
            orderItem = new ArrayList<OrderItemCoreType.OrderItem>();
        }
        return this.orderItem;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemID" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem" minOccurs="0"/&gt;
     *         &lt;element name="TimeLimits" type="{http://www.iata.org/IATA/EDIST}OrderItemTimeLimitsType" minOccurs="0"/&gt;
     *         &lt;element name="InventoryGuarantee" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Associations" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderID" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemID" maxOccurs="unbounded" minOccurs="0"/&gt;
     *                   &lt;element name="Passengers" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;choice&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
     *                           &lt;/choice&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}IncludedService" minOccurs="0"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AssociatedService" minOccurs="0"/&gt;
     *                   &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
     *                           &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Disclosures" minOccurs="0"/&gt;
     *         &lt;element name="Penalty" type="{http://www.iata.org/IATA/EDIST}OrderPenaltyType" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemDetails" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "orderItemID",
        "seatItem",
        "otherItem",
        "flightItem",
        "baggageItem",
        "timeLimits",
        "inventoryGuarantee",
        "associations",
        "disclosures",
        "penalty",
        "orderItemDetails"
    })
    public static class OrderItem
        extends KeyWithMetaObjectBaseType
    {

        @XmlElement(name = "OrderItemID")
        protected ItemIDType orderItemID;
        @XmlElement(name = "SeatItem")
        protected SeatItem seatItem;
        @XmlElement(name = "OtherItem")
        protected OtherItemType otherItem;
        @XmlElement(name = "FlightItem")
        protected FlightItemType flightItem;
        @XmlElement(name = "BaggageItem")
        protected BaggageItemType baggageItem;
        @XmlElement(name = "TimeLimits")
        protected OrderItemTimeLimitsType timeLimits;
        @XmlElement(name = "InventoryGuarantee")
        protected OrderItemCoreType.OrderItem.InventoryGuarantee inventoryGuarantee;
        @XmlElement(name = "Associations")
        protected OrderItemCoreType.OrderItem.Associations associations;
        @XmlElement(name = "Disclosures")
        protected Disclosures disclosures;
        @XmlElement(name = "Penalty")
        protected OrderPenaltyType penalty;
        @XmlElement(name = "OrderItemDetails")
        protected OrderItemDetails orderItemDetails;

        /**
         * 获取orderItemID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ItemIDType }
         *     
         */
        public ItemIDType getOrderItemID() {
            return orderItemID;
        }

        /**
         * 设置orderItemID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ItemIDType }
         *     
         */
        public void setOrderItemID(ItemIDType value) {
            this.orderItemID = value;
        }

        /**
         * 获取seatItem属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SeatItem }
         *     
         */
        public SeatItem getSeatItem() {
            return seatItem;
        }

        /**
         * 设置seatItem属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SeatItem }
         *     
         */
        public void setSeatItem(SeatItem value) {
            this.seatItem = value;
        }

        /**
         * 获取otherItem属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OtherItemType }
         *     
         */
        public OtherItemType getOtherItem() {
            return otherItem;
        }

        /**
         * 设置otherItem属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OtherItemType }
         *     
         */
        public void setOtherItem(OtherItemType value) {
            this.otherItem = value;
        }

        /**
         * 获取flightItem属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FlightItemType }
         *     
         */
        public FlightItemType getFlightItem() {
            return flightItem;
        }

        /**
         * 设置flightItem属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FlightItemType }
         *     
         */
        public void setFlightItem(FlightItemType value) {
            this.flightItem = value;
        }

        /**
         * 获取baggageItem属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BaggageItemType }
         *     
         */
        public BaggageItemType getBaggageItem() {
            return baggageItem;
        }

        /**
         * 设置baggageItem属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BaggageItemType }
         *     
         */
        public void setBaggageItem(BaggageItemType value) {
            this.baggageItem = value;
        }

        /**
         * 获取timeLimits属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OrderItemTimeLimitsType }
         *     
         */
        public OrderItemTimeLimitsType getTimeLimits() {
            return timeLimits;
        }

        /**
         * 设置timeLimits属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OrderItemTimeLimitsType }
         *     
         */
        public void setTimeLimits(OrderItemTimeLimitsType value) {
            this.timeLimits = value;
        }

        /**
         * 获取inventoryGuarantee属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OrderItemCoreType.OrderItem.InventoryGuarantee }
         *     
         */
        public OrderItemCoreType.OrderItem.InventoryGuarantee getInventoryGuarantee() {
            return inventoryGuarantee;
        }

        /**
         * 设置inventoryGuarantee属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OrderItemCoreType.OrderItem.InventoryGuarantee }
         *     
         */
        public void setInventoryGuarantee(OrderItemCoreType.OrderItem.InventoryGuarantee value) {
            this.inventoryGuarantee = value;
        }

        /**
         * 获取associations属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OrderItemCoreType.OrderItem.Associations }
         *     
         */
        public OrderItemCoreType.OrderItem.Associations getAssociations() {
            return associations;
        }

        /**
         * 设置associations属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OrderItemCoreType.OrderItem.Associations }
         *     
         */
        public void setAssociations(OrderItemCoreType.OrderItem.Associations value) {
            this.associations = value;
        }

        /**
         * 获取disclosures属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Disclosures }
         *     
         */
        public Disclosures getDisclosures() {
            return disclosures;
        }

        /**
         * 设置disclosures属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Disclosures }
         *     
         */
        public void setDisclosures(Disclosures value) {
            this.disclosures = value;
        }

        /**
         * 获取penalty属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OrderPenaltyType }
         *     
         */
        public OrderPenaltyType getPenalty() {
            return penalty;
        }

        /**
         * 设置penalty属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OrderPenaltyType }
         *     
         */
        public void setPenalty(OrderPenaltyType value) {
            this.penalty = value;
        }

        /**
         * 获取orderItemDetails属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OrderItemDetails }
         *     
         */
        public OrderItemDetails getOrderItemDetails() {
            return orderItemDetails;
        }

        /**
         * 设置orderItemDetails属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OrderItemDetails }
         *     
         */
        public void setOrderItemDetails(OrderItemDetails value) {
            this.orderItemDetails = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderID" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderItemID" maxOccurs="unbounded" minOccurs="0"/&gt;
         *         &lt;element name="Passengers" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;choice&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
         *                 &lt;/choice&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}IncludedService" minOccurs="0"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AssociatedService" minOccurs="0"/&gt;
         *         &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
         *                 &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "orderID",
            "orderItemID",
            "passengers",
            "includedService",
            "associatedService",
            "otherAssociation"
        })
        public static class Associations {

            @XmlElement(name = "OrderID")
            protected OrderIDType orderID;
            @XmlElement(name = "OrderItemID")
            protected List<ItemIDType> orderItemID;
            @XmlElement(name = "Passengers")
            protected OrderItemCoreType.OrderItem.Associations.Passengers passengers;
            @XmlElement(name = "IncludedService")
            protected ServiceInfoAssocType includedService;
            @XmlElement(name = "AssociatedService")
            protected ServiceInfoAssocType associatedService;
            @XmlElement(name = "OtherAssociation")
            protected List<OrderItemCoreType.OrderItem.Associations.OtherAssociation> otherAssociation;

            /**
             * Associated Order ID Example: ORDER7333
             * 
             * @return
             *     possible object is
             *     {@link OrderIDType }
             *     
             */
            public OrderIDType getOrderID() {
                return orderID;
            }

            /**
             * 设置orderID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link OrderIDType }
             *     
             */
            public void setOrderID(OrderIDType value) {
                this.orderID = value;
            }

            /**
             * Associated Order Item ID(s). Example: ORD7333-001 Gets the value of the orderItemID property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the orderItemID property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOrderItemID().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ItemIDType }
             * 
             * 
             */
            public List<ItemIDType> getOrderItemID() {
                if (orderItemID == null) {
                    orderItemID = new ArrayList<ItemIDType>();
                }
                return this.orderItemID;
            }

            /**
             * 获取passengers属性的值。
             * 
             * @return
             *     possible object is
             *     {@link OrderItemCoreType.OrderItem.Associations.Passengers }
             *     
             */
            public OrderItemCoreType.OrderItem.Associations.Passengers getPassengers() {
                return passengers;
            }

            /**
             * 设置passengers属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link OrderItemCoreType.OrderItem.Associations.Passengers }
             *     
             */
            public void setPassengers(OrderItemCoreType.OrderItem.Associations.Passengers value) {
                this.passengers = value;
            }

            /**
             * 获取includedService属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ServiceInfoAssocType }
             *     
             */
            public ServiceInfoAssocType getIncludedService() {
                return includedService;
            }

            /**
             * 设置includedService属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ServiceInfoAssocType }
             *     
             */
            public void setIncludedService(ServiceInfoAssocType value) {
                this.includedService = value;
            }

            /**
             * 获取associatedService属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ServiceInfoAssocType }
             *     
             */
            public ServiceInfoAssocType getAssociatedService() {
                return associatedService;
            }

            /**
             * 设置associatedService属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ServiceInfoAssocType }
             *     
             */
            public void setAssociatedService(ServiceInfoAssocType value) {
                this.associatedService = value;
            }

            /**
             * Gets the value of the otherAssociation property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the otherAssociation property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOtherAssociation().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link OrderItemCoreType.OrderItem.Associations.OtherAssociation }
             * 
             * 
             */
            public List<OrderItemCoreType.OrderItem.Associations.OtherAssociation> getOtherAssociation() {
                if (otherAssociation == null) {
                    otherAssociation = new ArrayList<OrderItemCoreType.OrderItem.Associations.OtherAssociation>();
                }
                return this.otherAssociation;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
             *       &lt;attribute name="ReferenceValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class OtherAssociation {

                @XmlAttribute(name = "Type", required = true)
                protected String type;
                @XmlAttribute(name = "ReferenceValue", required = true)
                @XmlIDREF
                @XmlSchemaType(name = "IDREF")
                protected Object referenceValue;

                /**
                 * 获取type属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * 设置type属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * 获取referenceValue属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getReferenceValue() {
                    return referenceValue;
                }

                /**
                 * 设置referenceValue属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setReferenceValue(Object value) {
                    this.referenceValue = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;choice&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PassengerReferences"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupReference"/&gt;
             *       &lt;/choice&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "passengerReferences",
                "groupReference"
            })
            public static class Passengers {

                @XmlList
                @XmlElement(name = "PassengerReferences")
                @XmlIDREF
                @XmlSchemaType(name = "IDREFS")
                protected List<Object> passengerReferences;
                @XmlElement(name = "GroupReference")
                @XmlIDREF
                @XmlSchemaType(name = "IDREF")
                protected Object groupReference;

                /**
                 * 
                 *                                   Association to multiple Passenger instance(s). Example: pax1 pax2
                 *                                 Gets the value of the passengerReferences property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the passengerReferences property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPassengerReferences().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Object }
                 * 
                 * 
                 */
                public List<Object> getPassengerReferences() {
                    if (passengerReferences == null) {
                        passengerReferences = new ArrayList<Object>();
                    }
                    return this.passengerReferences;
                }

                /**
                 * 获取groupReference属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getGroupReference() {
                    return groupReference;
                }

                /**
                 * 设置groupReference属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setGroupReference(Object value) {
                    this.groupReference = value;
                }

            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="InvGuaranteeID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}InvGuaranteeTimeLimits"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "invGuaranteeID",
            "invGuaranteeTimeLimits"
        })
        public static class InventoryGuarantee {

            @XmlElement(name = "InvGuaranteeID", required = true)
            protected String invGuaranteeID;
            @XmlElement(name = "InvGuaranteeTimeLimits", required = true)
            protected InvGuaranteeTimeLimits invGuaranteeTimeLimits;

            /**
             * 获取invGuaranteeID属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvGuaranteeID() {
                return invGuaranteeID;
            }

            /**
             * 设置invGuaranteeID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvGuaranteeID(String value) {
                this.invGuaranteeID = value;
            }

            /**
             * 获取invGuaranteeTimeLimits属性的值。
             * 
             * @return
             *     possible object is
             *     {@link InvGuaranteeTimeLimits }
             *     
             */
            public InvGuaranteeTimeLimits getInvGuaranteeTimeLimits() {
                return invGuaranteeTimeLimits;
            }

            /**
             * 设置invGuaranteeTimeLimits属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link InvGuaranteeTimeLimits }
             *     
             */
            public void setInvGuaranteeTimeLimits(InvGuaranteeTimeLimits value) {
                this.invGuaranteeTimeLimits = value;
            }

        }

    }

}
