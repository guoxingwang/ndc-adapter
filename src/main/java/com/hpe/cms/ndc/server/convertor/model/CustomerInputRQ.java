//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Document"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Party"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CustomerInputParameters" minOccurs="0"/&gt;
 *         &lt;element name="Query"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PointOfSale" minOccurs="0"/&gt;
 *                   &lt;element name="TravelerAuthentication" type="{http://www.iata.org/IATA/EDIST}PIN_AuthTravelerType" minOccurs="0"/&gt;
 *                   &lt;element name="SecurePayments" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="SecurePayment" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}SecurePaymentAlertType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPriceType" minOccurs="0"/&gt;
 *                                       &lt;element name="PaymentCard" type="{http://www.iata.org/IATA/EDIST}PaymentCardType" minOccurs="0"/&gt;
 *                                       &lt;element name="Approval" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="ApprovalType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                                                 &lt;element name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
 *                                                 &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
 *                                                 &lt;element name="TrxReference" minOccurs="0"&gt;
 *                                                   &lt;simpleType&gt;
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                                       &lt;minLength value="1"/&gt;
 *                                                       &lt;maxLength value="25"/&gt;
 *                                                     &lt;/restriction&gt;
 *                                                   &lt;/simpleType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                               &lt;attribute name="PreAuthorizedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                       &lt;element name="Qualifier" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}Associations" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}IATA_PayloadStdAttributes"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "document",
    "party",
    "customerInputParameters",
    "query"
})
@XmlRootElement(name = "CustomerInputRQ")
public class CustomerInputRQ {

    @XmlElement(name = "Document", required = true)
    protected MsgDocumentType document;
    @XmlElement(name = "Party", required = true)
    protected MsgPartiesType party;
    @XmlElement(name = "CustomerInputParameters")
    protected CusInputReqParamsType customerInputParameters;
    @XmlElement(name = "Query", required = true)
    protected CustomerInputRQ.Query query;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String target;
    @XmlAttribute(name = "Version", required = true)
    protected String version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "AsynchronousAllowedInd")
    protected Boolean asynchronousAllowedInd;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;

    /**
     * 获取document属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgDocumentType }
     *     
     */
    public MsgDocumentType getDocument() {
        return document;
    }

    /**
     * 设置document属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgDocumentType }
     *     
     */
    public void setDocument(MsgDocumentType value) {
        this.document = value;
    }

    /**
     * 获取party属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgPartiesType }
     *     
     */
    public MsgPartiesType getParty() {
        return party;
    }

    /**
     * 设置party属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgPartiesType }
     *     
     */
    public void setParty(MsgPartiesType value) {
        this.party = value;
    }

    /**
     * 获取customerInputParameters属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CusInputReqParamsType }
     *     
     */
    public CusInputReqParamsType getCustomerInputParameters() {
        return customerInputParameters;
    }

    /**
     * 设置customerInputParameters属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CusInputReqParamsType }
     *     
     */
    public void setCustomerInputParameters(CusInputReqParamsType value) {
        this.customerInputParameters = value;
    }

    /**
     * 获取query属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CustomerInputRQ.Query }
     *     
     */
    public CustomerInputRQ.Query getQuery() {
        return query;
    }

    /**
     * 设置query属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerInputRQ.Query }
     *     
     */
    public void setQuery(CustomerInputRQ.Query value) {
        this.query = value;
    }

    /**
     * 获取echoToken属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * 设置echoToken属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * 获取target属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        if (target == null) {
            return "Production";
        } else {
            return target;
        }
    }

    /**
     * 设置target属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取transactionIdentifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * 设置transactionIdentifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * 获取sequenceNmbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * 设置sequenceNmbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * 获取transactionStatusCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * 设置transactionStatusCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * 获取retransmissionIndicator属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * 设置retransmissionIndicator属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * 获取correlationID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * 设置correlationID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * 获取asynchronousAllowedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAsynchronousAllowedInd() {
        return asynchronousAllowedInd;
    }

    /**
     * 设置asynchronousAllowedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAsynchronousAllowedInd(Boolean value) {
        this.asynchronousAllowedInd = value;
    }

    /**
     * 获取altLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * 设置altLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * 获取primaryLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * 设置primaryLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PointOfSale" minOccurs="0"/&gt;
     *         &lt;element name="TravelerAuthentication" type="{http://www.iata.org/IATA/EDIST}PIN_AuthTravelerType" minOccurs="0"/&gt;
     *         &lt;element name="SecurePayments" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="SecurePayment" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}SecurePaymentAlertType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPriceType" minOccurs="0"/&gt;
     *                             &lt;element name="PaymentCard" type="{http://www.iata.org/IATA/EDIST}PaymentCardType" minOccurs="0"/&gt;
     *                             &lt;element name="Approval" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="ApprovalType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *                                       &lt;element name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
     *                                       &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
     *                                       &lt;element name="TrxReference" minOccurs="0"&gt;
     *                                         &lt;simpleType&gt;
     *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *                                             &lt;minLength value="1"/&gt;
     *                                             &lt;maxLength value="25"/&gt;
     *                                           &lt;/restriction&gt;
     *                                         &lt;/simpleType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                     &lt;attribute name="PreAuthorizedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="Qualifier" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}Associations" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pointOfSale",
        "travelerAuthentication",
        "securePayments"
    })
    public static class Query {

        @XmlElement(name = "PointOfSale")
        protected PointOfSaleType pointOfSale;
        @XmlElement(name = "TravelerAuthentication")
        protected PINAuthTravelerType travelerAuthentication;
        @XmlElement(name = "SecurePayments")
        protected CustomerInputRQ.Query.SecurePayments securePayments;

        /**
         * 获取pointOfSale属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PointOfSaleType }
         *     
         */
        public PointOfSaleType getPointOfSale() {
            return pointOfSale;
        }

        /**
         * 设置pointOfSale属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PointOfSaleType }
         *     
         */
        public void setPointOfSale(PointOfSaleType value) {
            this.pointOfSale = value;
        }

        /**
         * 获取travelerAuthentication属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PINAuthTravelerType }
         *     
         */
        public PINAuthTravelerType getTravelerAuthentication() {
            return travelerAuthentication;
        }

        /**
         * 设置travelerAuthentication属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PINAuthTravelerType }
         *     
         */
        public void setTravelerAuthentication(PINAuthTravelerType value) {
            this.travelerAuthentication = value;
        }

        /**
         * 获取securePayments属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CustomerInputRQ.Query.SecurePayments }
         *     
         */
        public CustomerInputRQ.Query.SecurePayments getSecurePayments() {
            return securePayments;
        }

        /**
         * 设置securePayments属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CustomerInputRQ.Query.SecurePayments }
         *     
         */
        public void setSecurePayments(CustomerInputRQ.Query.SecurePayments value) {
            this.securePayments = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="SecurePayment" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;extension base="{http://www.iata.org/IATA/EDIST}SecurePaymentAlertType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPriceType" minOccurs="0"/&gt;
         *                   &lt;element name="PaymentCard" type="{http://www.iata.org/IATA/EDIST}PaymentCardType" minOccurs="0"/&gt;
         *                   &lt;element name="Approval" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="ApprovalType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
         *                             &lt;element name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
         *                             &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
         *                             &lt;element name="TrxReference" minOccurs="0"&gt;
         *                               &lt;simpleType&gt;
         *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
         *                                   &lt;minLength value="1"/&gt;
         *                                   &lt;maxLength value="25"/&gt;
         *                                 &lt;/restriction&gt;
         *                               &lt;/simpleType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                           &lt;attribute name="PreAuthorizedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="Qualifier" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Associations" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/extension&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "securePayment"
        })
        public static class SecurePayments {

            @XmlElement(name = "SecurePayment", required = true)
            protected List<CustomerInputRQ.Query.SecurePayments.SecurePayment> securePayment;

            /**
             * Gets the value of the securePayment property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the securePayment property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSecurePayment().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CustomerInputRQ.Query.SecurePayments.SecurePayment }
             * 
             * 
             */
            public List<CustomerInputRQ.Query.SecurePayments.SecurePayment> getSecurePayment() {
                if (securePayment == null) {
                    securePayment = new ArrayList<CustomerInputRQ.Query.SecurePayments.SecurePayment>();
                }
                return this.securePayment;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;extension base="{http://www.iata.org/IATA/EDIST}SecurePaymentAlertType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}SimpleCurrencyPriceType" minOccurs="0"/&gt;
             *         &lt;element name="PaymentCard" type="{http://www.iata.org/IATA/EDIST}PaymentCardType" minOccurs="0"/&gt;
             *         &lt;element name="Approval" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="ApprovalType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
             *                   &lt;element name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
             *                   &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
             *                   &lt;element name="TrxReference" minOccurs="0"&gt;
             *                     &lt;simpleType&gt;
             *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
             *                         &lt;minLength value="1"/&gt;
             *                         &lt;maxLength value="25"/&gt;
             *                       &lt;/restriction&gt;
             *                     &lt;/simpleType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *                 &lt;attribute name="PreAuthorizedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="Qualifier" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Associations" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/extension&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amount",
                "paymentCard",
                "approval",
                "qualifier",
                "associations"
            })
            public static class SecurePayment
                extends SecurePaymentAlertType
            {

                @XmlElement(name = "Amount")
                protected SimpleCurrencyPriceType amount;
                @XmlElement(name = "PaymentCard")
                protected PaymentCardType paymentCard;
                @XmlElement(name = "Approval")
                protected CustomerInputRQ.Query.SecurePayments.SecurePayment.Approval approval;
                @XmlElement(name = "Qualifier")
                protected CodesetType qualifier;
                @XmlElement(name = "Associations")
                protected OrderItemAssociationType associations;

                /**
                 * 获取amount属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link SimpleCurrencyPriceType }
                 *     
                 */
                public SimpleCurrencyPriceType getAmount() {
                    return amount;
                }

                /**
                 * 设置amount属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link SimpleCurrencyPriceType }
                 *     
                 */
                public void setAmount(SimpleCurrencyPriceType value) {
                    this.amount = value;
                }

                /**
                 * 获取paymentCard属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link PaymentCardType }
                 *     
                 */
                public PaymentCardType getPaymentCard() {
                    return paymentCard;
                }

                /**
                 * 设置paymentCard属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PaymentCardType }
                 *     
                 */
                public void setPaymentCard(PaymentCardType value) {
                    this.paymentCard = value;
                }

                /**
                 * 获取approval属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CustomerInputRQ.Query.SecurePayments.SecurePayment.Approval }
                 *     
                 */
                public CustomerInputRQ.Query.SecurePayments.SecurePayment.Approval getApproval() {
                    return approval;
                }

                /**
                 * 设置approval属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CustomerInputRQ.Query.SecurePayments.SecurePayment.Approval }
                 *     
                 */
                public void setApproval(CustomerInputRQ.Query.SecurePayments.SecurePayment.Approval value) {
                    this.approval = value;
                }

                /**
                 * 获取qualifier属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CodesetType }
                 *     
                 */
                public CodesetType getQualifier() {
                    return qualifier;
                }

                /**
                 * 设置qualifier属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CodesetType }
                 *     
                 */
                public void setQualifier(CodesetType value) {
                    this.qualifier = value;
                }

                /**
                 * 获取associations属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrderItemAssociationType }
                 *     
                 */
                public OrderItemAssociationType getAssociations() {
                    return associations;
                }

                /**
                 * 设置associations属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrderItemAssociationType }
                 *     
                 */
                public void setAssociations(OrderItemAssociationType value) {
                    this.associations = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="ApprovalType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
                 *         &lt;element name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
                 *         &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
                 *         &lt;element name="TrxReference" minOccurs="0"&gt;
                 *           &lt;simpleType&gt;
                 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
                 *               &lt;minLength value="1"/&gt;
                 *               &lt;maxLength value="25"/&gt;
                 *             &lt;/restriction&gt;
                 *           &lt;/simpleType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *       &lt;attribute name="PreAuthorizedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "approvalType",
                    "code",
                    "trxTimestamp",
                    "trxReference"
                })
                public static class Approval {

                    @XmlElement(name = "ApprovalType")
                    protected CodesetType approvalType;
                    @XmlElement(name = "Code")
                    protected String code;
                    @XmlElement(name = "TrxTimestamp")
                    protected TimestampRepType trxTimestamp;
                    @XmlElement(name = "TrxReference")
                    protected String trxReference;
                    @XmlAttribute(name = "PreAuthorizedInd")
                    protected Boolean preAuthorizedInd;

                    /**
                     * 获取approvalType属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link CodesetType }
                     *     
                     */
                    public CodesetType getApprovalType() {
                        return approvalType;
                    }

                    /**
                     * 设置approvalType属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link CodesetType }
                     *     
                     */
                    public void setApprovalType(CodesetType value) {
                        this.approvalType = value;
                    }

                    /**
                     * 获取code属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * 设置code属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                    /**
                     * 获取trxTimestamp属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link TimestampRepType }
                     *     
                     */
                    public TimestampRepType getTrxTimestamp() {
                        return trxTimestamp;
                    }

                    /**
                     * 设置trxTimestamp属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link TimestampRepType }
                     *     
                     */
                    public void setTrxTimestamp(TimestampRepType value) {
                        this.trxTimestamp = value;
                    }

                    /**
                     * 获取trxReference属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTrxReference() {
                        return trxReference;
                    }

                    /**
                     * 设置trxReference属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTrxReference(String value) {
                        this.trxReference = value;
                    }

                    /**
                     * 获取preAuthorizedInd属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link Boolean }
                     *     
                     */
                    public Boolean isPreAuthorizedInd() {
                        return preAuthorizedInd;
                    }

                    /**
                     * 设置preAuthorizedInd属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Boolean }
                     *     
                     */
                    public void setPreAuthorizedInd(Boolean value) {
                        this.preAuthorizedInd = value;
                    }

                }

            }

        }

    }

}
