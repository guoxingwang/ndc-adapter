package com.hpe.cms.ndc.server.endpoint;

import com.hpe.cms.ndc.client.common.WsClientTemplate;
import com.hpe.cms.ndc.common.InterfaceInfoFactory;
import com.hpe.cms.ndc.common.InterfaceRouter;
import com.hpe.cms.ndc.common.InterfaceRouterUtil;
import com.hpe.cms.ndc.server.constance.WsServerConstance;
import com.hpe.cms.ndc.server.security.Security;
import com.hpe.cms.ndc.server.v18_2.model.iata_ordercancelrq.IATAOrderCancelRQ;
import com.hpe.cms.ndc.server.v18_2.model.iata_ordercancelrs.IATAOrderCancelRS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.server.endpoint.annotation.SoapHeader;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:36 2019/7/5
 * @ Description：
 * @ Modified By：
 */
@Endpoint
@Slf4j
public class IATAOrderCancelEndpoint_v18_2 extends AbstractEndpoint<IATAOrderCancelRQ, IATAOrderCancelRS> {

    private static final String INTERFACE_NAME = "IATA_OrderCancelRQ";
    private static final String VERSION = WsServerConstance.V18_2;
    private static final String NAMESPACE = WsServerConstance.V18_2_NAMESPACE + "/" + INTERFACE_NAME;

    public IATAOrderCancelEndpoint_v18_2(WsClientTemplate<IATAOrderCancelRQ, IATAOrderCancelRS> wsClientTemplate) {
        super(wsClientTemplate);
    }

    @Override
    @PayloadRoot(namespace = NAMESPACE, localPart = INTERFACE_NAME + "_" + VERSION)
    @ResponsePayload
    public IATAOrderCancelRS sendAndReceive(@RequestPayload IATAOrderCancelRQ request,
                                            @SoapHeader("{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd}Security") SoapHeaderElement headerElement) {
        return super.sendAndReceive(request, headerElement);
    }

    @Override
    void configInterfaceInfo(IATAOrderCancelRQ request, Security security) {
        InterfaceRouter interfaceRouter = InterfaceRouterUtil.getInterfaRouterBySourceAndInterfaceName(security.getUsernameToken().getUsername(), INTERFACE_NAME);
        String targetVersion = interfaceRouter.getTargetVersion();
        String sourceVersion = VERSION;
        this.interfaceInfo = new InterfaceInfoFactory<IATAOrderCancelRQ, IATAOrderCancelRS>().createInterfaceInfo(INTERFACE_NAME, sourceVersion, targetVersion);
        interfaceInfo.setSourceVersion(VERSION);
        interfaceInfo.setSourceInterfaceName(INTERFACE_NAME);
        interfaceInfo.setSourceNamespace(NAMESPACE);
        interfaceInfo.setSourceRq(request);
        interfaceInfo.setSecurity(security);
    }
}
