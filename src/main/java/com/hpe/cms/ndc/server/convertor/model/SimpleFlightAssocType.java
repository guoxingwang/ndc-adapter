//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Specific structure for providing flight associations, would typically associate to flights from a Data List.
 * 
 * <p>SimpleFlightAssocType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="SimpleFlightAssocType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginDestinationReference" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightReferences" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightSegmentReferences" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleFlightAssocType", propOrder = {
    "originDestinationReference",
    "flightReferences",
    "flightSegmentReferences"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.SeatAvailabilityRS.Flights.class
})
public class SimpleFlightAssocType {

    @XmlElement(name = "OriginDestinationReference")
    @XmlIDREF
    @XmlSchemaType(name = "IDREF")
    protected Object originDestinationReference;
    @XmlElement(name = "FlightReferences")
    protected FlightReferences flightReferences;
    @XmlElement(name = "FlightSegmentReferences")
    protected FlightSegmentReferences flightSegmentReferences;

    /**
     * 获取originDestinationReference属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getOriginDestinationReference() {
        return originDestinationReference;
    }

    /**
     * 设置originDestinationReference属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setOriginDestinationReference(Object value) {
        this.originDestinationReference = value;
    }

    /**
     * 获取flightReferences属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FlightReferences }
     *     
     */
    public FlightReferences getFlightReferences() {
        return flightReferences;
    }

    /**
     * 设置flightReferences属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FlightReferences }
     *     
     */
    public void setFlightReferences(FlightReferences value) {
        this.flightReferences = value;
    }

    /**
     * 获取flightSegmentReferences属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FlightSegmentReferences }
     *     
     */
    public FlightSegmentReferences getFlightSegmentReferences() {
        return flightSegmentReferences;
    }

    /**
     * 设置flightSegmentReferences属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FlightSegmentReferences }
     *     
     */
    public void setFlightSegmentReferences(FlightSegmentReferences value) {
        this.flightSegmentReferences = value;
    }

}
