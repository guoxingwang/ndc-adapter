//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>OtherOfferAssocSimpleType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="OtherOfferAssocSimpleType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AircraftEquipment"/&gt;
 *     &lt;enumeration value="BaggageDisclosure"/&gt;
 *     &lt;enumeration value="CarryOnAllowance"/&gt;
 *     &lt;enumeration value="CheckedBagAllowance"/&gt;
 *     &lt;enumeration value="MediaGroup"/&gt;
 *     &lt;enumeration value="MediaItem"/&gt;
 *     &lt;enumeration value="OfferDisclosure"/&gt;
 *     &lt;enumeration value="OfferInstructions"/&gt;
 *     &lt;enumeration value="OfferTerms"/&gt;
 *     &lt;enumeration value="Penalty"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OtherOfferAssocSimpleType")
@XmlEnum
public enum OtherOfferAssocSimpleType {

    @XmlEnumValue("AircraftEquipment")
    AIRCRAFT_EQUIPMENT("AircraftEquipment"),
    @XmlEnumValue("BaggageDisclosure")
    BAGGAGE_DISCLOSURE("BaggageDisclosure"),
    @XmlEnumValue("CarryOnAllowance")
    CARRY_ON_ALLOWANCE("CarryOnAllowance"),
    @XmlEnumValue("CheckedBagAllowance")
    CHECKED_BAG_ALLOWANCE("CheckedBagAllowance"),
    @XmlEnumValue("MediaGroup")
    MEDIA_GROUP("MediaGroup"),
    @XmlEnumValue("MediaItem")
    MEDIA_ITEM("MediaItem"),
    @XmlEnumValue("OfferDisclosure")
    OFFER_DISCLOSURE("OfferDisclosure"),
    @XmlEnumValue("OfferInstructions")
    OFFER_INSTRUCTIONS("OfferInstructions"),
    @XmlEnumValue("OfferTerms")
    OFFER_TERMS("OfferTerms"),
    @XmlEnumValue("Penalty")
    PENALTY("Penalty");
    private final String value;

    OtherOfferAssocSimpleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OtherOfferAssocSimpleType fromValue(String v) {
        for (OtherOfferAssocSimpleType c: OtherOfferAssocSimpleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
