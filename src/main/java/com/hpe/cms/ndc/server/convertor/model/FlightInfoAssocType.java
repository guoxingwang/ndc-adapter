//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Flight that the offer applies to.
 * 
 * <p>FlightInfoAssocType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="FlightInfoAssocType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="AllFlightInd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;sequence&gt;
 *           &lt;element name="AllOriginDestinationInd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *           &lt;element name="AllSegmentInd" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;sequence&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginDestinationReferences" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightReferences" minOccurs="0"/&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightSegmentReference" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightInfoAssocType", propOrder = {
    "allFlightInd",
    "allOriginDestinationInd",
    "allSegmentInd",
    "originDestinationReferences",
    "flightReferences",
    "flightSegmentReference"
})
@XmlSeeAlso({
    ApplicableFlight.class
})
public class FlightInfoAssocType {

    @XmlElement(name = "AllFlightInd")
    protected Boolean allFlightInd;
    @XmlElement(name = "AllOriginDestinationInd")
    protected Boolean allOriginDestinationInd;
    @XmlElement(name = "AllSegmentInd")
    protected Boolean allSegmentInd;
    @XmlList
    @XmlElement(name = "OriginDestinationReferences")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> originDestinationReferences;
    @XmlElement(name = "FlightReferences")
    protected FlightReferences flightReferences;
    @XmlElement(name = "FlightSegmentReference")
    protected List<FlightSegmentReference> flightSegmentReference;

    /**
     * 获取allFlightInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllFlightInd() {
        return allFlightInd;
    }

    /**
     * 设置allFlightInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllFlightInd(Boolean value) {
        this.allFlightInd = value;
    }

    /**
     * 获取allOriginDestinationInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllOriginDestinationInd() {
        return allOriginDestinationInd;
    }

    /**
     * 设置allOriginDestinationInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllOriginDestinationInd(Boolean value) {
        this.allOriginDestinationInd = value;
    }

    /**
     * 获取allSegmentInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllSegmentInd() {
        return allSegmentInd;
    }

    /**
     * 设置allSegmentInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllSegmentInd(Boolean value) {
        this.allSegmentInd = value;
    }

    /**
     * Gets the value of the originDestinationReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the originDestinationReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginDestinationReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getOriginDestinationReferences() {
        if (originDestinationReferences == null) {
            originDestinationReferences = new ArrayList<Object>();
        }
        return this.originDestinationReferences;
    }

    /**
     * 获取flightReferences属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FlightReferences }
     *     
     */
    public FlightReferences getFlightReferences() {
        return flightReferences;
    }

    /**
     * 设置flightReferences属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FlightReferences }
     *     
     */
    public void setFlightReferences(FlightReferences value) {
        this.flightReferences = value;
    }

    /**
     * Gets the value of the flightSegmentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flightSegmentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlightSegmentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlightSegmentReference }
     * 
     * 
     */
    public List<FlightSegmentReference> getFlightSegmentReference() {
        if (flightSegmentReference == null) {
            flightSegmentReference = new ArrayList<FlightSegmentReference>();
        }
        return this.flightSegmentReference;
    }

}
