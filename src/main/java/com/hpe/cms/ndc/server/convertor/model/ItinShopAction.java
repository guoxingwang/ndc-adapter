//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Transaction Processing Notice(s).
 * 
 * Note:
 *  1. These notices contain information for requested functionality (utilized during transaction processing) that may not be supported by all implementers.
 *  2. Specified notice(s) will be ignored by receiving systems that do not support this capability.
 * 
 * 
 * <p>ItinShopAction complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ItinShopAction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CRUD_Rule" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;element name="Associations" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItinShopAction", propOrder = {
    "crudRule",
    "associations"
})
public class ItinShopAction {

    @XmlElement(name = "CRUD_Rule", required = true)
    protected Object crudRule;
    @XmlElement(name = "Associations", required = true)
    protected Object associations;

    /**
     * 获取crudRule属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCRUDRule() {
        return crudRule;
    }

    /**
     * 设置crudRule属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCRUDRule(Object value) {
        this.crudRule = value;
    }

    /**
     * 获取associations属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAssociations() {
        return associations;
    }

    /**
     * 设置associations属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAssociations(Object value) {
        this.associations = value;
    }

}
