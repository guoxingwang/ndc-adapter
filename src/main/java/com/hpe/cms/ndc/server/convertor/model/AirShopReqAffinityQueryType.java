//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * AirShopReq Business Object: AFFINITY QUERY definition.
 * 
 * <p>AirShopReqAffinityQueryType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AirShopReqAffinityQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OriginDestination" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCityDeparture" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CountryDeparture" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ReferencePointDeparture" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}StateProvDeparture" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCityArrival" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CountryArrival" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ReferencePointArrival" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}StateProvArrival" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}StayPeriodDateRange" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}StayPeriodSeason" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TripDistance" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TripTime" minOccurs="0"/&gt;
 *         &lt;element name="Budget" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupBudget"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PerPersonBudget"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="KeywordPreferences" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="KeywordPreference" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWordType"&gt;
 *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ShoppingResponseIDs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirShopReqAffinityQueryType", propOrder = {
    "originDestination",
    "stayPeriodDateRange",
    "stayPeriodSeason",
    "tripDistance",
    "tripTime",
    "budget",
    "keywordPreferences",
    "shoppingResponseIDs"
})
public class AirShopReqAffinityQueryType {

    @XmlElement(name = "OriginDestination", required = true)
    protected List<AirShopReqAffinityQueryType.OriginDestination> originDestination;
    @XmlElement(name = "StayPeriodDateRange")
    protected StayPeriodDateRangeType stayPeriodDateRange;
    @XmlElement(name = "StayPeriodSeason")
    protected StayPeriodSeasonType stayPeriodSeason;
    @XmlElement(name = "TripDistance")
    protected TravelDistancePreferencesType tripDistance;
    @XmlElement(name = "TripTime")
    protected TravelTimePreferencesType tripTime;
    @XmlElement(name = "Budget")
    protected AirShopReqAffinityQueryType.Budget budget;
    @XmlElement(name = "KeywordPreferences")
    protected AirShopReqAffinityQueryType.KeywordPreferences keywordPreferences;
    @XmlElement(name = "ShoppingResponseIDs")
    protected ShoppingResponseIDType shoppingResponseIDs;

    /**
     * Gets the value of the originDestination property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the originDestination property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginDestination().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirShopReqAffinityQueryType.OriginDestination }
     * 
     * 
     */
    public List<AirShopReqAffinityQueryType.OriginDestination> getOriginDestination() {
        if (originDestination == null) {
            originDestination = new ArrayList<AirShopReqAffinityQueryType.OriginDestination>();
        }
        return this.originDestination;
    }

    /**
     * 获取stayPeriodDateRange属性的值。
     * 
     * @return
     *     possible object is
     *     {@link StayPeriodDateRangeType }
     *     
     */
    public StayPeriodDateRangeType getStayPeriodDateRange() {
        return stayPeriodDateRange;
    }

    /**
     * 设置stayPeriodDateRange属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link StayPeriodDateRangeType }
     *     
     */
    public void setStayPeriodDateRange(StayPeriodDateRangeType value) {
        this.stayPeriodDateRange = value;
    }

    /**
     * 获取stayPeriodSeason属性的值。
     * 
     * @return
     *     possible object is
     *     {@link StayPeriodSeasonType }
     *     
     */
    public StayPeriodSeasonType getStayPeriodSeason() {
        return stayPeriodSeason;
    }

    /**
     * 设置stayPeriodSeason属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link StayPeriodSeasonType }
     *     
     */
    public void setStayPeriodSeason(StayPeriodSeasonType value) {
        this.stayPeriodSeason = value;
    }

    /**
     * 获取tripDistance属性的值。
     * 
     * @return
     *     possible object is
     *     {@link TravelDistancePreferencesType }
     *     
     */
    public TravelDistancePreferencesType getTripDistance() {
        return tripDistance;
    }

    /**
     * 设置tripDistance属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link TravelDistancePreferencesType }
     *     
     */
    public void setTripDistance(TravelDistancePreferencesType value) {
        this.tripDistance = value;
    }

    /**
     * 获取tripTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link TravelTimePreferencesType }
     *     
     */
    public TravelTimePreferencesType getTripTime() {
        return tripTime;
    }

    /**
     * 设置tripTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link TravelTimePreferencesType }
     *     
     */
    public void setTripTime(TravelTimePreferencesType value) {
        this.tripTime = value;
    }

    /**
     * 获取budget属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AirShopReqAffinityQueryType.Budget }
     *     
     */
    public AirShopReqAffinityQueryType.Budget getBudget() {
        return budget;
    }

    /**
     * 设置budget属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AirShopReqAffinityQueryType.Budget }
     *     
     */
    public void setBudget(AirShopReqAffinityQueryType.Budget value) {
        this.budget = value;
    }

    /**
     * 获取keywordPreferences属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AirShopReqAffinityQueryType.KeywordPreferences }
     *     
     */
    public AirShopReqAffinityQueryType.KeywordPreferences getKeywordPreferences() {
        return keywordPreferences;
    }

    /**
     * 设置keywordPreferences属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AirShopReqAffinityQueryType.KeywordPreferences }
     *     
     */
    public void setKeywordPreferences(AirShopReqAffinityQueryType.KeywordPreferences value) {
        this.keywordPreferences = value;
    }

    /**
     * 获取shoppingResponseIDs属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ShoppingResponseIDType }
     *     
     */
    public ShoppingResponseIDType getShoppingResponseIDs() {
        return shoppingResponseIDs;
    }

    /**
     * 设置shoppingResponseIDs属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ShoppingResponseIDType }
     *     
     */
    public void setShoppingResponseIDs(ShoppingResponseIDType value) {
        this.shoppingResponseIDs = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}GroupBudget"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PerPersonBudget"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "groupBudget",
        "perPersonBudget"
    })
    public static class Budget {

        @XmlElement(name = "GroupBudget")
        protected GroupBudgetType groupBudget;
        @XmlElement(name = "PerPersonBudget")
        protected PersonBudgetType perPersonBudget;

        /**
         * 获取groupBudget属性的值。
         * 
         * @return
         *     possible object is
         *     {@link GroupBudgetType }
         *     
         */
        public GroupBudgetType getGroupBudget() {
            return groupBudget;
        }

        /**
         * 设置groupBudget属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link GroupBudgetType }
         *     
         */
        public void setGroupBudget(GroupBudgetType value) {
            this.groupBudget = value;
        }

        /**
         * 获取perPersonBudget属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PersonBudgetType }
         *     
         */
        public PersonBudgetType getPerPersonBudget() {
            return perPersonBudget;
        }

        /**
         * 设置perPersonBudget属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PersonBudgetType }
         *     
         */
        public void setPerPersonBudget(PersonBudgetType value) {
            this.perPersonBudget = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="KeywordPreference" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWordType"&gt;
     *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "keywordPreference"
    })
    public static class KeywordPreferences {

        @XmlElement(name = "KeywordPreference", required = true)
        protected List<AirShopReqAffinityQueryType.KeywordPreferences.KeywordPreference> keywordPreference;

        /**
         * Gets the value of the keywordPreference property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the keywordPreference property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getKeywordPreference().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AirShopReqAffinityQueryType.KeywordPreferences.KeywordPreference }
         * 
         * 
         */
        public List<AirShopReqAffinityQueryType.KeywordPreferences.KeywordPreference> getKeywordPreference() {
            if (keywordPreference == null) {
                keywordPreference = new ArrayList<AirShopReqAffinityQueryType.KeywordPreferences.KeywordPreference>();
            }
            return this.keywordPreference;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWordType"&gt;
         *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class KeywordPreference
            extends KeyWordType
        {

            @XmlAttribute(name = "PreferencesLevel")
            protected String preferencesLevel;
            @XmlAttribute(name = "PreferencesContext")
            protected String preferencesContext;

            /**
             * 获取preferencesLevel属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPreferencesLevel() {
                return preferencesLevel;
            }

            /**
             * 设置preferencesLevel属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPreferencesLevel(String value) {
                this.preferencesLevel = value;
            }

            /**
             * 获取preferencesContext属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPreferencesContext() {
                return preferencesContext;
            }

            /**
             * 设置preferencesContext属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPreferencesContext(String value) {
                this.preferencesContext = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCityDeparture" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CountryDeparture" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ReferencePointDeparture" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}StateProvDeparture" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCityArrival" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CountryArrival" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ReferencePointArrival" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}StateProvArrival" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SegMaxTimePreferences" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airportCityDeparture",
        "countryDeparture",
        "referencePointDeparture",
        "stateProvDeparture",
        "airportCityArrival",
        "countryArrival",
        "referencePointArrival",
        "stateProvArrival",
        "segMaxTimePreferences"
    })
    public static class OriginDestination
        extends AssociatedObjectBaseType
    {

        @XmlElement(name = "AirportCityDeparture")
        protected FltDepartQualifiedQueryType airportCityDeparture;
        @XmlElement(name = "CountryDeparture")
        protected AffinityCountryDepartType countryDeparture;
        @XmlElement(name = "ReferencePointDeparture")
        protected AffinityReferencePointDepartType referencePointDeparture;
        @XmlElement(name = "StateProvDeparture")
        protected AffinityStateProvDepartType stateProvDeparture;
        @XmlElement(name = "AirportCityArrival")
        protected FltArriveQualifiedQueryType airportCityArrival;
        @XmlElement(name = "CountryArrival")
        protected AffinityCountryArriveType countryArrival;
        @XmlElement(name = "ReferencePointArrival")
        protected AffinityReferencePointArriveType referencePointArrival;
        @XmlElement(name = "StateProvArrival")
        protected AffinityStateProvArriveType stateProvArrival;
        @XmlElement(name = "SegMaxTimePreferences")
        protected FltSegmentMaxTimePreferencesType segMaxTimePreferences;

        /**
         * 获取airportCityDeparture属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FltDepartQualifiedQueryType }
         *     
         */
        public FltDepartQualifiedQueryType getAirportCityDeparture() {
            return airportCityDeparture;
        }

        /**
         * 设置airportCityDeparture属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FltDepartQualifiedQueryType }
         *     
         */
        public void setAirportCityDeparture(FltDepartQualifiedQueryType value) {
            this.airportCityDeparture = value;
        }

        /**
         * 获取countryDeparture属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AffinityCountryDepartType }
         *     
         */
        public AffinityCountryDepartType getCountryDeparture() {
            return countryDeparture;
        }

        /**
         * 设置countryDeparture属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AffinityCountryDepartType }
         *     
         */
        public void setCountryDeparture(AffinityCountryDepartType value) {
            this.countryDeparture = value;
        }

        /**
         * 获取referencePointDeparture属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AffinityReferencePointDepartType }
         *     
         */
        public AffinityReferencePointDepartType getReferencePointDeparture() {
            return referencePointDeparture;
        }

        /**
         * 设置referencePointDeparture属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AffinityReferencePointDepartType }
         *     
         */
        public void setReferencePointDeparture(AffinityReferencePointDepartType value) {
            this.referencePointDeparture = value;
        }

        /**
         * 获取stateProvDeparture属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AffinityStateProvDepartType }
         *     
         */
        public AffinityStateProvDepartType getStateProvDeparture() {
            return stateProvDeparture;
        }

        /**
         * 设置stateProvDeparture属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AffinityStateProvDepartType }
         *     
         */
        public void setStateProvDeparture(AffinityStateProvDepartType value) {
            this.stateProvDeparture = value;
        }

        /**
         * 获取airportCityArrival属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FltArriveQualifiedQueryType }
         *     
         */
        public FltArriveQualifiedQueryType getAirportCityArrival() {
            return airportCityArrival;
        }

        /**
         * 设置airportCityArrival属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FltArriveQualifiedQueryType }
         *     
         */
        public void setAirportCityArrival(FltArriveQualifiedQueryType value) {
            this.airportCityArrival = value;
        }

        /**
         * 获取countryArrival属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AffinityCountryArriveType }
         *     
         */
        public AffinityCountryArriveType getCountryArrival() {
            return countryArrival;
        }

        /**
         * 设置countryArrival属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AffinityCountryArriveType }
         *     
         */
        public void setCountryArrival(AffinityCountryArriveType value) {
            this.countryArrival = value;
        }

        /**
         * 获取referencePointArrival属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AffinityReferencePointArriveType }
         *     
         */
        public AffinityReferencePointArriveType getReferencePointArrival() {
            return referencePointArrival;
        }

        /**
         * 设置referencePointArrival属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AffinityReferencePointArriveType }
         *     
         */
        public void setReferencePointArrival(AffinityReferencePointArriveType value) {
            this.referencePointArrival = value;
        }

        /**
         * 获取stateProvArrival属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AffinityStateProvArriveType }
         *     
         */
        public AffinityStateProvArriveType getStateProvArrival() {
            return stateProvArrival;
        }

        /**
         * 设置stateProvArrival属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AffinityStateProvArriveType }
         *     
         */
        public void setStateProvArrival(AffinityStateProvArriveType value) {
            this.stateProvArrival = value;
        }

        /**
         * 获取segMaxTimePreferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link FltSegmentMaxTimePreferencesType }
         *     
         */
        public FltSegmentMaxTimePreferencesType getSegMaxTimePreferences() {
            return segMaxTimePreferences;
        }

        /**
         * 设置segMaxTimePreferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link FltSegmentMaxTimePreferencesType }
         *     
         */
        public void setSegMaxTimePreferences(FltSegmentMaxTimePreferencesType value) {
            this.segMaxTimePreferences = value;
        }

    }

}
