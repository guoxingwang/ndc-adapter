//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for a list of Classes of Service.
 * 
 * <p>ListOfClassOfServiceType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfClassOfServiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceClass" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}DataListObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Code" type="{http://www.iata.org/IATA/EDIST}Flight_COS_SimpleType" minOccurs="0"/&gt;
 *                   &lt;element name="MarketingName" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
 *                   &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}COS_AssocType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfClassOfServiceType", propOrder = {
    "serviceClass"
})
public class ListOfClassOfServiceType {

    @XmlElement(name = "ServiceClass", required = true)
    protected List<ListOfClassOfServiceType.ServiceClass> serviceClass;

    /**
     * Gets the value of the serviceClass property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceClass property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceClass().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListOfClassOfServiceType.ServiceClass }
     * 
     * 
     */
    public List<ListOfClassOfServiceType.ServiceClass> getServiceClass() {
        if (serviceClass == null) {
            serviceClass = new ArrayList<ListOfClassOfServiceType.ServiceClass>();
        }
        return this.serviceClass;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}DataListObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Code" type="{http://www.iata.org/IATA/EDIST}Flight_COS_SimpleType" minOccurs="0"/&gt;
     *         &lt;element name="MarketingName" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
     *         &lt;element name="Associations" type="{http://www.iata.org/IATA/EDIST}COS_AssocType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "marketingName",
        "associations"
    })
    public static class ServiceClass
        extends DataListObjectBaseType
    {

        @XmlElement(name = "Code")
        protected String code;
        @XmlElement(name = "MarketingName")
        protected String marketingName;
        @XmlElement(name = "Associations")
        protected COSAssocType associations;

        /**
         * 获取code属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * 设置code属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * 获取marketingName属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMarketingName() {
            return marketingName;
        }

        /**
         * 设置marketingName属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMarketingName(String value) {
            this.marketingName = value;
        }

        /**
         * 获取associations属性的值。
         * 
         * @return
         *     possible object is
         *     {@link COSAssocType }
         *     
         */
        public COSAssocType getAssociations() {
            return associations;
        }

        /**
         * 设置associations属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link COSAssocType }
         *     
         */
        public void setAssociations(COSAssocType value) {
            this.associations = value;
        }

    }

}
