//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * OPTIONAL SERVICE BUNDLE Data List definition.
 * 
 * <p>ListOfServiceBundleType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ListOfServiceBundleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceBundle" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}DataListObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ItemCount" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
 *                   &lt;element name="Associations"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;choice&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceReference" maxOccurs="unbounded"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" maxOccurs="unbounded"/&gt;
 *                           &lt;/choice&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Selection" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;choice maxOccurs="2"&gt;
 *                             &lt;element name="MinimumQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
 *                             &lt;element name="MaximumQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
 *                           &lt;/choice&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="BundleID" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;UniqueStringID_SimpleType"&gt;
 *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectKeyAttrGroup"/&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Price" type="{http://www.iata.org/IATA/EDIST}ServicePriceType" minOccurs="0"/&gt;
 *                   &lt;element name="BundleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfServiceBundleType", propOrder = {
    "serviceBundle"
})
public class ListOfServiceBundleType {

    @XmlElement(name = "ServiceBundle", required = true)
    protected List<ListOfServiceBundleType.ServiceBundle> serviceBundle;

    /**
     * Gets the value of the serviceBundle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceBundle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceBundle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListOfServiceBundleType.ServiceBundle }
     * 
     * 
     */
    public List<ListOfServiceBundleType.ServiceBundle> getServiceBundle() {
        if (serviceBundle == null) {
            serviceBundle = new ArrayList<ListOfServiceBundleType.ServiceBundle>();
        }
        return this.serviceBundle;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}DataListObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ItemCount" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
     *         &lt;element name="Associations"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;choice&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceReference" maxOccurs="unbounded"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" maxOccurs="unbounded"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Selection" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;choice maxOccurs="2"&gt;
     *                   &lt;element name="MinimumQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
     *                   &lt;element name="MaximumQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="BundleID" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;UniqueStringID_SimpleType"&gt;
     *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectKeyAttrGroup"/&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Price" type="{http://www.iata.org/IATA/EDIST}ServicePriceType" minOccurs="0"/&gt;
     *         &lt;element name="BundleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itemCount",
        "associations",
        "selection",
        "bundleID",
        "price",
        "bundleName"
    })
    public static class ServiceBundle
        extends DataListObjectBaseType
    {

        @XmlElement(name = "ItemCount", required = true)
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger itemCount;
        @XmlElement(name = "Associations", required = true)
        protected ListOfServiceBundleType.ServiceBundle.Associations associations;
        @XmlElement(name = "Selection")
        protected ListOfServiceBundleType.ServiceBundle.Selection selection;
        @XmlElement(name = "BundleID")
        protected ListOfServiceBundleType.ServiceBundle.BundleID bundleID;
        @XmlElement(name = "Price")
        protected ServicePriceType price;
        @XmlElement(name = "BundleName")
        protected String bundleName;

        /**
         * 获取itemCount属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getItemCount() {
            return itemCount;
        }

        /**
         * 设置itemCount属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setItemCount(BigInteger value) {
            this.itemCount = value;
        }

        /**
         * 获取associations属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ListOfServiceBundleType.ServiceBundle.Associations }
         *     
         */
        public ListOfServiceBundleType.ServiceBundle.Associations getAssociations() {
            return associations;
        }

        /**
         * 设置associations属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfServiceBundleType.ServiceBundle.Associations }
         *     
         */
        public void setAssociations(ListOfServiceBundleType.ServiceBundle.Associations value) {
            this.associations = value;
        }

        /**
         * 获取selection属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ListOfServiceBundleType.ServiceBundle.Selection }
         *     
         */
        public ListOfServiceBundleType.ServiceBundle.Selection getSelection() {
            return selection;
        }

        /**
         * 设置selection属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfServiceBundleType.ServiceBundle.Selection }
         *     
         */
        public void setSelection(ListOfServiceBundleType.ServiceBundle.Selection value) {
            this.selection = value;
        }

        /**
         * 获取bundleID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ListOfServiceBundleType.ServiceBundle.BundleID }
         *     
         */
        public ListOfServiceBundleType.ServiceBundle.BundleID getBundleID() {
            return bundleID;
        }

        /**
         * 设置bundleID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfServiceBundleType.ServiceBundle.BundleID }
         *     
         */
        public void setBundleID(ListOfServiceBundleType.ServiceBundle.BundleID value) {
            this.bundleID = value;
        }

        /**
         * 获取price属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServicePriceType }
         *     
         */
        public ServicePriceType getPrice() {
            return price;
        }

        /**
         * 设置price属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServicePriceType }
         *     
         */
        public void setPrice(ServicePriceType value) {
            this.price = value;
        }

        /**
         * 获取bundleName属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBundleName() {
            return bundleName;
        }

        /**
         * 设置bundleName属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBundleName(String value) {
            this.bundleName = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceReference" maxOccurs="unbounded"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceID" maxOccurs="unbounded"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "serviceReference",
            "serviceID"
        })
        public static class Associations {

            @XmlElementRef(name = "ServiceReference", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false)
            protected List<JAXBElement<Object>> serviceReference;
            @XmlElement(name = "ServiceID")
            protected List<ServiceIDType> serviceID;

            /**
             * Gets the value of the serviceReference property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the serviceReference property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getServiceReference().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link JAXBElement }{@code <}{@link Object }{@code >}
             * 
             * 
             */
            public List<JAXBElement<Object>> getServiceReference() {
                if (serviceReference == null) {
                    serviceReference = new ArrayList<JAXBElement<Object>>();
                }
                return this.serviceReference;
            }

            /**
             * Gets the value of the serviceID property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the serviceID property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getServiceID().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ServiceIDType }
             * 
             * 
             */
            public List<ServiceIDType> getServiceID() {
                if (serviceID == null) {
                    serviceID = new ArrayList<ServiceIDType>();
                }
                return this.serviceID;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;UniqueStringID_SimpleType"&gt;
         *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectKeyAttrGroup"/&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class BundleID {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "refs")
            @XmlIDREF
            protected List<Object> refs;
            @XmlAttribute(name = "ObjectKey")
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            @XmlID
            @XmlSchemaType(name = "ID")
            protected String objectKey;

            /**
             * A data type for a Unique String Identifier constraint.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the refs property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the refs property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRefs().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public List<Object> getRefs() {
                if (refs == null) {
                    refs = new ArrayList<Object>();
                }
                return this.refs;
            }

            /**
             * 获取objectKey属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getObjectKey() {
                return objectKey;
            }

            /**
             * 设置objectKey属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setObjectKey(String value) {
                this.objectKey = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice maxOccurs="2"&gt;
         *         &lt;element name="MinimumQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
         *         &lt;element name="MaximumQuantity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "minimumQuantityOrMaximumQuantity"
        })
        public static class Selection {

            @XmlElementRefs({
                @XmlElementRef(name = "MinimumQuantity", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false),
                @XmlElementRef(name = "MaximumQuantity", namespace = "http://www.iata.org/IATA/EDIST", type = JAXBElement.class, required = false)
            })
            protected List<JAXBElement<BigInteger>> minimumQuantityOrMaximumQuantity;

            /**
             * Gets the value of the minimumQuantityOrMaximumQuantity property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the minimumQuantityOrMaximumQuantity property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getMinimumQuantityOrMaximumQuantity().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
             * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
             * 
             * 
             */
            public List<JAXBElement<BigInteger>> getMinimumQuantityOrMaximumQuantity() {
                if (minimumQuantityOrMaximumQuantity == null) {
                    minimumQuantityOrMaximumQuantity = new ArrayList<JAXBElement<BigInteger>>();
                }
                return this.minimumQuantityOrMaximumQuantity;
            }

        }

    }

}
