package com.hpe.cms.ndc.server.convertor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:09 2019/7/27
 * @ Description：
 * @ Modified By：
 */
public abstract class AbstractConvertor {

    private String sourceVersion;
    private String targetVersion;

    public AbstractConvertor(String sourceVersion, String targetVersion) {
        this.sourceVersion = sourceVersion;
        this.targetVersion = targetVersion;
    }

    /**
     * 通过方法反射，调用具体的converte方法
     *
     * @param source
     * @return
     */
    public Object converte(Object source) {
        try {
            Method method = this.getClass().getDeclaredMethod("converte" + "_" + this.sourceVersion + "_" + this.targetVersion, Object.class);
            return method.invoke(this, source);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return source;
    }
}
