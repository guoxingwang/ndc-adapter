//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         Optional Service Filter Notification. Note: Supports ATPCO group and subgroup code(s) that specify the requested categories of Optional Services to be returned in the results.
 *       
 * 
 * <p>ServiceFilterNoticeType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ServiceFilterNoticeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}TrxProcessObjectBaseType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="Query"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ServiceFilter" type="{http://www.iata.org/IATA/EDIST}ServiceFilterType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Results"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="AppliedFilters" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="AppliedFilter" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}ServiceFilterType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="Associations" minOccurs="0"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;choice&gt;
 *                                                 &lt;element name="Offer"&gt;
 *                                                   &lt;complexType&gt;
 *                                                     &lt;complexContent&gt;
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                         &lt;sequence&gt;
 *                                                           &lt;element name="Shopper" type="{http://www.iata.org/IATA/EDIST}ShopperInfoAssocType" minOccurs="0"/&gt;
 *                                                           &lt;element name="Flight" type="{http://www.iata.org/IATA/EDIST}FlightInfoAssocType" minOccurs="0"/&gt;
 *                                                           &lt;element name="PriceClass" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;sequence&gt;
 *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
 *                                                                   &lt;/sequence&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                           &lt;element name="BagDetails" type="{http://www.iata.org/IATA/EDIST}BagDetailAssocType" minOccurs="0"/&gt;
 *                                                           &lt;element name="OfferDetails" type="{http://www.iata.org/IATA/EDIST}OfferDetailInfoAssocType" minOccurs="0"/&gt;
 *                                                           &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                                             &lt;complexType&gt;
 *                                                               &lt;complexContent&gt;
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                   &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
 *                                                                   &lt;attribute name="RefValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
 *                                                                 &lt;/restriction&gt;
 *                                                               &lt;/complexContent&gt;
 *                                                             &lt;/complexType&gt;
 *                                                           &lt;/element&gt;
 *                                                         &lt;/sequence&gt;
 *                                                       &lt;/restriction&gt;
 *                                                     &lt;/complexContent&gt;
 *                                                   &lt;/complexType&gt;
 *                                                 &lt;/element&gt;
 *                                                 &lt;element name="Order" type="{http://www.iata.org/IATA/EDIST}OrderItemAssociationType"/&gt;
 *                                               &lt;/choice&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="SrvcFilterAppliedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceFilterNoticeType", propOrder = {
    "query",
    "results"
})
public class ServiceFilterNoticeType
    extends TrxProcessObjectBaseType
{

    @XmlElement(name = "Query")
    protected ServiceFilterNoticeType.Query query;
    @XmlElement(name = "Results")
    protected ServiceFilterNoticeType.Results results;

    /**
     * 获取query属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ServiceFilterNoticeType.Query }
     *     
     */
    public ServiceFilterNoticeType.Query getQuery() {
        return query;
    }

    /**
     * 设置query属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFilterNoticeType.Query }
     *     
     */
    public void setQuery(ServiceFilterNoticeType.Query value) {
        this.query = value;
    }

    /**
     * 获取results属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ServiceFilterNoticeType.Results }
     *     
     */
    public ServiceFilterNoticeType.Results getResults() {
        return results;
    }

    /**
     * 设置results属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFilterNoticeType.Results }
     *     
     */
    public void setResults(ServiceFilterNoticeType.Results value) {
        this.results = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ServiceFilter" type="{http://www.iata.org/IATA/EDIST}ServiceFilterType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceFilter"
    })
    public static class Query {

        @XmlElement(name = "ServiceFilter", required = true)
        protected List<ServiceFilterType> serviceFilter;

        /**
         * Gets the value of the serviceFilter property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the serviceFilter property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getServiceFilter().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ServiceFilterType }
         * 
         * 
         */
        public List<ServiceFilterType> getServiceFilter() {
            if (serviceFilter == null) {
                serviceFilter = new ArrayList<ServiceFilterType>();
            }
            return this.serviceFilter;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AppliedFilters" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="AppliedFilter" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}ServiceFilterType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="Associations" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;choice&gt;
     *                                       &lt;element name="Offer"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="Shopper" type="{http://www.iata.org/IATA/EDIST}ShopperInfoAssocType" minOccurs="0"/&gt;
     *                                                 &lt;element name="Flight" type="{http://www.iata.org/IATA/EDIST}FlightInfoAssocType" minOccurs="0"/&gt;
     *                                                 &lt;element name="PriceClass" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                                 &lt;element name="BagDetails" type="{http://www.iata.org/IATA/EDIST}BagDetailAssocType" minOccurs="0"/&gt;
     *                                                 &lt;element name="OfferDetails" type="{http://www.iata.org/IATA/EDIST}OfferDetailInfoAssocType" minOccurs="0"/&gt;
     *                                                 &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
     *                                                         &lt;attribute name="RefValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="Order" type="{http://www.iata.org/IATA/EDIST}OrderItemAssociationType"/&gt;
     *                                     &lt;/choice&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="SrvcFilterAppliedInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "appliedFilters"
    })
    public static class Results {

        @XmlElement(name = "AppliedFilters")
        protected ServiceFilterNoticeType.Results.AppliedFilters appliedFilters;
        @XmlAttribute(name = "SrvcFilterAppliedInd")
        protected Boolean srvcFilterAppliedInd;

        /**
         * 获取appliedFilters属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceFilterNoticeType.Results.AppliedFilters }
         *     
         */
        public ServiceFilterNoticeType.Results.AppliedFilters getAppliedFilters() {
            return appliedFilters;
        }

        /**
         * 设置appliedFilters属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceFilterNoticeType.Results.AppliedFilters }
         *     
         */
        public void setAppliedFilters(ServiceFilterNoticeType.Results.AppliedFilters value) {
            this.appliedFilters = value;
        }

        /**
         * 获取srvcFilterAppliedInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isSrvcFilterAppliedInd() {
            return srvcFilterAppliedInd;
        }

        /**
         * 设置srvcFilterAppliedInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSrvcFilterAppliedInd(Boolean value) {
            this.srvcFilterAppliedInd = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="AppliedFilter" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;extension base="{http://www.iata.org/IATA/EDIST}ServiceFilterType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="Associations" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;choice&gt;
         *                             &lt;element name="Offer"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="Shopper" type="{http://www.iata.org/IATA/EDIST}ShopperInfoAssocType" minOccurs="0"/&gt;
         *                                       &lt;element name="Flight" type="{http://www.iata.org/IATA/EDIST}FlightInfoAssocType" minOccurs="0"/&gt;
         *                                       &lt;element name="PriceClass" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                       &lt;element name="BagDetails" type="{http://www.iata.org/IATA/EDIST}BagDetailAssocType" minOccurs="0"/&gt;
         *                                       &lt;element name="OfferDetails" type="{http://www.iata.org/IATA/EDIST}OfferDetailInfoAssocType" minOccurs="0"/&gt;
         *                                       &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
         *                                               &lt;attribute name="RefValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="Order" type="{http://www.iata.org/IATA/EDIST}OrderItemAssociationType"/&gt;
         *                           &lt;/choice&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/extension&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "appliedFilter"
        })
        public static class AppliedFilters {

            @XmlElement(name = "AppliedFilter", required = true)
            protected List<ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter> appliedFilter;

            /**
             * Gets the value of the appliedFilter property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the appliedFilter property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAppliedFilter().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter }
             * 
             * 
             */
            public List<ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter> getAppliedFilter() {
                if (appliedFilter == null) {
                    appliedFilter = new ArrayList<ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter>();
                }
                return this.appliedFilter;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;extension base="{http://www.iata.org/IATA/EDIST}ServiceFilterType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="Associations" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;choice&gt;
             *                   &lt;element name="Offer"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="Shopper" type="{http://www.iata.org/IATA/EDIST}ShopperInfoAssocType" minOccurs="0"/&gt;
             *                             &lt;element name="Flight" type="{http://www.iata.org/IATA/EDIST}FlightInfoAssocType" minOccurs="0"/&gt;
             *                             &lt;element name="PriceClass" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                             &lt;element name="BagDetails" type="{http://www.iata.org/IATA/EDIST}BagDetailAssocType" minOccurs="0"/&gt;
             *                             &lt;element name="OfferDetails" type="{http://www.iata.org/IATA/EDIST}OfferDetailInfoAssocType" minOccurs="0"/&gt;
             *                             &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
             *                                     &lt;attribute name="RefValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="Order" type="{http://www.iata.org/IATA/EDIST}OrderItemAssociationType"/&gt;
             *                 &lt;/choice&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/extension&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "associations"
            })
            public static class AppliedFilter
                extends ServiceFilterType
            {

                @XmlElement(name = "Associations")
                protected ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations associations;

                /**
                 * 获取associations属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations }
                 *     
                 */
                public ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations getAssociations() {
                    return associations;
                }

                /**
                 * 设置associations属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations }
                 *     
                 */
                public void setAssociations(ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations value) {
                    this.associations = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;choice&gt;
                 *         &lt;element name="Offer"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="Shopper" type="{http://www.iata.org/IATA/EDIST}ShopperInfoAssocType" minOccurs="0"/&gt;
                 *                   &lt;element name="Flight" type="{http://www.iata.org/IATA/EDIST}FlightInfoAssocType" minOccurs="0"/&gt;
                 *                   &lt;element name="PriceClass" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                   &lt;element name="BagDetails" type="{http://www.iata.org/IATA/EDIST}BagDetailAssocType" minOccurs="0"/&gt;
                 *                   &lt;element name="OfferDetails" type="{http://www.iata.org/IATA/EDIST}OfferDetailInfoAssocType" minOccurs="0"/&gt;
                 *                   &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                 *                           &lt;attribute name="RefValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="Order" type="{http://www.iata.org/IATA/EDIST}OrderItemAssociationType"/&gt;
                 *       &lt;/choice&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "offer",
                    "order"
                })
                public static class Associations {

                    @XmlElement(name = "Offer")
                    protected ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer offer;
                    @XmlElement(name = "Order")
                    protected OrderItemAssociationType order;

                    /**
                     * 获取offer属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer }
                     *     
                     */
                    public ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer getOffer() {
                        return offer;
                    }

                    /**
                     * 设置offer属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer }
                     *     
                     */
                    public void setOffer(ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer value) {
                        this.offer = value;
                    }

                    /**
                     * 获取order属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link OrderItemAssociationType }
                     *     
                     */
                    public OrderItemAssociationType getOrder() {
                        return order;
                    }

                    /**
                     * 设置order属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OrderItemAssociationType }
                     *     
                     */
                    public void setOrder(OrderItemAssociationType value) {
                        this.order = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="Shopper" type="{http://www.iata.org/IATA/EDIST}ShopperInfoAssocType" minOccurs="0"/&gt;
                     *         &lt;element name="Flight" type="{http://www.iata.org/IATA/EDIST}FlightInfoAssocType" minOccurs="0"/&gt;
                     *         &lt;element name="PriceClass" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *         &lt;element name="BagDetails" type="{http://www.iata.org/IATA/EDIST}BagDetailAssocType" minOccurs="0"/&gt;
                     *         &lt;element name="OfferDetails" type="{http://www.iata.org/IATA/EDIST}OfferDetailInfoAssocType" minOccurs="0"/&gt;
                     *         &lt;element name="OtherAssociation" maxOccurs="unbounded" minOccurs="0"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                     *                 &lt;attribute name="RefValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "shopper",
                        "flight",
                        "priceClass",
                        "bagDetails",
                        "offerDetails",
                        "otherAssociation"
                    })
                    public static class Offer {

                        @XmlElement(name = "Shopper")
                        protected ShopperInfoAssocType shopper;
                        @XmlElement(name = "Flight")
                        protected FlightInfoAssocType flight;
                        @XmlElement(name = "PriceClass")
                        protected ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.PriceClass priceClass;
                        @XmlElement(name = "BagDetails")
                        protected BagDetailAssocType bagDetails;
                        @XmlElement(name = "OfferDetails")
                        protected OfferDetailInfoAssocType offerDetails;
                        @XmlElement(name = "OtherAssociation")
                        protected List<ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.OtherAssociation> otherAssociation;

                        /**
                         * 获取shopper属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ShopperInfoAssocType }
                         *     
                         */
                        public ShopperInfoAssocType getShopper() {
                            return shopper;
                        }

                        /**
                         * 设置shopper属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ShopperInfoAssocType }
                         *     
                         */
                        public void setShopper(ShopperInfoAssocType value) {
                            this.shopper = value;
                        }

                        /**
                         * 获取flight属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link FlightInfoAssocType }
                         *     
                         */
                        public FlightInfoAssocType getFlight() {
                            return flight;
                        }

                        /**
                         * 设置flight属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link FlightInfoAssocType }
                         *     
                         */
                        public void setFlight(FlightInfoAssocType value) {
                            this.flight = value;
                        }

                        /**
                         * 获取priceClass属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.PriceClass }
                         *     
                         */
                        public ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.PriceClass getPriceClass() {
                            return priceClass;
                        }

                        /**
                         * 设置priceClass属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.PriceClass }
                         *     
                         */
                        public void setPriceClass(ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.PriceClass value) {
                            this.priceClass = value;
                        }

                        /**
                         * 获取bagDetails属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link BagDetailAssocType }
                         *     
                         */
                        public BagDetailAssocType getBagDetails() {
                            return bagDetails;
                        }

                        /**
                         * 设置bagDetails属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BagDetailAssocType }
                         *     
                         */
                        public void setBagDetails(BagDetailAssocType value) {
                            this.bagDetails = value;
                        }

                        /**
                         * 获取offerDetails属性的值。
                         * 
                         * @return
                         *     possible object is
                         *     {@link OfferDetailInfoAssocType }
                         *     
                         */
                        public OfferDetailInfoAssocType getOfferDetails() {
                            return offerDetails;
                        }

                        /**
                         * 设置offerDetails属性的值。
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link OfferDetailInfoAssocType }
                         *     
                         */
                        public void setOfferDetails(OfferDetailInfoAssocType value) {
                            this.offerDetails = value;
                        }

                        /**
                         * Gets the value of the otherAssociation property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the otherAssociation property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getOtherAssociation().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.OtherAssociation }
                         * 
                         * 
                         */
                        public List<ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.OtherAssociation> getOtherAssociation() {
                            if (otherAssociation == null) {
                                otherAssociation = new ArrayList<ServiceFilterNoticeType.Results.AppliedFilters.AppliedFilter.Associations.Offer.OtherAssociation>();
                            }
                            return this.otherAssociation;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;attribute name="Type" use="required" type="{http://www.iata.org/IATA/EDIST}OtherOrderAssocListType" /&gt;
                         *       &lt;attribute name="RefValue" use="required" type="{http://www.w3.org/2001/XMLSchema}IDREF" /&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class OtherAssociation {

                            @XmlAttribute(name = "Type", required = true)
                            protected String type;
                            @XmlAttribute(name = "RefValue", required = true)
                            @XmlIDREF
                            @XmlSchemaType(name = "IDREF")
                            protected Object refValue;

                            /**
                             * 获取type属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getType() {
                                return type;
                            }

                            /**
                             * 设置type属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setType(String value) {
                                this.type = value;
                            }

                            /**
                             * 获取refValue属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getRefValue() {
                                return refValue;
                            }

                            /**
                             * 设置refValue属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setRefValue(Object value) {
                                this.refValue = value;
                            }

                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PriceClassReference"/&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "priceClassReference"
                        })
                        public static class PriceClass {

                            @XmlElement(name = "PriceClassReference", required = true)
                            @XmlIDREF
                            @XmlSchemaType(name = "IDREF")
                            protected Object priceClassReference;

                            /**
                             * 获取priceClassReference属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link Object }
                             *     
                             */
                            public Object getPriceClassReference() {
                                return priceClassReference;
                            }

                            /**
                             * 设置priceClassReference属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link Object }
                             *     
                             */
                            public void setPriceClassReference(Object value) {
                                this.priceClassReference = value;
                            }

                        }

                    }

                }

            }

        }

    }

}
