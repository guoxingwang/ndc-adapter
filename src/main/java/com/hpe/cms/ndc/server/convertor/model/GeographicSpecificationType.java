//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Geographic Specification information
 * 
 * <p>GeographicSpecificationType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="GeographicSpecificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}CodesetType"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Area" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *           &lt;element name="SubArea" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}CountryCode"/&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}CityCode"/&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}StateCode"/&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
 *           &lt;element name="Coordinates" type="{http://www.iata.org/IATA/EDIST}PositionType"/&gt;
 *           &lt;element name="CoordinateRadius" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeographicSpecificationType", propOrder = {
    "type",
    "area",
    "subArea",
    "countryCode",
    "cityCode",
    "stateCode",
    "airportCode",
    "coordinates",
    "coordinateRadius"
})
public class GeographicSpecificationType {

    @XmlElement(name = "Type", required = true)
    protected CodesetType type;
    @XmlElement(name = "Area")
    protected Object area;
    @XmlElement(name = "SubArea")
    protected Object subArea;
    @XmlElement(name = "CountryCode")
    protected CountryCode countryCode;
    @XmlElement(name = "CityCode")
    protected CityCode cityCode;
    @XmlElement(name = "StateCode")
    protected StateProvCodeType stateCode;
    @XmlElement(name = "AirportCode")
    protected AirportCode airportCode;
    @XmlElement(name = "Coordinates")
    protected PositionType coordinates;
    @XmlElement(name = "CoordinateRadius")
    protected Object coordinateRadius;

    /**
     * 获取type属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getType() {
        return type;
    }

    /**
     * 设置type属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setType(CodesetType value) {
        this.type = value;
    }

    /**
     * 获取area属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getArea() {
        return area;
    }

    /**
     * 设置area属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setArea(Object value) {
        this.area = value;
    }

    /**
     * 获取subArea属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getSubArea() {
        return subArea;
    }

    /**
     * 设置subArea属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setSubArea(Object value) {
        this.subArea = value;
    }

    /**
     * 获取countryCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CountryCode }
     *     
     */
    public CountryCode getCountryCode() {
        return countryCode;
    }

    /**
     * 设置countryCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCode }
     *     
     */
    public void setCountryCode(CountryCode value) {
        this.countryCode = value;
    }

    /**
     * 获取cityCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CityCode }
     *     
     */
    public CityCode getCityCode() {
        return cityCode;
    }

    /**
     * 设置cityCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CityCode }
     *     
     */
    public void setCityCode(CityCode value) {
        this.cityCode = value;
    }

    /**
     * 获取stateCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link StateProvCodeType }
     *     
     */
    public StateProvCodeType getStateCode() {
        return stateCode;
    }

    /**
     * 设置stateCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link StateProvCodeType }
     *     
     */
    public void setStateCode(StateProvCodeType value) {
        this.stateCode = value;
    }

    /**
     * 获取airportCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AirportCode }
     *     
     */
    public AirportCode getAirportCode() {
        return airportCode;
    }

    /**
     * 设置airportCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AirportCode }
     *     
     */
    public void setAirportCode(AirportCode value) {
        this.airportCode = value;
    }

    /**
     * 获取coordinates属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PositionType }
     *     
     */
    public PositionType getCoordinates() {
        return coordinates;
    }

    /**
     * 设置coordinates属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PositionType }
     *     
     */
    public void setCoordinates(PositionType value) {
        this.coordinates = value;
    }

    /**
     * 获取coordinateRadius属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCoordinateRadius() {
        return coordinateRadius;
    }

    /**
     * 设置coordinateRadius属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCoordinateRadius(Object value) {
        this.coordinateRadius = value;
    }

}
