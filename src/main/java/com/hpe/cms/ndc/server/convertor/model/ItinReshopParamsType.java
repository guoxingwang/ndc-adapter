//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlType;


/**
 * Request Message Parameters.
 * 
 * <p>ItinReshopParamsType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ItinReshopParamsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Alerts" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PIN_AuthAlertResponse" type="{http://www.iata.org/IATA/EDIST}PinPhraseAnswerType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Notices" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}NoticeBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PricingParameters" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="AutoExchRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="IncludeAwardRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="AwardOnlyRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="SimpleRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="refs" type="{http://www.iata.org/IATA/EDIST}MultiAssocSimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItinReshopParamsType", propOrder = {
    "alerts",
    "notices"
})
public class ItinReshopParamsType {

    @XmlElement(name = "Alerts")
    protected ItinReshopParamsType.Alerts alerts;
    @XmlElement(name = "Notices")
    protected ItinReshopParamsType.Notices notices;
    @XmlAttribute(name = "refs")
    @XmlIDREF
    protected List<Object> refs;

    /**
     * 获取alerts属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ItinReshopParamsType.Alerts }
     *     
     */
    public ItinReshopParamsType.Alerts getAlerts() {
        return alerts;
    }

    /**
     * 设置alerts属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ItinReshopParamsType.Alerts }
     *     
     */
    public void setAlerts(ItinReshopParamsType.Alerts value) {
        this.alerts = value;
    }

    /**
     * 获取notices属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ItinReshopParamsType.Notices }
     *     
     */
    public ItinReshopParamsType.Notices getNotices() {
        return notices;
    }

    /**
     * 设置notices属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ItinReshopParamsType.Notices }
     *     
     */
    public void setNotices(ItinReshopParamsType.Notices value) {
        this.notices = value;
    }

    /**
     * Gets the value of the refs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getRefs() {
        if (refs == null) {
            refs = new ArrayList<Object>();
        }
        return this.refs;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PIN_AuthAlertResponse" type="{http://www.iata.org/IATA/EDIST}PinPhraseAnswerType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pinAuthAlertResponse"
    })
    public static class Alerts {

        @XmlElement(name = "PIN_AuthAlertResponse")
        protected PinPhraseAnswerType pinAuthAlertResponse;

        /**
         * 获取pinAuthAlertResponse属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PinPhraseAnswerType }
         *     
         */
        public PinPhraseAnswerType getPINAuthAlertResponse() {
            return pinAuthAlertResponse;
        }

        /**
         * 设置pinAuthAlertResponse属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PinPhraseAnswerType }
         *     
         */
        public void setPINAuthAlertResponse(PinPhraseAnswerType value) {
            this.pinAuthAlertResponse = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}NoticeBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PricingParameters" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="AutoExchRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="IncludeAwardRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="AwardOnlyRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="SimpleRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pricingParameters"
    })
    public static class Notices
        extends NoticeBaseType
    {

        @XmlElement(name = "PricingParameters")
        protected ItinReshopParamsType.Notices.PricingParameters pricingParameters;

        /**
         * 获取pricingParameters属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ItinReshopParamsType.Notices.PricingParameters }
         *     
         */
        public ItinReshopParamsType.Notices.PricingParameters getPricingParameters() {
            return pricingParameters;
        }

        /**
         * 设置pricingParameters属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ItinReshopParamsType.Notices.PricingParameters }
         *     
         */
        public void setPricingParameters(ItinReshopParamsType.Notices.PricingParameters value) {
            this.pricingParameters = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="AutoExchRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="IncludeAwardRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="AwardOnlyRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="SimpleRequestInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class PricingParameters {

            @XmlAttribute(name = "AutoExchRequestInd")
            protected Boolean autoExchRequestInd;
            @XmlAttribute(name = "IncludeAwardRequestInd")
            protected Boolean includeAwardRequestInd;
            @XmlAttribute(name = "AwardOnlyRequestInd")
            protected Boolean awardOnlyRequestInd;
            @XmlAttribute(name = "SimpleRequestInd")
            protected Boolean simpleRequestInd;

            /**
             * 获取autoExchRequestInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isAutoExchRequestInd() {
                return autoExchRequestInd;
            }

            /**
             * 设置autoExchRequestInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setAutoExchRequestInd(Boolean value) {
                this.autoExchRequestInd = value;
            }

            /**
             * 获取includeAwardRequestInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isIncludeAwardRequestInd() {
                return includeAwardRequestInd;
            }

            /**
             * 设置includeAwardRequestInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setIncludeAwardRequestInd(Boolean value) {
                this.includeAwardRequestInd = value;
            }

            /**
             * 获取awardOnlyRequestInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isAwardOnlyRequestInd() {
                return awardOnlyRequestInd;
            }

            /**
             * 设置awardOnlyRequestInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setAwardOnlyRequestInd(Boolean value) {
                this.awardOnlyRequestInd = value;
            }

            /**
             * 获取simpleRequestInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isSimpleRequestInd() {
                return simpleRequestInd;
            }

            /**
             * 设置simpleRequestInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setSimpleRequestInd(Boolean value) {
                this.simpleRequestInd = value;
            }

        }

    }

}
