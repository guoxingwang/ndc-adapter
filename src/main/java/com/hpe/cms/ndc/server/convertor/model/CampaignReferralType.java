//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * UTM Search Campaign Referral definition.
 * 
 * <p>CampaignReferralType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="CampaignReferralType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}TrxProcessObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UTM"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ReferrerURL" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" minOccurs="0"/&gt;
 *                   &lt;element name="Other_UTMs" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}CampaignUTM_ParameterType"&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="Campaign" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
 *                 &lt;attribute name="Source" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
 *                 &lt;attribute name="Medium" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
 *                 &lt;attribute name="Content" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
 *                 &lt;attribute name="Term" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Localization" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="CountryCode" type="{http://www.iata.org/IATA/EDIST}CountrySimpleType" /&gt;
 *                 &lt;attribute name="CurrencyCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" /&gt;
 *                 &lt;attribute name="LanguageCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CampaignReferralType", propOrder = {
    "utm",
    "localization"
})
public class CampaignReferralType
    extends TrxProcessObjectBaseType
{

    @XmlElement(name = "UTM", required = true)
    protected CampaignReferralType.UTM utm;
    @XmlElement(name = "Localization")
    protected CampaignReferralType.Localization localization;

    /**
     * 获取utm属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CampaignReferralType.UTM }
     *     
     */
    public CampaignReferralType.UTM getUTM() {
        return utm;
    }

    /**
     * 设置utm属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CampaignReferralType.UTM }
     *     
     */
    public void setUTM(CampaignReferralType.UTM value) {
        this.utm = value;
    }

    /**
     * 获取localization属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CampaignReferralType.Localization }
     *     
     */
    public CampaignReferralType.Localization getLocalization() {
        return localization;
    }

    /**
     * 设置localization属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CampaignReferralType.Localization }
     *     
     */
    public void setLocalization(CampaignReferralType.Localization value) {
        this.localization = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="CountryCode" type="{http://www.iata.org/IATA/EDIST}CountrySimpleType" /&gt;
     *       &lt;attribute name="CurrencyCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" /&gt;
     *       &lt;attribute name="LanguageCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Localization {

        @XmlAttribute(name = "CountryCode")
        protected String countryCode;
        @XmlAttribute(name = "CurrencyCode")
        protected String currencyCode;
        @XmlAttribute(name = "LanguageCode")
        protected String languageCode;

        /**
         * 获取countryCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountryCode() {
            return countryCode;
        }

        /**
         * 设置countryCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountryCode(String value) {
            this.countryCode = value;
        }

        /**
         * 获取currencyCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrencyCode() {
            return currencyCode;
        }

        /**
         * 设置currencyCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrencyCode(String value) {
            this.currencyCode = value;
        }

        /**
         * 获取languageCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguageCode() {
            return languageCode;
        }

        /**
         * 设置languageCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguageCode(String value) {
            this.languageCode = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ReferrerURL" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" minOccurs="0"/&gt;
     *         &lt;element name="Other_UTMs" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CampaignUTM_ParameterType"&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="Campaign" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
     *       &lt;attribute name="Source" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
     *       &lt;attribute name="Medium" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
     *       &lt;attribute name="Content" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
     *       &lt;attribute name="Term" type="{http://www.iata.org/IATA/EDIST}DescriptionSimpleType" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "referrerURL",
        "otherUTMs"
    })
    public static class UTM {

        @XmlElement(name = "ReferrerURL")
        protected String referrerURL;
        @XmlElement(name = "Other_UTMs")
        protected CampaignReferralType.UTM.OtherUTMs otherUTMs;
        @XmlAttribute(name = "Campaign")
        protected String campaign;
        @XmlAttribute(name = "Source")
        protected String source;
        @XmlAttribute(name = "Medium")
        protected String medium;
        @XmlAttribute(name = "Content")
        protected String content;
        @XmlAttribute(name = "Term")
        protected String term;

        /**
         * 获取referrerURL属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferrerURL() {
            return referrerURL;
        }

        /**
         * 设置referrerURL属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferrerURL(String value) {
            this.referrerURL = value;
        }

        /**
         * 获取otherUTMs属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CampaignReferralType.UTM.OtherUTMs }
         *     
         */
        public CampaignReferralType.UTM.OtherUTMs getOtherUTMs() {
            return otherUTMs;
        }

        /**
         * 设置otherUTMs属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CampaignReferralType.UTM.OtherUTMs }
         *     
         */
        public void setOtherUTMs(CampaignReferralType.UTM.OtherUTMs value) {
            this.otherUTMs = value;
        }

        /**
         * 获取campaign属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCampaign() {
            return campaign;
        }

        /**
         * 设置campaign属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCampaign(String value) {
            this.campaign = value;
        }

        /**
         * 获取source属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSource() {
            return source;
        }

        /**
         * 设置source属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSource(String value) {
            this.source = value;
        }

        /**
         * 获取medium属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMedium() {
            return medium;
        }

        /**
         * 设置medium属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMedium(String value) {
            this.medium = value;
        }

        /**
         * 获取content属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContent() {
            return content;
        }

        /**
         * 设置content属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContent(String value) {
            this.content = value;
        }

        /**
         * 获取term属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTerm() {
            return term;
        }

        /**
         * 设置term属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTerm(String value) {
            this.term = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CampaignUTM_ParameterType"&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class OtherUTMs
            extends CampaignUTMParameterType
        {


        }

    }

}
