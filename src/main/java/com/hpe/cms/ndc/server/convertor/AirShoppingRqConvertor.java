package com.hpe.cms.ndc.server.convertor;

import lombok.extern.slf4j.Slf4j;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:07 2019/7/27
 * @ Description：
 * @ Modified By：
 */
@Slf4j
public class AirShoppingRqConvertor extends AbstractConvertor {

    public AirShoppingRqConvertor(String sourceVersion, String targetVersion) {
        super(sourceVersion, targetVersion);
    }

    public com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrq.IATAAirShoppingRQ converte_v15_2_v18_2(Object source) {
        source = (com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ) source;
        log.info("source object is {}", source);
        //todo 具体转换规则
        return new com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrq.IATAAirShoppingRQ();
    }

    public com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrq.IATAAirShoppingRQ converte_v16_2_v18_2(Object source) {
        source = (com.hpe.cms.ndc.server.v16_2.model.AirShoppingRQ) source;
        log.info("source object is {}", source);
        //todo 具体转换规则
        return new com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrq.IATAAirShoppingRQ();
    }
}
