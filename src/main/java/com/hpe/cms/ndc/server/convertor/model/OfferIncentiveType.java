//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * OFFER INCENTIVE information definition.
 * 
 * <p>OfferIncentiveType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="OfferIncentiveType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Incentive" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="OfferCodeID" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
 *                   &lt;element name="ExpirationDate"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="OfferSubCode" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CodesetValueSimpleType"&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="AvailableUnits" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
 *                   &lt;element name="DiscountLevel" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DiscountAmount"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DiscountPercent"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AugmentationPoint" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferIncentiveType", propOrder = {
    "incentive"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.OfferItemMetadataType.Incentives.class
})
public class OfferIncentiveType {

    @XmlElement(name = "Incentive", required = true)
    protected List<OfferIncentiveType.Incentive> incentive;

    /**
     * Gets the value of the incentive property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the incentive property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIncentive().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OfferIncentiveType.Incentive }
     * 
     * 
     */
    public List<OfferIncentiveType.Incentive> getIncentive() {
        if (incentive == null) {
            incentive = new ArrayList<OfferIncentiveType.Incentive>();
        }
        return this.incentive;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="OfferCodeID" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"/&gt;
     *         &lt;element name="ExpirationDate"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="OfferSubCode" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CodesetValueSimpleType"&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="AvailableUnits" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
     *         &lt;element name="DiscountLevel" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DiscountAmount"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DiscountPercent"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AugmentationPoint" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offerCodeID",
        "expirationDate",
        "offerSubCode",
        "availableUnits",
        "discountLevel",
        "augmentationPoint"
    })
    public static class Incentive
        extends KeyWithMetaObjectBaseType
    {

        @XmlElement(name = "OfferCodeID", required = true)
        protected String offerCodeID;
        @XmlElement(name = "ExpirationDate", required = true)
        protected OfferIncentiveType.Incentive.ExpirationDate expirationDate;
        @XmlElement(name = "OfferSubCode")
        protected OfferIncentiveType.Incentive.OfferSubCode offerSubCode;
        @XmlElement(name = "AvailableUnits")
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger availableUnits;
        @XmlElement(name = "DiscountLevel")
        protected OfferIncentiveType.Incentive.DiscountLevel discountLevel;
        @XmlElement(name = "AugmentationPoint")
        protected AugPointInfoType augmentationPoint;

        /**
         * 获取offerCodeID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOfferCodeID() {
            return offerCodeID;
        }

        /**
         * 设置offerCodeID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOfferCodeID(String value) {
            this.offerCodeID = value;
        }

        /**
         * 获取expirationDate属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OfferIncentiveType.Incentive.ExpirationDate }
         *     
         */
        public OfferIncentiveType.Incentive.ExpirationDate getExpirationDate() {
            return expirationDate;
        }

        /**
         * 设置expirationDate属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OfferIncentiveType.Incentive.ExpirationDate }
         *     
         */
        public void setExpirationDate(OfferIncentiveType.Incentive.ExpirationDate value) {
            this.expirationDate = value;
        }

        /**
         * 获取offerSubCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OfferIncentiveType.Incentive.OfferSubCode }
         *     
         */
        public OfferIncentiveType.Incentive.OfferSubCode getOfferSubCode() {
            return offerSubCode;
        }

        /**
         * 设置offerSubCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OfferIncentiveType.Incentive.OfferSubCode }
         *     
         */
        public void setOfferSubCode(OfferIncentiveType.Incentive.OfferSubCode value) {
            this.offerSubCode = value;
        }

        /**
         * 获取availableUnits属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getAvailableUnits() {
            return availableUnits;
        }

        /**
         * 设置availableUnits属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setAvailableUnits(BigInteger value) {
            this.availableUnits = value;
        }

        /**
         * 获取discountLevel属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OfferIncentiveType.Incentive.DiscountLevel }
         *     
         */
        public OfferIncentiveType.Incentive.DiscountLevel getDiscountLevel() {
            return discountLevel;
        }

        /**
         * 设置discountLevel属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OfferIncentiveType.Incentive.DiscountLevel }
         *     
         */
        public void setDiscountLevel(OfferIncentiveType.Incentive.DiscountLevel value) {
            this.discountLevel = value;
        }

        /**
         * 获取augmentationPoint属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AugPointInfoType }
         *     
         */
        public AugPointInfoType getAugmentationPoint() {
            return augmentationPoint;
        }

        /**
         * 设置augmentationPoint属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AugPointInfoType }
         *     
         */
        public void setAugmentationPoint(AugPointInfoType value) {
            this.augmentationPoint = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DiscountAmount"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DiscountPercent"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "discountAmount",
            "discountPercent"
        })
        public static class DiscountLevel {

            @XmlElement(name = "DiscountAmount", required = true)
            protected CurrencyAmountOptType discountAmount;
            @XmlElement(name = "DiscountPercent")
            protected int discountPercent;

            /**
             * 获取discountAmount属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public CurrencyAmountOptType getDiscountAmount() {
                return discountAmount;
            }

            /**
             * 设置discountAmount属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public void setDiscountAmount(CurrencyAmountOptType value) {
                this.discountAmount = value;
            }

            /**
             * 获取discountPercent属性的值。
             * 
             */
            public int getDiscountPercent() {
                return discountPercent;
            }

            /**
             * 设置discountPercent属性的值。
             * 
             */
            public void setDiscountPercent(int value) {
                this.discountPercent = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class ExpirationDate
            extends CoreDateGrpType
        {


        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CodesetValueSimpleType"&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class OfferSubCode {

            @XmlValue
            protected String value;

            /**
             * Examples: A, ABC, 1
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }

    }

}
