//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>MetaSrchLinkAppListType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="MetaSrchLinkAppListType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AirlineSite"/&gt;
 *     &lt;enumeration value="IntraService"/&gt;
 *     &lt;enumeration value="MetaSearchSite"/&gt;
 *     &lt;enumeration value="SellerAgencySite"/&gt;
 *     &lt;enumeration value="Other"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MetaSrchLinkAppListType")
@XmlEnum
public enum MetaSrchLinkAppListType {

    @XmlEnumValue("AirlineSite")
    AIRLINE_SITE("AirlineSite"),
    @XmlEnumValue("IntraService")
    INTRA_SERVICE("IntraService"),
    @XmlEnumValue("MetaSearchSite")
    META_SEARCH_SITE("MetaSearchSite"),
    @XmlEnumValue("SellerAgencySite")
    SELLER_AGENCY_SITE("SellerAgencySite"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    MetaSrchLinkAppListType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MetaSrchLinkAppListType fromValue(String v) {
        for (MetaSrchLinkAppListType c: MetaSrchLinkAppListType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
