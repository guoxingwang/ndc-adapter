package com.hpe.cms.ndc.server.util;

import com.hpe.cms.ndc.server.security.Security;
import org.springframework.ws.soap.SoapHeaderElement;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 17:33 2019/7/27
 * @ Description：
 * @ Modified By：
 */
public class SecurityUtil {

    public static Security unmarshallSecurityFromSoapHeader(SoapHeaderElement header) {
        Security security = null;
        try {

            JAXBContext context = JAXBContext.newInstance(Security.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            security = (Security) unmarshaller.unmarshal(header.getSource());

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return security;
    }
}
