//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * A data type definition for Commonly Used Date Representation Formats: Short Date, Date and Time, Timestamp
 * 
 * <p>CoreDateGrpType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="CoreDateGrpType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="DateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="ShortDate" type="{http://www.w3.org/2001/XMLSchema}date" /&gt;
 *       &lt;attribute name="Timestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="Time" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoreDateGrpType")
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.FulfillmentPartnerType.Fulfillments.Fulfillment.OfferValidDates.Start.class,
    com.hpe.cms.ndc.server.convertor.model.FulfillmentPartnerType.Fulfillments.Fulfillment.OfferValidDates.End.class,
    com.hpe.cms.ndc.server.convertor.model.InvGuaranteeType.InvGuaranteeTimeLimit.class,
    com.hpe.cms.ndc.server.convertor.model.TimeLimits.PaymentTimeLimit.class,
    com.hpe.cms.ndc.server.convertor.model.ServiceFulfillmentType.OfferValidDates.Start.class,
    com.hpe.cms.ndc.server.convertor.model.ServiceFulfillmentType.OfferValidDates.End.class,
    com.hpe.cms.ndc.server.convertor.model.ServiceQualifierPriceType.Fulfillment.OfferValidDates.Start.class,
    com.hpe.cms.ndc.server.convertor.model.ServiceQualifierPriceType.Fulfillment.OfferValidDates.End.class,
    com.hpe.cms.ndc.server.convertor.model.OfferTimeLimitSetType.OfferExpiration.class,
    com.hpe.cms.ndc.server.convertor.model.OfferIncentiveType.Incentive.ExpirationDate.class,
    com.hpe.cms.ndc.server.convertor.model.OfferValidPeriodType.Earliest.class,
    com.hpe.cms.ndc.server.convertor.model.OfferValidPeriodType.Latest.class,
    com.hpe.cms.ndc.server.convertor.model.OrderCoreType.TimeLimits.PaymentTimeLimit.class,
    com.hpe.cms.ndc.server.convertor.model.CouponSoldAirlineType.DepartureDateTime.class,
    com.hpe.cms.ndc.server.convertor.model.CouponSoldAirlineType.ArrivalDateTime.class,
    com.hpe.cms.ndc.server.convertor.model.CouponFlightSegmentType.DepartureDateTime.class,
    InvGuaranteeTimeLimits.class
})
public class CoreDateGrpType {

    @XmlAttribute(name = "DateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTime;
    @XmlAttribute(name = "ShortDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar shortDate;
    @XmlAttribute(name = "Timestamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timestamp;
    @XmlAttribute(name = "Time")
    protected String time;

    /**
     * 获取dateTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTime() {
        return dateTime;
    }

    /**
     * 设置dateTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTime(XMLGregorianCalendar value) {
        this.dateTime = value;
    }

    /**
     * 获取shortDate属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getShortDate() {
        return shortDate;
    }

    /**
     * 设置shortDate属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setShortDate(XMLGregorianCalendar value) {
        this.shortDate = value;
    }

    /**
     * 获取timestamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimestamp() {
        return timestamp;
    }

    /**
     * 设置timestamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimestamp(XMLGregorianCalendar value) {
        this.timestamp = value;
    }

    /**
     * 获取time属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTime() {
        return time;
    }

    /**
     * 设置time属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTime(String value) {
        this.time = value;
    }

}
