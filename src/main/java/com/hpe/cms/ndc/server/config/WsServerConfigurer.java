package com.hpe.cms.ndc.server.config;

import com.hpe.cms.ndc.server.constance.WsServerConstance;
import lombok.extern.slf4j.Slf4j;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;
import org.springframework.ws.soap.security.wss4j2.callback.SimplePasswordValidationCallbackHandler;
import org.springframework.ws.soap.security.wss4j2.callback.SpringSecurityPasswordValidationCallbackHandler;
import org.springframework.ws.transport.http.MessageDispatcherServlet;

import java.util.List;
import java.util.Properties;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:36 2019/7/5
 * @ Description：
 * @ Modified By：
 */
@EnableWs
@Configuration
@Slf4j
public class WsServerConfigurer extends WsConfigurerAdapter {

    public static final String SCHEMA_RQ = "RQ";
    public static final String SCHEMA_RS = "RS";

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);//true 地址会进行转换，不然都是本地地址
        //这里可以设置 请求的工厂类，实现有两个：SaajSoapMessageFactory 和 AxiomSoapMessageFactory
        //默认是 SaajSoapMessageFactory
        //servlet.setMessageFactoryBeanName(messageFactoryBeanName);
        return new ServletRegistrationBean(servlet, WsServerConstance.WSDL_PATH + "*");
    }

    /**
     * SpringSecurityPasswordValidationCallbackHandler，方便与SpringSecurity集成，如果工程使用SpringSecurity，建议使用此handler
     * 使用此handler，<soapenv:Header>中的password需要使用SpringSecurity的加密方式，同时需要增加加密方式前缀，如：{MD5}5ebe2294ecd0e0f08eab7690d2a6ee69
     *
     * @return
     */
    @Bean
    public SpringSecurityPasswordValidationCallbackHandler springSecurityPasswordValidationCallbackHandler() {
        SpringSecurityPasswordValidationCallbackHandler callbackHandler = new SpringSecurityPasswordValidationCallbackHandler();
        callbackHandler.setUserDetailsService(userDetailsService);
        try {
            callbackHandler.afterPropertiesSet();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return callbackHandler;
    }

    /**
     * SimplePasswordValidationCallbackHandler，简单用户+密码方式，使用简单
     *
     * @return
     */
    @Bean
    @Deprecated
    public SimplePasswordValidationCallbackHandler simplePasswordValidationCallbackHandler() {
        SimplePasswordValidationCallbackHandler callbackHandler = new SimplePasswordValidationCallbackHandler();
        Properties users = new Properties();
        users.setProperty("admin", "secret");
        callbackHandler.setUsers(users);
        try {
            callbackHandler.afterPropertiesSet();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return callbackHandler;
    }

    /**
     * 设置服务端安全验证Interceptor
     *
     * @return
     */
    public Wss4jSecurityInterceptor serverWss4jSecurityInterceptor() {
        Wss4jSecurityInterceptor wss4jSecurityInterceptor = new Wss4jSecurityInterceptor();

        /*客户端设置SecurementActions,服务端设置ValidationActions*/ //WSHandlerConstants.USERNAME_TOKEN + "" + WSHandlerConstants.TIMESTAMP
        wss4jSecurityInterceptor.setSecurementActions(WSHandlerConstants.USERNAME_TOKEN);
        //wss4jSecurityInterceptor.setValidationActions(WSHandlerConstants.TIMESTAMP + " " + WSHandlerConstants.USERNAME_TOKEN);
        /* header 如下
           <soapenv:Header>
              <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soapenv:mustUnderstand="1">
                 <wsse:UsernameToken wsu:Id="UsernameToken-EC9F7473E024359C6A14589178984712">
                    <wsse:Username>admin</wsse:Username>
                    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">secret</wsse:Password>
                    <wsu:Created>2019-07-14T06:57:18.471Z</wsu:Created>
                 </wsse:UsernameToken>
                 <wsu:Timestamp wsu:Id="TS-EC9F7473E024359C6A14589178984531">
                    <wsu:Created>2019-07-14T06:57:18.471Z</wsu:Created>
                    <wsu:Expires>2019-07-14T07:01:18.471Z</wsu:Expires>
                 </wsu:Timestamp>
              </wsse:Security>
           </soapenv:Header>
        **/

        /* Default Password encoding is digest, that is not supported by EP hence need to set need to set following password type. */
        wss4jSecurityInterceptor.setSecurementPasswordType(WSConstants.PW_TEXT);
        wss4jSecurityInterceptor.setRemoveSecurityHeader(false);
        //wss4jSecurityInterceptor.setValidationCallbackHandler(simplePasswordValidationCallbackHandler());  //后续可以集成spring-security的handler
        wss4jSecurityInterceptor.setValidationCallbackHandler(springSecurityPasswordValidationCallbackHandler());
        try {
            wss4jSecurityInterceptor.afterPropertiesSet();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wss4jSecurityInterceptor;
    }

    @Override
    public void addInterceptors(List interceptors) {
        interceptors.add(serverWss4jSecurityInterceptor());
    }
}
