//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * COUPON definition.
 * 
 * <p>CouponInfoType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="CouponInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CouponItinSequenceNbr" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *               &lt;minInclusive value="1"/&gt;
 *               &lt;maxInclusive value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CouponNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="CouponReference" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="8"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FareBasisCode" minOccurs="0"/&gt;
 *         &lt;element name="CouponMedia" type="{http://www.iata.org/IATA/EDIST}CouponMediaSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="CouponValid" type="{http://www.iata.org/IATA/EDIST}CouponEffectiveType" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://www.iata.org/IATA/EDIST}CodesetType"/&gt;
 *         &lt;element name="InvoluntaryIndicatorCode" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *         &lt;element name="SettlementAuthCode" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="AssocFareBasis" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="PromoCode" type="{http://www.iata.org/IATA/EDIST}PromotionType" minOccurs="0"/&gt;
 *         &lt;element name="DateOfService" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="ServiceQuantity" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer"&gt;
 *               &lt;minInclusive value="1"/&gt;
 *               &lt;maxInclusive value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ServiceReferences" minOccurs="0"/&gt;
 *         &lt;element name="Value" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
 *         &lt;element name="SoldAirlineInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CouponSoldAirlineType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrentAirlineInfo"/&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}CheckedInAirlineInfo"/&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}FlownAirlineInfo"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}InConnectionWithInfo" minOccurs="0"/&gt;
 *         &lt;element name="PresentInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="To"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;pattern value="[0-9a-zA-Z]{1,45}"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *                 &lt;attribute name="At"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;pattern value="[0-9a-zA-Z]{1,43}"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ReasonForIssuance" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}RFIC" minOccurs="0"/&gt;
 *                   &lt;element name="Code" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;pattern value="[0-9a-zA-Z]{1,3}"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Description" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FiledFeeInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="BSR_Rate"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ProductCharacteristic" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Type" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;pattern value="[0-9a-zA-Z]{1,35}"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Description" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;pattern value="[0-9a-zA-Z]{1,70}"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AdditionalServicesInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Group" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;pattern value="[0-9a-zA-Z]{1,3}"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="SubGroup" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;pattern value="[0-9a-zA-Z]{1,3}"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="RFISC_DefinedType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                   &lt;element name="ServiceType" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;pattern value="[0-9a-zA-Z]{1}"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ExcessBaggage" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
 *                   &lt;element name="UnitOfMeasureCode" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                   &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *                   &lt;element name="UnitOfMeasure" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;minLength value="1"/&gt;
 *                         &lt;maxLength value="16"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ValidatingAirline" type="{http://www.iata.org/IATA/EDIST}AirlineDesigSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to71" minOccurs="0"/&gt;
 *         &lt;element name="AddlBaggageInfo" type="{http://www.iata.org/IATA/EDIST}AddlBaggageInfoType" minOccurs="0"/&gt;
 *         &lt;element name="ResAirlineDesig" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AlphaLength3"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ResDateOfFlight" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="ResDesigAirportCityCode" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirportCitySimpleType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ResFlightDepartureTime" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;TimeSimpleType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ResFlightNumber" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;FlightNumberType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ResOriginAirportCityCode" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirportCitySimpleType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ResBookDesign" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="ResStatusCode" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ConsumedAtIssuanceInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CouponInfoType", propOrder = {
    "couponItinSequenceNbr",
    "couponNumber",
    "couponReference",
    "fareBasisCode",
    "couponMedia",
    "couponValid",
    "status",
    "involuntaryIndicatorCode",
    "settlementAuthCode",
    "assocFareBasis",
    "promoCode",
    "dateOfService",
    "serviceQuantity",
    "serviceReferences",
    "value",
    "soldAirlineInfo",
    "currentAirlineInfo",
    "checkedInAirlineInfo",
    "flownAirlineInfo",
    "inConnectionWithInfo",
    "presentInfo",
    "reasonForIssuance",
    "filedFeeInfo",
    "productCharacteristic",
    "additionalServicesInfo",
    "excessBaggage",
    "validatingAirline",
    "remark",
    "addlBaggageInfo",
    "resAirlineDesig",
    "resDateOfFlight",
    "resDesigAirportCityCode",
    "resFlightDepartureTime",
    "resFlightNumber",
    "resOriginAirportCityCode",
    "resBookDesign",
    "resStatusCode"
})
public class CouponInfoType {

    @XmlElement(name = "CouponItinSequenceNbr")
    protected Integer couponItinSequenceNbr;
    @XmlElement(name = "CouponNumber", required = true)
    protected BigInteger couponNumber;
    @XmlElement(name = "CouponReference")
    protected String couponReference;
    @XmlElement(name = "FareBasisCode")
    protected FareBasisCodeType fareBasisCode;
    @XmlElement(name = "CouponMedia")
    protected String couponMedia;
    @XmlElement(name = "CouponValid")
    protected CouponEffectiveType couponValid;
    @XmlElement(name = "Status", required = true)
    protected CodesetType status;
    @XmlElement(name = "InvoluntaryIndicatorCode")
    protected CodesetType involuntaryIndicatorCode;
    @XmlElement(name = "SettlementAuthCode")
    protected String settlementAuthCode;
    @XmlElement(name = "AssocFareBasis")
    protected String assocFareBasis;
    @XmlElement(name = "PromoCode")
    protected PromotionType promoCode;
    @XmlElement(name = "DateOfService")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfService;
    @XmlElement(name = "ServiceQuantity")
    protected Integer serviceQuantity;
    @XmlList
    @XmlElement(name = "ServiceReferences")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> serviceReferences;
    @XmlElement(name = "Value")
    protected CurrencyAmountOptType value;
    @XmlElement(name = "SoldAirlineInfo")
    protected CouponInfoType.SoldAirlineInfo soldAirlineInfo;
    @XmlElement(name = "CurrentAirlineInfo")
    protected CouponSoldAirlineType currentAirlineInfo;
    @XmlElement(name = "CheckedInAirlineInfo")
    protected CouponSoldAirlineType checkedInAirlineInfo;
    @XmlElement(name = "FlownAirlineInfo")
    protected CouponSoldAirlineType flownAirlineInfo;
    @XmlElement(name = "InConnectionWithInfo")
    protected InConnectionWithType inConnectionWithInfo;
    @XmlElement(name = "PresentInfo")
    protected CouponInfoType.PresentInfo presentInfo;
    @XmlElement(name = "ReasonForIssuance")
    protected CouponInfoType.ReasonForIssuance reasonForIssuance;
    @XmlElement(name = "FiledFeeInfo")
    protected CouponInfoType.FiledFeeInfo filedFeeInfo;
    @XmlElement(name = "ProductCharacteristic")
    protected CouponInfoType.ProductCharacteristic productCharacteristic;
    @XmlElement(name = "AdditionalServicesInfo")
    protected CouponInfoType.AdditionalServicesInfo additionalServicesInfo;
    @XmlElement(name = "ExcessBaggage")
    protected CouponInfoType.ExcessBaggage excessBaggage;
    @XmlElement(name = "ValidatingAirline")
    protected String validatingAirline;
    @XmlElement(name = "Remark")
    protected String remark;
    @XmlElement(name = "AddlBaggageInfo")
    protected AddlBaggageInfoType addlBaggageInfo;
    @XmlElement(name = "ResAirlineDesig")
    protected CouponInfoType.ResAirlineDesig resAirlineDesig;
    @XmlElement(name = "ResDateOfFlight")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar resDateOfFlight;
    @XmlElement(name = "ResDesigAirportCityCode")
    protected CouponInfoType.ResDesigAirportCityCode resDesigAirportCityCode;
    @XmlElement(name = "ResFlightDepartureTime")
    protected CouponInfoType.ResFlightDepartureTime resFlightDepartureTime;
    @XmlElement(name = "ResFlightNumber")
    protected CouponInfoType.ResFlightNumber resFlightNumber;
    @XmlElement(name = "ResOriginAirportCityCode")
    protected CouponInfoType.ResOriginAirportCityCode resOriginAirportCityCode;
    @XmlElement(name = "ResBookDesign")
    protected String resBookDesign;
    @XmlElement(name = "ResStatusCode")
    protected CodesetType resStatusCode;
    @XmlAttribute(name = "ConsumedAtIssuanceInd")
    protected Boolean consumedAtIssuanceInd;

    /**
     * 获取couponItinSequenceNbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCouponItinSequenceNbr() {
        return couponItinSequenceNbr;
    }

    /**
     * 设置couponItinSequenceNbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCouponItinSequenceNbr(Integer value) {
        this.couponItinSequenceNbr = value;
    }

    /**
     * 获取couponNumber属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCouponNumber() {
        return couponNumber;
    }

    /**
     * 设置couponNumber属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCouponNumber(BigInteger value) {
        this.couponNumber = value;
    }

    /**
     * 获取couponReference属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponReference() {
        return couponReference;
    }

    /**
     * 设置couponReference属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponReference(String value) {
        this.couponReference = value;
    }

    /**
     * 获取fareBasisCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FareBasisCodeType }
     *     
     */
    public FareBasisCodeType getFareBasisCode() {
        return fareBasisCode;
    }

    /**
     * 设置fareBasisCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FareBasisCodeType }
     *     
     */
    public void setFareBasisCode(FareBasisCodeType value) {
        this.fareBasisCode = value;
    }

    /**
     * 获取couponMedia属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponMedia() {
        return couponMedia;
    }

    /**
     * 设置couponMedia属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponMedia(String value) {
        this.couponMedia = value;
    }

    /**
     * 获取couponValid属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponEffectiveType }
     *     
     */
    public CouponEffectiveType getCouponValid() {
        return couponValid;
    }

    /**
     * 设置couponValid属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponEffectiveType }
     *     
     */
    public void setCouponValid(CouponEffectiveType value) {
        this.couponValid = value;
    }

    /**
     * 获取status属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getStatus() {
        return status;
    }

    /**
     * 设置status属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setStatus(CodesetType value) {
        this.status = value;
    }

    /**
     * 获取involuntaryIndicatorCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getInvoluntaryIndicatorCode() {
        return involuntaryIndicatorCode;
    }

    /**
     * 设置involuntaryIndicatorCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setInvoluntaryIndicatorCode(CodesetType value) {
        this.involuntaryIndicatorCode = value;
    }

    /**
     * 获取settlementAuthCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSettlementAuthCode() {
        return settlementAuthCode;
    }

    /**
     * 设置settlementAuthCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSettlementAuthCode(String value) {
        this.settlementAuthCode = value;
    }

    /**
     * 获取assocFareBasis属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssocFareBasis() {
        return assocFareBasis;
    }

    /**
     * 设置assocFareBasis属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssocFareBasis(String value) {
        this.assocFareBasis = value;
    }

    /**
     * 获取promoCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PromotionType }
     *     
     */
    public PromotionType getPromoCode() {
        return promoCode;
    }

    /**
     * 设置promoCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PromotionType }
     *     
     */
    public void setPromoCode(PromotionType value) {
        this.promoCode = value;
    }

    /**
     * 获取dateOfService属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfService() {
        return dateOfService;
    }

    /**
     * 设置dateOfService属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfService(XMLGregorianCalendar value) {
        this.dateOfService = value;
    }

    /**
     * 获取serviceQuantity属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getServiceQuantity() {
        return serviceQuantity;
    }

    /**
     * 设置serviceQuantity属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setServiceQuantity(Integer value) {
        this.serviceQuantity = value;
    }

    /**
     * Association to services paid by the EMD             Association to multiple Optional Service instance(s). Example: srvc1 srvc2  Gets the value of the serviceReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getServiceReferences() {
        if (serviceReferences == null) {
            serviceReferences = new ArrayList<Object>();
        }
        return this.serviceReferences;
    }

    /**
     * 获取value属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmountOptType }
     *     
     */
    public CurrencyAmountOptType getValue() {
        return value;
    }

    /**
     * 设置value属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmountOptType }
     *     
     */
    public void setValue(CurrencyAmountOptType value) {
        this.value = value;
    }

    /**
     * 获取soldAirlineInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.SoldAirlineInfo }
     *     
     */
    public CouponInfoType.SoldAirlineInfo getSoldAirlineInfo() {
        return soldAirlineInfo;
    }

    /**
     * 设置soldAirlineInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.SoldAirlineInfo }
     *     
     */
    public void setSoldAirlineInfo(CouponInfoType.SoldAirlineInfo value) {
        this.soldAirlineInfo = value;
    }

    /**
     * 获取currentAirlineInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponSoldAirlineType }
     *     
     */
    public CouponSoldAirlineType getCurrentAirlineInfo() {
        return currentAirlineInfo;
    }

    /**
     * 设置currentAirlineInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponSoldAirlineType }
     *     
     */
    public void setCurrentAirlineInfo(CouponSoldAirlineType value) {
        this.currentAirlineInfo = value;
    }

    /**
     * 获取checkedInAirlineInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponSoldAirlineType }
     *     
     */
    public CouponSoldAirlineType getCheckedInAirlineInfo() {
        return checkedInAirlineInfo;
    }

    /**
     * 设置checkedInAirlineInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponSoldAirlineType }
     *     
     */
    public void setCheckedInAirlineInfo(CouponSoldAirlineType value) {
        this.checkedInAirlineInfo = value;
    }

    /**
     * 获取flownAirlineInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponSoldAirlineType }
     *     
     */
    public CouponSoldAirlineType getFlownAirlineInfo() {
        return flownAirlineInfo;
    }

    /**
     * 设置flownAirlineInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponSoldAirlineType }
     *     
     */
    public void setFlownAirlineInfo(CouponSoldAirlineType value) {
        this.flownAirlineInfo = value;
    }

    /**
     * In connection with document number (A document number representing a transaction that is issued in relation to this transaction); and coupon number (the coupon number(s) associated with the 'in connection with' document number.)
     * 
     * @return
     *     possible object is
     *     {@link InConnectionWithType }
     *     
     */
    public InConnectionWithType getInConnectionWithInfo() {
        return inConnectionWithInfo;
    }

    /**
     * 设置inConnectionWithInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link InConnectionWithType }
     *     
     */
    public void setInConnectionWithInfo(InConnectionWithType value) {
        this.inConnectionWithInfo = value;
    }

    /**
     * 获取presentInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.PresentInfo }
     *     
     */
    public CouponInfoType.PresentInfo getPresentInfo() {
        return presentInfo;
    }

    /**
     * 设置presentInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.PresentInfo }
     *     
     */
    public void setPresentInfo(CouponInfoType.PresentInfo value) {
        this.presentInfo = value;
    }

    /**
     * 获取reasonForIssuance属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.ReasonForIssuance }
     *     
     */
    public CouponInfoType.ReasonForIssuance getReasonForIssuance() {
        return reasonForIssuance;
    }

    /**
     * 设置reasonForIssuance属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.ReasonForIssuance }
     *     
     */
    public void setReasonForIssuance(CouponInfoType.ReasonForIssuance value) {
        this.reasonForIssuance = value;
    }

    /**
     * 获取filedFeeInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.FiledFeeInfo }
     *     
     */
    public CouponInfoType.FiledFeeInfo getFiledFeeInfo() {
        return filedFeeInfo;
    }

    /**
     * 设置filedFeeInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.FiledFeeInfo }
     *     
     */
    public void setFiledFeeInfo(CouponInfoType.FiledFeeInfo value) {
        this.filedFeeInfo = value;
    }

    /**
     * 获取productCharacteristic属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.ProductCharacteristic }
     *     
     */
    public CouponInfoType.ProductCharacteristic getProductCharacteristic() {
        return productCharacteristic;
    }

    /**
     * 设置productCharacteristic属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.ProductCharacteristic }
     *     
     */
    public void setProductCharacteristic(CouponInfoType.ProductCharacteristic value) {
        this.productCharacteristic = value;
    }

    /**
     * 获取additionalServicesInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.AdditionalServicesInfo }
     *     
     */
    public CouponInfoType.AdditionalServicesInfo getAdditionalServicesInfo() {
        return additionalServicesInfo;
    }

    /**
     * 设置additionalServicesInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.AdditionalServicesInfo }
     *     
     */
    public void setAdditionalServicesInfo(CouponInfoType.AdditionalServicesInfo value) {
        this.additionalServicesInfo = value;
    }

    /**
     * 获取excessBaggage属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.ExcessBaggage }
     *     
     */
    public CouponInfoType.ExcessBaggage getExcessBaggage() {
        return excessBaggage;
    }

    /**
     * 设置excessBaggage属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.ExcessBaggage }
     *     
     */
    public void setExcessBaggage(CouponInfoType.ExcessBaggage value) {
        this.excessBaggage = value;
    }

    /**
     * 获取validatingAirline属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidatingAirline() {
        return validatingAirline;
    }

    /**
     * 设置validatingAirline属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidatingAirline(String value) {
        this.validatingAirline = value;
    }

    /**
     * 获取remark属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置remark属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * 获取addlBaggageInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AddlBaggageInfoType }
     *     
     */
    public AddlBaggageInfoType getAddlBaggageInfo() {
        return addlBaggageInfo;
    }

    /**
     * 设置addlBaggageInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AddlBaggageInfoType }
     *     
     */
    public void setAddlBaggageInfo(AddlBaggageInfoType value) {
        this.addlBaggageInfo = value;
    }

    /**
     * 获取resAirlineDesig属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.ResAirlineDesig }
     *     
     */
    public CouponInfoType.ResAirlineDesig getResAirlineDesig() {
        return resAirlineDesig;
    }

    /**
     * 设置resAirlineDesig属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.ResAirlineDesig }
     *     
     */
    public void setResAirlineDesig(CouponInfoType.ResAirlineDesig value) {
        this.resAirlineDesig = value;
    }

    /**
     * 获取resDateOfFlight属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getResDateOfFlight() {
        return resDateOfFlight;
    }

    /**
     * 设置resDateOfFlight属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setResDateOfFlight(XMLGregorianCalendar value) {
        this.resDateOfFlight = value;
    }

    /**
     * 获取resDesigAirportCityCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.ResDesigAirportCityCode }
     *     
     */
    public CouponInfoType.ResDesigAirportCityCode getResDesigAirportCityCode() {
        return resDesigAirportCityCode;
    }

    /**
     * 设置resDesigAirportCityCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.ResDesigAirportCityCode }
     *     
     */
    public void setResDesigAirportCityCode(CouponInfoType.ResDesigAirportCityCode value) {
        this.resDesigAirportCityCode = value;
    }

    /**
     * 获取resFlightDepartureTime属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.ResFlightDepartureTime }
     *     
     */
    public CouponInfoType.ResFlightDepartureTime getResFlightDepartureTime() {
        return resFlightDepartureTime;
    }

    /**
     * 设置resFlightDepartureTime属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.ResFlightDepartureTime }
     *     
     */
    public void setResFlightDepartureTime(CouponInfoType.ResFlightDepartureTime value) {
        this.resFlightDepartureTime = value;
    }

    /**
     * 获取resFlightNumber属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.ResFlightNumber }
     *     
     */
    public CouponInfoType.ResFlightNumber getResFlightNumber() {
        return resFlightNumber;
    }

    /**
     * 设置resFlightNumber属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.ResFlightNumber }
     *     
     */
    public void setResFlightNumber(CouponInfoType.ResFlightNumber value) {
        this.resFlightNumber = value;
    }

    /**
     * 获取resOriginAirportCityCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CouponInfoType.ResOriginAirportCityCode }
     *     
     */
    public CouponInfoType.ResOriginAirportCityCode getResOriginAirportCityCode() {
        return resOriginAirportCityCode;
    }

    /**
     * 设置resOriginAirportCityCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CouponInfoType.ResOriginAirportCityCode }
     *     
     */
    public void setResOriginAirportCityCode(CouponInfoType.ResOriginAirportCityCode value) {
        this.resOriginAirportCityCode = value;
    }

    /**
     * 获取resBookDesign属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResBookDesign() {
        return resBookDesign;
    }

    /**
     * 设置resBookDesign属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResBookDesign(String value) {
        this.resBookDesign = value;
    }

    /**
     * 获取resStatusCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getResStatusCode() {
        return resStatusCode;
    }

    /**
     * 设置resStatusCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setResStatusCode(CodesetType value) {
        this.resStatusCode = value;
    }

    /**
     * 获取consumedAtIssuanceInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConsumedAtIssuanceInd() {
        return consumedAtIssuanceInd;
    }

    /**
     * 设置consumedAtIssuanceInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConsumedAtIssuanceInd(Boolean value) {
        this.consumedAtIssuanceInd = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Group" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;pattern value="[0-9a-zA-Z]{1,3}"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="SubGroup" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;pattern value="[0-9a-zA-Z]{1,3}"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RFISC_DefinedType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *         &lt;element name="ServiceType" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;pattern value="[0-9a-zA-Z]{1}"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "group",
        "subGroup",
        "rfiscDefinedType",
        "serviceType"
    })
    public static class AdditionalServicesInfo {

        @XmlElement(name = "Group")
        protected String group;
        @XmlElement(name = "SubGroup")
        protected String subGroup;
        @XmlElement(name = "RFISC_DefinedType")
        protected CodesetType rfiscDefinedType;
        @XmlElement(name = "ServiceType")
        protected String serviceType;

        /**
         * 获取group属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroup() {
            return group;
        }

        /**
         * 设置group属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroup(String value) {
            this.group = value;
        }

        /**
         * 获取subGroup属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubGroup() {
            return subGroup;
        }

        /**
         * 设置subGroup属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubGroup(String value) {
            this.subGroup = value;
        }

        /**
         * 获取rfiscDefinedType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CodesetType }
         *     
         */
        public CodesetType getRFISCDefinedType() {
            return rfiscDefinedType;
        }

        /**
         * 设置rfiscDefinedType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CodesetType }
         *     
         */
        public void setRFISCDefinedType(CodesetType value) {
            this.rfiscDefinedType = value;
        }

        /**
         * 获取serviceType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceType() {
            return serviceType;
        }

        /**
         * 设置serviceType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceType(String value) {
            this.serviceType = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
     *         &lt;element name="UnitOfMeasureCode" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
     *         &lt;element name="UnitOfMeasure" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;minLength value="1"/&gt;
     *               &lt;maxLength value="16"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "amount",
        "unitOfMeasureCode",
        "quantity",
        "unitOfMeasure"
    })
    public static class ExcessBaggage {

        @XmlElement(name = "Amount")
        protected CurrencyAmountOptType amount;
        @XmlElement(name = "UnitOfMeasureCode")
        protected CodesetType unitOfMeasureCode;
        @XmlElement(name = "Quantity")
        protected BigDecimal quantity;
        @XmlElement(name = "UnitOfMeasure")
        protected String unitOfMeasure;

        /**
         * 获取amount属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CurrencyAmountOptType }
         *     
         */
        public CurrencyAmountOptType getAmount() {
            return amount;
        }

        /**
         * 设置amount属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CurrencyAmountOptType }
         *     
         */
        public void setAmount(CurrencyAmountOptType value) {
            this.amount = value;
        }

        /**
         * 获取unitOfMeasureCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CodesetType }
         *     
         */
        public CodesetType getUnitOfMeasureCode() {
            return unitOfMeasureCode;
        }

        /**
         * 设置unitOfMeasureCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CodesetType }
         *     
         */
        public void setUnitOfMeasureCode(CodesetType value) {
            this.unitOfMeasureCode = value;
        }

        /**
         * 获取quantity属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getQuantity() {
            return quantity;
        }

        /**
         * 设置quantity属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setQuantity(BigDecimal value) {
            this.quantity = value;
        }

        /**
         * 获取unitOfMeasure属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitOfMeasure() {
            return unitOfMeasure;
        }

        /**
         * 设置unitOfMeasure属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitOfMeasure(String value) {
            this.unitOfMeasure = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="BSR_Rate"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "amount"
    })
    public static class FiledFeeInfo {

        @XmlElement(name = "Amount")
        protected CurrencyAmountOptType amount;
        @XmlAttribute(name = "BSR_Rate")
        protected BigDecimal bsrRate;

        /**
         * 获取amount属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CurrencyAmountOptType }
         *     
         */
        public CurrencyAmountOptType getAmount() {
            return amount;
        }

        /**
         * 设置amount属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CurrencyAmountOptType }
         *     
         */
        public void setAmount(CurrencyAmountOptType value) {
            this.amount = value;
        }

        /**
         * 获取bsrRate属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getBSRRate() {
            return bsrRate;
        }

        /**
         * 设置bsrRate属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setBSRRate(BigDecimal value) {
            this.bsrRate = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="To"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;pattern value="[0-9a-zA-Z]{1,45}"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *       &lt;attribute name="At"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;pattern value="[0-9a-zA-Z]{1,43}"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class PresentInfo {

        @XmlAttribute(name = "To")
        protected String to;
        @XmlAttribute(name = "At")
        protected String at;

        /**
         * 获取to属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTo() {
            return to;
        }

        /**
         * 设置to属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTo(String value) {
            this.to = value;
        }

        /**
         * 获取at属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAt() {
            return at;
        }

        /**
         * 设置at属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAt(String value) {
            this.at = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Type" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;pattern value="[0-9a-zA-Z]{1,35}"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Description" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;pattern value="[0-9a-zA-Z]{1,70}"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "type",
        "description"
    })
    public static class ProductCharacteristic {

        @XmlElement(name = "Type")
        protected String type;
        @XmlElement(name = "Description")
        protected String description;

        /**
         * 获取type属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * 设置type属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * 获取description属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * 设置description属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}RFIC" minOccurs="0"/&gt;
     *         &lt;element name="Code" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;pattern value="[0-9a-zA-Z]{1,3}"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Description" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rfic",
        "code",
        "description"
    })
    public static class ReasonForIssuance {

        @XmlElement(name = "RFIC")
        protected RFICType rfic;
        @XmlElement(name = "Code")
        protected String code;
        @XmlElement(name = "Description")
        protected String description;

        /**
         * 获取rfic属性的值。
         * 
         * @return
         *     possible object is
         *     {@link RFICType }
         *     
         */
        public RFICType getRFIC() {
            return rfic;
        }

        /**
         * 设置rfic属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link RFICType }
         *     
         */
        public void setRFIC(RFICType value) {
            this.rfic = value;
        }

        /**
         * 获取code属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * 设置code属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * 获取description属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * 设置description属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AlphaLength3"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class ResAirlineDesig {

        @XmlValue
        protected String value;

        /**
         * Used for Alphabetic Strings, length exactly 3
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirportCitySimpleType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class ResDesigAirportCityCode {

        @XmlValue
        protected String value;

        /**
         * Examples: JFK, NYC
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;TimeSimpleType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class ResFlightDepartureTime {

        @XmlValue
        protected String value;

        /**
         * A data type for Time encoding constraint: SO 8601 Military/ 24 Hour (HH:MM)
         * Example: 14:00
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;FlightNumberType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class ResFlightNumber {

        @XmlValue
        protected String value;

        /**
         * Identifies a flight number (1 to 4 numbers) or OPEN or ARNK.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;AirportCitySimpleType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class ResOriginAirportCityCode {

        @XmlValue
        protected String value;

        /**
         * Examples: JFK, NYC
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CouponSoldAirlineType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SoldAirlineInfo
        extends CouponSoldAirlineType
    {


    }

}
