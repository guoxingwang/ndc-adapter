//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Payment Processing Rules Notice definition.
 * 
 * <p>PaymentRulesNoticeType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="PaymentRulesNoticeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}TrxProcessObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FormOfPayment" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element name="Query"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="ProceedOnFailureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Results"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Code" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}Associations"/&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attribute name="PaymentFailureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentRulesNoticeType", propOrder = {
    "formOfPayment"
})
public class PaymentRulesNoticeType
    extends TrxProcessObjectBaseType
{

    @XmlElement(name = "FormOfPayment")
    protected PaymentRulesNoticeType.FormOfPayment formOfPayment;

    /**
     * 获取formOfPayment属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PaymentRulesNoticeType.FormOfPayment }
     *     
     */
    public PaymentRulesNoticeType.FormOfPayment getFormOfPayment() {
        return formOfPayment;
    }

    /**
     * 设置formOfPayment属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentRulesNoticeType.FormOfPayment }
     *     
     */
    public void setFormOfPayment(PaymentRulesNoticeType.FormOfPayment value) {
        this.formOfPayment = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="Query"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="ProceedOnFailureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Results"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Code" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Associations"/&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attribute name="PaymentFailureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "query",
        "results"
    })
    public static class FormOfPayment {

        @XmlElement(name = "Query")
        protected PaymentRulesNoticeType.FormOfPayment.Query query;
        @XmlElement(name = "Results")
        protected PaymentRulesNoticeType.FormOfPayment.Results results;

        /**
         * 获取query属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PaymentRulesNoticeType.FormOfPayment.Query }
         *     
         */
        public PaymentRulesNoticeType.FormOfPayment.Query getQuery() {
            return query;
        }

        /**
         * 设置query属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentRulesNoticeType.FormOfPayment.Query }
         *     
         */
        public void setQuery(PaymentRulesNoticeType.FormOfPayment.Query value) {
            this.query = value;
        }

        /**
         * 获取results属性的值。
         * 
         * @return
         *     possible object is
         *     {@link PaymentRulesNoticeType.FormOfPayment.Results }
         *     
         */
        public PaymentRulesNoticeType.FormOfPayment.Results getResults() {
            return results;
        }

        /**
         * 设置results属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentRulesNoticeType.FormOfPayment.Results }
         *     
         */
        public void setResults(PaymentRulesNoticeType.FormOfPayment.Results value) {
            this.results = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="ProceedOnFailureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Query {

            @XmlAttribute(name = "ProceedOnFailureInd")
            protected Boolean proceedOnFailureInd;

            /**
             * 获取proceedOnFailureInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isProceedOnFailureInd() {
                return proceedOnFailureInd;
            }

            /**
             * 设置proceedOnFailureInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setProceedOnFailureInd(Boolean value) {
                this.proceedOnFailureInd = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Code" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
         *               &lt;/extension&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Associations"/&gt;
         *       &lt;/sequence&gt;
         *       &lt;attribute name="PaymentFailureInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "code",
            "associations"
        })
        public static class Results {

            @XmlElement(name = "Code")
            protected PaymentRulesNoticeType.FormOfPayment.Results.Code code;
            @XmlElement(name = "Associations", required = true)
            protected OrderItemAssociationType associations;
            @XmlAttribute(name = "PaymentFailureInd")
            protected Boolean paymentFailureInd;

            /**
             * 获取code属性的值。
             * 
             * @return
             *     possible object is
             *     {@link PaymentRulesNoticeType.FormOfPayment.Results.Code }
             *     
             */
            public PaymentRulesNoticeType.FormOfPayment.Results.Code getCode() {
                return code;
            }

            /**
             * 设置code属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link PaymentRulesNoticeType.FormOfPayment.Results.Code }
             *     
             */
            public void setCode(PaymentRulesNoticeType.FormOfPayment.Results.Code value) {
                this.code = value;
            }

            /**
             * 获取associations属性的值。
             * 
             * @return
             *     possible object is
             *     {@link OrderItemAssociationType }
             *     
             */
            public OrderItemAssociationType getAssociations() {
                return associations;
            }

            /**
             * 设置associations属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link OrderItemAssociationType }
             *     
             */
            public void setAssociations(OrderItemAssociationType value) {
                this.associations = value;
            }

            /**
             * 获取paymentFailureInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPaymentFailureInd() {
                return paymentFailureInd;
            }

            /**
             * 设置paymentFailureInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPaymentFailureInd(Boolean value) {
                this.paymentFailureInd = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
             *     &lt;/extension&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Code
                extends CodesetType
            {


            }

        }

    }

}
