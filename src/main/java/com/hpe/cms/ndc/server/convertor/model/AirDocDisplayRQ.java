//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}PointOfSale" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Document"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Party"/&gt;
 *         &lt;element name="Query"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="DisplayCriteria" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;attribute name="ET_OnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="EMD_OnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Flight" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlineID"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightNumber"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DepartureDate"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}DepartureCode"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}ArrivalCode"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="TicketDocument" type="{http://www.iata.org/IATA/EDIST}TicketDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="Passenger" type="{http://www.iata.org/IATA/EDIST}TravelerSummaryType" minOccurs="0"/&gt;
 *                   &lt;element name="OrderID" type="{http://www.iata.org/IATA/EDIST}CouponOrderKeyType" minOccurs="0"/&gt;
 *                   &lt;element name="FQTV" type="{http://www.iata.org/IATA/EDIST}TravelerFQTV_Type" minOccurs="0"/&gt;
 *                   &lt;element name="FOID" type="{http://www.iata.org/IATA/EDIST}TravelerFOID_Type" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}IATA_PayloadStdAttributes"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pointOfSale",
    "document",
    "party",
    "query"
})
@XmlRootElement(name = "AirDocDisplayRQ")
public class AirDocDisplayRQ {

    @XmlElement(name = "PointOfSale")
    protected PointOfSaleType pointOfSale;
    @XmlElement(name = "Document", required = true)
    protected MsgDocumentType document;
    @XmlElement(name = "Party", required = true)
    protected MsgPartiesType party;
    @XmlElement(name = "Query", required = true)
    protected AirDocDisplayRQ.Query query;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String target;
    @XmlAttribute(name = "Version", required = true)
    protected String version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "AsynchronousAllowedInd")
    protected Boolean asynchronousAllowedInd;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;

    /**
     * 获取pointOfSale属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PointOfSaleType }
     *     
     */
    public PointOfSaleType getPointOfSale() {
        return pointOfSale;
    }

    /**
     * 设置pointOfSale属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PointOfSaleType }
     *     
     */
    public void setPointOfSale(PointOfSaleType value) {
        this.pointOfSale = value;
    }

    /**
     * 获取document属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgDocumentType }
     *     
     */
    public MsgDocumentType getDocument() {
        return document;
    }

    /**
     * 设置document属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgDocumentType }
     *     
     */
    public void setDocument(MsgDocumentType value) {
        this.document = value;
    }

    /**
     * 获取party属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgPartiesType }
     *     
     */
    public MsgPartiesType getParty() {
        return party;
    }

    /**
     * 设置party属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgPartiesType }
     *     
     */
    public void setParty(MsgPartiesType value) {
        this.party = value;
    }

    /**
     * 获取query属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AirDocDisplayRQ.Query }
     *     
     */
    public AirDocDisplayRQ.Query getQuery() {
        return query;
    }

    /**
     * 设置query属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AirDocDisplayRQ.Query }
     *     
     */
    public void setQuery(AirDocDisplayRQ.Query value) {
        this.query = value;
    }

    /**
     * 获取echoToken属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * 设置echoToken属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * 获取target属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        if (target == null) {
            return "Production";
        } else {
            return target;
        }
    }

    /**
     * 设置target属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取transactionIdentifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * 设置transactionIdentifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * 获取sequenceNmbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * 设置sequenceNmbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * 获取transactionStatusCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * 设置transactionStatusCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * 获取retransmissionIndicator属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * 设置retransmissionIndicator属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * 获取correlationID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * 设置correlationID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * 获取asynchronousAllowedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAsynchronousAllowedInd() {
        return asynchronousAllowedInd;
    }

    /**
     * 设置asynchronousAllowedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAsynchronousAllowedInd(Boolean value) {
        this.asynchronousAllowedInd = value;
    }

    /**
     * 获取altLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * 设置altLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * 获取primaryLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * 设置primaryLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="DisplayCriteria" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;attribute name="ET_OnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="EMD_OnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Flight" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlineID"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightNumber"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DepartureDate"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}DepartureCode"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ArrivalCode"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="TicketDocument" type="{http://www.iata.org/IATA/EDIST}TicketDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="Passenger" type="{http://www.iata.org/IATA/EDIST}TravelerSummaryType" minOccurs="0"/&gt;
     *         &lt;element name="OrderID" type="{http://www.iata.org/IATA/EDIST}CouponOrderKeyType" minOccurs="0"/&gt;
     *         &lt;element name="FQTV" type="{http://www.iata.org/IATA/EDIST}TravelerFQTV_Type" minOccurs="0"/&gt;
     *         &lt;element name="FOID" type="{http://www.iata.org/IATA/EDIST}TravelerFOID_Type" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BookingReferences" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "displayCriteria",
        "flight",
        "ticketDocument",
        "passenger",
        "orderID",
        "fqtv",
        "foid",
        "bookingReferences"
    })
    public static class Query {

        @XmlElement(name = "DisplayCriteria")
        protected AirDocDisplayRQ.Query.DisplayCriteria displayCriteria;
        @XmlElement(name = "Flight")
        protected AirDocDisplayRQ.Query.Flight flight;
        @XmlElement(name = "TicketDocument")
        protected List<TicketDocumentType> ticketDocument;
        @XmlElement(name = "Passenger")
        protected TravelerSummaryType passenger;
        @XmlElement(name = "OrderID")
        protected CouponOrderKeyType orderID;
        @XmlElement(name = "FQTV")
        protected TravelerFQTVType fqtv;
        @XmlElement(name = "FOID")
        protected TravelerFOIDType foid;
        @XmlElement(name = "BookingReferences")
        protected BookingReferences bookingReferences;

        /**
         * 获取displayCriteria属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirDocDisplayRQ.Query.DisplayCriteria }
         *     
         */
        public AirDocDisplayRQ.Query.DisplayCriteria getDisplayCriteria() {
            return displayCriteria;
        }

        /**
         * 设置displayCriteria属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirDocDisplayRQ.Query.DisplayCriteria }
         *     
         */
        public void setDisplayCriteria(AirDocDisplayRQ.Query.DisplayCriteria value) {
            this.displayCriteria = value;
        }

        /**
         * 获取flight属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirDocDisplayRQ.Query.Flight }
         *     
         */
        public AirDocDisplayRQ.Query.Flight getFlight() {
            return flight;
        }

        /**
         * 设置flight属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirDocDisplayRQ.Query.Flight }
         *     
         */
        public void setFlight(AirDocDisplayRQ.Query.Flight value) {
            this.flight = value;
        }

        /**
         * Gets the value of the ticketDocument property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ticketDocument property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTicketDocument().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TicketDocumentType }
         * 
         * 
         */
        public List<TicketDocumentType> getTicketDocument() {
            if (ticketDocument == null) {
                ticketDocument = new ArrayList<TicketDocumentType>();
            }
            return this.ticketDocument;
        }

        /**
         * 获取passenger属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TravelerSummaryType }
         *     
         */
        public TravelerSummaryType getPassenger() {
            return passenger;
        }

        /**
         * 设置passenger属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TravelerSummaryType }
         *     
         */
        public void setPassenger(TravelerSummaryType value) {
            this.passenger = value;
        }

        /**
         * 获取orderID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CouponOrderKeyType }
         *     
         */
        public CouponOrderKeyType getOrderID() {
            return orderID;
        }

        /**
         * 设置orderID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CouponOrderKeyType }
         *     
         */
        public void setOrderID(CouponOrderKeyType value) {
            this.orderID = value;
        }

        /**
         * 获取fqtv属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TravelerFQTVType }
         *     
         */
        public TravelerFQTVType getFQTV() {
            return fqtv;
        }

        /**
         * 设置fqtv属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TravelerFQTVType }
         *     
         */
        public void setFQTV(TravelerFQTVType value) {
            this.fqtv = value;
        }

        /**
         * 获取foid属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TravelerFOIDType }
         *     
         */
        public TravelerFOIDType getFOID() {
            return foid;
        }

        /**
         * 设置foid属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TravelerFOIDType }
         *     
         */
        public void setFOID(TravelerFOIDType value) {
            this.foid = value;
        }

        /**
         * 获取bookingReferences属性的值。
         * 
         * @return
         *     possible object is
         *     {@link BookingReferences }
         *     
         */
        public BookingReferences getBookingReferences() {
            return bookingReferences;
        }

        /**
         * 设置bookingReferences属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link BookingReferences }
         *     
         */
        public void setBookingReferences(BookingReferences value) {
            this.bookingReferences = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;attribute name="ET_OnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="EMD_OnlyInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class DisplayCriteria {

            @XmlAttribute(name = "ET_OnlyInd")
            protected Boolean etOnlyInd;
            @XmlAttribute(name = "EMD_OnlyInd")
            protected Boolean emdOnlyInd;

            /**
             * 获取etOnlyInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isETOnlyInd() {
                return etOnlyInd;
            }

            /**
             * 设置etOnlyInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setETOnlyInd(Boolean value) {
                this.etOnlyInd = value;
            }

            /**
             * 获取emdOnlyInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isEMDOnlyInd() {
                return emdOnlyInd;
            }

            /**
             * 设置emdOnlyInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setEMDOnlyInd(Boolean value) {
                this.emdOnlyInd = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlineID"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightNumber"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DepartureDate"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}DepartureCode"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ArrivalCode"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "airlineID",
            "flightNumber",
            "departureDate",
            "departureCode",
            "arrivalCode"
        })
        public static class Flight {

            @XmlElement(name = "AirlineID", required = true)
            protected AirlineID airlineID;
            @XmlElement(name = "FlightNumber", required = true)
            protected FlightNumber flightNumber;
            @XmlElement(name = "DepartureDate", required = true)
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar departureDate;
            @XmlElement(name = "DepartureCode", required = true)
            protected DepartureCode departureCode;
            @XmlElement(name = "ArrivalCode", required = true)
            protected ArrivalCode arrivalCode;

            /**
             * 获取airlineID属性的值。
             * 
             * @return
             *     possible object is
             *     {@link AirlineID }
             *     
             */
            public AirlineID getAirlineID() {
                return airlineID;
            }

            /**
             * 设置airlineID属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link AirlineID }
             *     
             */
            public void setAirlineID(AirlineID value) {
                this.airlineID = value;
            }

            /**
             * 获取flightNumber属性的值。
             * 
             * @return
             *     possible object is
             *     {@link FlightNumber }
             *     
             */
            public FlightNumber getFlightNumber() {
                return flightNumber;
            }

            /**
             * 设置flightNumber属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link FlightNumber }
             *     
             */
            public void setFlightNumber(FlightNumber value) {
                this.flightNumber = value;
            }

            /**
             * 获取departureDate属性的值。
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDepartureDate() {
                return departureDate;
            }

            /**
             * 设置departureDate属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDepartureDate(XMLGregorianCalendar value) {
                this.departureDate = value;
            }

            /**
             * 获取departureCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link DepartureCode }
             *     
             */
            public DepartureCode getDepartureCode() {
                return departureCode;
            }

            /**
             * 设置departureCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link DepartureCode }
             *     
             */
            public void setDepartureCode(DepartureCode value) {
                this.departureCode = value;
            }

            /**
             * 获取arrivalCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link ArrivalCode }
             *     
             */
            public ArrivalCode getArrivalCode() {
                return arrivalCode;
            }

            /**
             * 设置arrivalCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link ArrivalCode }
             *     
             */
            public void setArrivalCode(ArrivalCode value) {
                this.arrivalCode = value;
            }

        }

    }

}
