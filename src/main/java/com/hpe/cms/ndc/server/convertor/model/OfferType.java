//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Offer definition.
 * 
 * <p>OfferType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="OfferType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}OfferItemDetailType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferType")
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.InvDiscrepencyAlertType.AlternateOffers.AlternateOffer.class,
    com.hpe.cms.ndc.server.convertor.model.AirShoppingRS.OffersGroup.AirlineOffers.AirlineOffer.class,
    com.hpe.cms.ndc.server.convertor.model.FlightPriceRS.AirlineOffers.AirlineOffer.class,
    com.hpe.cms.ndc.server.convertor.model.ItinReshopRS.Response.ReShopOffers.ReShopOffer.class,
    com.hpe.cms.ndc.server.convertor.model.ShopProductRS.Response.Offers.Offer.class,
    com.hpe.cms.ndc.server.convertor.model.ItinReshopAlertsType.OrderItemExceptions.Exception.ExpiredTimeLimits.ExpiredTimeLimit.AlternateOffers.AlternateOffer.class,
    com.hpe.cms.ndc.server.convertor.model.ItinReshopAlertsType.OrderItemExceptions.Exception.InvDiscrepency.AlternateOffers.AlternateOffer.class,
    com.hpe.cms.ndc.server.convertor.model.ItinReshopAlertsType.OrderItemExceptions.Exception.Eligibility.AlternateOffers.AlternateOffer.class
})
public class OfferType
    extends OfferItemDetailType
{


}
