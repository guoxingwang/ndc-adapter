//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Traveler" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AnonymousTraveler" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}RecognizedTraveler" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "traveler"
})
@XmlRootElement(name = "Travelers")
public class Travelers {

    @XmlElement(name = "Traveler", required = true)
    protected List<Travelers.Traveler> traveler;

    /**
     * Gets the value of the traveler property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the traveler property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTraveler().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Travelers.Traveler }
     * 
     * 
     */
    public List<Travelers.Traveler> getTraveler() {
        if (traveler == null) {
            traveler = new ArrayList<Travelers.Traveler>();
        }
        return this.traveler;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AnonymousTraveler" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}RecognizedTraveler" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "anonymousTraveler",
        "recognizedTraveler"
    })
    public static class Traveler {

        @XmlElement(name = "AnonymousTraveler")
        protected AnonymousTravelerType anonymousTraveler;
        @XmlElement(name = "RecognizedTraveler")
        protected TravelerDetailType recognizedTraveler;

        /**
         * 获取anonymousTraveler属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AnonymousTravelerType }
         *     
         */
        public AnonymousTravelerType getAnonymousTraveler() {
            return anonymousTraveler;
        }

        /**
         * 设置anonymousTraveler属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AnonymousTravelerType }
         *     
         */
        public void setAnonymousTraveler(AnonymousTravelerType value) {
            this.anonymousTraveler = value;
        }

        /**
         * 获取recognizedTraveler属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TravelerDetailType }
         *     
         */
        public TravelerDetailType getRecognizedTraveler() {
            return recognizedTraveler;
        }

        /**
         * 设置recognizedTraveler属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TravelerDetailType }
         *     
         */
        public void setRecognizedTraveler(TravelerDetailType value) {
            this.recognizedTraveler = value;
        }

    }

}
