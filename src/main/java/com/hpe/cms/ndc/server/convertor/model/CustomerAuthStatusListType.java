//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>CustomerAuthStatusListType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerAuthStatusListType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="A"/&gt;
 *     &lt;enumeration value="N"/&gt;
 *     &lt;enumeration value="U"/&gt;
 *     &lt;enumeration value="Y"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CustomerAuthStatusListType")
@XmlEnum
public enum CustomerAuthStatusListType {


    /**
     * 
     *             A- An authentication attempt occurred but could not be completed
     *           
     * 
     */
    A,

    /**
     * N- Customer not authenticated
     * 
     */
    N,

    /**
     * U- Unable to perform authentication
     * 
     */
    U,

    /**
     * Y- Customer authenticated
     * 
     */
    Y;

    public String value() {
        return name();
    }

    public static CustomerAuthStatusListType fromValue(String v) {
        return valueOf(v);
    }

}
