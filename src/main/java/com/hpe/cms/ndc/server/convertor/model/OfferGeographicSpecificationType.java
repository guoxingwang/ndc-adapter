//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Offer Geographic Specification information
 * 
 * <p>OfferGeographicSpecificationType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="OfferGeographicSpecificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DirectionalIndicator" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *         &lt;element name="OfferOriginPoint" type="{http://www.iata.org/IATA/EDIST}GeographicSpecificationType" minOccurs="0"/&gt;
 *         &lt;element name="OfferDestinationPoint" type="{http://www.iata.org/IATA/EDIST}GeographicSpecificationType" minOccurs="0"/&gt;
 *         &lt;element name="TravelWithinLocation" type="{http://www.iata.org/IATA/EDIST}GeographicSpecificationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferGeographicSpecificationType", propOrder = {
    "directionalIndicator",
    "offerOriginPoint",
    "offerDestinationPoint",
    "travelWithinLocation"
})
public class OfferGeographicSpecificationType {

    @XmlElement(name = "DirectionalIndicator")
    protected CodesetType directionalIndicator;
    @XmlElement(name = "OfferOriginPoint")
    protected GeographicSpecificationType offerOriginPoint;
    @XmlElement(name = "OfferDestinationPoint")
    protected GeographicSpecificationType offerDestinationPoint;
    @XmlElement(name = "TravelWithinLocation")
    protected GeographicSpecificationType travelWithinLocation;

    /**
     * 获取directionalIndicator属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getDirectionalIndicator() {
        return directionalIndicator;
    }

    /**
     * 设置directionalIndicator属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setDirectionalIndicator(CodesetType value) {
        this.directionalIndicator = value;
    }

    /**
     * 获取offerOriginPoint属性的值。
     * 
     * @return
     *     possible object is
     *     {@link GeographicSpecificationType }
     *     
     */
    public GeographicSpecificationType getOfferOriginPoint() {
        return offerOriginPoint;
    }

    /**
     * 设置offerOriginPoint属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link GeographicSpecificationType }
     *     
     */
    public void setOfferOriginPoint(GeographicSpecificationType value) {
        this.offerOriginPoint = value;
    }

    /**
     * 获取offerDestinationPoint属性的值。
     * 
     * @return
     *     possible object is
     *     {@link GeographicSpecificationType }
     *     
     */
    public GeographicSpecificationType getOfferDestinationPoint() {
        return offerDestinationPoint;
    }

    /**
     * 设置offerDestinationPoint属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link GeographicSpecificationType }
     *     
     */
    public void setOfferDestinationPoint(GeographicSpecificationType value) {
        this.offerDestinationPoint = value;
    }

    /**
     * 获取travelWithinLocation属性的值。
     * 
     * @return
     *     possible object is
     *     {@link GeographicSpecificationType }
     *     
     */
    public GeographicSpecificationType getTravelWithinLocation() {
        return travelWithinLocation;
    }

    /**
     * 设置travelWithinLocation属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link GeographicSpecificationType }
     *     
     */
    public void setTravelWithinLocation(GeographicSpecificationType value) {
        this.travelWithinLocation = value;
    }

}
