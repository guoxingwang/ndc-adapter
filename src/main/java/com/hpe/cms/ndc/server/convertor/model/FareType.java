//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * NDC Capability Model: FARE
 * 
 *  ============
 * Representation(s)
 *  ============
 * • Core Fare information
 * • Detail Fare information (Fare Component, Fare Rules)
 * 
 *  ============
 * Feature(s)
 *  ============
 * • Fare Indicators for Cat35 Fare, Reissue Pricing, Auto Exchange Pricing
 * • Fare Code (PADIS codeset element 9910 - Fare Qualifier)
 * 
 *  ============
 * Metadata
 *  ============
 * • FareMetadata
 * 
 * 
 * <p>FareType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="FareType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FareCode"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FareDetail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FareType", propOrder = {
    "fareCode",
    "fareDetail"
})
public class FareType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "FareCode", required = true)
    protected FareCodeType fareCode;
    @XmlElement(name = "FareDetail")
    protected FareDetailType fareDetail;

    /**
     * 获取fareCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FareCodeType }
     *     
     */
    public FareCodeType getFareCode() {
        return fareCode;
    }

    /**
     * 设置fareCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FareCodeType }
     *     
     */
    public void setFareCode(FareCodeType value) {
        this.fareCode = value;
    }

    /**
     * 获取fareDetail属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FareDetailType }
     *     
     */
    public FareDetailType getFareDetail() {
        return fareDetail;
    }

    /**
     * 设置fareDetail属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FareDetailType }
     *     
     */
    public void setFareDetail(FareDetailType value) {
        this.fareDetail = value;
    }

}
