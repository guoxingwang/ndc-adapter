package com.hpe.cms.ndc.server.config;

import com.hpe.cms.ndc.server.constance.WsServerConstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.validation.XmlValidator;
import org.springframework.xml.xsd.XsdSchema;
import org.springframework.xml.xsd.XsdSchemaCollection;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 17:56 2019/7/6
 * @ Description：
 * @ Modified By：
 */
@Slf4j
public class NDCDefaultWsdl11Definition<T> extends DefaultWsdl11Definition {

    public NDCDefaultWsdl11Definition(String portType, String version) {
        switch (version) {
            case "v15_2":
                this.setTargetNamespace(WsServerConstance.V15_2_NAMESPACE);
                break;
            case "v16_1":
                this.setTargetNamespace(WsServerConstance.V16_1_NAMESPACE);
                break;
            case "v16_2":
                this.setTargetNamespace(WsServerConstance.V16_2_NAMESPACE);
                break;
            case "v17_1":
                this.setTargetNamespace(WsServerConstance.V17_1_NAMESPACE);
                break;
            case "v17_2":
                this.setTargetNamespace(WsServerConstance.V17_2_NAMESPACE);
                break;
            case "v18_1":
                this.setTargetNamespace(WsServerConstance.V18_1_NAMESPACE);
                break;
            case "v18_2":
                this.setTargetNamespace(WsServerConstance.V18_2_NAMESPACE);
                break;
            default:
                this.setTargetNamespace(null);
        }
        this.setRequestSuffix(WsServerConstance.REQUEST_SUFFIX);
        this.setResponseSuffix(WsServerConstance.RESPONSE_SUFFIX);
        this.setPortTypeName(portType);
        this.setLocationUri(WsServerConstance.WSDL_PATH + version);
    }

    /**
     * 单xsd注册为一个wsdl
     *
     * @param portType portType
     * @param schemaRq request
     * @param schemaRs response
     */
    @Deprecated
    public NDCDefaultWsdl11Definition(String portType, String version, XsdSchema schemaRq, XsdSchema schemaRs) {
        this(portType, version);
        XsdSchemaCollection schemaCollection = new XsdSchemaCollection() {
            @Override
            public XsdSchema[] getXsdSchemas() {
                XsdSchema[] xsdSchemas = new XsdSchema[2];
                xsdSchemas[0] = schemaRq;
                xsdSchemas[1] = schemaRs;
                return xsdSchemas;
            }

            @Override
            public XmlValidator createValidator() {
                return null;
            }
        };
        this.setSchemaCollection(schemaCollection);
    }
}
