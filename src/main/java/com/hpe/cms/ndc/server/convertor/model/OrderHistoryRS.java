//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Document"/&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="Success" type="{http://www.iata.org/IATA/EDIST}SuccessType"/&gt;
 *             &lt;element name="Warnings" type="{http://www.iata.org/IATA/EDIST}WarningsType" minOccurs="0"/&gt;
 *             &lt;element name="Response"&gt;
 *               &lt;complexType&gt;
 *                 &lt;complexContent&gt;
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                     &lt;sequence&gt;
 *                       &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderHistoryProcessing"/&gt;
 *                       &lt;element name="Actions"&gt;
 *                         &lt;complexType&gt;
 *                           &lt;complexContent&gt;
 *                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                               &lt;sequence&gt;
 *                                 &lt;element name="Action" maxOccurs="unbounded"&gt;
 *                                   &lt;complexType&gt;
 *                                     &lt;complexContent&gt;
 *                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                         &lt;sequence&gt;
 *                                           &lt;element name="ActionType"&gt;
 *                                             &lt;complexType&gt;
 *                                               &lt;simpleContent&gt;
 *                                                 &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
 *                                                   &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                                                 &lt;/extension&gt;
 *                                               &lt;/simpleContent&gt;
 *                                             &lt;/complexType&gt;
 *                                           &lt;/element&gt;
 *                                           &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                                           &lt;element name="Entity"&gt;
 *                                             &lt;complexType&gt;
 *                                               &lt;complexContent&gt;
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                   &lt;sequence&gt;
 *                                                     &lt;element name="EntityID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
 *                                                     &lt;element name="Name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
 *                                                   &lt;/sequence&gt;
 *                                                 &lt;/restriction&gt;
 *                                               &lt;/complexContent&gt;
 *                                             &lt;/complexType&gt;
 *                                           &lt;/element&gt;
 *                                           &lt;element name="TransactionType" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType"/&gt;
 *                                           &lt;element name="Details" minOccurs="0"&gt;
 *                                             &lt;complexType&gt;
 *                                               &lt;complexContent&gt;
 *                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                   &lt;sequence&gt;
 *                                                     &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
 *                                                     &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
 *                                                     &lt;element name="OrderItems" minOccurs="0"&gt;
 *                                                       &lt;complexType&gt;
 *                                                         &lt;complexContent&gt;
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                             &lt;sequence&gt;
 *                                                               &lt;element name="OrderItem" maxOccurs="unbounded"&gt;
 *                                                                 &lt;complexType&gt;
 *                                                                   &lt;complexContent&gt;
 *                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                       &lt;choice&gt;
 *                                                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem"/&gt;
 *                                                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem"/&gt;
 *                                                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem"/&gt;
 *                                                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem"/&gt;
 *                                                                       &lt;/choice&gt;
 *                                                                     &lt;/restriction&gt;
 *                                                                   &lt;/complexContent&gt;
 *                                                                 &lt;/complexType&gt;
 *                                                               &lt;/element&gt;
 *                                                             &lt;/sequence&gt;
 *                                                           &lt;/restriction&gt;
 *                                                         &lt;/complexContent&gt;
 *                                                       &lt;/complexType&gt;
 *                                                     &lt;/element&gt;
 *                                                     &lt;element name="Passengers" minOccurs="0"&gt;
 *                                                       &lt;complexType&gt;
 *                                                         &lt;complexContent&gt;
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                             &lt;sequence&gt;
 *                                                               &lt;element name="Passenger" maxOccurs="unbounded"&gt;
 *                                                                 &lt;complexType&gt;
 *                                                                   &lt;complexContent&gt;
 *                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                       &lt;sequence&gt;
 *                                                                         &lt;element name="Name"&gt;
 *                                                                           &lt;complexType&gt;
 *                                                                             &lt;complexContent&gt;
 *                                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                                 &lt;sequence&gt;
 *                                                                                   &lt;element name="Surname"&gt;
 *                                                                                     &lt;complexType&gt;
 *                                                                                       &lt;simpleContent&gt;
 *                                                                                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
 *                                                                                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                                                                                         &lt;/extension&gt;
 *                                                                                       &lt;/simpleContent&gt;
 *                                                                                     &lt;/complexType&gt;
 *                                                                                   &lt;/element&gt;
 *                                                                                   &lt;element name="Given" maxOccurs="5" minOccurs="0"&gt;
 *                                                                                     &lt;complexType&gt;
 *                                                                                       &lt;simpleContent&gt;
 *                                                                                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
 *                                                                                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                                                                                         &lt;/extension&gt;
 *                                                                                       &lt;/simpleContent&gt;
 *                                                                                     &lt;/complexType&gt;
 *                                                                                   &lt;/element&gt;
 *                                                                                   &lt;element name="Middle" maxOccurs="3" minOccurs="0"&gt;
 *                                                                                     &lt;complexType&gt;
 *                                                                                       &lt;simpleContent&gt;
 *                                                                                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                                                                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                                                                                         &lt;/extension&gt;
 *                                                                                       &lt;/simpleContent&gt;
 *                                                                                     &lt;/complexType&gt;
 *                                                                                   &lt;/element&gt;
 *                                                                                 &lt;/sequence&gt;
 *                                                                                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectPolicyMetaAttrGroup"/&gt;
 *                                                                               &lt;/restriction&gt;
 *                                                                             &lt;/complexContent&gt;
 *                                                                           &lt;/complexType&gt;
 *                                                                         &lt;/element&gt;
 *                                                                         &lt;element name="ProfileID" minOccurs="0"&gt;
 *                                                                           &lt;complexType&gt;
 *                                                                             &lt;simpleContent&gt;
 *                                                                               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                                                                                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                                                                               &lt;/extension&gt;
 *                                                                             &lt;/simpleContent&gt;
 *                                                                           &lt;/complexType&gt;
 *                                                                         &lt;/element&gt;
 *                                                                       &lt;/sequence&gt;
 *                                                                     &lt;/restriction&gt;
 *                                                                   &lt;/complexContent&gt;
 *                                                                 &lt;/complexType&gt;
 *                                                               &lt;/element&gt;
 *                                                             &lt;/sequence&gt;
 *                                                           &lt;/restriction&gt;
 *                                                         &lt;/complexContent&gt;
 *                                                       &lt;/complexType&gt;
 *                                                     &lt;/element&gt;
 *                                                     &lt;element name="Payments" minOccurs="0"&gt;
 *                                                       &lt;complexType&gt;
 *                                                         &lt;complexContent&gt;
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                             &lt;sequence&gt;
 *                                                               &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentMethodType" maxOccurs="unbounded"/&gt;
 *                                                             &lt;/sequence&gt;
 *                                                           &lt;/restriction&gt;
 *                                                         &lt;/complexContent&gt;
 *                                                       &lt;/complexType&gt;
 *                                                     &lt;/element&gt;
 *                                                     &lt;element name="TotalOrderPrice" type="{http://www.iata.org/IATA/EDIST}DetailCurrencyPriceType" minOccurs="0"/&gt;
 *                                                     &lt;element name="TicketDocInfos" minOccurs="0"&gt;
 *                                                       &lt;complexType&gt;
 *                                                         &lt;complexContent&gt;
 *                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                             &lt;sequence&gt;
 *                                                               &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
 *                                                                 &lt;complexType&gt;
 *                                                                   &lt;complexContent&gt;
 *                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                       &lt;sequence&gt;
 *                                                                         &lt;element name="AgentIDs" minOccurs="0"&gt;
 *                                                                           &lt;complexType&gt;
 *                                                                             &lt;complexContent&gt;
 *                                                                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                                                                 &lt;sequence&gt;
 *                                                                                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
 *                                                                                 &lt;/sequence&gt;
 *                                                                               &lt;/restriction&gt;
 *                                                                             &lt;/complexContent&gt;
 *                                                                           &lt;/complexType&gt;
 *                                                                         &lt;/element&gt;
 *                                                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo" minOccurs="0"/&gt;
 *                                                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
 *                                                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
 *                                                                         &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
 *                                                                       &lt;/sequence&gt;
 *                                                                     &lt;/restriction&gt;
 *                                                                   &lt;/complexContent&gt;
 *                                                                 &lt;/complexType&gt;
 *                                                               &lt;/element&gt;
 *                                                             &lt;/sequence&gt;
 *                                                           &lt;/restriction&gt;
 *                                                         &lt;/complexContent&gt;
 *                                                       &lt;/complexType&gt;
 *                                                     &lt;/element&gt;
 *                                                   &lt;/sequence&gt;
 *                                                 &lt;/restriction&gt;
 *                                               &lt;/complexContent&gt;
 *                                             &lt;/complexType&gt;
 *                                           &lt;/element&gt;
 *                                         &lt;/sequence&gt;
 *                                       &lt;/restriction&gt;
 *                                     &lt;/complexContent&gt;
 *                                   &lt;/complexType&gt;
 *                                 &lt;/element&gt;
 *                               &lt;/sequence&gt;
 *                             &lt;/restriction&gt;
 *                           &lt;/complexContent&gt;
 *                         &lt;/complexType&gt;
 *                       &lt;/element&gt;
 *                     &lt;/sequence&gt;
 *                   &lt;/restriction&gt;
 *                 &lt;/complexContent&gt;
 *               &lt;/complexType&gt;
 *             &lt;/element&gt;
 *           &lt;/sequence&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}Errors"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}IATA_PayloadStdAttributes"/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "document",
    "success",
    "warnings",
    "response",
    "errors"
})
@XmlRootElement(name = "OrderHistoryRS")
public class OrderHistoryRS {

    @XmlElement(name = "Document", required = true)
    protected MsgDocumentType document;
    @XmlElement(name = "Success")
    protected SuccessType success;
    @XmlElement(name = "Warnings")
    protected WarningsType warnings;
    @XmlElement(name = "Response")
    protected OrderHistoryRS.Response response;
    @XmlElement(name = "Errors")
    protected ErrorsType errors;
    @XmlAttribute(name = "EchoToken")
    protected String echoToken;
    @XmlAttribute(name = "TimeStamp")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlAttribute(name = "Target")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String target;
    @XmlAttribute(name = "Version", required = true)
    protected String version;
    @XmlAttribute(name = "TransactionIdentifier")
    protected String transactionIdentifier;
    @XmlAttribute(name = "SequenceNmbr")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger sequenceNmbr;
    @XmlAttribute(name = "TransactionStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String transactionStatusCode;
    @XmlAttribute(name = "RetransmissionIndicator")
    protected Boolean retransmissionIndicator;
    @XmlAttribute(name = "CorrelationID")
    protected String correlationID;
    @XmlAttribute(name = "AsynchronousAllowedInd")
    protected Boolean asynchronousAllowedInd;
    @XmlAttribute(name = "AltLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String altLangID;
    @XmlAttribute(name = "PrimaryLangID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String primaryLangID;

    /**
     * 获取document属性的值。
     * 
     * @return
     *     possible object is
     *     {@link MsgDocumentType }
     *     
     */
    public MsgDocumentType getDocument() {
        return document;
    }

    /**
     * 设置document属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link MsgDocumentType }
     *     
     */
    public void setDocument(MsgDocumentType value) {
        this.document = value;
    }

    /**
     * 获取success属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SuccessType }
     *     
     */
    public SuccessType getSuccess() {
        return success;
    }

    /**
     * 设置success属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SuccessType }
     *     
     */
    public void setSuccess(SuccessType value) {
        this.success = value;
    }

    /**
     * 获取warnings属性的值。
     * 
     * @return
     *     possible object is
     *     {@link WarningsType }
     *     
     */
    public WarningsType getWarnings() {
        return warnings;
    }

    /**
     * 设置warnings属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link WarningsType }
     *     
     */
    public void setWarnings(WarningsType value) {
        this.warnings = value;
    }

    /**
     * 获取response属性的值。
     * 
     * @return
     *     possible object is
     *     {@link OrderHistoryRS.Response }
     *     
     */
    public OrderHistoryRS.Response getResponse() {
        return response;
    }

    /**
     * 设置response属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link OrderHistoryRS.Response }
     *     
     */
    public void setResponse(OrderHistoryRS.Response value) {
        this.response = value;
    }

    /**
     * 获取errors属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ErrorsType }
     *     
     */
    public ErrorsType getErrors() {
        return errors;
    }

    /**
     * 设置errors属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorsType }
     *     
     */
    public void setErrors(ErrorsType value) {
        this.errors = value;
    }

    /**
     * 获取echoToken属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEchoToken() {
        return echoToken;
    }

    /**
     * 设置echoToken属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEchoToken(String value) {
        this.echoToken = value;
    }

    /**
     * 获取timeStamp属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置timeStamp属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * 获取target属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        if (target == null) {
            return "Production";
        } else {
            return target;
        }
    }

    /**
     * 设置target属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * 获取version属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置version属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * 获取transactionIdentifier属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    /**
     * 设置transactionIdentifier属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionIdentifier(String value) {
        this.transactionIdentifier = value;
    }

    /**
     * 获取sequenceNmbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNmbr() {
        return sequenceNmbr;
    }

    /**
     * 设置sequenceNmbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNmbr(BigInteger value) {
        this.sequenceNmbr = value;
    }

    /**
     * 获取transactionStatusCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    /**
     * 设置transactionStatusCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionStatusCode(String value) {
        this.transactionStatusCode = value;
    }

    /**
     * 获取retransmissionIndicator属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetransmissionIndicator() {
        return retransmissionIndicator;
    }

    /**
     * 设置retransmissionIndicator属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetransmissionIndicator(Boolean value) {
        this.retransmissionIndicator = value;
    }

    /**
     * 获取correlationID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationID() {
        return correlationID;
    }

    /**
     * 设置correlationID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationID(String value) {
        this.correlationID = value;
    }

    /**
     * 获取asynchronousAllowedInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAsynchronousAllowedInd() {
        return asynchronousAllowedInd;
    }

    /**
     * 设置asynchronousAllowedInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAsynchronousAllowedInd(Boolean value) {
        this.asynchronousAllowedInd = value;
    }

    /**
     * 获取altLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAltLangID() {
        return altLangID;
    }

    /**
     * 设置altLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAltLangID(String value) {
        this.altLangID = value;
    }

    /**
     * 获取primaryLangID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryLangID() {
        return primaryLangID;
    }

    /**
     * 设置primaryLangID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryLangID(String value) {
        this.primaryLangID = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OrderHistoryProcessing"/&gt;
     *         &lt;element name="Actions"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Action" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ActionType"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;simpleContent&gt;
     *                                   &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
     *                                     &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *                                   &lt;/extension&gt;
     *                                 &lt;/simpleContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *                             &lt;element name="Entity"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="EntityID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
     *                                       &lt;element name="Name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                             &lt;element name="TransactionType" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType"/&gt;
     *                             &lt;element name="Details" minOccurs="0"&gt;
     *                               &lt;complexType&gt;
     *                                 &lt;complexContent&gt;
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                     &lt;sequence&gt;
     *                                       &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
     *                                       &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
     *                                       &lt;element name="OrderItems" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="OrderItem" maxOccurs="unbounded"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;choice&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem"/&gt;
     *                                                         &lt;/choice&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="Passengers" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="Passenger" maxOccurs="unbounded"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element name="Name"&gt;
     *                                                             &lt;complexType&gt;
     *                                                               &lt;complexContent&gt;
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                                   &lt;sequence&gt;
     *                                                                     &lt;element name="Surname"&gt;
     *                                                                       &lt;complexType&gt;
     *                                                                         &lt;simpleContent&gt;
     *                                                                           &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
     *                                                                             &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *                                                                           &lt;/extension&gt;
     *                                                                         &lt;/simpleContent&gt;
     *                                                                       &lt;/complexType&gt;
     *                                                                     &lt;/element&gt;
     *                                                                     &lt;element name="Given" maxOccurs="5" minOccurs="0"&gt;
     *                                                                       &lt;complexType&gt;
     *                                                                         &lt;simpleContent&gt;
     *                                                                           &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
     *                                                                             &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *                                                                           &lt;/extension&gt;
     *                                                                         &lt;/simpleContent&gt;
     *                                                                       &lt;/complexType&gt;
     *                                                                     &lt;/element&gt;
     *                                                                     &lt;element name="Middle" maxOccurs="3" minOccurs="0"&gt;
     *                                                                       &lt;complexType&gt;
     *                                                                         &lt;simpleContent&gt;
     *                                                                           &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                                                                             &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *                                                                           &lt;/extension&gt;
     *                                                                         &lt;/simpleContent&gt;
     *                                                                       &lt;/complexType&gt;
     *                                                                     &lt;/element&gt;
     *                                                                   &lt;/sequence&gt;
     *                                                                   &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectPolicyMetaAttrGroup"/&gt;
     *                                                                 &lt;/restriction&gt;
     *                                                               &lt;/complexContent&gt;
     *                                                             &lt;/complexType&gt;
     *                                                           &lt;/element&gt;
     *                                                           &lt;element name="ProfileID" minOccurs="0"&gt;
     *                                                             &lt;complexType&gt;
     *                                                               &lt;simpleContent&gt;
     *                                                                 &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                                                                   &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *                                                                 &lt;/extension&gt;
     *                                                               &lt;/simpleContent&gt;
     *                                                             &lt;/complexType&gt;
     *                                                           &lt;/element&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="Payments" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentMethodType" maxOccurs="unbounded"/&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                       &lt;element name="TotalOrderPrice" type="{http://www.iata.org/IATA/EDIST}DetailCurrencyPriceType" minOccurs="0"/&gt;
     *                                       &lt;element name="TicketDocInfos" minOccurs="0"&gt;
     *                                         &lt;complexType&gt;
     *                                           &lt;complexContent&gt;
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                               &lt;sequence&gt;
     *                                                 &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
     *                                                   &lt;complexType&gt;
     *                                                     &lt;complexContent&gt;
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                         &lt;sequence&gt;
     *                                                           &lt;element name="AgentIDs" minOccurs="0"&gt;
     *                                                             &lt;complexType&gt;
     *                                                               &lt;complexContent&gt;
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                                                                   &lt;sequence&gt;
     *                                                                     &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
     *                                                                   &lt;/sequence&gt;
     *                                                                 &lt;/restriction&gt;
     *                                                               &lt;/complexContent&gt;
     *                                                             &lt;/complexType&gt;
     *                                                           &lt;/element&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo" minOccurs="0"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
     *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
     *                                                         &lt;/sequence&gt;
     *                                                       &lt;/restriction&gt;
     *                                                     &lt;/complexContent&gt;
     *                                                   &lt;/complexType&gt;
     *                                                 &lt;/element&gt;
     *                                               &lt;/sequence&gt;
     *                                             &lt;/restriction&gt;
     *                                           &lt;/complexContent&gt;
     *                                         &lt;/complexType&gt;
     *                                       &lt;/element&gt;
     *                                     &lt;/sequence&gt;
     *                                   &lt;/restriction&gt;
     *                                 &lt;/complexContent&gt;
     *                               &lt;/complexType&gt;
     *                             &lt;/element&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "orderHistoryProcessing",
        "actions"
    })
    public static class Response {

        @XmlElement(name = "OrderHistoryProcessing", required = true)
        protected OrderHistProcessType orderHistoryProcessing;
        @XmlElement(name = "Actions", required = true)
        protected OrderHistoryRS.Response.Actions actions;

        /**
         * Transaction Processing Results, including processing status, alerts, notices, business warnings, supplemental marketing messages and policy.
         * 
         * @return
         *     possible object is
         *     {@link OrderHistProcessType }
         *     
         */
        public OrderHistProcessType getOrderHistoryProcessing() {
            return orderHistoryProcessing;
        }

        /**
         * 设置orderHistoryProcessing属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OrderHistProcessType }
         *     
         */
        public void setOrderHistoryProcessing(OrderHistProcessType value) {
            this.orderHistoryProcessing = value;
        }

        /**
         * 获取actions属性的值。
         * 
         * @return
         *     possible object is
         *     {@link OrderHistoryRS.Response.Actions }
         *     
         */
        public OrderHistoryRS.Response.Actions getActions() {
            return actions;
        }

        /**
         * 设置actions属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link OrderHistoryRS.Response.Actions }
         *     
         */
        public void setActions(OrderHistoryRS.Response.Actions value) {
            this.actions = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Action" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ActionType"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;simpleContent&gt;
         *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
         *                           &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *                         &lt;/extension&gt;
         *                       &lt;/simpleContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
         *                   &lt;element name="Entity"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="EntityID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
         *                             &lt;element name="Name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                   &lt;element name="TransactionType" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType"/&gt;
         *                   &lt;element name="Details" minOccurs="0"&gt;
         *                     &lt;complexType&gt;
         *                       &lt;complexContent&gt;
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                           &lt;sequence&gt;
         *                             &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
         *                             &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
         *                             &lt;element name="OrderItems" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="OrderItem" maxOccurs="unbounded"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;choice&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem"/&gt;
         *                                               &lt;/choice&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="Passengers" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="Passenger" maxOccurs="unbounded"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element name="Name"&gt;
         *                                                   &lt;complexType&gt;
         *                                                     &lt;complexContent&gt;
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                                         &lt;sequence&gt;
         *                                                           &lt;element name="Surname"&gt;
         *                                                             &lt;complexType&gt;
         *                                                               &lt;simpleContent&gt;
         *                                                                 &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
         *                                                                   &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *                                                                 &lt;/extension&gt;
         *                                                               &lt;/simpleContent&gt;
         *                                                             &lt;/complexType&gt;
         *                                                           &lt;/element&gt;
         *                                                           &lt;element name="Given" maxOccurs="5" minOccurs="0"&gt;
         *                                                             &lt;complexType&gt;
         *                                                               &lt;simpleContent&gt;
         *                                                                 &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
         *                                                                   &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *                                                                 &lt;/extension&gt;
         *                                                               &lt;/simpleContent&gt;
         *                                                             &lt;/complexType&gt;
         *                                                           &lt;/element&gt;
         *                                                           &lt;element name="Middle" maxOccurs="3" minOccurs="0"&gt;
         *                                                             &lt;complexType&gt;
         *                                                               &lt;simpleContent&gt;
         *                                                                 &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                                                                   &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *                                                                 &lt;/extension&gt;
         *                                                               &lt;/simpleContent&gt;
         *                                                             &lt;/complexType&gt;
         *                                                           &lt;/element&gt;
         *                                                         &lt;/sequence&gt;
         *                                                         &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectPolicyMetaAttrGroup"/&gt;
         *                                                       &lt;/restriction&gt;
         *                                                     &lt;/complexContent&gt;
         *                                                   &lt;/complexType&gt;
         *                                                 &lt;/element&gt;
         *                                                 &lt;element name="ProfileID" minOccurs="0"&gt;
         *                                                   &lt;complexType&gt;
         *                                                     &lt;simpleContent&gt;
         *                                                       &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *                                                         &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *                                                       &lt;/extension&gt;
         *                                                     &lt;/simpleContent&gt;
         *                                                   &lt;/complexType&gt;
         *                                                 &lt;/element&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="Payments" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentMethodType" maxOccurs="unbounded"/&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                             &lt;element name="TotalOrderPrice" type="{http://www.iata.org/IATA/EDIST}DetailCurrencyPriceType" minOccurs="0"/&gt;
         *                             &lt;element name="TicketDocInfos" minOccurs="0"&gt;
         *                               &lt;complexType&gt;
         *                                 &lt;complexContent&gt;
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                     &lt;sequence&gt;
         *                                       &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
         *                                         &lt;complexType&gt;
         *                                           &lt;complexContent&gt;
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                               &lt;sequence&gt;
         *                                                 &lt;element name="AgentIDs" minOccurs="0"&gt;
         *                                                   &lt;complexType&gt;
         *                                                     &lt;complexContent&gt;
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                                                         &lt;sequence&gt;
         *                                                           &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
         *                                                         &lt;/sequence&gt;
         *                                                       &lt;/restriction&gt;
         *                                                     &lt;/complexContent&gt;
         *                                                   &lt;/complexType&gt;
         *                                                 &lt;/element&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo" minOccurs="0"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
         *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
         *                                               &lt;/sequence&gt;
         *                                             &lt;/restriction&gt;
         *                                           &lt;/complexContent&gt;
         *                                         &lt;/complexType&gt;
         *                                       &lt;/element&gt;
         *                                     &lt;/sequence&gt;
         *                                   &lt;/restriction&gt;
         *                                 &lt;/complexContent&gt;
         *                               &lt;/complexType&gt;
         *                             &lt;/element&gt;
         *                           &lt;/sequence&gt;
         *                         &lt;/restriction&gt;
         *                       &lt;/complexContent&gt;
         *                     &lt;/complexType&gt;
         *                   &lt;/element&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "action"
        })
        public static class Actions {

            @XmlElement(name = "Action", required = true)
            protected List<OrderHistoryRS.Response.Actions.Action> action;

            /**
             * Gets the value of the action property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the action property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAction().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link OrderHistoryRS.Response.Actions.Action }
             * 
             * 
             */
            public List<OrderHistoryRS.Response.Actions.Action> getAction() {
                if (action == null) {
                    action = new ArrayList<OrderHistoryRS.Response.Actions.Action>();
                }
                return this.action;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ActionType"&gt;
             *           &lt;complexType&gt;
             *             &lt;simpleContent&gt;
             *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
             *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
             *               &lt;/extension&gt;
             *             &lt;/simpleContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="Date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
             *         &lt;element name="Entity"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="EntityID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
             *                   &lt;element name="Name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *         &lt;element name="TransactionType" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType"/&gt;
             *         &lt;element name="Details" minOccurs="0"&gt;
             *           &lt;complexType&gt;
             *             &lt;complexContent&gt;
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                 &lt;sequence&gt;
             *                   &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
             *                   &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
             *                   &lt;element name="OrderItems" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="OrderItem" maxOccurs="unbounded"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;choice&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem"/&gt;
             *                                     &lt;/choice&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="Passengers" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="Passenger" maxOccurs="unbounded"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element name="Name"&gt;
             *                                         &lt;complexType&gt;
             *                                           &lt;complexContent&gt;
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                               &lt;sequence&gt;
             *                                                 &lt;element name="Surname"&gt;
             *                                                   &lt;complexType&gt;
             *                                                     &lt;simpleContent&gt;
             *                                                       &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
             *                                                         &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
             *                                                       &lt;/extension&gt;
             *                                                     &lt;/simpleContent&gt;
             *                                                   &lt;/complexType&gt;
             *                                                 &lt;/element&gt;
             *                                                 &lt;element name="Given" maxOccurs="5" minOccurs="0"&gt;
             *                                                   &lt;complexType&gt;
             *                                                     &lt;simpleContent&gt;
             *                                                       &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
             *                                                         &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
             *                                                       &lt;/extension&gt;
             *                                                     &lt;/simpleContent&gt;
             *                                                   &lt;/complexType&gt;
             *                                                 &lt;/element&gt;
             *                                                 &lt;element name="Middle" maxOccurs="3" minOccurs="0"&gt;
             *                                                   &lt;complexType&gt;
             *                                                     &lt;simpleContent&gt;
             *                                                       &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *                                                         &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
             *                                                       &lt;/extension&gt;
             *                                                     &lt;/simpleContent&gt;
             *                                                   &lt;/complexType&gt;
             *                                                 &lt;/element&gt;
             *                                               &lt;/sequence&gt;
             *                                               &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectPolicyMetaAttrGroup"/&gt;
             *                                             &lt;/restriction&gt;
             *                                           &lt;/complexContent&gt;
             *                                         &lt;/complexType&gt;
             *                                       &lt;/element&gt;
             *                                       &lt;element name="ProfileID" minOccurs="0"&gt;
             *                                         &lt;complexType&gt;
             *                                           &lt;simpleContent&gt;
             *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
             *                                               &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
             *                                             &lt;/extension&gt;
             *                                           &lt;/simpleContent&gt;
             *                                         &lt;/complexType&gt;
             *                                       &lt;/element&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="Payments" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentMethodType" maxOccurs="unbounded"/&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                   &lt;element name="TotalOrderPrice" type="{http://www.iata.org/IATA/EDIST}DetailCurrencyPriceType" minOccurs="0"/&gt;
             *                   &lt;element name="TicketDocInfos" minOccurs="0"&gt;
             *                     &lt;complexType&gt;
             *                       &lt;complexContent&gt;
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                           &lt;sequence&gt;
             *                             &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
             *                               &lt;complexType&gt;
             *                                 &lt;complexContent&gt;
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                     &lt;sequence&gt;
             *                                       &lt;element name="AgentIDs" minOccurs="0"&gt;
             *                                         &lt;complexType&gt;
             *                                           &lt;complexContent&gt;
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *                                               &lt;sequence&gt;
             *                                                 &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
             *                                               &lt;/sequence&gt;
             *                                             &lt;/restriction&gt;
             *                                           &lt;/complexContent&gt;
             *                                         &lt;/complexType&gt;
             *                                       &lt;/element&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo" minOccurs="0"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
             *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
             *                                     &lt;/sequence&gt;
             *                                   &lt;/restriction&gt;
             *                                 &lt;/complexContent&gt;
             *                               &lt;/complexType&gt;
             *                             &lt;/element&gt;
             *                           &lt;/sequence&gt;
             *                         &lt;/restriction&gt;
             *                       &lt;/complexContent&gt;
             *                     &lt;/complexType&gt;
             *                   &lt;/element&gt;
             *                 &lt;/sequence&gt;
             *               &lt;/restriction&gt;
             *             &lt;/complexContent&gt;
             *           &lt;/complexType&gt;
             *         &lt;/element&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "actionType",
                "date",
                "entity",
                "transactionType",
                "details"
            })
            public static class Action {

                @XmlElement(name = "ActionType", required = true)
                protected OrderHistoryRS.Response.Actions.Action.ActionType actionType;
                @XmlElement(name = "Date", required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar date;
                @XmlElement(name = "Entity", required = true)
                protected OrderHistoryRS.Response.Actions.Action.Entity entity;
                @XmlElement(name = "TransactionType", required = true)
                protected String transactionType;
                @XmlElement(name = "Details")
                protected OrderHistoryRS.Response.Actions.Action.Details details;

                /**
                 * 获取actionType属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrderHistoryRS.Response.Actions.Action.ActionType }
                 *     
                 */
                public OrderHistoryRS.Response.Actions.Action.ActionType getActionType() {
                    return actionType;
                }

                /**
                 * 设置actionType属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrderHistoryRS.Response.Actions.Action.ActionType }
                 *     
                 */
                public void setActionType(OrderHistoryRS.Response.Actions.Action.ActionType value) {
                    this.actionType = value;
                }

                /**
                 * 获取date属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getDate() {
                    return date;
                }

                /**
                 * 设置date属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setDate(XMLGregorianCalendar value) {
                    this.date = value;
                }

                /**
                 * 获取entity属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrderHistoryRS.Response.Actions.Action.Entity }
                 *     
                 */
                public OrderHistoryRS.Response.Actions.Action.Entity getEntity() {
                    return entity;
                }

                /**
                 * 设置entity属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrderHistoryRS.Response.Actions.Action.Entity }
                 *     
                 */
                public void setEntity(OrderHistoryRS.Response.Actions.Action.Entity value) {
                    this.entity = value;
                }

                /**
                 * 获取transactionType属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTransactionType() {
                    return transactionType;
                }

                /**
                 * 设置transactionType属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTransactionType(String value) {
                    this.transactionType = value;
                }

                /**
                 * 获取details属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrderHistoryRS.Response.Actions.Action.Details }
                 *     
                 */
                public OrderHistoryRS.Response.Actions.Action.Details getDetails() {
                    return details;
                }

                /**
                 * 设置details属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrderHistoryRS.Response.Actions.Action.Details }
                 *     
                 */
                public void setDetails(OrderHistoryRS.Response.Actions.Action.Details value) {
                    this.details = value;
                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;simpleContent&gt;
                 *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;OrderItemActionListType"&gt;
                 *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
                 *     &lt;/extension&gt;
                 *   &lt;/simpleContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "value"
                })
                public static class ActionType {

                    @XmlValue
                    protected String value;
                    @XmlAttribute(name = "Context")
                    protected String context;

                    /**
                     * 
                     *         A data type for an Order Action. Examples: Create, Update, Delete, Reissue, Reprice
                     *       
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getValue() {
                        return value;
                    }

                    /**
                     * 设置value属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setValue(String value) {
                        this.value = value;
                    }

                    /**
                     * 获取context属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getContext() {
                        return context;
                    }

                    /**
                     * 设置context属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setContext(String value) {
                        this.context = value;
                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="Commission" type="{http://www.iata.org/IATA/EDIST}CommissionType" minOccurs="0"/&gt;
                 *         &lt;element name="Group" type="{http://www.iata.org/IATA/EDIST}GroupType" minOccurs="0"/&gt;
                 *         &lt;element name="OrderItems" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="OrderItem" maxOccurs="unbounded"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;choice&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem"/&gt;
                 *                           &lt;/choice&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="Passengers" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="Passenger" maxOccurs="unbounded"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element name="Name"&gt;
                 *                               &lt;complexType&gt;
                 *                                 &lt;complexContent&gt;
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                                     &lt;sequence&gt;
                 *                                       &lt;element name="Surname"&gt;
                 *                                         &lt;complexType&gt;
                 *                                           &lt;simpleContent&gt;
                 *                                             &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                 *                                               &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                 *                                             &lt;/extension&gt;
                 *                                           &lt;/simpleContent&gt;
                 *                                         &lt;/complexType&gt;
                 *                                       &lt;/element&gt;
                 *                                       &lt;element name="Given" maxOccurs="5" minOccurs="0"&gt;
                 *                                         &lt;complexType&gt;
                 *                                           &lt;simpleContent&gt;
                 *                                             &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                 *                                               &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                 *                                             &lt;/extension&gt;
                 *                                           &lt;/simpleContent&gt;
                 *                                         &lt;/complexType&gt;
                 *                                       &lt;/element&gt;
                 *                                       &lt;element name="Middle" maxOccurs="3" minOccurs="0"&gt;
                 *                                         &lt;complexType&gt;
                 *                                           &lt;simpleContent&gt;
                 *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                 *                                               &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                 *                                             &lt;/extension&gt;
                 *                                           &lt;/simpleContent&gt;
                 *                                         &lt;/complexType&gt;
                 *                                       &lt;/element&gt;
                 *                                     &lt;/sequence&gt;
                 *                                     &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectPolicyMetaAttrGroup"/&gt;
                 *                                   &lt;/restriction&gt;
                 *                                 &lt;/complexContent&gt;
                 *                               &lt;/complexType&gt;
                 *                             &lt;/element&gt;
                 *                             &lt;element name="ProfileID" minOccurs="0"&gt;
                 *                               &lt;complexType&gt;
                 *                                 &lt;simpleContent&gt;
                 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                 *                                     &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                 *                                   &lt;/extension&gt;
                 *                                 &lt;/simpleContent&gt;
                 *                               &lt;/complexType&gt;
                 *                             &lt;/element&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="Payments" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentMethodType" maxOccurs="unbounded"/&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *         &lt;element name="TotalOrderPrice" type="{http://www.iata.org/IATA/EDIST}DetailCurrencyPriceType" minOccurs="0"/&gt;
                 *         &lt;element name="TicketDocInfos" minOccurs="0"&gt;
                 *           &lt;complexType&gt;
                 *             &lt;complexContent&gt;
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                 &lt;sequence&gt;
                 *                   &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
                 *                     &lt;complexType&gt;
                 *                       &lt;complexContent&gt;
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                           &lt;sequence&gt;
                 *                             &lt;element name="AgentIDs" minOccurs="0"&gt;
                 *                               &lt;complexType&gt;
                 *                                 &lt;complexContent&gt;
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *                                     &lt;sequence&gt;
                 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
                 *                                     &lt;/sequence&gt;
                 *                                   &lt;/restriction&gt;
                 *                                 &lt;/complexContent&gt;
                 *                               &lt;/complexType&gt;
                 *                             &lt;/element&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo" minOccurs="0"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
                 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
                 *                           &lt;/sequence&gt;
                 *                         &lt;/restriction&gt;
                 *                       &lt;/complexContent&gt;
                 *                     &lt;/complexType&gt;
                 *                   &lt;/element&gt;
                 *                 &lt;/sequence&gt;
                 *               &lt;/restriction&gt;
                 *             &lt;/complexContent&gt;
                 *           &lt;/complexType&gt;
                 *         &lt;/element&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "commission",
                    "group",
                    "orderItems",
                    "passengers",
                    "payments",
                    "totalOrderPrice",
                    "ticketDocInfos"
                })
                public static class Details {

                    @XmlElement(name = "Commission")
                    protected CommissionType commission;
                    @XmlElement(name = "Group")
                    protected GroupType group;
                    @XmlElement(name = "OrderItems")
                    protected OrderHistoryRS.Response.Actions.Action.Details.OrderItems orderItems;
                    @XmlElement(name = "Passengers")
                    protected OrderHistoryRS.Response.Actions.Action.Details.Passengers passengers;
                    @XmlElement(name = "Payments")
                    protected OrderHistoryRS.Response.Actions.Action.Details.Payments payments;
                    @XmlElement(name = "TotalOrderPrice")
                    protected DetailCurrencyPriceType totalOrderPrice;
                    @XmlElement(name = "TicketDocInfos")
                    protected OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos ticketDocInfos;

                    /**
                     * 获取commission属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link CommissionType }
                     *     
                     */
                    public CommissionType getCommission() {
                        return commission;
                    }

                    /**
                     * 设置commission属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link CommissionType }
                     *     
                     */
                    public void setCommission(CommissionType value) {
                        this.commission = value;
                    }

                    /**
                     * 获取group属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link GroupType }
                     *     
                     */
                    public GroupType getGroup() {
                        return group;
                    }

                    /**
                     * 设置group属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link GroupType }
                     *     
                     */
                    public void setGroup(GroupType value) {
                        this.group = value;
                    }

                    /**
                     * 获取orderItems属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link OrderHistoryRS.Response.Actions.Action.Details.OrderItems }
                     *     
                     */
                    public OrderHistoryRS.Response.Actions.Action.Details.OrderItems getOrderItems() {
                        return orderItems;
                    }

                    /**
                     * 设置orderItems属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OrderHistoryRS.Response.Actions.Action.Details.OrderItems }
                     *     
                     */
                    public void setOrderItems(OrderHistoryRS.Response.Actions.Action.Details.OrderItems value) {
                        this.orderItems = value;
                    }

                    /**
                     * 获取passengers属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers }
                     *     
                     */
                    public OrderHistoryRS.Response.Actions.Action.Details.Passengers getPassengers() {
                        return passengers;
                    }

                    /**
                     * 设置passengers属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers }
                     *     
                     */
                    public void setPassengers(OrderHistoryRS.Response.Actions.Action.Details.Passengers value) {
                        this.passengers = value;
                    }

                    /**
                     * 获取payments属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link OrderHistoryRS.Response.Actions.Action.Details.Payments }
                     *     
                     */
                    public OrderHistoryRS.Response.Actions.Action.Details.Payments getPayments() {
                        return payments;
                    }

                    /**
                     * 设置payments属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OrderHistoryRS.Response.Actions.Action.Details.Payments }
                     *     
                     */
                    public void setPayments(OrderHistoryRS.Response.Actions.Action.Details.Payments value) {
                        this.payments = value;
                    }

                    /**
                     * 获取totalOrderPrice属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link DetailCurrencyPriceType }
                     *     
                     */
                    public DetailCurrencyPriceType getTotalOrderPrice() {
                        return totalOrderPrice;
                    }

                    /**
                     * 设置totalOrderPrice属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link DetailCurrencyPriceType }
                     *     
                     */
                    public void setTotalOrderPrice(DetailCurrencyPriceType value) {
                        this.totalOrderPrice = value;
                    }

                    /**
                     * 获取ticketDocInfos属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos }
                     *     
                     */
                    public OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos getTicketDocInfos() {
                        return ticketDocInfos;
                    }

                    /**
                     * 设置ticketDocInfos属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos }
                     *     
                     */
                    public void setTicketDocInfos(OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos value) {
                        this.ticketDocInfos = value;
                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="OrderItem" maxOccurs="unbounded"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;choice&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem"/&gt;
                     *                 &lt;/choice&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "orderItem"
                    })
                    public static class OrderItems {

                        @XmlElement(name = "OrderItem", required = true)
                        protected List<OrderHistoryRS.Response.Actions.Action.Details.OrderItems.OrderItem> orderItem;

                        /**
                         * Gets the value of the orderItem property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the orderItem property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getOrderItem().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link OrderHistoryRS.Response.Actions.Action.Details.OrderItems.OrderItem }
                         * 
                         * 
                         */
                        public List<OrderHistoryRS.Response.Actions.Action.Details.OrderItems.OrderItem> getOrderItem() {
                            if (orderItem == null) {
                                orderItem = new ArrayList<OrderHistoryRS.Response.Actions.Action.Details.OrderItems.OrderItem>();
                            }
                            return this.orderItem;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;choice&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}BaggageItem"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}FlightItem"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SeatItem"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherItem"/&gt;
                         *       &lt;/choice&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "baggageItem",
                            "flightItem",
                            "seatItem",
                            "otherItem"
                        })
                        public static class OrderItem {

                            @XmlElement(name = "BaggageItem")
                            protected BaggageItemType baggageItem;
                            @XmlElement(name = "FlightItem")
                            protected FlightItemType flightItem;
                            @XmlElement(name = "SeatItem")
                            protected SeatItem seatItem;
                            @XmlElement(name = "OtherItem")
                            protected OtherItemType otherItem;

                            /**
                             * 获取baggageItem属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link BaggageItemType }
                             *     
                             */
                            public BaggageItemType getBaggageItem() {
                                return baggageItem;
                            }

                            /**
                             * 设置baggageItem属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link BaggageItemType }
                             *     
                             */
                            public void setBaggageItem(BaggageItemType value) {
                                this.baggageItem = value;
                            }

                            /**
                             * 获取flightItem属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link FlightItemType }
                             *     
                             */
                            public FlightItemType getFlightItem() {
                                return flightItem;
                            }

                            /**
                             * 设置flightItem属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link FlightItemType }
                             *     
                             */
                            public void setFlightItem(FlightItemType value) {
                                this.flightItem = value;
                            }

                            /**
                             * 获取seatItem属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link SeatItem }
                             *     
                             */
                            public SeatItem getSeatItem() {
                                return seatItem;
                            }

                            /**
                             * 设置seatItem属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link SeatItem }
                             *     
                             */
                            public void setSeatItem(SeatItem value) {
                                this.seatItem = value;
                            }

                            /**
                             * 获取otherItem属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link OtherItemType }
                             *     
                             */
                            public OtherItemType getOtherItem() {
                                return otherItem;
                            }

                            /**
                             * 设置otherItem属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OtherItemType }
                             *     
                             */
                            public void setOtherItem(OtherItemType value) {
                                this.otherItem = value;
                            }

                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="Passenger" maxOccurs="unbounded"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element name="Name"&gt;
                     *                     &lt;complexType&gt;
                     *                       &lt;complexContent&gt;
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                           &lt;sequence&gt;
                     *                             &lt;element name="Surname"&gt;
                     *                               &lt;complexType&gt;
                     *                                 &lt;simpleContent&gt;
                     *                                   &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                     *                                     &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                     *                                   &lt;/extension&gt;
                     *                                 &lt;/simpleContent&gt;
                     *                               &lt;/complexType&gt;
                     *                             &lt;/element&gt;
                     *                             &lt;element name="Given" maxOccurs="5" minOccurs="0"&gt;
                     *                               &lt;complexType&gt;
                     *                                 &lt;simpleContent&gt;
                     *                                   &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                     *                                     &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                     *                                   &lt;/extension&gt;
                     *                                 &lt;/simpleContent&gt;
                     *                               &lt;/complexType&gt;
                     *                             &lt;/element&gt;
                     *                             &lt;element name="Middle" maxOccurs="3" minOccurs="0"&gt;
                     *                               &lt;complexType&gt;
                     *                                 &lt;simpleContent&gt;
                     *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                     *                                     &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                     *                                   &lt;/extension&gt;
                     *                                 &lt;/simpleContent&gt;
                     *                               &lt;/complexType&gt;
                     *                             &lt;/element&gt;
                     *                           &lt;/sequence&gt;
                     *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectPolicyMetaAttrGroup"/&gt;
                     *                         &lt;/restriction&gt;
                     *                       &lt;/complexContent&gt;
                     *                     &lt;/complexType&gt;
                     *                   &lt;/element&gt;
                     *                   &lt;element name="ProfileID" minOccurs="0"&gt;
                     *                     &lt;complexType&gt;
                     *                       &lt;simpleContent&gt;
                     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                     *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                     *                         &lt;/extension&gt;
                     *                       &lt;/simpleContent&gt;
                     *                     &lt;/complexType&gt;
                     *                   &lt;/element&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "passenger"
                    })
                    public static class Passengers {

                        @XmlElement(name = "Passenger", required = true)
                        protected List<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger> passenger;

                        /**
                         * Gets the value of the passenger property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the passenger property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getPassenger().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger }
                         * 
                         * 
                         */
                        public List<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger> getPassenger() {
                            if (passenger == null) {
                                passenger = new ArrayList<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger>();
                            }
                            return this.passenger;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element name="Name"&gt;
                         *           &lt;complexType&gt;
                         *             &lt;complexContent&gt;
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *                 &lt;sequence&gt;
                         *                   &lt;element name="Surname"&gt;
                         *                     &lt;complexType&gt;
                         *                       &lt;simpleContent&gt;
                         *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                         *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                         *                         &lt;/extension&gt;
                         *                       &lt;/simpleContent&gt;
                         *                     &lt;/complexType&gt;
                         *                   &lt;/element&gt;
                         *                   &lt;element name="Given" maxOccurs="5" minOccurs="0"&gt;
                         *                     &lt;complexType&gt;
                         *                       &lt;simpleContent&gt;
                         *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                         *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                         *                         &lt;/extension&gt;
                         *                       &lt;/simpleContent&gt;
                         *                     &lt;/complexType&gt;
                         *                   &lt;/element&gt;
                         *                   &lt;element name="Middle" maxOccurs="3" minOccurs="0"&gt;
                         *                     &lt;complexType&gt;
                         *                       &lt;simpleContent&gt;
                         *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                         *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                         *                         &lt;/extension&gt;
                         *                       &lt;/simpleContent&gt;
                         *                     &lt;/complexType&gt;
                         *                   &lt;/element&gt;
                         *                 &lt;/sequence&gt;
                         *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectPolicyMetaAttrGroup"/&gt;
                         *               &lt;/restriction&gt;
                         *             &lt;/complexContent&gt;
                         *           &lt;/complexType&gt;
                         *         &lt;/element&gt;
                         *         &lt;element name="ProfileID" minOccurs="0"&gt;
                         *           &lt;complexType&gt;
                         *             &lt;simpleContent&gt;
                         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                         *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                         *               &lt;/extension&gt;
                         *             &lt;/simpleContent&gt;
                         *           &lt;/complexType&gt;
                         *         &lt;/element&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "name",
                            "profileID"
                        })
                        public static class Passenger {

                            @XmlElement(name = "Name", required = true)
                            protected OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name name;
                            @XmlElement(name = "ProfileID")
                            protected OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.ProfileID profileID;

                            /**
                             * 获取name属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name }
                             *     
                             */
                            public OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name getName() {
                                return name;
                            }

                            /**
                             * 设置name属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name }
                             *     
                             */
                            public void setName(OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name value) {
                                this.name = value;
                            }

                            /**
                             * 获取profileID属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.ProfileID }
                             *     
                             */
                            public OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.ProfileID getProfileID() {
                                return profileID;
                            }

                            /**
                             * 设置profileID属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.ProfileID }
                             *     
                             */
                            public void setProfileID(OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.ProfileID value) {
                                this.profileID = value;
                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType&gt;
                             *   &lt;complexContent&gt;
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                             *       &lt;sequence&gt;
                             *         &lt;element name="Surname"&gt;
                             *           &lt;complexType&gt;
                             *             &lt;simpleContent&gt;
                             *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                             *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                             *               &lt;/extension&gt;
                             *             &lt;/simpleContent&gt;
                             *           &lt;/complexType&gt;
                             *         &lt;/element&gt;
                             *         &lt;element name="Given" maxOccurs="5" minOccurs="0"&gt;
                             *           &lt;complexType&gt;
                             *             &lt;simpleContent&gt;
                             *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                             *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                             *               &lt;/extension&gt;
                             *             &lt;/simpleContent&gt;
                             *           &lt;/complexType&gt;
                             *         &lt;/element&gt;
                             *         &lt;element name="Middle" maxOccurs="3" minOccurs="0"&gt;
                             *           &lt;complexType&gt;
                             *             &lt;simpleContent&gt;
                             *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                             *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                             *               &lt;/extension&gt;
                             *             &lt;/simpleContent&gt;
                             *           &lt;/complexType&gt;
                             *         &lt;/element&gt;
                             *       &lt;/sequence&gt;
                             *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjectPolicyMetaAttrGroup"/&gt;
                             *     &lt;/restriction&gt;
                             *   &lt;/complexContent&gt;
                             * &lt;/complexType&gt;
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "surname",
                                "given",
                                "middle"
                            })
                            public static class Name {

                                @XmlElement(name = "Surname", required = true)
                                protected OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Surname surname;
                                @XmlElement(name = "Given")
                                protected List<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Given> given;
                                @XmlElement(name = "Middle")
                                protected List<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Middle> middle;
                                @XmlAttribute(name = "refs")
                                @XmlIDREF
                                protected List<Object> refs;
                                @XmlAttribute(name = "ObjectMetaReferences")
                                @XmlIDREF
                                protected List<Object> objectMetaReferences;

                                /**
                                 * 获取surname属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Surname }
                                 *     
                                 */
                                public OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Surname getSurname() {
                                    return surname;
                                }

                                /**
                                 * 设置surname属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Surname }
                                 *     
                                 */
                                public void setSurname(OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Surname value) {
                                    this.surname = value;
                                }

                                /**
                                 * Gets the value of the given property.
                                 * 
                                 * <p>
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a <CODE>set</CODE> method for the given property.
                                 * 
                                 * <p>
                                 * For example, to add a new item, do as follows:
                                 * <pre>
                                 *    getGiven().add(newItem);
                                 * </pre>
                                 * 
                                 * 
                                 * <p>
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Given }
                                 * 
                                 * 
                                 */
                                public List<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Given> getGiven() {
                                    if (given == null) {
                                        given = new ArrayList<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Given>();
                                    }
                                    return this.given;
                                }

                                /**
                                 * Gets the value of the middle property.
                                 * 
                                 * <p>
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a <CODE>set</CODE> method for the middle property.
                                 * 
                                 * <p>
                                 * For example, to add a new item, do as follows:
                                 * <pre>
                                 *    getMiddle().add(newItem);
                                 * </pre>
                                 * 
                                 * 
                                 * <p>
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Middle }
                                 * 
                                 * 
                                 */
                                public List<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Middle> getMiddle() {
                                    if (middle == null) {
                                        middle = new ArrayList<OrderHistoryRS.Response.Actions.Action.Details.Passengers.Passenger.Name.Middle>();
                                    }
                                    return this.middle;
                                }

                                /**
                                 * Gets the value of the refs property.
                                 * 
                                 * <p>
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a <CODE>set</CODE> method for the refs property.
                                 * 
                                 * <p>
                                 * For example, to add a new item, do as follows:
                                 * <pre>
                                 *    getRefs().add(newItem);
                                 * </pre>
                                 * 
                                 * 
                                 * <p>
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link Object }
                                 * 
                                 * 
                                 */
                                public List<Object> getRefs() {
                                    if (refs == null) {
                                        refs = new ArrayList<Object>();
                                    }
                                    return this.refs;
                                }

                                /**
                                 * Gets the value of the objectMetaReferences property.
                                 * 
                                 * <p>
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a <CODE>set</CODE> method for the objectMetaReferences property.
                                 * 
                                 * <p>
                                 * For example, to add a new item, do as follows:
                                 * <pre>
                                 *    getObjectMetaReferences().add(newItem);
                                 * </pre>
                                 * 
                                 * 
                                 * <p>
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link Object }
                                 * 
                                 * 
                                 */
                                public List<Object> getObjectMetaReferences() {
                                    if (objectMetaReferences == null) {
                                        objectMetaReferences = new ArrayList<Object>();
                                    }
                                    return this.objectMetaReferences;
                                }


                                /**
                                 * <p>anonymous complex type的 Java 类。
                                 * 
                                 * <p>以下模式片段指定包含在此类中的预期内容。
                                 * 
                                 * <pre>
                                 * &lt;complexType&gt;
                                 *   &lt;simpleContent&gt;
                                 *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                                 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                                 *     &lt;/extension&gt;
                                 *   &lt;/simpleContent&gt;
                                 * &lt;/complexType&gt;
                                 * </pre>
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "value"
                                })
                                public static class Given {

                                    @XmlValue
                                    protected String value;
                                    @XmlAttribute(name = "refs")
                                    @XmlIDREF
                                    protected List<Object> refs;

                                    /**
                                     * A data type for Proper Name size constraint.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getValue() {
                                        return value;
                                    }

                                    /**
                                     * 设置value属性的值。
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setValue(String value) {
                                        this.value = value;
                                    }

                                    /**
                                     * Gets the value of the refs property.
                                     * 
                                     * <p>
                                     * This accessor method returns a reference to the live list,
                                     * not a snapshot. Therefore any modification you make to the
                                     * returned list will be present inside the JAXB object.
                                     * This is why there is not a <CODE>set</CODE> method for the refs property.
                                     * 
                                     * <p>
                                     * For example, to add a new item, do as follows:
                                     * <pre>
                                     *    getRefs().add(newItem);
                                     * </pre>
                                     * 
                                     * 
                                     * <p>
                                     * Objects of the following type(s) are allowed in the list
                                     * {@link Object }
                                     * 
                                     * 
                                     */
                                    public List<Object> getRefs() {
                                        if (refs == null) {
                                            refs = new ArrayList<Object>();
                                        }
                                        return this.refs;
                                    }

                                }


                                /**
                                 * <p>anonymous complex type的 Java 类。
                                 * 
                                 * <p>以下模式片段指定包含在此类中的预期内容。
                                 * 
                                 * <pre>
                                 * &lt;complexType&gt;
                                 *   &lt;simpleContent&gt;
                                 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                                 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                                 *     &lt;/extension&gt;
                                 *   &lt;/simpleContent&gt;
                                 * &lt;/complexType&gt;
                                 * </pre>
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "value"
                                })
                                public static class Middle {

                                    @XmlValue
                                    protected String value;
                                    @XmlAttribute(name = "refs")
                                    @XmlIDREF
                                    protected List<Object> refs;

                                    /**
                                     * 获取value属性的值。
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getValue() {
                                        return value;
                                    }

                                    /**
                                     * 设置value属性的值。
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setValue(String value) {
                                        this.value = value;
                                    }

                                    /**
                                     * Gets the value of the refs property.
                                     * 
                                     * <p>
                                     * This accessor method returns a reference to the live list,
                                     * not a snapshot. Therefore any modification you make to the
                                     * returned list will be present inside the JAXB object.
                                     * This is why there is not a <CODE>set</CODE> method for the refs property.
                                     * 
                                     * <p>
                                     * For example, to add a new item, do as follows:
                                     * <pre>
                                     *    getRefs().add(newItem);
                                     * </pre>
                                     * 
                                     * 
                                     * <p>
                                     * Objects of the following type(s) are allowed in the list
                                     * {@link Object }
                                     * 
                                     * 
                                     */
                                    public List<Object> getRefs() {
                                        if (refs == null) {
                                            refs = new ArrayList<Object>();
                                        }
                                        return this.refs;
                                    }

                                }


                                /**
                                 * <p>anonymous complex type的 Java 类。
                                 * 
                                 * <p>以下模式片段指定包含在此类中的预期内容。
                                 * 
                                 * <pre>
                                 * &lt;complexType&gt;
                                 *   &lt;simpleContent&gt;
                                 *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;ProperNameSimpleType"&gt;
                                 *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                                 *     &lt;/extension&gt;
                                 *   &lt;/simpleContent&gt;
                                 * &lt;/complexType&gt;
                                 * </pre>
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "value"
                                })
                                public static class Surname {

                                    @XmlValue
                                    protected String value;
                                    @XmlAttribute(name = "refs")
                                    @XmlIDREF
                                    protected List<Object> refs;

                                    /**
                                     * A data type for Proper Name size constraint.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getValue() {
                                        return value;
                                    }

                                    /**
                                     * 设置value属性的值。
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setValue(String value) {
                                        this.value = value;
                                    }

                                    /**
                                     * Gets the value of the refs property.
                                     * 
                                     * <p>
                                     * This accessor method returns a reference to the live list,
                                     * not a snapshot. Therefore any modification you make to the
                                     * returned list will be present inside the JAXB object.
                                     * This is why there is not a <CODE>set</CODE> method for the refs property.
                                     * 
                                     * <p>
                                     * For example, to add a new item, do as follows:
                                     * <pre>
                                     *    getRefs().add(newItem);
                                     * </pre>
                                     * 
                                     * 
                                     * <p>
                                     * Objects of the following type(s) are allowed in the list
                                     * {@link Object }
                                     * 
                                     * 
                                     */
                                    public List<Object> getRefs() {
                                        if (refs == null) {
                                            refs = new ArrayList<Object>();
                                        }
                                        return this.refs;
                                    }

                                }

                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType&gt;
                             *   &lt;simpleContent&gt;
                             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
                             *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
                             *     &lt;/extension&gt;
                             *   &lt;/simpleContent&gt;
                             * &lt;/complexType&gt;
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "value"
                            })
                            public static class ProfileID {

                                @XmlValue
                                protected String value;
                                @XmlAttribute(name = "refs")
                                @XmlIDREF
                                protected List<Object> refs;

                                /**
                                 * 获取value属性的值。
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getValue() {
                                    return value;
                                }

                                /**
                                 * 设置value属性的值。
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setValue(String value) {
                                    this.value = value;
                                }

                                /**
                                 * Gets the value of the refs property.
                                 * 
                                 * <p>
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a <CODE>set</CODE> method for the refs property.
                                 * 
                                 * <p>
                                 * For example, to add a new item, do as follows:
                                 * <pre>
                                 *    getRefs().add(newItem);
                                 * </pre>
                                 * 
                                 * 
                                 * <p>
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link Object }
                                 * 
                                 * 
                                 */
                                public List<Object> getRefs() {
                                    if (refs == null) {
                                        refs = new ArrayList<Object>();
                                    }
                                    return this.refs;
                                }

                            }

                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="Payment" type="{http://www.iata.org/IATA/EDIST}OrderPaymentMethodType" maxOccurs="unbounded"/&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "payment"
                    })
                    public static class Payments {

                        @XmlElement(name = "Payment", required = true)
                        protected List<OrderPaymentMethodType> payment;

                        /**
                         * Gets the value of the payment property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the payment property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getPayment().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link OrderPaymentMethodType }
                         * 
                         * 
                         */
                        public List<OrderPaymentMethodType> getPayment() {
                            if (payment == null) {
                                payment = new ArrayList<OrderPaymentMethodType>();
                            }
                            return this.payment;
                        }

                    }


                    /**
                     * <p>anonymous complex type的 Java 类。
                     * 
                     * <p>以下模式片段指定包含在此类中的预期内容。
                     * 
                     * <pre>
                     * &lt;complexType&gt;
                     *   &lt;complexContent&gt;
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *       &lt;sequence&gt;
                     *         &lt;element name="TicketDocInfo" maxOccurs="unbounded"&gt;
                     *           &lt;complexType&gt;
                     *             &lt;complexContent&gt;
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                 &lt;sequence&gt;
                     *                   &lt;element name="AgentIDs" minOccurs="0"&gt;
                     *                     &lt;complexType&gt;
                     *                       &lt;complexContent&gt;
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                     *                           &lt;sequence&gt;
                     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
                     *                           &lt;/sequence&gt;
                     *                         &lt;/restriction&gt;
                     *                       &lt;/complexContent&gt;
                     *                     &lt;/complexType&gt;
                     *                   &lt;/element&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo" minOccurs="0"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
                     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
                     *                 &lt;/sequence&gt;
                     *               &lt;/restriction&gt;
                     *             &lt;/complexContent&gt;
                     *           &lt;/complexType&gt;
                     *         &lt;/element&gt;
                     *       &lt;/sequence&gt;
                     *     &lt;/restriction&gt;
                     *   &lt;/complexContent&gt;
                     * &lt;/complexType&gt;
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ticketDocInfo"
                    })
                    public static class TicketDocInfos {

                        @XmlElement(name = "TicketDocInfo", required = true)
                        protected List<OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo> ticketDocInfo;

                        /**
                         * Gets the value of the ticketDocInfo property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the ticketDocInfo property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getTicketDocInfo().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo }
                         * 
                         * 
                         */
                        public List<OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo> getTicketDocInfo() {
                            if (ticketDocInfo == null) {
                                ticketDocInfo = new ArrayList<OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo>();
                            }
                            return this.ticketDocInfo;
                        }


                        /**
                         * <p>anonymous complex type的 Java 类。
                         * 
                         * <p>以下模式片段指定包含在此类中的预期内容。
                         * 
                         * <pre>
                         * &lt;complexType&gt;
                         *   &lt;complexContent&gt;
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *       &lt;sequence&gt;
                         *         &lt;element name="AgentIDs" minOccurs="0"&gt;
                         *           &lt;complexType&gt;
                         *             &lt;complexContent&gt;
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                         *                 &lt;sequence&gt;
                         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
                         *                 &lt;/sequence&gt;
                         *               &lt;/restriction&gt;
                         *             &lt;/complexContent&gt;
                         *           &lt;/complexType&gt;
                         *         &lt;/element&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}IssuingAirlineInfo" minOccurs="0"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}TicketDocument" maxOccurs="4"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CarrierFeeInfo" minOccurs="0"/&gt;
                         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OriginalIssueInfo" minOccurs="0"/&gt;
                         *       &lt;/sequence&gt;
                         *     &lt;/restriction&gt;
                         *   &lt;/complexContent&gt;
                         * &lt;/complexType&gt;
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "agentIDs",
                            "issuingAirlineInfo",
                            "ticketDocument",
                            "carrierFeeInfo",
                            "originalIssueInfo"
                        })
                        public static class TicketDocInfo {

                            @XmlElement(name = "AgentIDs")
                            protected OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo.AgentIDs agentIDs;
                            @XmlElement(name = "IssuingAirlineInfo")
                            protected IssuingAirlineInfo issuingAirlineInfo;
                            @XmlElement(name = "TicketDocument", required = true)
                            protected List<TicketDocument> ticketDocument;
                            @XmlElement(name = "CarrierFeeInfo")
                            protected CarrierFeeInfoType carrierFeeInfo;
                            @XmlElement(name = "OriginalIssueInfo")
                            protected OriginalIssueInfo originalIssueInfo;

                            /**
                             * 获取agentIDs属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo.AgentIDs }
                             *     
                             */
                            public OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo.AgentIDs getAgentIDs() {
                                return agentIDs;
                            }

                            /**
                             * 设置agentIDs属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo.AgentIDs }
                             *     
                             */
                            public void setAgentIDs(OrderHistoryRS.Response.Actions.Action.Details.TicketDocInfos.TicketDocInfo.AgentIDs value) {
                                this.agentIDs = value;
                            }

                            /**
                             * 获取issuingAirlineInfo属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link IssuingAirlineInfo }
                             *     
                             */
                            public IssuingAirlineInfo getIssuingAirlineInfo() {
                                return issuingAirlineInfo;
                            }

                            /**
                             * 设置issuingAirlineInfo属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link IssuingAirlineInfo }
                             *     
                             */
                            public void setIssuingAirlineInfo(IssuingAirlineInfo value) {
                                this.issuingAirlineInfo = value;
                            }

                            /**
                             * Gets the value of the ticketDocument property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the ticketDocument property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getTicketDocument().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link TicketDocument }
                             * 
                             * 
                             */
                            public List<TicketDocument> getTicketDocument() {
                                if (ticketDocument == null) {
                                    ticketDocument = new ArrayList<TicketDocument>();
                                }
                                return this.ticketDocument;
                            }

                            /**
                             * 获取carrierFeeInfo属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link CarrierFeeInfoType }
                             *     
                             */
                            public CarrierFeeInfoType getCarrierFeeInfo() {
                                return carrierFeeInfo;
                            }

                            /**
                             * 设置carrierFeeInfo属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link CarrierFeeInfoType }
                             *     
                             */
                            public void setCarrierFeeInfo(CarrierFeeInfoType value) {
                                this.carrierFeeInfo = value;
                            }

                            /**
                             * 获取originalIssueInfo属性的值。
                             * 
                             * @return
                             *     possible object is
                             *     {@link OriginalIssueInfo }
                             *     
                             */
                            public OriginalIssueInfo getOriginalIssueInfo() {
                                return originalIssueInfo;
                            }

                            /**
                             * 设置originalIssueInfo属性的值。
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link OriginalIssueInfo }
                             *     
                             */
                            public void setOriginalIssueInfo(OriginalIssueInfo value) {
                                this.originalIssueInfo = value;
                            }


                            /**
                             * <p>anonymous complex type的 Java 类。
                             * 
                             * <p>以下模式片段指定包含在此类中的预期内容。
                             * 
                             * <pre>
                             * &lt;complexType&gt;
                             *   &lt;complexContent&gt;
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                             *       &lt;sequence&gt;
                             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentID" maxOccurs="2"/&gt;
                             *       &lt;/sequence&gt;
                             *     &lt;/restriction&gt;
                             *   &lt;/complexContent&gt;
                             * &lt;/complexType&gt;
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "agentID"
                            })
                            public static class AgentIDs {

                                @XmlElement(name = "AgentID", required = true)
                                protected List<AgentID> agentID;

                                /**
                                 * Gets the value of the agentID property.
                                 * 
                                 * <p>
                                 * This accessor method returns a reference to the live list,
                                 * not a snapshot. Therefore any modification you make to the
                                 * returned list will be present inside the JAXB object.
                                 * This is why there is not a <CODE>set</CODE> method for the agentID property.
                                 * 
                                 * <p>
                                 * For example, to add a new item, do as follows:
                                 * <pre>
                                 *    getAgentID().add(newItem);
                                 * </pre>
                                 * 
                                 * 
                                 * <p>
                                 * Objects of the following type(s) are allowed in the list
                                 * {@link AgentID }
                                 * 
                                 * 
                                 */
                                public List<AgentID> getAgentID() {
                                    if (agentID == null) {
                                        agentID = new ArrayList<AgentID>();
                                    }
                                    return this.agentID;
                                }

                            }

                        }

                    }

                }


                /**
                 * <p>anonymous complex type的 Java 类。
                 * 
                 * <p>以下模式片段指定包含在此类中的预期内容。
                 * 
                 * <pre>
                 * &lt;complexType&gt;
                 *   &lt;complexContent&gt;
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
                 *       &lt;sequence&gt;
                 *         &lt;element name="EntityID" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType"/&gt;
                 *         &lt;element name="Name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
                 *       &lt;/sequence&gt;
                 *     &lt;/restriction&gt;
                 *   &lt;/complexContent&gt;
                 * &lt;/complexType&gt;
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "entityID",
                    "name"
                })
                public static class Entity {

                    @XmlElement(name = "EntityID", required = true)
                    protected String entityID;
                    @XmlElement(name = "Name")
                    protected String name;

                    /**
                     * 获取entityID属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEntityID() {
                        return entityID;
                    }

                    /**
                     * 设置entityID属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEntityID(String value) {
                        this.entityID = value;
                    }

                    /**
                     * 获取name属性的值。
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getName() {
                        return name;
                    }

                    /**
                     * 设置name属性的值。
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setName(String value) {
                        this.name = value;
                    }

                }

            }

        }

    }

}
