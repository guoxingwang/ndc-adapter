//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * LEAD DATE PERIOD representation.
 * 
 * Notes: Supports Calendar Shopping. Days, Months, etc. before and/ or after the specified date.
 * 
 * <p>LeadDatePeriodType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="LeadDatePeriodType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;attribute name="Before" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *       &lt;attribute name="After" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LeadDatePeriodType")
public class LeadDatePeriodType
    extends AssociatedObjectBaseType
{

    @XmlAttribute(name = "Before")
    protected Duration before;
    @XmlAttribute(name = "After")
    protected Duration after;

    /**
     * 获取before属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getBefore() {
        return before;
    }

    /**
     * 设置before属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setBefore(Duration value) {
        this.before = value;
    }

    /**
     * 获取after属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getAfter() {
        return after;
    }

    /**
     * 设置after属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setAfter(Duration value) {
        this.after = value;
    }

}
