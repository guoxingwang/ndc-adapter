//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * COUPON TICKET DOCUMENT definition.
 * 
 * <p>CouponTicketDocType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="CouponTicketDocType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TicketDocNbr"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="14"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}CodesetType"/&gt;
 *         &lt;element name="InConnectionDocNbr" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[0-9a-zA-Z]{1,14}"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="NumberofBooklets" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="DateOfIssue" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="TimeOfIssue" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType" minOccurs="0"/&gt;
 *         &lt;element name="TicketingLocation" type="{http://www.iata.org/IATA/EDIST}AirportCitySimpleType" minOccurs="0"/&gt;
 *         &lt;element name="RoutingDomIntCode" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType"&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="FeeOwner" type="{http://www.iata.org/IATA/EDIST}AirlineID_Type" minOccurs="0"/&gt;
 *         &lt;element name="AddlBaggageInfo" type="{http://www.iata.org/IATA/EDIST}AddlBaggageInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to71" minOccurs="0"/&gt;
 *         &lt;element name="CouponInfo" type="{http://www.iata.org/IATA/EDIST}CouponInfoType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PenaltyReferences" type="{http://www.w3.org/2001/XMLSchema}IDREFS" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CouponTicketDocType", propOrder = {
    "ticketDocNbr",
    "type",
    "inConnectionDocNbr",
    "numberofBooklets",
    "dateOfIssue",
    "timeOfIssue",
    "ticketingLocation",
    "routingDomIntCode",
    "feeOwner",
    "addlBaggageInfo",
    "remark",
    "couponInfo",
    "penaltyReferences"
})
@XmlSeeAlso({
    TicketDocument.class
})
public class CouponTicketDocType {

    @XmlElement(name = "TicketDocNbr", required = true)
    protected String ticketDocNbr;
    @XmlElement(name = "Type", required = true)
    protected CodesetType type;
    @XmlElement(name = "InConnectionDocNbr")
    protected String inConnectionDocNbr;
    @XmlElement(name = "NumberofBooklets", required = true)
    protected BigInteger numberofBooklets;
    @XmlElement(name = "DateOfIssue", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfIssue;
    @XmlElement(name = "TimeOfIssue")
    protected String timeOfIssue;
    @XmlElement(name = "TicketingLocation")
    protected String ticketingLocation;
    @XmlElement(name = "RoutingDomIntCode")
    protected String routingDomIntCode;
    @XmlElement(name = "FeeOwner")
    protected AirlineIDType feeOwner;
    @XmlElement(name = "AddlBaggageInfo")
    protected AddlBaggageInfoType addlBaggageInfo;
    @XmlElement(name = "Remark")
    protected String remark;
    @XmlElement(name = "CouponInfo")
    protected List<CouponInfoType> couponInfo;
    @XmlList
    @XmlElement(name = "PenaltyReferences")
    @XmlIDREF
    @XmlSchemaType(name = "IDREFS")
    protected List<Object> penaltyReferences;

    /**
     * 获取ticketDocNbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketDocNbr() {
        return ticketDocNbr;
    }

    /**
     * 设置ticketDocNbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketDocNbr(String value) {
        this.ticketDocNbr = value;
    }

    /**
     * 获取type属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getType() {
        return type;
    }

    /**
     * 设置type属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setType(CodesetType value) {
        this.type = value;
    }

    /**
     * 获取inConnectionDocNbr属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInConnectionDocNbr() {
        return inConnectionDocNbr;
    }

    /**
     * 设置inConnectionDocNbr属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInConnectionDocNbr(String value) {
        this.inConnectionDocNbr = value;
    }

    /**
     * 获取numberofBooklets属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberofBooklets() {
        return numberofBooklets;
    }

    /**
     * 设置numberofBooklets属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberofBooklets(BigInteger value) {
        this.numberofBooklets = value;
    }

    /**
     * 获取dateOfIssue属性的值。
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfIssue() {
        return dateOfIssue;
    }

    /**
     * 设置dateOfIssue属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfIssue(XMLGregorianCalendar value) {
        this.dateOfIssue = value;
    }

    /**
     * 获取timeOfIssue属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeOfIssue() {
        return timeOfIssue;
    }

    /**
     * 设置timeOfIssue属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeOfIssue(String value) {
        this.timeOfIssue = value;
    }

    /**
     * 获取ticketingLocation属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketingLocation() {
        return ticketingLocation;
    }

    /**
     * 设置ticketingLocation属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketingLocation(String value) {
        this.ticketingLocation = value;
    }

    /**
     * 获取routingDomIntCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoutingDomIntCode() {
        return routingDomIntCode;
    }

    /**
     * 设置routingDomIntCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoutingDomIntCode(String value) {
        this.routingDomIntCode = value;
    }

    /**
     * 获取feeOwner属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AirlineIDType }
     *     
     */
    public AirlineIDType getFeeOwner() {
        return feeOwner;
    }

    /**
     * 设置feeOwner属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AirlineIDType }
     *     
     */
    public void setFeeOwner(AirlineIDType value) {
        this.feeOwner = value;
    }

    /**
     * 获取addlBaggageInfo属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AddlBaggageInfoType }
     *     
     */
    public AddlBaggageInfoType getAddlBaggageInfo() {
        return addlBaggageInfo;
    }

    /**
     * 设置addlBaggageInfo属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AddlBaggageInfoType }
     *     
     */
    public void setAddlBaggageInfo(AddlBaggageInfoType value) {
        this.addlBaggageInfo = value;
    }

    /**
     * 获取remark属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置remark属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemark(String value) {
        this.remark = value;
    }

    /**
     * Gets the value of the couponInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the couponInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCouponInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CouponInfoType }
     * 
     * 
     */
    public List<CouponInfoType> getCouponInfo() {
        if (couponInfo == null) {
            couponInfo = new ArrayList<CouponInfoType>();
        }
        return this.couponInfo;
    }

    /**
     * Gets the value of the penaltyReferences property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the penaltyReferences property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPenaltyReferences().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getPenaltyReferences() {
        if (penaltyReferences == null) {
            penaltyReferences = new ArrayList<Object>();
        }
        return this.penaltyReferences;
    }

}
