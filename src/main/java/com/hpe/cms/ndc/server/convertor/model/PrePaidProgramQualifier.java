//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PrepaidProgram"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirPassCore"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirPassDetail"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "prepaidProgram"
})
@XmlRootElement(name = "PrePaidProgramQualifier")
public class PrePaidProgramQualifier
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "PrepaidProgram", required = true)
    protected PrePaidProgramQualifier.PrepaidProgram prepaidProgram;

    /**
     * 获取prepaidProgram属性的值。
     * 
     * @return
     *     possible object is
     *     {@link PrePaidProgramQualifier.PrepaidProgram }
     *     
     */
    public PrePaidProgramQualifier.PrepaidProgram getPrepaidProgram() {
        return prepaidProgram;
    }

    /**
     * 设置prepaidProgram属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link PrePaidProgramQualifier.PrepaidProgram }
     *     
     */
    public void setPrepaidProgram(PrePaidProgramQualifier.PrepaidProgram value) {
        this.prepaidProgram = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirPassCore"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirPassDetail"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airPassCore",
        "airPassDetail"
    })
    public static class PrepaidProgram {

        @XmlElement(name = "AirPassCore", required = true)
        protected AirPassCoreType airPassCore;
        @XmlElement(name = "AirPassDetail", required = true)
        protected AirPassDetailType airPassDetail;

        /**
         * 获取airPassCore属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirPassCoreType }
         *     
         */
        public AirPassCoreType getAirPassCore() {
            return airPassCore;
        }

        /**
         * 设置airPassCore属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirPassCoreType }
         *     
         */
        public void setAirPassCore(AirPassCoreType value) {
            this.airPassCore = value;
        }

        /**
         * 获取airPassDetail属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirPassDetailType }
         *     
         */
        public AirPassDetailType getAirPassDetail() {
            return airPassDetail;
        }

        /**
         * 设置airPassDetail属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirPassDetailType }
         *     
         */
        public void setAirPassDetail(AirPassDetailType value) {
            this.airPassDetail = value;
        }

    }

}
