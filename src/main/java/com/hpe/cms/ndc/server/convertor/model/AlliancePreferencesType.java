//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * AIRLINE ALLIANCE PROGRAM Preferences.
 * 
 * <p>AlliancePreferencesType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AlliancePreferencesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Alliance" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="GeoIndicatorType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                   &lt;element name="FlightHaulLength" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlliancePreferencesType", propOrder = {
    "alliance"
})
public class AlliancePreferencesType
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "Alliance", required = true)
    protected List<AlliancePreferencesType.Alliance> alliance;

    /**
     * Gets the value of the alliance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alliance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlliance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlliancePreferencesType.Alliance }
     * 
     * 
     */
    public List<AlliancePreferencesType.Alliance> getAlliance() {
        if (alliance == null) {
            alliance = new ArrayList<AlliancePreferencesType.Alliance>();
        }
        return this.alliance;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CodesetType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="GeoIndicatorType" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *         &lt;element name="FlightHaulLength" type="{http://www.iata.org/IATA/EDIST}CodesetType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}PreferenceAttrGroup"/&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "geoIndicatorType",
        "flightHaulLength"
    })
    public static class Alliance
        extends CodesetType
    {

        @XmlElement(name = "GeoIndicatorType")
        protected CodesetType geoIndicatorType;
        @XmlElement(name = "FlightHaulLength")
        protected CodesetType flightHaulLength;
        @XmlAttribute(name = "PreferencesLevel")
        protected String preferencesLevel;
        @XmlAttribute(name = "PreferencesContext")
        protected String preferencesContext;

        /**
         * 获取geoIndicatorType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CodesetType }
         *     
         */
        public CodesetType getGeoIndicatorType() {
            return geoIndicatorType;
        }

        /**
         * 设置geoIndicatorType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CodesetType }
         *     
         */
        public void setGeoIndicatorType(CodesetType value) {
            this.geoIndicatorType = value;
        }

        /**
         * 获取flightHaulLength属性的值。
         * 
         * @return
         *     possible object is
         *     {@link CodesetType }
         *     
         */
        public CodesetType getFlightHaulLength() {
            return flightHaulLength;
        }

        /**
         * 设置flightHaulLength属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link CodesetType }
         *     
         */
        public void setFlightHaulLength(CodesetType value) {
            this.flightHaulLength = value;
        }

        /**
         * 获取preferencesLevel属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreferencesLevel() {
            return preferencesLevel;
        }

        /**
         * 设置preferencesLevel属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreferencesLevel(String value) {
            this.preferencesLevel = value;
        }

        /**
         * 获取preferencesContext属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPreferencesContext() {
            return preferencesContext;
        }

        /**
         * 设置preferencesContext属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPreferencesContext(String value) {
            this.preferencesContext = value;
        }

    }

}
