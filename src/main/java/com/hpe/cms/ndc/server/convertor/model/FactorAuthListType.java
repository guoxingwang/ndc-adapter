//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>FactorAuthListType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="FactorAuthListType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="OneTimeCode"/&gt;
 *     &lt;enumeration value="PinPhrase"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FactorAuthListType")
@XmlEnum
public enum FactorAuthListType {


    /**
     * 
     *             OATH-compliant One Time Code generation for sending a one-time code to a Traveler's mobile phone as a Text/SMS/Email.
     *           
     * 
     */
    @XmlEnumValue("OneTimeCode")
    ONE_TIME_CODE("OneTimeCode"),

    /**
     * 
     *             Random Character Pin Phrase Authentication prompts a user to enter the random characters from their specified password.
     *           
     * 
     */
    @XmlEnumValue("PinPhrase")
    PIN_PHRASE("PinPhrase");
    private final String value;

    FactorAuthListType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FactorAuthListType fromValue(String v) {
        for (FactorAuthListType c: FactorAuthListType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
