//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>TimeLimitTypeListType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="TimeLimitTypeListType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Bilateral"/&gt;
 *     &lt;enumeration value="Deposit"/&gt;
 *     &lt;enumeration value="InventoryGuarantee"/&gt;
 *     &lt;enumeration value="Naming"/&gt;
 *     &lt;enumeration value="Offer"/&gt;
 *     &lt;enumeration value="Payment"/&gt;
 *     &lt;enumeration value="PriceGuarantee"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TimeLimitTypeListType")
@XmlEnum
public enum TimeLimitTypeListType {

    @XmlEnumValue("Bilateral")
    BILATERAL("Bilateral"),
    @XmlEnumValue("Deposit")
    DEPOSIT("Deposit"),
    @XmlEnumValue("InventoryGuarantee")
    INVENTORY_GUARANTEE("InventoryGuarantee"),
    @XmlEnumValue("Naming")
    NAMING("Naming"),
    @XmlEnumValue("Offer")
    OFFER("Offer"),
    @XmlEnumValue("Payment")
    PAYMENT("Payment"),
    @XmlEnumValue("PriceGuarantee")
    PRICE_GUARANTEE("PriceGuarantee");
    private final String value;

    TimeLimitTypeListType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TimeLimitTypeListType fromValue(String v) {
        for (TimeLimitTypeListType c: TimeLimitTypeListType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
