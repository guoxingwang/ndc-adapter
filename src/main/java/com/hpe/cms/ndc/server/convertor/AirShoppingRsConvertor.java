package com.hpe.cms.ndc.server.convertor;

import lombok.extern.slf4j.Slf4j;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:07 2019/7/27
 * @ Description：
 * @ Modified By：
 */
@Slf4j
public class AirShoppingRsConvertor extends AbstractConvertor {

    public AirShoppingRsConvertor(String sourceVersion, String targetVersion) {
        super(sourceVersion, targetVersion);
    }

    public com.hpe.cms.ndc.server.v15_2.model.AirShoppingRS converte_v18_2_v15_2(Object source) {
        source = (com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRS) source;
        log.info("source object is {}", source);
        //todo 具体转换规则
        return new com.hpe.cms.ndc.server.v15_2.model.AirShoppingRS();
    }

    public com.hpe.cms.ndc.server.v16_2.model.AirShoppingRS converte_v18_2_v16_2(Object source) {
        source = (com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRS) source;
        log.info("source object is {}", source);
        //todo 具体转换规则
        return new com.hpe.cms.ndc.server.v16_2.model.AirShoppingRS();
    }
}
