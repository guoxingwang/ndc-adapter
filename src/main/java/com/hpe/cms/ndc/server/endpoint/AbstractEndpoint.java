package com.hpe.cms.ndc.server.endpoint;

import com.hpe.cms.ndc.client.common.WsClientTemplate;
import com.hpe.cms.ndc.common.InterfaceInfo;
import com.hpe.cms.ndc.common.util.JaxbUtil;
import com.hpe.cms.ndc.server.security.Security;
import com.hpe.cms.ndc.server.util.SecurityUtil;
import com.hpe.cms.ndc.server.v16_2.model.OrderCancelRQ;
import com.hpe.cms.ndc.server.v16_2.model.OrderCancelRS;
import lombok.Data;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.soap.SoapHeaderElement;

import java.lang.reflect.ParameterizedType;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:36 2019/7/5
 * @ Description：
 * @ Modified By：
 */
@Slf4j
@Data
public abstract class AbstractEndpoint<RQ, RS> {

    @Setter
    public InterfaceInfo interfaceInfo;
    private WsClientTemplate<RQ, RS> wsClientTemplate;

    public AbstractEndpoint(WsClientTemplate<RQ, RS> wsClientTemplate) {
        this.wsClientTemplate = wsClientTemplate;
    }

    public RS sendAndReceive(RQ request, SoapHeaderElement headerElement) {
        //初始化request以及header
        Security security = SecurityUtil.unmarshallSecurityFromSoapHeader(headerElement);
        this.configInterfaceInfo(request, security);
        /**
         * 1、获取目标版本
         * 2、转换目标版本request
         * 3、请求目标版本对应接口
         * 4、将response转换为来源版本的response
         */

        //获取目标请求报文以及目标接口的版本
        String interfaceName = interfaceInfo.getSourceInterfaceName();
        String version = interfaceInfo.getTargetVersion();

        Object targetRq = interfaceInfo.converteRq();

        //请求目标接口
        wsClientTemplate.configClientTemplate(interfaceName, version);
        Object targetRs = wsClientTemplate.sendAndReceive(targetRq);

        //设置目标版本的返回对象
        interfaceInfo.setTargetRs(targetRs);
        return (RS) interfaceInfo.converteRs();

    }

    @Deprecated
    public Object sendAndReceive(Object rq, String targetVersion) {

        log.info("-------------------------------i am log ---------------------------");

        if (rq instanceof OrderCancelRQ) {
            return (RS) new OrderCancelRS();
        }

        String filePath = Thread.currentThread().getContextClassLoader().getResource("use-case/AirShoppingRS.xml").getPath();
        RS tmp = (RS) JaxbUtil.converyToJavaBeanFromFile(filePath, (Class<RS>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
        if (tmp != null) {
            return tmp;
        }

        RS rs;
        switch (targetVersion) {
            case "v15_2":
                rs = wsClientTemplate.sendAndReceiveV15_2(rq);
                break;
            case "v16_1":
                rs = wsClientTemplate.sendAndReceiveV16_1(rq);
                break;
            case "v16_2":
                rs = wsClientTemplate.sendAndReceiveV16_2(rq);
                break;
            case "v17_1":
                rs = wsClientTemplate.sendAndReceiveV17_1(rq);
                break;
            case "v17_2":
                rs = wsClientTemplate.sendAndReceiveV17_2(rq);
                break;
            case "v18_1":
                rs = wsClientTemplate.sendAndReceiveV18_1(rq);
                break;
            case "v18_2":
                rs = wsClientTemplate.sendAndReceiveV18_2(rq);
                break;
            default:
                log.info("version is not config");
        }
        return null;
    }

    /**
     * 子类定义个性化参数
     *
     * @return
     */
    abstract void configInterfaceInfo(RQ request, Security security);

}
