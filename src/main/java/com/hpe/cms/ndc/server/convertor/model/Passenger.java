//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}PassengerDetailType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdditionalRoles" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="PrimaryContactInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="PaymentContactInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "additionalRoles"
})
@XmlRootElement(name = "Passenger")
public class Passenger
    extends PassengerDetailType
{

    @XmlElement(name = "AdditionalRoles")
    protected Passenger.AdditionalRoles additionalRoles;

    /**
     * 获取additionalRoles属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Passenger.AdditionalRoles }
     *     
     */
    public Passenger.AdditionalRoles getAdditionalRoles() {
        return additionalRoles;
    }

    /**
     * 设置additionalRoles属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Passenger.AdditionalRoles }
     *     
     */
    public void setAdditionalRoles(Passenger.AdditionalRoles value) {
        this.additionalRoles = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="PrimaryContactInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="PaymentContactInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AdditionalRoles {

        @XmlAttribute(name = "PrimaryContactInd")
        protected Boolean primaryContactInd;
        @XmlAttribute(name = "PaymentContactInd")
        protected Boolean paymentContactInd;

        /**
         * 获取primaryContactInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPrimaryContactInd() {
            return primaryContactInd;
        }

        /**
         * 设置primaryContactInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPrimaryContactInd(Boolean value) {
            this.primaryContactInd = value;
        }

        /**
         * 获取paymentContactInd属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isPaymentContactInd() {
            return paymentContactInd;
        }

        /**
         * 设置paymentContactInd属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPaymentContactInd(Boolean value) {
            this.paymentContactInd = value;
        }

    }

}
