//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>AirportCodeRoleSimpleType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="AirportCodeRoleSimpleType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Arrival_Destination"/&gt;
 *     &lt;enumeration value="Connection"/&gt;
 *     &lt;enumeration value="Departure_Origin"/&gt;
 *     &lt;enumeration value="Stop"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AirportCodeRoleSimpleType")
@XmlEnum
public enum AirportCodeRoleSimpleType {

    @XmlEnumValue("Arrival_Destination")
    ARRIVAL_DESTINATION("Arrival_Destination"),
    @XmlEnumValue("Connection")
    CONNECTION("Connection"),
    @XmlEnumValue("Departure_Origin")
    DEPARTURE_ORIGIN("Departure_Origin"),
    @XmlEnumValue("Stop")
    STOP("Stop");
    private final String value;

    AirportCodeRoleSimpleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AirportCodeRoleSimpleType fromValue(String v) {
        for (AirportCodeRoleSimpleType c: AirportCodeRoleSimpleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
