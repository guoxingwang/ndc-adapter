//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * TAX DETAIL representation.
 * 
 * <p>TaxDetailType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="TaxDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}TaxCoreType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Total" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CurrencyAmountOptType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Breakdown" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Tax" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Qualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType"/&gt;
 *                             &lt;element name="Nation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="TaxType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="CollectionPoint" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrCode" minOccurs="0"/&gt;
 *                                       &lt;element name="AirportAmount" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="LocalAmount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
 *                             &lt;element name="Description" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" minOccurs="0"/&gt;
 *                             &lt;element name="Conversion" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmount" maxOccurs="2" minOccurs="2"/&gt;
 *                                       &lt;element name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="FiledAmount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
 *                             &lt;element name="FileTaxType" minOccurs="0"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="AddlTaxType" minOccurs="0"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="AddlFiledTaxType" minOccurs="0"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                           &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *                           &lt;attribute name="RefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="ApproxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="CollectionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                           &lt;attribute name="RefundInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxDetailType", propOrder = {
    "total",
    "breakdown"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.CarrierFeeInfoType.CarrierFees.Fee.Taxes.class,
    com.hpe.cms.ndc.server.convertor.model.CarrierFeeInfoType.Taxes.class,
    com.hpe.cms.ndc.server.convertor.model.AirDocDisplayRS.Response.TicketDocInfos.TicketDocInfo.FareInfo.Taxes.class,
    com.hpe.cms.ndc.server.convertor.model.AirDocRefundRS.Response.TicketDocInfos.TicketDocInfo.FareInfo.Taxes.class,
    com.hpe.cms.ndc.server.convertor.model.OrderCancelRS.Response.TicketDocInfos.TicketDocInfo.FareInfo.Taxes.class,
    com.hpe.cms.ndc.server.convertor.model.DetailCurrencyPriceType.Taxes.class,
    com.hpe.cms.ndc.server.convertor.model.TaxCouponInfoType.TicketDocument.CouponNumber.Tax.class
})
public class TaxDetailType
    extends TaxCoreType
{

    @XmlElement(name = "Total")
    protected TaxDetailType.Total total;
    @XmlElement(name = "Breakdown")
    protected TaxDetailType.Breakdown breakdown;

    /**
     * 获取total属性的值。
     * 
     * @return
     *     possible object is
     *     {@link TaxDetailType.Total }
     *     
     */
    public TaxDetailType.Total getTotal() {
        return total;
    }

    /**
     * 设置total属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link TaxDetailType.Total }
     *     
     */
    public void setTotal(TaxDetailType.Total value) {
        this.total = value;
    }

    /**
     * 获取breakdown属性的值。
     * 
     * @return
     *     possible object is
     *     {@link TaxDetailType.Breakdown }
     *     
     */
    public TaxDetailType.Breakdown getBreakdown() {
        return breakdown;
    }

    /**
     * 设置breakdown属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link TaxDetailType.Breakdown }
     *     
     */
    public void setBreakdown(TaxDetailType.Breakdown value) {
        this.breakdown = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Tax" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Qualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType"/&gt;
     *                   &lt;element name="Nation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="TaxType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="CollectionPoint" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrCode" minOccurs="0"/&gt;
     *                             &lt;element name="AirportAmount" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="LocalAmount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
     *                   &lt;element name="Description" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" minOccurs="0"/&gt;
     *                   &lt;element name="Conversion" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmount" maxOccurs="2" minOccurs="2"/&gt;
     *                             &lt;element name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="FiledAmount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
     *                   &lt;element name="FileTaxType" minOccurs="0"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="AddlTaxType" minOccurs="0"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="AddlFiledTaxType" minOccurs="0"&gt;
     *                     &lt;simpleType&gt;
     *                       &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
     *                       &lt;/restriction&gt;
     *                     &lt;/simpleType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *                 &lt;attribute name="RefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="ApproxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="CollectionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                 &lt;attribute name="RefundInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tax"
    })
    public static class Breakdown {

        @XmlElement(name = "Tax", required = true)
        protected List<TaxDetailType.Breakdown.Tax> tax;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * Gets the value of the tax property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tax property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTax().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TaxDetailType.Breakdown.Tax }
         * 
         * 
         */
        public List<TaxDetailType.Breakdown.Tax> getTax() {
            if (tax == null) {
                tax = new ArrayList<TaxDetailType.Breakdown.Tax>();
            }
            return this.tax;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Qualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="Amount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType"/&gt;
         *         &lt;element name="Nation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="TaxType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="CollectionPoint" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrCode" minOccurs="0"/&gt;
         *                   &lt;element name="AirportAmount" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="LocalAmount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
         *         &lt;element name="Description" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" minOccurs="0"/&gt;
         *         &lt;element name="Conversion" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmount" maxOccurs="2" minOccurs="2"/&gt;
         *                   &lt;element name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="FiledAmount" type="{http://www.iata.org/IATA/EDIST}CurrencyAmountOptType" minOccurs="0"/&gt;
         *         &lt;element name="FileTaxType" minOccurs="0"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="AddlTaxType" minOccurs="0"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="AddlFiledTaxType" minOccurs="0"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.iata.org/IATA/EDIST}AlphaNumericStringLength1to3"&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
         *       &lt;attribute name="RefundableInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="ApproxInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="CollectionInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *       &lt;attribute name="RefundInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "qualifier",
            "amount",
            "nation",
            "taxCode",
            "taxType",
            "collectionPoint",
            "localAmount",
            "description",
            "conversion",
            "filedAmount",
            "fileTaxType",
            "addlTaxType",
            "addlFiledTaxType"
        })
        public static class Tax {

            @XmlElement(name = "Qualifier")
            protected String qualifier;
            @XmlElement(name = "Amount", required = true)
            protected CurrencyAmountOptType amount;
            @XmlElement(name = "Nation")
            protected String nation;
            @XmlElement(name = "TaxCode")
            protected String taxCode;
            @XmlElement(name = "TaxType")
            protected String taxType;
            @XmlElement(name = "CollectionPoint")
            protected List<TaxDetailType.Breakdown.Tax.CollectionPoint> collectionPoint;
            @XmlElement(name = "LocalAmount")
            protected CurrencyAmountOptType localAmount;
            @XmlElement(name = "Description")
            protected String description;
            @XmlElement(name = "Conversion")
            protected TaxDetailType.Breakdown.Tax.Conversion conversion;
            @XmlElement(name = "FiledAmount")
            protected CurrencyAmountOptType filedAmount;
            @XmlElement(name = "FileTaxType")
            protected String fileTaxType;
            @XmlElement(name = "AddlTaxType")
            protected String addlTaxType;
            @XmlElement(name = "AddlFiledTaxType")
            protected String addlFiledTaxType;
            @XmlAttribute(name = "RefundableInd")
            protected Boolean refundableInd;
            @XmlAttribute(name = "ApproxInd")
            protected Boolean approxInd;
            @XmlAttribute(name = "CollectionInd")
            protected Boolean collectionInd;
            @XmlAttribute(name = "RefundInd")
            protected Boolean refundInd;
            @XmlAttribute(name = "refs")
            @XmlIDREF
            protected List<Object> refs;

            /**
             * 获取qualifier属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQualifier() {
                return qualifier;
            }

            /**
             * 设置qualifier属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQualifier(String value) {
                this.qualifier = value;
            }

            /**
             * 获取amount属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public CurrencyAmountOptType getAmount() {
                return amount;
            }

            /**
             * 设置amount属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public void setAmount(CurrencyAmountOptType value) {
                this.amount = value;
            }

            /**
             * 获取nation属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNation() {
                return nation;
            }

            /**
             * 设置nation属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNation(String value) {
                this.nation = value;
            }

            /**
             * 获取taxCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxCode() {
                return taxCode;
            }

            /**
             * 设置taxCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxCode(String value) {
                this.taxCode = value;
            }

            /**
             * 获取taxType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxType() {
                return taxType;
            }

            /**
             * 设置taxType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxType(String value) {
                this.taxType = value;
            }

            /**
             * Gets the value of the collectionPoint property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the collectionPoint property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCollectionPoint().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link TaxDetailType.Breakdown.Tax.CollectionPoint }
             * 
             * 
             */
            public List<TaxDetailType.Breakdown.Tax.CollectionPoint> getCollectionPoint() {
                if (collectionPoint == null) {
                    collectionPoint = new ArrayList<TaxDetailType.Breakdown.Tax.CollectionPoint>();
                }
                return this.collectionPoint;
            }

            /**
             * 获取localAmount属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public CurrencyAmountOptType getLocalAmount() {
                return localAmount;
            }

            /**
             * 设置localAmount属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public void setLocalAmount(CurrencyAmountOptType value) {
                this.localAmount = value;
            }

            /**
             * 获取description属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * 设置description属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * 获取conversion属性的值。
             * 
             * @return
             *     possible object is
             *     {@link TaxDetailType.Breakdown.Tax.Conversion }
             *     
             */
            public TaxDetailType.Breakdown.Tax.Conversion getConversion() {
                return conversion;
            }

            /**
             * 设置conversion属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link TaxDetailType.Breakdown.Tax.Conversion }
             *     
             */
            public void setConversion(TaxDetailType.Breakdown.Tax.Conversion value) {
                this.conversion = value;
            }

            /**
             * 获取filedAmount属性的值。
             * 
             * @return
             *     possible object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public CurrencyAmountOptType getFiledAmount() {
                return filedAmount;
            }

            /**
             * 设置filedAmount属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link CurrencyAmountOptType }
             *     
             */
            public void setFiledAmount(CurrencyAmountOptType value) {
                this.filedAmount = value;
            }

            /**
             * 获取fileTaxType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFileTaxType() {
                return fileTaxType;
            }

            /**
             * 设置fileTaxType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFileTaxType(String value) {
                this.fileTaxType = value;
            }

            /**
             * 获取addlTaxType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddlTaxType() {
                return addlTaxType;
            }

            /**
             * 设置addlTaxType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddlTaxType(String value) {
                this.addlTaxType = value;
            }

            /**
             * 获取addlFiledTaxType属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddlFiledTaxType() {
                return addlFiledTaxType;
            }

            /**
             * 设置addlFiledTaxType属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddlFiledTaxType(String value) {
                this.addlFiledTaxType = value;
            }

            /**
             * 获取refundableInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRefundableInd() {
                return refundableInd;
            }

            /**
             * 设置refundableInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRefundableInd(Boolean value) {
                this.refundableInd = value;
            }

            /**
             * 获取approxInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isApproxInd() {
                return approxInd;
            }

            /**
             * 设置approxInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setApproxInd(Boolean value) {
                this.approxInd = value;
            }

            /**
             * 获取collectionInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isCollectionInd() {
                return collectionInd;
            }

            /**
             * 设置collectionInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setCollectionInd(Boolean value) {
                this.collectionInd = value;
            }

            /**
             * 获取refundInd属性的值。
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isRefundInd() {
                return refundInd;
            }

            /**
             * 设置refundInd属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setRefundInd(Boolean value) {
                this.refundInd = value;
            }

            /**
             * Gets the value of the refs property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the refs property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRefs().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Object }
             * 
             * 
             */
            public List<Object> getRefs() {
                if (refs == null) {
                    refs = new ArrayList<Object>();
                }
                return this.refs;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrCode" minOccurs="0"/&gt;
             *         &lt;element name="AirportAmount" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "currCode",
                "airportAmount",
                "airportCode"
            })
            public static class CollectionPoint {

                @XmlElement(name = "CurrCode")
                protected CurrCode currCode;
                @XmlElement(name = "AirportAmount", required = true)
                protected Object airportAmount;
                @XmlElement(name = "AirportCode", required = true)
                protected AirportCode airportCode;

                /**
                 * 获取currCode属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link CurrCode }
                 *     
                 */
                public CurrCode getCurrCode() {
                    return currCode;
                }

                /**
                 * 设置currCode属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CurrCode }
                 *     
                 */
                public void setCurrCode(CurrCode value) {
                    this.currCode = value;
                }

                /**
                 * 获取airportAmount属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Object }
                 *     
                 */
                public Object getAirportAmount() {
                    return airportAmount;
                }

                /**
                 * 设置airportAmount属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Object }
                 *     
                 */
                public void setAirportAmount(Object value) {
                    this.airportAmount = value;
                }

                /**
                 * 获取airportCode属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link AirportCode }
                 *     
                 */
                public AirportCode getAirportCode() {
                    return airportCode;
                }

                /**
                 * 设置airportCode属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AirportCode }
                 *     
                 */
                public void setAirportCode(AirportCode value) {
                    this.airportCode = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element ref="{http://www.iata.org/IATA/EDIST}CurrencyAmount" maxOccurs="2" minOccurs="2"/&gt;
             *         &lt;element name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "currencyAmount",
                "conversionRate"
            })
            public static class Conversion {

                @XmlElement(name = "CurrencyAmount", required = true)
                protected List<CurrencyAmountOptType> currencyAmount;
                @XmlElement(name = "ConversionRate", required = true)
                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar conversionRate;

                /**
                 * Gets the value of the currencyAmount property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the currencyAmount property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getCurrencyAmount().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link CurrencyAmountOptType }
                 * 
                 * 
                 */
                public List<CurrencyAmountOptType> getCurrencyAmount() {
                    if (currencyAmount == null) {
                        currencyAmount = new ArrayList<CurrencyAmountOptType>();
                    }
                    return this.currencyAmount;
                }

                /**
                 * 获取conversionRate属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getConversionRate() {
                    return conversionRate;
                }

                /**
                 * 设置conversionRate属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setConversionRate(XMLGregorianCalendar value) {
                    this.conversionRate = value;
                }

            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CurrencyAmountOptType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Total
        extends CurrencyAmountOptType
    {


    }

}
