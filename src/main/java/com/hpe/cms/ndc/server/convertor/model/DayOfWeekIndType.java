//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type definition for Day of Week Indicators.
 * 
 * <p>DayOfWeekIndType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="DayOfWeekIndType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="MondayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="TuesdayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="WednesdayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ThursdayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FridayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="SaturdayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="SundayInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DayOfWeekIndType")
public class DayOfWeekIndType {

    @XmlAttribute(name = "MondayInd")
    protected Boolean mondayInd;
    @XmlAttribute(name = "TuesdayInd")
    protected Boolean tuesdayInd;
    @XmlAttribute(name = "WednesdayInd")
    protected Boolean wednesdayInd;
    @XmlAttribute(name = "ThursdayInd")
    protected Boolean thursdayInd;
    @XmlAttribute(name = "FridayInd")
    protected Boolean fridayInd;
    @XmlAttribute(name = "SaturdayInd")
    protected Boolean saturdayInd;
    @XmlAttribute(name = "SundayInd")
    protected Boolean sundayInd;

    /**
     * 获取mondayInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMondayInd() {
        return mondayInd;
    }

    /**
     * 设置mondayInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMondayInd(Boolean value) {
        this.mondayInd = value;
    }

    /**
     * 获取tuesdayInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTuesdayInd() {
        return tuesdayInd;
    }

    /**
     * 设置tuesdayInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTuesdayInd(Boolean value) {
        this.tuesdayInd = value;
    }

    /**
     * 获取wednesdayInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWednesdayInd() {
        return wednesdayInd;
    }

    /**
     * 设置wednesdayInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWednesdayInd(Boolean value) {
        this.wednesdayInd = value;
    }

    /**
     * 获取thursdayInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isThursdayInd() {
        return thursdayInd;
    }

    /**
     * 设置thursdayInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setThursdayInd(Boolean value) {
        this.thursdayInd = value;
    }

    /**
     * 获取fridayInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFridayInd() {
        return fridayInd;
    }

    /**
     * 设置fridayInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFridayInd(Boolean value) {
        this.fridayInd = value;
    }

    /**
     * 获取saturdayInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSaturdayInd() {
        return saturdayInd;
    }

    /**
     * 设置saturdayInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSaturdayInd(Boolean value) {
        this.saturdayInd = value;
    }

    /**
     * 获取sundayInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSundayInd() {
        return sundayInd;
    }

    /**
     * 设置sundayInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSundayInd(Boolean value) {
        this.sundayInd = value;
    }

}
