//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * FLIGHT STOP LOCATION definition.
 * 
 * <p>StopLocationType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="StopLocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StopLocation" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
 *                   &lt;element name="Name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
 *                   &lt;element name="ArrivalDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                   &lt;element name="ArrivalTime" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType"/&gt;
 *                   &lt;element name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *                   &lt;element name="DepartureTime" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}Equipment" minOccurs="0"/&gt;
 *                   &lt;element name="GroundTime" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ChangeOfGaugeInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StopLocationType", propOrder = {
    "stopLocation"
})
public class StopLocationType {

    @XmlElement(name = "StopLocation", required = true)
    protected List<StopLocationType.StopLocation> stopLocation;
    @XmlAttribute(name = "ChangeOfGaugeInd")
    protected Boolean changeOfGaugeInd;

    /**
     * Gets the value of the stopLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stopLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopLocationType.StopLocation }
     * 
     * 
     */
    public List<StopLocationType.StopLocation> getStopLocation() {
        if (stopLocation == null) {
            stopLocation = new ArrayList<StopLocationType.StopLocation>();
        }
        return this.stopLocation;
    }

    /**
     * 获取changeOfGaugeInd属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChangeOfGaugeInd() {
        return changeOfGaugeInd;
    }

    /**
     * 设置changeOfGaugeInd属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeOfGaugeInd(Boolean value) {
        this.changeOfGaugeInd = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportCode"/&gt;
     *         &lt;element name="Name" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" minOccurs="0"/&gt;
     *         &lt;element name="ArrivalDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *         &lt;element name="ArrivalTime" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType"/&gt;
     *         &lt;element name="DepartureDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
     *         &lt;element name="DepartureTime" type="{http://www.iata.org/IATA/EDIST}TimeSimpleType"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Equipment" minOccurs="0"/&gt;
     *         &lt;element name="GroundTime" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airportCode",
        "name",
        "arrivalDate",
        "arrivalTime",
        "departureDate",
        "departureTime",
        "equipment",
        "groundTime"
    })
    public static class StopLocation
        extends AssociatedObjectBaseType
    {

        @XmlElement(name = "AirportCode", required = true)
        protected AirportCode airportCode;
        @XmlElement(name = "Name")
        protected String name;
        @XmlElement(name = "ArrivalDate", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar arrivalDate;
        @XmlElement(name = "ArrivalTime", required = true)
        protected String arrivalTime;
        @XmlElement(name = "DepartureDate", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar departureDate;
        @XmlElement(name = "DepartureTime", required = true)
        protected String departureTime;
        @XmlElement(name = "Equipment")
        protected AircraftSummaryType equipment;
        @XmlElement(name = "GroundTime")
        protected Duration groundTime;

        /**
         * 获取airportCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AirportCode }
         *     
         */
        public AirportCode getAirportCode() {
            return airportCode;
        }

        /**
         * 设置airportCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AirportCode }
         *     
         */
        public void setAirportCode(AirportCode value) {
            this.airportCode = value;
        }

        /**
         * 获取name属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * 设置name属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * 获取arrivalDate属性的值。
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getArrivalDate() {
            return arrivalDate;
        }

        /**
         * 设置arrivalDate属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setArrivalDate(XMLGregorianCalendar value) {
            this.arrivalDate = value;
        }

        /**
         * 获取arrivalTime属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArrivalTime() {
            return arrivalTime;
        }

        /**
         * 设置arrivalTime属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArrivalTime(String value) {
            this.arrivalTime = value;
        }

        /**
         * 获取departureDate属性的值。
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDepartureDate() {
            return departureDate;
        }

        /**
         * 设置departureDate属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDepartureDate(XMLGregorianCalendar value) {
            this.departureDate = value;
        }

        /**
         * 获取departureTime属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDepartureTime() {
            return departureTime;
        }

        /**
         * 设置departureTime属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDepartureTime(String value) {
            this.departureTime = value;
        }

        /**
         * 获取equipment属性的值。
         * 
         * @return
         *     possible object is
         *     {@link AircraftSummaryType }
         *     
         */
        public AircraftSummaryType getEquipment() {
            return equipment;
        }

        /**
         * 设置equipment属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link AircraftSummaryType }
         *     
         */
        public void setEquipment(AircraftSummaryType value) {
            this.equipment = value;
        }

        /**
         * 获取groundTime属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getGroundTime() {
            return groundTime;
        }

        /**
         * 设置groundTime属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setGroundTime(Duration value) {
            this.groundTime = value;
        }

    }

}
