//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Detail Frequent Traveler Program.
 * 
 * <p>FQTV_ProgramDetailType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="FQTV_ProgramDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}FQTV_ProgramSummaryType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Alliances" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Alliance" type="{http://www.iata.org/IATA/EDIST}FQTV_AllianceType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FQTV_ProgramDetailType", propOrder = {
    "alliances"
})
public class FQTVProgramDetailType
    extends FQTVProgramSummaryType
{

    @XmlElement(name = "Alliances")
    protected FQTVProgramDetailType.Alliances alliances;

    /**
     * 获取alliances属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FQTVProgramDetailType.Alliances }
     *     
     */
    public FQTVProgramDetailType.Alliances getAlliances() {
        return alliances;
    }

    /**
     * 设置alliances属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FQTVProgramDetailType.Alliances }
     *     
     */
    public void setAlliances(FQTVProgramDetailType.Alliances value) {
        this.alliances = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Alliance" type="{http://www.iata.org/IATA/EDIST}FQTV_AllianceType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alliance"
    })
    public static class Alliances {

        @XmlElement(name = "Alliance", required = true)
        protected List<FQTVAllianceType> alliance;

        /**
         * Gets the value of the alliance property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alliance property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlliance().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FQTVAllianceType }
         * 
         * 
         */
        public List<FQTVAllianceType> getAlliance() {
            if (alliance == null) {
                alliance = new ArrayList<FQTVAllianceType>();
            }
            return this.alliance;
        }

    }

}
