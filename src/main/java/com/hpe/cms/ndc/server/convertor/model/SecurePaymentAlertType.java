//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * SECURE PAYMENT definiton.
 * 
 * 
 * <p>SecurePaymentAlertType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="SecurePaymentAlertType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}TrxProcessObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PayerAuth" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element name="PAReq" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PARes" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TransactionType" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;SecureTransactionSimpleType"&gt;
 *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="EnrollmentStatus" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;EnrollStatusSimpleType"&gt;
 *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Airline" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ID" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;UniqueStringID_SimpleType"&gt;
 *                           &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Name" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                           &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="CountryCode" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CountrySimpleType"&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Reference" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ACS_TxnReference" minOccurs="0"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}SPM_TxnReference" minOccurs="0"/&gt;
 *                   &lt;element name="OriginalTransactionID" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
 *                           &lt;attribute name="source" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
 *                   &lt;element name="TxnDescription" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType" minOccurs="0"/&gt;
 *                   &lt;element name="TxnDatas" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="TxnDate" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" maxOccurs="3"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="URLs" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.iata.org/IATA/EDIST}SecurePaymentUrlType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Details" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
 *                   &lt;element name="ClientType" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;simpleContent&gt;
 *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;SecureTrxClientSimpleType"&gt;
 *                           &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/simpleContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="CustomerDevice" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}DeviceType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="BowserAcceptHeader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="BrowserUserAgentHeader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="DeviceCategoryCode" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Currency" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="InputCurrCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" minOccurs="0"/&gt;
 *                             &lt;element name="SettlementCurrCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}ReservationInfo" minOccurs="0"/&gt;
 *                   &lt;element name="TxnDatas" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="TxnDate" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" maxOccurs="3"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ProcessingInfos" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ProcessingInfo" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="AddrVerification" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;attribute name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                                     &lt;attribute name="Text" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" /&gt;
 *                                     &lt;attribute name="InvalidInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                     &lt;attribute name="NoMatchInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="CAVV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="CustomerAuthStatus" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;simpleContent&gt;
 *                                   &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CustomerAuthStatusSimpleType"&gt;
 *                                     &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
 *                                   &lt;/extension&gt;
 *                                 &lt;/simpleContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="ECI" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecurePaymentAlertType", propOrder = {
    "payerAuth",
    "transactionType",
    "enrollmentStatus",
    "airline",
    "reference",
    "urLs",
    "details",
    "processingInfos"
})
@XmlSeeAlso({
    com.hpe.cms.ndc.server.convertor.model.CustomerInputRQ.Query.SecurePayments.SecurePayment.class,
    com.hpe.cms.ndc.server.convertor.model.CustomerInputRS.Response.SecurePayments.SecurePayment.class
})
public class SecurePaymentAlertType
    extends TrxProcessObjectBaseType
{

    @XmlElement(name = "PayerAuth")
    protected SecurePaymentAlertType.PayerAuth payerAuth;
    @XmlElement(name = "TransactionType")
    protected SecurePaymentAlertType.TransactionType transactionType;
    @XmlElement(name = "EnrollmentStatus")
    protected SecurePaymentAlertType.EnrollmentStatus enrollmentStatus;
    @XmlElement(name = "Airline")
    protected SecurePaymentAlertType.Airline airline;
    @XmlElement(name = "Reference")
    protected SecurePaymentAlertType.Reference reference;
    @XmlElement(name = "URLs")
    protected SecurePaymentAlertType.URLs urLs;
    @XmlElement(name = "Details")
    protected SecurePaymentAlertType.Details details;
    @XmlElement(name = "ProcessingInfos")
    protected SecurePaymentAlertType.ProcessingInfos processingInfos;

    /**
     * 获取payerAuth属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType.PayerAuth }
     *     
     */
    public SecurePaymentAlertType.PayerAuth getPayerAuth() {
        return payerAuth;
    }

    /**
     * 设置payerAuth属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType.PayerAuth }
     *     
     */
    public void setPayerAuth(SecurePaymentAlertType.PayerAuth value) {
        this.payerAuth = value;
    }

    /**
     * 获取transactionType属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType.TransactionType }
     *     
     */
    public SecurePaymentAlertType.TransactionType getTransactionType() {
        return transactionType;
    }

    /**
     * 设置transactionType属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType.TransactionType }
     *     
     */
    public void setTransactionType(SecurePaymentAlertType.TransactionType value) {
        this.transactionType = value;
    }

    /**
     * 获取enrollmentStatus属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType.EnrollmentStatus }
     *     
     */
    public SecurePaymentAlertType.EnrollmentStatus getEnrollmentStatus() {
        return enrollmentStatus;
    }

    /**
     * 设置enrollmentStatus属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType.EnrollmentStatus }
     *     
     */
    public void setEnrollmentStatus(SecurePaymentAlertType.EnrollmentStatus value) {
        this.enrollmentStatus = value;
    }

    /**
     * 获取airline属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType.Airline }
     *     
     */
    public SecurePaymentAlertType.Airline getAirline() {
        return airline;
    }

    /**
     * 设置airline属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType.Airline }
     *     
     */
    public void setAirline(SecurePaymentAlertType.Airline value) {
        this.airline = value;
    }

    /**
     * 获取reference属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType.Reference }
     *     
     */
    public SecurePaymentAlertType.Reference getReference() {
        return reference;
    }

    /**
     * 设置reference属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType.Reference }
     *     
     */
    public void setReference(SecurePaymentAlertType.Reference value) {
        this.reference = value;
    }

    /**
     * 获取urLs属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType.URLs }
     *     
     */
    public SecurePaymentAlertType.URLs getURLs() {
        return urLs;
    }

    /**
     * 设置urLs属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType.URLs }
     *     
     */
    public void setURLs(SecurePaymentAlertType.URLs value) {
        this.urLs = value;
    }

    /**
     * 获取details属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType.Details }
     *     
     */
    public SecurePaymentAlertType.Details getDetails() {
        return details;
    }

    /**
     * 设置details属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType.Details }
     *     
     */
    public void setDetails(SecurePaymentAlertType.Details value) {
        this.details = value;
    }

    /**
     * 获取processingInfos属性的值。
     * 
     * @return
     *     possible object is
     *     {@link SecurePaymentAlertType.ProcessingInfos }
     *     
     */
    public SecurePaymentAlertType.ProcessingInfos getProcessingInfos() {
        return processingInfos;
    }

    /**
     * 设置processingInfos属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link SecurePaymentAlertType.ProcessingInfos }
     *     
     */
    public void setProcessingInfos(SecurePaymentAlertType.ProcessingInfos value) {
        this.processingInfos = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ID" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;UniqueStringID_SimpleType"&gt;
     *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Name" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="CountryCode" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CountrySimpleType"&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "id",
        "name",
        "countryCode"
    })
    public static class Airline {

        @XmlElement(name = "ID")
        protected SecurePaymentAlertType.Airline.ID id;
        @XmlElement(name = "Name")
        protected SecurePaymentAlertType.Airline.Name name;
        @XmlElement(name = "CountryCode")
        protected SecurePaymentAlertType.Airline.CountryCode countryCode;

        /**
         * 获取id属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Airline.ID }
         *     
         */
        public SecurePaymentAlertType.Airline.ID getID() {
            return id;
        }

        /**
         * 设置id属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Airline.ID }
         *     
         */
        public void setID(SecurePaymentAlertType.Airline.ID value) {
            this.id = value;
        }

        /**
         * 获取name属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Airline.Name }
         *     
         */
        public SecurePaymentAlertType.Airline.Name getName() {
            return name;
        }

        /**
         * 设置name属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Airline.Name }
         *     
         */
        public void setName(SecurePaymentAlertType.Airline.Name value) {
            this.name = value;
        }

        /**
         * 获取countryCode属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Airline.CountryCode }
         *     
         */
        public SecurePaymentAlertType.Airline.CountryCode getCountryCode() {
            return countryCode;
        }

        /**
         * 设置countryCode属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Airline.CountryCode }
         *     
         */
        public void setCountryCode(SecurePaymentAlertType.Airline.CountryCode value) {
            this.countryCode = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CountrySimpleType"&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class CountryCode {

            @XmlValue
            protected String value;

            /**
             * A data type for Country Code encoding constraint: ISO 3166-1 (two character) Country Code
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;UniqueStringID_SimpleType"&gt;
         *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class ID {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "Context")
            protected String context;

            /**
             * A data type for a Unique String Identifier constraint.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * 获取context属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContext() {
                return context;
            }

            /**
             * 设置context属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContext(String value) {
                this.context = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Name {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "Context")
            protected String context;

            /**
             * 获取value属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * 获取context属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContext() {
                return context;
            }

            /**
             * 设置context属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContext(String value) {
                this.context = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
     *         &lt;element name="ClientType" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;SecureTrxClientSimpleType"&gt;
     *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="CustomerDevice" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}DeviceType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="BowserAcceptHeader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="BrowserUserAgentHeader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="DeviceCategoryCode" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Currency" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="InputCurrCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" minOccurs="0"/&gt;
     *                   &lt;element name="SettlementCurrCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ReservationInfo" minOccurs="0"/&gt;
     *         &lt;element name="TxnDatas" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="TxnDate" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" maxOccurs="3"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "trxTimestamp",
        "clientType",
        "customerDevice",
        "currency",
        "reservationInfo",
        "txnDatas"
    })
    public static class Details {

        @XmlElement(name = "TrxTimestamp")
        protected TimestampRepType trxTimestamp;
        @XmlElement(name = "ClientType")
        protected SecurePaymentAlertType.Details.ClientType clientType;
        @XmlElement(name = "CustomerDevice")
        protected SecurePaymentAlertType.Details.CustomerDevice customerDevice;
        @XmlElement(name = "Currency")
        protected SecurePaymentAlertType.Details.Currency currency;
        @XmlElement(name = "ReservationInfo")
        protected ReservationInfo reservationInfo;
        @XmlElement(name = "TxnDatas")
        protected SecurePaymentAlertType.Details.TxnDatas txnDatas;

        /**
         * 获取trxTimestamp属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TimestampRepType }
         *     
         */
        public TimestampRepType getTrxTimestamp() {
            return trxTimestamp;
        }

        /**
         * 设置trxTimestamp属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TimestampRepType }
         *     
         */
        public void setTrxTimestamp(TimestampRepType value) {
            this.trxTimestamp = value;
        }

        /**
         * 获取clientType属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Details.ClientType }
         *     
         */
        public SecurePaymentAlertType.Details.ClientType getClientType() {
            return clientType;
        }

        /**
         * 设置clientType属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Details.ClientType }
         *     
         */
        public void setClientType(SecurePaymentAlertType.Details.ClientType value) {
            this.clientType = value;
        }

        /**
         * 获取customerDevice属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Details.CustomerDevice }
         *     
         */
        public SecurePaymentAlertType.Details.CustomerDevice getCustomerDevice() {
            return customerDevice;
        }

        /**
         * 设置customerDevice属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Details.CustomerDevice }
         *     
         */
        public void setCustomerDevice(SecurePaymentAlertType.Details.CustomerDevice value) {
            this.customerDevice = value;
        }

        /**
         * 获取currency属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Details.Currency }
         *     
         */
        public SecurePaymentAlertType.Details.Currency getCurrency() {
            return currency;
        }

        /**
         * 设置currency属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Details.Currency }
         *     
         */
        public void setCurrency(SecurePaymentAlertType.Details.Currency value) {
            this.currency = value;
        }

        /**
         *  3D Secure Transaction Airline Reservation information.
         * 
         * @return
         *     possible object is
         *     {@link ReservationInfo }
         *     
         */
        public ReservationInfo getReservationInfo() {
            return reservationInfo;
        }

        /**
         * 设置reservationInfo属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ReservationInfo }
         *     
         */
        public void setReservationInfo(ReservationInfo value) {
            this.reservationInfo = value;
        }

        /**
         * 获取txnDatas属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Details.TxnDatas }
         *     
         */
        public SecurePaymentAlertType.Details.TxnDatas getTxnDatas() {
            return txnDatas;
        }

        /**
         * 设置txnDatas属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Details.TxnDatas }
         *     
         */
        public void setTxnDatas(SecurePaymentAlertType.Details.TxnDatas value) {
            this.txnDatas = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;SecureTrxClientSimpleType"&gt;
         *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class ClientType {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "Context")
            protected String context;

            /**
             * 
             *         A data type for 3DS Secure Authorization Client Type. Examples: InteractiveVoiceResponse (IVR) Internet MailOrTelephoneOrder (MOTO) Recurring Unattended
             *       
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * 获取context属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContext() {
                return context;
            }

            /**
             * 设置context属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContext(String value) {
                this.context = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="InputCurrCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" minOccurs="0"/&gt;
         *         &lt;element name="SettlementCurrCode" type="{http://www.iata.org/IATA/EDIST}CurrencySimpleType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "inputCurrCode",
            "settlementCurrCode"
        })
        public static class Currency {

            @XmlElement(name = "InputCurrCode")
            protected String inputCurrCode;
            @XmlElement(name = "SettlementCurrCode")
            protected String settlementCurrCode;

            /**
             * 获取inputCurrCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInputCurrCode() {
                return inputCurrCode;
            }

            /**
             * 设置inputCurrCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInputCurrCode(String value) {
                this.inputCurrCode = value;
            }

            /**
             * 获取settlementCurrCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSettlementCurrCode() {
                return settlementCurrCode;
            }

            /**
             * 设置settlementCurrCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSettlementCurrCode(String value) {
                this.settlementCurrCode = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}DeviceType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="BowserAcceptHeader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="BrowserUserAgentHeader" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="DeviceCategoryCode" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "bowserAcceptHeader",
            "browserUserAgentHeader",
            "deviceCategoryCode"
        })
        public static class CustomerDevice
            extends DeviceType
        {

            @XmlElement(name = "BowserAcceptHeader")
            protected String bowserAcceptHeader;
            @XmlElement(name = "BrowserUserAgentHeader")
            protected String browserUserAgentHeader;
            @XmlElement(name = "DeviceCategoryCode")
            protected String deviceCategoryCode;

            /**
             * 获取bowserAcceptHeader属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBowserAcceptHeader() {
                return bowserAcceptHeader;
            }

            /**
             * 设置bowserAcceptHeader属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBowserAcceptHeader(String value) {
                this.bowserAcceptHeader = value;
            }

            /**
             * 获取browserUserAgentHeader属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBrowserUserAgentHeader() {
                return browserUserAgentHeader;
            }

            /**
             * 设置browserUserAgentHeader属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBrowserUserAgentHeader(String value) {
                this.browserUserAgentHeader = value;
            }

            /**
             * 获取deviceCategoryCode属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDeviceCategoryCode() {
                return deviceCategoryCode;
            }

            /**
             * 设置deviceCategoryCode属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDeviceCategoryCode(String value) {
                this.deviceCategoryCode = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="TxnDate" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" maxOccurs="3"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "txnDate"
        })
        public static class TxnDatas {

            @XmlElement(name = "TxnDate", required = true)
            protected List<String> txnDate;

            /**
             * Gets the value of the txnDate property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the txnDate property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTxnDate().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getTxnDate() {
                if (txnDate == null) {
                    txnDate = new ArrayList<String>();
                }
                return this.txnDate;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;EnrollStatusSimpleType"&gt;
     *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class EnrollmentStatus {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "Context")
        protected String context;

        /**
         * 
         *         A data type for 3DS Program Enrollment Status. Examples: CardNotEnrolled, CardEnrolled, CardMayBeEnrolled, Unknown
         *       
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * 获取context属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContext() {
            return context;
        }

        /**
         * 设置context属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContext(String value) {
            this.context = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="PAReq" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PARes" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paReq",
        "paRes"
    })
    public static class PayerAuth {

        @XmlElement(name = "PAReq")
        protected String paReq;
        @XmlElement(name = "PARes")
        protected String paRes;

        /**
         * 获取paReq属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPAReq() {
            return paReq;
        }

        /**
         * 设置paReq属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPAReq(String value) {
            this.paReq = value;
        }

        /**
         * 获取paRes属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPARes() {
            return paRes;
        }

        /**
         * 设置paRes属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPARes(String value) {
            this.paRes = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ProcessingInfo" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="AddrVerification" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;attribute name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *                           &lt;attribute name="Text" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" /&gt;
     *                           &lt;attribute name="InvalidInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                           &lt;attribute name="NoMatchInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="CAVV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="CustomerAuthStatus" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;simpleContent&gt;
     *                         &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CustomerAuthStatusSimpleType"&gt;
     *                           &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *                         &lt;/extension&gt;
     *                       &lt;/simpleContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="ECI" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "processingInfo"
    })
    public static class ProcessingInfos {

        @XmlElement(name = "ProcessingInfo", required = true)
        protected List<SecurePaymentAlertType.ProcessingInfos.ProcessingInfo> processingInfo;

        /**
         * Gets the value of the processingInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the processingInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProcessingInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SecurePaymentAlertType.ProcessingInfos.ProcessingInfo }
         * 
         * 
         */
        public List<SecurePaymentAlertType.ProcessingInfos.ProcessingInfo> getProcessingInfo() {
            if (processingInfo == null) {
                processingInfo = new ArrayList<SecurePaymentAlertType.ProcessingInfos.ProcessingInfo>();
            }
            return this.processingInfo;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="AddrVerification" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;attribute name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *                 &lt;attribute name="Text" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" /&gt;
         *                 &lt;attribute name="InvalidInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *                 &lt;attribute name="NoMatchInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="CAVV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="CustomerAuthStatus" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;simpleContent&gt;
         *               &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CustomerAuthStatusSimpleType"&gt;
         *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
         *               &lt;/extension&gt;
         *             &lt;/simpleContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="ECI" type="{http://www.iata.org/IATA/EDIST}CodesetValueSimpleType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "addrVerification",
            "cavv",
            "customerAuthStatus",
            "eci"
        })
        public static class ProcessingInfo {

            @XmlElement(name = "AddrVerification")
            protected SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.AddrVerification addrVerification;
            @XmlElement(name = "CAVV")
            protected String cavv;
            @XmlElement(name = "CustomerAuthStatus")
            protected SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.CustomerAuthStatus customerAuthStatus;
            @XmlElement(name = "ECI")
            protected String eci;

            /**
             * 获取addrVerification属性的值。
             * 
             * @return
             *     possible object is
             *     {@link SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.AddrVerification }
             *     
             */
            public SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.AddrVerification getAddrVerification() {
                return addrVerification;
            }

            /**
             * 设置addrVerification属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.AddrVerification }
             *     
             */
            public void setAddrVerification(SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.AddrVerification value) {
                this.addrVerification = value;
            }

            /**
             * 获取cavv属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCAVV() {
                return cavv;
            }

            /**
             * 设置cavv属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCAVV(String value) {
                this.cavv = value;
            }

            /**
             * 获取customerAuthStatus属性的值。
             * 
             * @return
             *     possible object is
             *     {@link SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.CustomerAuthStatus }
             *     
             */
            public SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.CustomerAuthStatus getCustomerAuthStatus() {
                return customerAuthStatus;
            }

            /**
             * 设置customerAuthStatus属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.CustomerAuthStatus }
             *     
             */
            public void setCustomerAuthStatus(SecurePaymentAlertType.ProcessingInfos.ProcessingInfo.CustomerAuthStatus value) {
                this.customerAuthStatus = value;
            }

            /**
             * 获取eci属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getECI() {
                return eci;
            }

            /**
             * 设置eci属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setECI(String value) {
                this.eci = value;
            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;attribute name="Code" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
             *       &lt;attribute name="Text" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" /&gt;
             *       &lt;attribute name="InvalidInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *       &lt;attribute name="NoMatchInd" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AddrVerification {

                @XmlAttribute(name = "Code")
                protected String code;
                @XmlAttribute(name = "Text")
                protected String text;
                @XmlAttribute(name = "InvalidInd")
                protected Boolean invalidInd;
                @XmlAttribute(name = "NoMatchInd")
                protected Boolean noMatchInd;

                /**
                 * 获取code属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCode() {
                    return code;
                }

                /**
                 * 设置code属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCode(String value) {
                    this.code = value;
                }

                /**
                 * 获取text属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getText() {
                    return text;
                }

                /**
                 * 设置text属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setText(String value) {
                    this.text = value;
                }

                /**
                 * 获取invalidInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isInvalidInd() {
                    return invalidInd;
                }

                /**
                 * 设置invalidInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setInvalidInd(Boolean value) {
                    this.invalidInd = value;
                }

                /**
                 * 获取noMatchInd属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isNoMatchInd() {
                    return noMatchInd;
                }

                /**
                 * 设置noMatchInd属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setNoMatchInd(Boolean value) {
                    this.noMatchInd = value;
                }

            }


            /**
             * <p>anonymous complex type的 Java 类。
             * 
             * <p>以下模式片段指定包含在此类中的预期内容。
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;simpleContent&gt;
             *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;CustomerAuthStatusSimpleType"&gt;
             *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
             *     &lt;/extension&gt;
             *   &lt;/simpleContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class CustomerAuthStatus {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "Context")
                protected String context;

                /**
                 * 
                 *         A data type for 3DS Program ACS Customer Authentication status. Examples: Y- Customer authenticated N- Customer not authenticated A- An authentication attempt occurred but could not be completed U- Unable to perform authentication
                 *       
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * 设置value属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * 获取context属性的值。
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getContext() {
                    return context;
                }

                /**
                 * 设置context属性的值。
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setContext(String value) {
                    this.context = value;
                }

            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}ACS_TxnReference" minOccurs="0"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}SPM_TxnReference" minOccurs="0"/&gt;
     *         &lt;element name="OriginalTransactionID" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;simpleContent&gt;
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
     *                 &lt;attribute name="source" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" /&gt;
     *               &lt;/extension&gt;
     *             &lt;/simpleContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="TrxTimestamp" type="{http://www.iata.org/IATA/EDIST}TimestampRepType" minOccurs="0"/&gt;
     *         &lt;element name="TxnDescription" type="{http://www.iata.org/IATA/EDIST}UniqueStringID_SimpleType" minOccurs="0"/&gt;
     *         &lt;element name="TxnDatas" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="TxnDate" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" maxOccurs="3"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acsTxnReference",
        "spmTxnReference",
        "originalTransactionID",
        "trxTimestamp",
        "txnDescription",
        "txnDatas"
    })
    public static class Reference {

        @XmlElement(name = "ACS_TxnReference")
        protected ACSTxnReference acsTxnReference;
        @XmlElement(name = "SPM_TxnReference")
        protected SPMTxnReference spmTxnReference;
        @XmlElement(name = "OriginalTransactionID")
        protected SecurePaymentAlertType.Reference.OriginalTransactionID originalTransactionID;
        @XmlElement(name = "TrxTimestamp")
        protected TimestampRepType trxTimestamp;
        @XmlElement(name = "TxnDescription")
        protected String txnDescription;
        @XmlElement(name = "TxnDatas")
        protected SecurePaymentAlertType.Reference.TxnDatas txnDatas;

        /**
         * 获取acsTxnReference属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ACSTxnReference }
         *     
         */
        public ACSTxnReference getACSTxnReference() {
            return acsTxnReference;
        }

        /**
         * 设置acsTxnReference属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ACSTxnReference }
         *     
         */
        public void setACSTxnReference(ACSTxnReference value) {
            this.acsTxnReference = value;
        }

        /**
         * 获取spmTxnReference属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SPMTxnReference }
         *     
         */
        public SPMTxnReference getSPMTxnReference() {
            return spmTxnReference;
        }

        /**
         * 设置spmTxnReference属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SPMTxnReference }
         *     
         */
        public void setSPMTxnReference(SPMTxnReference value) {
            this.spmTxnReference = value;
        }

        /**
         * 获取originalTransactionID属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Reference.OriginalTransactionID }
         *     
         */
        public SecurePaymentAlertType.Reference.OriginalTransactionID getOriginalTransactionID() {
            return originalTransactionID;
        }

        /**
         * 设置originalTransactionID属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Reference.OriginalTransactionID }
         *     
         */
        public void setOriginalTransactionID(SecurePaymentAlertType.Reference.OriginalTransactionID value) {
            this.originalTransactionID = value;
        }

        /**
         * 获取trxTimestamp属性的值。
         * 
         * @return
         *     possible object is
         *     {@link TimestampRepType }
         *     
         */
        public TimestampRepType getTrxTimestamp() {
            return trxTimestamp;
        }

        /**
         * 设置trxTimestamp属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link TimestampRepType }
         *     
         */
        public void setTrxTimestamp(TimestampRepType value) {
            this.trxTimestamp = value;
        }

        /**
         * 获取txnDescription属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTxnDescription() {
            return txnDescription;
        }

        /**
         * 设置txnDescription属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTxnDescription(String value) {
            this.txnDescription = value;
        }

        /**
         * 获取txnDatas属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SecurePaymentAlertType.Reference.TxnDatas }
         *     
         */
        public SecurePaymentAlertType.Reference.TxnDatas getTxnDatas() {
            return txnDatas;
        }

        /**
         * 设置txnDatas属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SecurePaymentAlertType.Reference.TxnDatas }
         *     
         */
        public void setTxnDatas(SecurePaymentAlertType.Reference.TxnDatas value) {
            this.txnDatas = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;simpleContent&gt;
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;string"&gt;
         *       &lt;attribute name="source" type="{http://www.iata.org/IATA/EDIST}ProperNameSimpleType" /&gt;
         *     &lt;/extension&gt;
         *   &lt;/simpleContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class OriginalTransactionID {

            @XmlValue
            protected String value;
            @XmlAttribute(name = "source")
            protected String source;

            /**
             * 获取value属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * 设置value属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * 获取source属性的值。
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSource() {
                return source;
            }

            /**
             * 设置source属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSource(String value) {
                this.source = value;
            }

        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="TxnDate" type="{http://www.iata.org/IATA/EDIST}ShortDescSimpleType" maxOccurs="3"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "txnDate"
        })
        public static class TxnDatas {

            @XmlElement(name = "TxnDate", required = true)
            protected List<String> txnDate;

            /**
             * Gets the value of the txnDate property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the txnDate property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTxnDate().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getTxnDate() {
                if (txnDate == null) {
                    txnDate = new ArrayList<String>();
                }
                return this.txnDate;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.iata.org/IATA/EDIST&gt;SecureTransactionSimpleType"&gt;
     *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}ContextSimpleType" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class TransactionType {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "Context")
        protected String context;

        /**
         * 
         *         A data type for 3DS Transaction Type. Examples: Purchase, Refund, Authorization, Complete, Validate
         *       
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * 获取context属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContext() {
            return context;
        }

        /**
         * 设置context属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContext(String value) {
            this.context = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.iata.org/IATA/EDIST}SecurePaymentUrlType"&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class URLs
        extends SecurePaymentUrlType
    {


    }

}
