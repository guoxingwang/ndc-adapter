//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * OPTIONAL SERVICE FULFILLMENT representation, including Date/ Time Period, Location, Fulfillment Company.
 * 
 * <p>ServiceFulfillmentType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="ServiceFulfillmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}KeyWithMetaObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OfferValidDates" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Start" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="End" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Provider" type="{http://www.iata.org/IATA/EDIST}FulfillmentPartnerType" minOccurs="0"/&gt;
 *         &lt;element name="Location" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportFulfillmentLocation"/&gt;
 *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherFulfillmentLocation"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceFulfillmentType", propOrder = {
    "offerValidDates",
    "provider",
    "location"
})
public class ServiceFulfillmentType
    extends KeyWithMetaObjectBaseType
{

    @XmlElement(name = "OfferValidDates")
    protected ServiceFulfillmentType.OfferValidDates offerValidDates;
    @XmlElement(name = "Provider")
    protected FulfillmentPartnerType provider;
    @XmlElement(name = "Location")
    protected ServiceFulfillmentType.Location location;

    /**
     * 获取offerValidDates属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ServiceFulfillmentType.OfferValidDates }
     *     
     */
    public ServiceFulfillmentType.OfferValidDates getOfferValidDates() {
        return offerValidDates;
    }

    /**
     * 设置offerValidDates属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFulfillmentType.OfferValidDates }
     *     
     */
    public void setOfferValidDates(ServiceFulfillmentType.OfferValidDates value) {
        this.offerValidDates = value;
    }

    /**
     * 获取provider属性的值。
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentPartnerType }
     *     
     */
    public FulfillmentPartnerType getProvider() {
        return provider;
    }

    /**
     * 设置provider属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentPartnerType }
     *     
     */
    public void setProvider(FulfillmentPartnerType value) {
        this.provider = value;
    }

    /**
     * 获取location属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ServiceFulfillmentType.Location }
     *     
     */
    public ServiceFulfillmentType.Location getLocation() {
        return location;
    }

    /**
     * 设置location属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceFulfillmentType.Location }
     *     
     */
    public void setLocation(ServiceFulfillmentType.Location value) {
        this.location = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AirportFulfillmentLocation"/&gt;
     *         &lt;element ref="{http://www.iata.org/IATA/EDIST}OtherFulfillmentLocation"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airportFulfillmentLocation",
        "otherFulfillmentLocation"
    })
    public static class Location {

        @XmlElement(name = "AirportFulfillmentLocation", required = true)
        protected SrvcLocationAirportType airportFulfillmentLocation;
        @XmlElement(name = "OtherFulfillmentLocation", required = true)
        protected SrvcLocationAddressType otherFulfillmentLocation;

        /**
         * 获取airportFulfillmentLocation属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SrvcLocationAirportType }
         *     
         */
        public SrvcLocationAirportType getAirportFulfillmentLocation() {
            return airportFulfillmentLocation;
        }

        /**
         * 设置airportFulfillmentLocation属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SrvcLocationAirportType }
         *     
         */
        public void setAirportFulfillmentLocation(SrvcLocationAirportType value) {
            this.airportFulfillmentLocation = value;
        }

        /**
         * 获取otherFulfillmentLocation属性的值。
         * 
         * @return
         *     possible object is
         *     {@link SrvcLocationAddressType }
         *     
         */
        public SrvcLocationAddressType getOtherFulfillmentLocation() {
            return otherFulfillmentLocation;
        }

        /**
         * 设置otherFulfillmentLocation属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link SrvcLocationAddressType }
         *     
         */
        public void setOtherFulfillmentLocation(SrvcLocationAddressType value) {
            this.otherFulfillmentLocation = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Start" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="End" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "start",
        "end"
    })
    public static class OfferValidDates {

        @XmlElement(name = "Start")
        protected ServiceFulfillmentType.OfferValidDates.Start start;
        @XmlElement(name = "End")
        protected ServiceFulfillmentType.OfferValidDates.End end;

        /**
         * 获取start属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceFulfillmentType.OfferValidDates.Start }
         *     
         */
        public ServiceFulfillmentType.OfferValidDates.Start getStart() {
            return start;
        }

        /**
         * 设置start属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceFulfillmentType.OfferValidDates.Start }
         *     
         */
        public void setStart(ServiceFulfillmentType.OfferValidDates.Start value) {
            this.start = value;
        }

        /**
         * 获取end属性的值。
         * 
         * @return
         *     possible object is
         *     {@link ServiceFulfillmentType.OfferValidDates.End }
         *     
         */
        public ServiceFulfillmentType.OfferValidDates.End getEnd() {
            return end;
        }

        /**
         * 设置end属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link ServiceFulfillmentType.OfferValidDates.End }
         *     
         */
        public void setEnd(ServiceFulfillmentType.OfferValidDates.End value) {
            this.end = value;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class End
            extends CoreDateGrpType
        {


        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://www.iata.org/IATA/EDIST}CoreDateGrpType"&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Start
            extends CoreDateGrpType
        {


        }

    }

}
