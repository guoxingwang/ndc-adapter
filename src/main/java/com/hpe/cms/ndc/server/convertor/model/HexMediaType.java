//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * HEX ENCODED MEDIA representation.
 * 
 * <p>HexMediaType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="HexMediaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AssociatedObjectBaseType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EncodedValue"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;hexBinary"&gt;
 *                 &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Descriptions" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Description" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}LinkToMediaDescription"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}MediaMarkupDescription"/&gt;
 *                             &lt;element ref="{http://www.iata.org/IATA/EDIST}MediaPlainDescription"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ServiceEncoding" type="{http://www.iata.org/IATA/EDIST}ServiceEncodingType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HexMediaType", propOrder = {
    "encodedValue",
    "descriptions",
    "serviceEncoding"
})
public class HexMediaType
    extends AssociatedObjectBaseType
{

    @XmlElement(name = "EncodedValue", required = true)
    protected HexMediaType.EncodedValue encodedValue;
    @XmlElement(name = "Descriptions")
    protected HexMediaType.Descriptions descriptions;
    @XmlElement(name = "ServiceEncoding")
    protected ServiceEncodingType serviceEncoding;

    /**
     * 获取encodedValue属性的值。
     * 
     * @return
     *     possible object is
     *     {@link HexMediaType.EncodedValue }
     *     
     */
    public HexMediaType.EncodedValue getEncodedValue() {
        return encodedValue;
    }

    /**
     * 设置encodedValue属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link HexMediaType.EncodedValue }
     *     
     */
    public void setEncodedValue(HexMediaType.EncodedValue value) {
        this.encodedValue = value;
    }

    /**
     * 获取descriptions属性的值。
     * 
     * @return
     *     possible object is
     *     {@link HexMediaType.Descriptions }
     *     
     */
    public HexMediaType.Descriptions getDescriptions() {
        return descriptions;
    }

    /**
     * 设置descriptions属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link HexMediaType.Descriptions }
     *     
     */
    public void setDescriptions(HexMediaType.Descriptions value) {
        this.descriptions = value;
    }

    /**
     * 获取serviceEncoding属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ServiceEncodingType }
     *     
     */
    public ServiceEncodingType getServiceEncoding() {
        return serviceEncoding;
    }

    /**
     * 设置serviceEncoding属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceEncodingType }
     *     
     */
    public void setServiceEncoding(ServiceEncodingType value) {
        this.serviceEncoding = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Description" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}LinkToMediaDescription"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}MediaMarkupDescription"/&gt;
     *                   &lt;element ref="{http://www.iata.org/IATA/EDIST}MediaPlainDescription"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "description"
    })
    public static class Descriptions {

        @XmlElement(name = "Description", required = true)
        protected List<HexMediaType.Descriptions.Description> description;

        /**
         * Gets the value of the description property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the description property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDescription().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link HexMediaType.Descriptions.Description }
         * 
         * 
         */
        public List<HexMediaType.Descriptions.Description> getDescription() {
            if (description == null) {
                description = new ArrayList<HexMediaType.Descriptions.Description>();
            }
            return this.description;
        }


        /**
         * <p>anonymous complex type的 Java 类。
         * 
         * <p>以下模式片段指定包含在此类中的预期内容。
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}LinkToMediaDescription"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MediaMarkupDescription"/&gt;
         *         &lt;element ref="{http://www.iata.org/IATA/EDIST}MediaPlainDescription"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "linkToMediaDescription",
            "mediaMarkupDescription",
            "mediaPlainDescription"
        })
        public static class Description {

            @XmlElement(name = "LinkToMediaDescription", required = true)
            protected UriContextType linkToMediaDescription;
            @XmlElement(name = "MediaMarkupDescription", required = true)
            protected MediaMarkupDescType mediaMarkupDescription;
            @XmlElement(name = "MediaPlainDescription", required = true)
            protected MediaPlainDescType mediaPlainDescription;

            /**
             * 获取linkToMediaDescription属性的值。
             * 
             * @return
             *     possible object is
             *     {@link UriContextType }
             *     
             */
            public UriContextType getLinkToMediaDescription() {
                return linkToMediaDescription;
            }

            /**
             * 设置linkToMediaDescription属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link UriContextType }
             *     
             */
            public void setLinkToMediaDescription(UriContextType value) {
                this.linkToMediaDescription = value;
            }

            /**
             * 获取mediaMarkupDescription属性的值。
             * 
             * @return
             *     possible object is
             *     {@link MediaMarkupDescType }
             *     
             */
            public MediaMarkupDescType getMediaMarkupDescription() {
                return mediaMarkupDescription;
            }

            /**
             * 设置mediaMarkupDescription属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link MediaMarkupDescType }
             *     
             */
            public void setMediaMarkupDescription(MediaMarkupDescType value) {
                this.mediaMarkupDescription = value;
            }

            /**
             * 获取mediaPlainDescription属性的值。
             * 
             * @return
             *     possible object is
             *     {@link MediaPlainDescType }
             *     
             */
            public MediaPlainDescType getMediaPlainDescription() {
                return mediaPlainDescription;
            }

            /**
             * 设置mediaPlainDescription属性的值。
             * 
             * @param value
             *     allowed object is
             *     {@link MediaPlainDescType }
             *     
             */
            public void setMediaPlainDescription(MediaPlainDescType value) {
                this.mediaPlainDescription = value;
            }

        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;hexBinary"&gt;
     *       &lt;attGroup ref="{http://www.iata.org/IATA/EDIST}ObjAssociationAttrGroup"/&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class EncodedValue {

        @XmlValue
        @XmlJavaTypeAdapter(HexBinaryAdapter.class)
        @XmlSchemaType(name = "hexBinary")
        protected byte[] value;
        @XmlAttribute(name = "refs")
        @XmlIDREF
        protected List<Object> refs;

        /**
         * 获取value属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public byte[] getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(byte[] value) {
            this.value = value;
        }

        /**
         * Gets the value of the refs property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refs property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefs().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * 
         * 
         */
        public List<Object> getRefs() {
            if (refs == null) {
                refs = new ArrayList<Object>();
            }
            return this.refs;
        }

    }

}
