//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>COS_AssocListItemType的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * <p>
 * <pre>
 * &lt;simpleType name="COS_AssocListItemType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Equipment"/&gt;
 *     &lt;enumeration value="FlightGroup"/&gt;
 *     &lt;enumeration value="FlightOriginDestination"/&gt;
 *     &lt;enumeration value="FlightSegment"/&gt;
 *     &lt;enumeration value="MediaGroup"/&gt;
 *     &lt;enumeration value="MediaItem"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "COS_AssocListItemType")
@XmlEnum
public enum COSAssocListItemType {

    @XmlEnumValue("Equipment")
    EQUIPMENT("Equipment"),
    @XmlEnumValue("FlightGroup")
    FLIGHT_GROUP("FlightGroup"),
    @XmlEnumValue("FlightOriginDestination")
    FLIGHT_ORIGIN_DESTINATION("FlightOriginDestination"),
    @XmlEnumValue("FlightSegment")
    FLIGHT_SEGMENT("FlightSegment"),
    @XmlEnumValue("MediaGroup")
    MEDIA_GROUP("MediaGroup"),
    @XmlEnumValue("MediaItem")
    MEDIA_ITEM("MediaItem");
    private final String value;

    COSAssocListItemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static COSAssocListItemType fromValue(String v) {
        for (COSAssocListItemType c: COSAssocListItemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
