//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for Core Airline Message Party Representation. Derived from AirlineCoreRepType.
 * 
 * <p>AirlineMsgPartyCoreType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="AirlineMsgPartyCoreType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.iata.org/IATA/EDIST}AirlineCoreRepType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}AgentUser" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.iata.org/IATA/EDIST}Disclosures" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirlineMsgPartyCoreType", propOrder = {
    "agentUser",
    "disclosures"
})
@XmlSeeAlso({
    ORAAirlineSenderType.class,
    POAAirlineSenderType.class,
    MarketingCarrierSenderType.class,
    OperatingCarrierSenderType.class,
    MarketingCarrierRecipientType.class,
    OperatingCarrierRecipientType.class,
    ORAAirlineRecipientType.class,
    POAAirlineRecipientType.class,
    MarketingCarrierParticipantType.class,
    OperatingCarrierParticipantType.class,
    ORAAirlineParticipantType.class,
    POAAirlineParticipantType.class
})
public class AirlineMsgPartyCoreType
    extends AirlineCoreRepType
{

    @XmlElement(name = "AgentUser")
    protected AgentUserType agentUser;
    @XmlElement(name = "Disclosures")
    protected Disclosures disclosures;

    /**
     * 获取agentUser属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AgentUserType }
     *     
     */
    public AgentUserType getAgentUser() {
        return agentUser;
    }

    /**
     * 设置agentUser属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AgentUserType }
     *     
     */
    public void setAgentUser(AgentUserType value) {
        this.agentUser = value;
    }

    /**
     * 获取disclosures属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Disclosures }
     *     
     */
    public Disclosures getDisclosures() {
        return disclosures;
    }

    /**
     * 设置disclosures属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Disclosures }
     *     
     */
    public void setDisclosures(Disclosures value) {
        this.disclosures = value;
    }

}
