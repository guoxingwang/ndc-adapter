package com.hpe.cms.ndc.server.endpoint;

import com.hpe.cms.ndc.client.common.WsClientTemplate;
import com.hpe.cms.ndc.common.InterfaceInfoFactory;
import com.hpe.cms.ndc.common.InterfaceRouter;
import com.hpe.cms.ndc.common.InterfaceRouterUtil;
import com.hpe.cms.ndc.server.constance.WsServerConstance;
import com.hpe.cms.ndc.server.security.Security;
import com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ;
import com.hpe.cms.ndc.server.v15_2.model.AirShoppingRS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.server.endpoint.annotation.SoapHeader;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:36 2019/7/5
 * @ Description：
 * @ Modified By：
 */
@Endpoint
@Slf4j
public class AirShoppingEndpoint_v15_2 extends AbstractEndpoint<AirShoppingRQ, AirShoppingRS> {

    private static final String INTERFACE_NAME = "AirShoppingRQ";
    private static final String VERSION = WsServerConstance.V15_2;
    private static final String NAMESPACE = WsServerConstance.V15_2_NAMESPACE;

    public AirShoppingEndpoint_v15_2(WsClientTemplate<AirShoppingRQ, AirShoppingRS> wsClientTemplate) {
        super(wsClientTemplate);
    }

    @Override
    @PayloadRoot(namespace = NAMESPACE, localPart = INTERFACE_NAME + "_" + VERSION)
    @ResponsePayload
    public AirShoppingRS sendAndReceive(@RequestPayload AirShoppingRQ request,
                                        @SoapHeader("{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd}Security") SoapHeaderElement headerElement) {
        try {
            log.info("ActorOrRole is {}", headerElement.getActorOrRole());
            log.info("MustUnderstand is {}", String.valueOf(headerElement.getMustUnderstand()));
            log.info("Text is {}", headerElement.getText());
            log.info("Source is {}", headerElement.getSource().getSystemId());
            log.info("Result is {}", headerElement.getResult());
            log.info("Result.SystemId is {}", headerElement.getSource().getSystemId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.sendAndReceive(request, headerElement);
    }

    @Override
    void configInterfaceInfo(AirShoppingRQ request, Security security) {
        InterfaceRouter interfaceRouter = InterfaceRouterUtil.getInterfaRouterBySourceAndInterfaceName(security.getUsernameToken().getUsername(), INTERFACE_NAME);
        String targetVersion = interfaceRouter.getTargetVersion();
        String sourceVersion = VERSION;
        this.interfaceInfo = new InterfaceInfoFactory<AirShoppingRQ, AirShoppingRS>().createInterfaceInfo(interfaceRouter);
        interfaceInfo.setSourceVersion(VERSION);
        interfaceInfo.setSourceInterfaceName(INTERFACE_NAME);
        interfaceInfo.setSourceNamespace(NAMESPACE);
        interfaceInfo.setSourceRq(request);
        interfaceInfo.setSecurity(security);
    }
}

