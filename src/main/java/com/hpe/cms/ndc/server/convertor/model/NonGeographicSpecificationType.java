//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Non Geographic Specification information
 * 
 * <p>NonGeographicSpecificationType complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="NonGeographicSpecificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Type" type="{http://www.iata.org/IATA/EDIST}CodesetType"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="AgentPseudoCityCode"&gt;
 *             &lt;simpleType&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                 &lt;maxLength value="6"/&gt;
 *                 &lt;minLength value="1"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/simpleType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="AgentNumericCode"&gt;
 *             &lt;simpleType&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}positiveInteger"&gt;
 *                 &lt;totalDigits value="8"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/simpleType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="DepartmentID"&gt;
 *             &lt;simpleType&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}positiveInteger"&gt;
 *                 &lt;totalDigits value="7"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/simpleType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="CRSDepartmentCode"&gt;
 *             &lt;simpleType&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                 &lt;maxLength value="6"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/simpleType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="LNIATANumber"&gt;
 *             &lt;simpleType&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                 &lt;maxLength value="8"/&gt;
 *                 &lt;minLength value="1"/&gt;
 *               &lt;/restriction&gt;
 *             &lt;/simpleType&gt;
 *           &lt;/element&gt;
 *           &lt;element ref="{http://www.iata.org/IATA/EDIST}AirlineID" maxOccurs="unbounded"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NonGeographicSpecificationType", propOrder = {
    "type",
    "agentPseudoCityCode",
    "agentNumericCode",
    "departmentID",
    "crsDepartmentCode",
    "lniataNumber",
    "airlineID"
})
public class NonGeographicSpecificationType {

    @XmlElement(name = "Type", required = true)
    protected CodesetType type;
    @XmlElement(name = "AgentPseudoCityCode")
    protected String agentPseudoCityCode;
    @XmlElement(name = "AgentNumericCode")
    protected BigInteger agentNumericCode;
    @XmlElement(name = "DepartmentID")
    protected BigInteger departmentID;
    @XmlElement(name = "CRSDepartmentCode")
    protected String crsDepartmentCode;
    @XmlElement(name = "LNIATANumber")
    protected String lniataNumber;
    @XmlElement(name = "AirlineID")
    protected List<AirlineID> airlineID;

    /**
     * 获取type属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CodesetType }
     *     
     */
    public CodesetType getType() {
        return type;
    }

    /**
     * 设置type属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CodesetType }
     *     
     */
    public void setType(CodesetType value) {
        this.type = value;
    }

    /**
     * 获取agentPseudoCityCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentPseudoCityCode() {
        return agentPseudoCityCode;
    }

    /**
     * 设置agentPseudoCityCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentPseudoCityCode(String value) {
        this.agentPseudoCityCode = value;
    }

    /**
     * 获取agentNumericCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAgentNumericCode() {
        return agentNumericCode;
    }

    /**
     * 设置agentNumericCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAgentNumericCode(BigInteger value) {
        this.agentNumericCode = value;
    }

    /**
     * 获取departmentID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDepartmentID() {
        return departmentID;
    }

    /**
     * 设置departmentID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDepartmentID(BigInteger value) {
        this.departmentID = value;
    }

    /**
     * 获取crsDepartmentCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRSDepartmentCode() {
        return crsDepartmentCode;
    }

    /**
     * 设置crsDepartmentCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRSDepartmentCode(String value) {
        this.crsDepartmentCode = value;
    }

    /**
     * 获取lniataNumber属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLNIATANumber() {
        return lniataNumber;
    }

    /**
     * 设置lniataNumber属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLNIATANumber(String value) {
        this.lniataNumber = value;
    }

    /**
     * Gets the value of the airlineID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airlineID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirlineID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirlineID }
     * 
     * 
     */
    public List<AirlineID> getAirlineID() {
        if (airlineID == null) {
            airlineID = new ArrayList<AirlineID>();
        }
        return this.airlineID;
    }

}
