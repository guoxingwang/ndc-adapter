//
// 此文件是由 JavaTM Architecture for XML Binding (JAXB) 引用实现 v2.3.0 生成的
// 请访问 <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// 在重新编译源模式时, 对此文件的所有修改都将丢失。
// 生成时间: 2019.07.15 时间 07:00:39 PM CST 
//


package com.hpe.cms.ndc.server.convertor.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.Duration;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdvanceReservation"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;duration"&gt;
 *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}AdvPurchaseSimpleType" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AdvanceDeparture"&gt;
 *           &lt;complexType&gt;
 *             &lt;simpleContent&gt;
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;duration"&gt;
 *                 &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}AdvPurchaseSimpleType" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/simpleContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "advanceReservation",
    "advanceDeparture"
})
@XmlRootElement(name = "AdvanceTicketing")
public class AdvanceTicketing {

    @XmlElement(name = "AdvanceReservation", required = true)
    protected AdvanceTicketing.AdvanceReservation advanceReservation;
    @XmlElement(name = "AdvanceDeparture", required = true)
    protected AdvanceTicketing.AdvanceDeparture advanceDeparture;

    /**
     * 获取advanceReservation属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AdvanceTicketing.AdvanceReservation }
     *     
     */
    public AdvanceTicketing.AdvanceReservation getAdvanceReservation() {
        return advanceReservation;
    }

    /**
     * 设置advanceReservation属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AdvanceTicketing.AdvanceReservation }
     *     
     */
    public void setAdvanceReservation(AdvanceTicketing.AdvanceReservation value) {
        this.advanceReservation = value;
    }

    /**
     * 获取advanceDeparture属性的值。
     * 
     * @return
     *     possible object is
     *     {@link AdvanceTicketing.AdvanceDeparture }
     *     
     */
    public AdvanceTicketing.AdvanceDeparture getAdvanceDeparture() {
        return advanceDeparture;
    }

    /**
     * 设置advanceDeparture属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link AdvanceTicketing.AdvanceDeparture }
     *     
     */
    public void setAdvanceDeparture(AdvanceTicketing.AdvanceDeparture value) {
        this.advanceDeparture = value;
    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;duration"&gt;
     *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}AdvPurchaseSimpleType" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class AdvanceDeparture {

        @XmlValue
        protected Duration value;
        @XmlAttribute(name = "Context")
        protected String context;

        /**
         * 获取value属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setValue(Duration value) {
            this.value = value;
        }

        /**
         * 获取context属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContext() {
            return context;
        }

        /**
         * 设置context属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContext(String value) {
            this.context = value;
        }

    }


    /**
     * <p>anonymous complex type的 Java 类。
     * 
     * <p>以下模式片段指定包含在此类中的预期内容。
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;simpleContent&gt;
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;duration"&gt;
     *       &lt;attribute name="Context" type="{http://www.iata.org/IATA/EDIST}AdvPurchaseSimpleType" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/simpleContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class AdvanceReservation {

        @XmlValue
        protected Duration value;
        @XmlAttribute(name = "Context")
        protected String context;

        /**
         * 获取value属性的值。
         * 
         * @return
         *     possible object is
         *     {@link Duration }
         *     
         */
        public Duration getValue() {
            return value;
        }

        /**
         * 设置value属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link Duration }
         *     
         */
        public void setValue(Duration value) {
            this.value = value;
        }

        /**
         * 获取context属性的值。
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContext() {
            return context;
        }

        /**
         * 设置context属性的值。
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContext(String value) {
            this.context = value;
        }

    }

}
