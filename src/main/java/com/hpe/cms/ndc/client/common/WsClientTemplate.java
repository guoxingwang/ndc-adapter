package com.hpe.cms.ndc.client.common;

import com.hpe.cms.ndc.common.InterfaceRouter;
import com.hpe.cms.ndc.common.InterfaceRouterUtil;
import com.hpe.cms.ndc.server.constance.WsServerConstance;
import lombok.extern.slf4j.Slf4j;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;
import org.springframework.ws.transport.http.ClientHttpRequestMessageSender;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:05 2019/7/8
 * @ Description：WS-Client模板类
 * @ Modified By：
 */
@Component
@Slf4j
public class WsClientTemplate<RQ, RS> extends WebServiceGatewaySupport {

    /**
     * 按版本初始化WS-Client
     *
     * @param version
     */
    private void initConfig(String version) {
        WsClientEnums wsClientEnums = WsClientEnums.getWebServiceClientConfigEnumByVersion(version);
        Jaxb2Marshaller marshaller = this.getJaxb2Marshaller(wsClientEnums.JAXB2_MARSHALLER_CONTEXT_PATH);
        this.setMarshaller(marshaller);
        this.setUnmarshaller(marshaller);
        this.setDefaultUri(wsClientEnums.WEB_SERVICE_URI);
        this.setInterceptors(new ClientInterceptor[]{clientWss4jSecurityInterceptor()});

        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(120000);
        requestFactory.setReadTimeout(1200000);
        setMessageSender(new ClientHttpRequestMessageSender(requestFactory));
    }

    /**
     * 按版本初始化WS-Client
     * @param interfaceName
     * @param version
     */
    public void configClientTemplate(String interfaceName, String version) {
        InterfaceRouter interfaceRouter = InterfaceRouterUtil.getInterfaRouterBySourceAndInterfaceName(interfaceName, version);

        WsClientEnums wsClientEnums = WsClientEnums.getWebServiceClientConfigEnumByVersion(version);
        Jaxb2Marshaller marshaller = this.getJaxb2Marshaller(wsClientEnums.JAXB2_MARSHALLER_CONTEXT_PATH);
        this.setMarshaller(marshaller);
        this.setUnmarshaller(marshaller);
        //this.setDefaultUri(wsClientEnums.WEB_SERVICE_URI);
        this.setDefaultUri(interfaceRouter.getTargetUrl());
        this.setInterceptors(new ClientInterceptor[]{clientWss4jSecurityInterceptor()});

        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(120000);
        requestFactory.setReadTimeout(1200000);
        setMessageSender(new ClientHttpRequestMessageSender(requestFactory));
    }

    /**
     * 设置客户端Interceptor
     *
     * @return
     */
    public Wss4jSecurityInterceptor clientWss4jSecurityInterceptor() {
        Wss4jSecurityInterceptor wss4jSecurityInterceptor = new Wss4jSecurityInterceptor();

        /*客户端设置SecurementActions,服务端设置ValidationActions*/ //WSHandlerConstants.USERNAME_TOKEN + "" + WSHandlerConstants.TIMESTAMP
        wss4jSecurityInterceptor.setSecurementActions(WSHandlerConstants.TIMESTAMP + " " + WSHandlerConstants.USERNAME_TOKEN);
        //wss4jSecurityInterceptor.setValidationActions(WSHandlerConstants.USERNAME_TOKEN);
        /* Default Password encoding is digest, that is not supported by EP hence need to set need to set following password type. */
        wss4jSecurityInterceptor.setSecurementPasswordType(WSConstants.PW_TEXT);
        wss4jSecurityInterceptor.setSecurementUsername("admin");
        wss4jSecurityInterceptor.setSecurementPassword("secret");
        return wss4jSecurityInterceptor;
    }

    /**
     * 配置Jaxb2Marshaller
     *
     * @param contextPath
     * @return
     */
    private Jaxb2Marshaller getJaxb2Marshaller(String contextPath) {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        //会扫描此类下面的对应的 jaxb2实体类 因为是使用 Marshaller和 unmarshaller来进行xml和bean直接转换的
        //具体是判断此路径下是否包含 ObjectFactory.class 文件
        //设置 JAXBContext 对象
        marshaller.setContextPath(contextPath);
        //或者使用 以下方式设置
        //marshaller.setPackagesToScan(packagesToScan);
        //marshaller.setClassesToBeBound(classesToBeBound);
        return marshaller;
    }

    public RS sendAndReceive(Object rq) {
        log.info("WsClientTemplate.sendAndReceive");
        return (RS) getWebServiceTemplate().marshalSendAndReceive((RQ) rq);
    }

    public RS sendAndReceiveV15_2(Object rq) {
        initConfig(WsClientEnums.V15_2.VERSION);
        return sendAndReceive(rq);
    }

    public RS sendAndReceiveV16_1(Object rq) {
        initConfig(WsClientEnums.V16_1.VERSION);
        return sendAndReceive(rq);
    }

    public RS sendAndReceiveV16_2(Object rq) {
        initConfig(WsClientEnums.V16_2.VERSION);
        return sendAndReceive(rq);
    }

    public RS sendAndReceiveV17_1(Object rq) {
        initConfig(WsClientEnums.V17_1.VERSION);
        return sendAndReceive(rq);
    }

    public RS sendAndReceiveV17_2(Object rq) {
        initConfig(WsClientEnums.V17_2.VERSION);
        return sendAndReceive(rq);
    }

    public RS sendAndReceiveV18_1(Object rq) {
        initConfig(WsClientEnums.V18_1.VERSION);
        return sendAndReceive(rq);
    }

    public RS sendAndReceiveV18_2(Object rq) {
        initConfig(WsClientEnums.V18_2.VERSION);
        return sendAndReceive(rq);
    }

    /**
     * WS-Client不同版本配置信息
     */
    public enum WsClientEnums {
        V15_2("v15_2", "com.hpe.cms.ndc.server.v15_2.model", "http://localhost:8152" + WsServerConstance.WSDL_PATH + "v15_2"),
        V16_1("v16_1", "com.hpe.cms.ndc.server.v16_1.model", "http://localhost:8152" + WsServerConstance.WSDL_PATH + "v16_1"),
        V16_2("v16_2", "com.hpe.cms.ndc.server.v16_2.model", "http://localhost:8152" + WsServerConstance.WSDL_PATH + "v16_2"),
        V17_1("v17_1", "com.hpe.cms.ndc.server.v17_1.model", "http://localhost:8152" + WsServerConstance.WSDL_PATH + "v17_1"),
        V17_2("v17_2", "com.hpe.cms.ndc.server.v17_2.model", "http://localhost:8152" + WsServerConstance.WSDL_PATH + "v17_2"),
        V18_1("v18_1", "com.hpe.cms.ndc.server.v18_1.model", "http://localhost:8152" + WsServerConstance.WSDL_PATH + "v18_1"),
        V18_2("v18_2", "com.hpe.cms.ndc.server.v18_2.model", "http://localhost:8152" + WsServerConstance.WSDL_PATH + "v18_2");

        final String VERSION;
        final String JAXB2_MARSHALLER_CONTEXT_PATH;
        final String WEB_SERVICE_URI;

        WsClientEnums(String VERSION, String JAXB2_MARSHALLER_CONTEXT_PATH, String WEB_SERVICE_URI) {
            this.VERSION = VERSION;
            this.JAXB2_MARSHALLER_CONTEXT_PATH = JAXB2_MARSHALLER_CONTEXT_PATH;
            this.WEB_SERVICE_URI = WEB_SERVICE_URI;
        }

        public static WsClientEnums getWebServiceClientConfigEnumByVersion(String version) {
            switch (version) {
                case "v15_2":
                    return WsClientEnums.V15_2;
                case "v16_1":
                    return WsClientEnums.V16_1;
                case "v16_2":
                    return WsClientEnums.V16_2;
                case "v17_1":
                    return WsClientEnums.V17_1;
                case "v17_2":
                    return WsClientEnums.V17_2;
                case "v18_1":
                    return WsClientEnums.V18_1;
                case "v18_2":
                    return WsClientEnums.V18_2;
                default:
                    System.out.println("version is not config");
                    return null;
            }
        }
    }

}
