package com.hpe.cms.ndc.convertor;


import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 17:20 2019/7/7
 * @ Description：
 * @ Modified By：
 */
public class DemoConvertor {

    public static com.hpe.cms.ndc.server.v16_1.model.AirShoppingRQ airShoppingRQ_v15_2_to_v16_1(com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ airShoppingRQ_v15_2) {

        Mapper mapper = new DozerBeanMapper();
        com.hpe.cms.ndc.server.v16_1.model.AirShoppingRQ destObject =
                mapper.map(airShoppingRQ_v15_2, com.hpe.cms.ndc.server.v16_1.model.AirShoppingRQ.class);

        return destObject;
    }


}

