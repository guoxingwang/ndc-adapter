package com.hpe.cms.ndc.common.util;

import org.springframework.xml.transform.StringSource;

import javax.xml.transform.Source;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 10:18 2019/7/13
 * @ Description：Source工具类
 * @ Modified By：
 */
public class SourceUtil {

    /**
     * 根据文件路径获取Source
     *
     * @param filePath
     * @return
     */
    public static Source getSourceFromFilePath(String filePath) {
        File file = new File(Thread.currentThread().getContextClassLoader().getResource(filePath).getPath());
        BufferedReader reader = null;
        StringBuffer stringBuffer = new StringBuffer();
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                stringBuffer.append(tempString);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
//            return new StreamSource(is);
        System.out.println("stringBuffer = " + stringBuffer);
        return new StringSource(stringBuffer.toString());
    }
}
