package com.hpe.cms.ndc.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 14:40 2019/7/28
 * @ Description：接口路由表
 * @ Modified By：
 */
@ApiModel(description = "接口路由表")
@Data
public class InterfaceRouter {

    @ApiModelProperty(value = "来源", example = "伊比利亚")
    private String source;
    @ApiModelProperty(value = "目标", example = "春秋")
    private String target;

    @ApiModelProperty(value = "接口名称", example = "AirShopping")
    private String interfaceName;
    @ApiModelProperty(value = "目标URL", example = "http://localhost:8152/ndc-adapter/ws/v18_2")
    private String targetUrl;

    @ApiModelProperty(value = "来源版本", example = "v16_2")
    private String sourceVersion;
    @ApiModelProperty(value = "目标版本", example = "v18_2")
    private String targetVersion;

    private String sourceRqClassName;
    private String targetRqClassName;

    private String sourceRsClassName;
    private String targetRsClassName;
}
