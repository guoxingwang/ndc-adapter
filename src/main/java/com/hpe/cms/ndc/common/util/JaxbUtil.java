package com.hpe.cms.ndc.common.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Jaxb2工具类
 *
 * @author zhuc
 * @create 2013-3-29 下午2:40:14
 */
public class JaxbUtil {
    /**
     * JavaBean转换成xml
     * 默认编码UTF-8
     *
     * @param obj
     * @param writer
     * @return
     */
    public static String convertToXml(Object obj) {
        return convertToXml(obj, "UTF-8");
    }

    /**
     * JavaBean转换成xml
     *
     * @param obj
     * @param encoding
     * @return
     */
    public static String convertToXml(Object obj, String encoding) {
        String result = null;
        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, encoding);
            StringWriter writer = new StringWriter();
            marshaller.marshal(obj, writer);
            result = writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * xml转换成JavaBean
     *
     * @param xmlStr
     * @param c
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T converyToJavaBean(String xmlStr, Class<T> c) {
        T t = null;
        try {
            JAXBContext context = JAXBContext.newInstance(c);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            t = (T) unmarshaller.unmarshal(new StringReader(xmlStr));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     * xml转换成JavaBean
     *
     * @param xml
     * @param c
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T converyToJavaBeanFromFile(String filename, Class<T> c) {

        File file = new File(filename);
        String content = null;
        try {
            content = FileUtils.readFileToString(file, "utf-8");
            content = removeSoapActionXml(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return converyToJavaBean(content, c);
    }

    /**
     * xml转换成JavaBean
     *
     * @param xml
     * @param c
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T converyToJavaBeanFromString(String content, Class<T> c) {
        return converyToJavaBean(removeSoapActionXml(content), c);
    }

    public static String removeSoapActionXml(String content) {
        if (StringUtils.contains(content, "<soapenv:Body>")) {
            content = StringUtils.substringBetween(content, "<soapenv:Body>", "</soapenv:Body>");
        } else if (StringUtils.contains(content, "<soap:Body>")) {
            content = StringUtils.substringBetween(content, "<soap:Body>", "</soap:Body>");
        }

        return content;
    }
}
