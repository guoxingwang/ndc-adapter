package com.hpe.cms.ndc.common;

import com.hpe.cms.ndc.server.convertor.AbstractConvertor;
import com.hpe.cms.ndc.server.convertor.ConvertorFactory;
import com.hpe.cms.ndc.server.security.Security;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 12:43 2019/7/27
 * @ Description：接口信息
 * @ Modified By：
 */
@Data
@Slf4j
public class InterfaceInfo<SRQ, SRS, TRQ, TRS> {

    public InterfaceInfo(InterfaceRouter interfaceRouter) {
        this.interfaceRouter = interfaceRouter;
        this.sourceVersion = interfaceRouter.getSourceVersion();
        this.targetVersion = interfaceRouter.getTargetVersion();
        this.sourceInterfaceName = interfaceRouter.getInterfaceName();
    }

    private InterfaceRouter interfaceRouter;

    private String sourceVersion;
    private String targetVersion;

    private String sourceInterfaceName;
    private String targetInterfaceName;

    private String sourceNamespace;
    private String targetNamespace;

    private SRQ sourceRq;
    private TRQ targetRq;

    private SRS sourceRs;
    private TRS targetRs;

    private Security security;


    public TRQ converteRq() {
        if (sourceVersion.equals(targetVersion)) { //版本一致则不需要转换
            return (TRQ) this.getSourceRq();
        } else {
            AbstractConvertor convertor = ConvertorFactory.createConvertor(interfaceRouter.getTargetRqClassName(), sourceVersion, targetVersion);
            return (TRQ) convertor.converte(this.getSourceRq());
        }
    }

    public SRQ converteRs() {
        //通过targetRs转换sourceRs
        if (sourceVersion.equals(targetVersion)) { //版本一致则不需要转换
            return (SRQ) this.getTargetRs();
        } else {
            AbstractConvertor convertor = ConvertorFactory.createConvertor(interfaceRouter.getSourceRqClassName(), sourceVersion, targetVersion);
            return (SRQ) convertor.converte(this.getTargetRs());
        }
    }
}
