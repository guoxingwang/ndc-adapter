package com.hpe.cms.ndc.common;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 12:43 2019/7/27
 * @ Description：接口信息
 * @ Modified By：
 */
@Data
@Slf4j
public class InterfaceInfoFactory<RQ, RS> {

    public InterfaceInfo createInterfaceInfo(InterfaceRouter interfaceRouter) {
        String sourceVersion = interfaceRouter.getSourceVersion();
        String targetVersion = interfaceRouter.getTargetVersion();
        String interfaceName = interfaceRouter.getInterfaceName();
        String type = interfaceName + "_" + sourceVersion + "_" + targetVersion;
        switch (type) {
            case "AirShoppingRQ_v15_2_v18_2":
                return new InterfaceInfo<RQ, RS, com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrq.IATAAirShoppingRQ, com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRS>(interfaceRouter);
            case "AirShoppingRQ_v16_2_v18_2":
                return new InterfaceInfo<RQ, RS, com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrq.IATAAirShoppingRQ, com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRS>(interfaceRouter);
            case "AirShoppingRQ_v18_2_v12_2":
                return new InterfaceInfo<RQ, RS, com.hpe.cms.ndc.server.v16_2.model.AirShoppingRQ, com.hpe.cms.ndc.server.v16_2.model.AirShoppingRS>(interfaceRouter);
            case "other interfase":  //todo 其它接口需要在此处扩展
                return null;
            default:
                break;
        }
        return null;
    }

    public InterfaceInfo createInterfaceInfo(String interfaceName, String sourceVersion, String targetVersion) {
        return null;
    }

/*
    public InterfaceInfo createInterfaceInfo(InterfaceRouter interfaceRouter, String sourceVersion, String targetVersion) {
        *//*String rqName = "";

        ParameterizedType parameterizedType = (ParameterizedType) InterfaceInfoFactory.class.getGenericInterfaces()[0];
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        for (Type actualTypeArgument : actualTypeArguments) {
            rqName = String.valueOf(actualTypeArgument);
            break;
        }*//*
        String type = interfaceName + "_" + sourceVersion + "_" + targetVersion;
        switch (type) {
            case "AirShoppingRQ_v15_2_v18_2":
                return new InterfaceInfo<RQ, RS, com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrq.IATAAirShoppingRQ, com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRS>(sourceVersion, targetVersion, interfaceName);
            case "AirShoppingRQ_v16_2_v18_2":
                return new InterfaceInfo<RQ, RS, com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrq.IATAAirShoppingRQ, com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRS>(sourceVersion, targetVersion, interfaceName);
            case "AirShoppingRQ_v18_2_v12_2":
                return new InterfaceInfo<RQ, RS, com.hpe.cms.ndc.server.v16_2.model.AirShoppingRQ, com.hpe.cms.ndc.server.v16_2.model.AirShoppingRS>(sourceVersion, targetVersion, interfaceName);
            case "other interfase":  //todo 其它接口需要在此处扩展
                return null;
            default:
                break;
        }
        return null;
    }*/
}
