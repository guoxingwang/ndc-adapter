package com.hpe.cms.ndc.common.util;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.IOException;
import java.io.StringWriter;

public class JacksonUtil {
    private static ObjectMapper mapper = new ObjectMapper();
    private static ObjectMapper xmlMapper = new XmlMapper();
    //忽略pojo中不存在的字段

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static String bean2Json(Object obj) throws IOException {
        StringWriter sw = new StringWriter();
        JsonGenerator gen = new JsonFactory().createGenerator(sw);
        mapper.writeValue(gen, obj);
        gen.close();
        return sw.toString();
    }

    public static <T> T json2Bean(String str, Class<T> objClass)
            throws JsonParseException, JsonMappingException, IOException {
        return mapper.readValue(str, objClass);
    }


    public static String bean2Xml(Object obj) throws IOException {
        StringWriter sw = new StringWriter();
        xmlMapper.writeValue(sw, obj);
        return sw.toString();
    }

    public static <T> T xml2Bean(String str, Class<T> objClass)
            throws JsonParseException, JsonMappingException, IOException {
        return xmlMapper.readValue(str, objClass);
    }

}
