package com.hpe.cms.ndc.common;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 14:40 2019/7/28
 * @ Description：
 * @ Modified By：
 */
public class InterfaceRouterUtil {

    /**
     * 通过请求方、请求接口名称、目标版本获取路由信息
     *
     * @param source
     * @param interfaceName
     * @return
     */
    public static InterfaceRouter getInterfaRouterBySourceAndInterfaceName(String source, String interfaceName) {
        //todo 查询DB获取路由信息
        InterfaceRouter interfaceRouter = new InterfaceRouter();
        interfaceRouter.setSource(source);
        interfaceRouter.setInterfaceName(interfaceName);
        interfaceRouter.setTargetUrl("http://localhost:8152/ndc-adapter/ws/v18_2");
        interfaceRouter.setSourceVersion("v15_2");
        interfaceRouter.setTargetVersion("v18_2");
        interfaceRouter.setSourceRqClassName("com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ");
        interfaceRouter.setSourceRsClassName("com.hpe.cms.ndc.server.v15_2.model.AirShoppingRS");
        interfaceRouter.setTargetRqClassName("com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRQ");
        interfaceRouter.setTargetRsClassName("com.hpe.cms.ndc.server.v18_2.model.iata_airshoppingrs.IATAAirShoppingRS");

        return interfaceRouter;
    }
}
