package com.hpe.cms.ndc.common.config;


import lombok.extern.slf4j.Slf4j;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

@Configuration
@Slf4j
public class DozerBeanMapperConfigure {

    @Bean(name = "dozerBeanMapperFactoryBean")
    public DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean() throws Exception {
        StringBuilder info = new StringBuilder("\n");
        info.append("=========================================================\n");
        info.append("dozer is initializing ...\n");

        try {
            final DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean = new DozerBeanMapperFactoryBean();

            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource[] resources = resolver.getResources("classpath*:/dozer/*.xml");
            for (Resource resource : resources) {
                info.append("\tdozer config：").append(resource.getURL()).append("\n");
            }
            dozerBeanMapperFactoryBean.setMappingFiles(resources);
            return dozerBeanMapperFactoryBean;
        } finally {
            info.append("=========================================================\n");
            log.info(info.toString());
        }
    }
}
