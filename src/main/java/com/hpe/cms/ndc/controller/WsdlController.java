package com.hpe.cms.ndc.controller;

import com.hpe.cms.ndc.server.config.NDCDefaultWsdl11Definition;
import com.hpe.cms.ndc.server.constance.WsServerConstance;
import com.hpe.cms.ndc.server.util.ApplicationContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;

import java.util.*;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 13:28 2019/7/25
 * @ Description：
 * @ Modified By：
 */
@Controller
@Slf4j
public class WsdlController {

    public static Map<String, List> wsdlMap = Collections.emptyMap();

    @Autowired
    public Environment environment;

    @RequestMapping(value = "")
    public String wsdlList(ModelMap map) {
        map.addAttribute("v152WsdlList", getWsdlUrl().get(WsServerConstance.V15_2));
        map.addAttribute("v161WsdlList", getWsdlUrl().get(WsServerConstance.V16_1));
        map.addAttribute("v162WsdlList", getWsdlUrl().get(WsServerConstance.V16_2));
        map.addAttribute("v171WsdlList", getWsdlUrl().get(WsServerConstance.V17_1));
        map.addAttribute("v172WsdlList", getWsdlUrl().get(WsServerConstance.V17_2));
        map.addAttribute("v181WsdlList", getWsdlUrl().get(WsServerConstance.V18_1));
        map.addAttribute("v182WsdlList", getWsdlUrl().get(WsServerConstance.V18_2));
        return "index";
    }

    public static Map<String, List> getWsdlUrl() {
        if (!CollectionUtils.isEmpty(wsdlMap)) {
            return wsdlMap;
        }
        //规范的xsd文件生成的wsdl
        String[] wsdlArrayNdc = ApplicationContextUtil.getApplicationContext().getBeanNamesForType(NDCDefaultWsdl11Definition.class);
        //自定义的wsdl文件
        String[] wsdlArraySimple = ApplicationContextUtil.getApplicationContext().getBeanNamesForType(SimpleWsdl11Definition.class);

        List<String> v152WsdlList = new ArrayList<>();
        List<String> v161WsdlList = new ArrayList<>();
        List<String> v162WsdlList = new ArrayList<>();
        List<String> v171WsdlList = new ArrayList<>();
        List<String> v172WsdlList = new ArrayList<>();
        List<String> v181WsdlList = new ArrayList<>();
        List<String> v182WsdlList = new ArrayList<>();

        String[] wsdlArray = ArrayUtils.addAll(wsdlArrayNdc, wsdlArraySimple);
        for (int i = 0; i < wsdlArray.length; i++) {
            log.info("wsdlArray[" + i + "] = " + wsdlArray[i]);
            if (wsdlArray[i].endsWith(WsServerConstance.V15_2)) {
                v152WsdlList.add(wsdlBeanName2Url(wsdlArray[i]));
            } else if (wsdlArray[i].endsWith(WsServerConstance.V16_1)) {
                v161WsdlList.add(wsdlBeanName2Url(wsdlArray[i]));
            } else if (wsdlArray[i].endsWith(WsServerConstance.V16_2)) {
                v162WsdlList.add(wsdlBeanName2Url(wsdlArray[i]));
            } else if (wsdlArray[i].endsWith(WsServerConstance.V17_1)) {
                v171WsdlList.add(wsdlBeanName2Url(wsdlArray[i]));
            } else if (wsdlArray[i].endsWith(WsServerConstance.V17_2)) {
                v172WsdlList.add(wsdlBeanName2Url(wsdlArray[i]));
            } else if (wsdlArray[i].endsWith(WsServerConstance.V18_1)) {
                v181WsdlList.add(wsdlBeanName2Url(wsdlArray[i]));
            } else if (wsdlArray[i].endsWith(WsServerConstance.V18_2)) {
                v182WsdlList.add(wsdlBeanName2Url(wsdlArray[i]));
            }
        }
        Collections.sort(v152WsdlList);
        Collections.sort(v161WsdlList);
        Collections.sort(v162WsdlList);
        Collections.sort(v171WsdlList);
        Collections.sort(v172WsdlList);
        Collections.sort(v181WsdlList);
        Collections.sort(v182WsdlList);

        wsdlMap = new HashMap<>();
        wsdlMap.put(WsServerConstance.V15_2, v152WsdlList);
        wsdlMap.put(WsServerConstance.V16_1, v161WsdlList);
        wsdlMap.put(WsServerConstance.V16_2, v162WsdlList);
        wsdlMap.put(WsServerConstance.V17_1, v171WsdlList);
        wsdlMap.put(WsServerConstance.V17_2, v172WsdlList);
        wsdlMap.put(WsServerConstance.V18_1, v181WsdlList);
        wsdlMap.put(WsServerConstance.V18_2, v182WsdlList);
        return wsdlMap;
    }

    public static String wsdlBeanName2Url(String beanName) {
        Environment environment = ApplicationContextUtil.getApplicationContext().getBean(Environment.class);
        String urlPre = "http://localhost:" + environment.getProperty("server.port") + WsServerConstance.WSDL_PATH;
        String wsdlUrl = urlPre + StringUtils.substring(beanName, beanName.length() - 5) + "/" + StringUtils.substring(beanName, 0, beanName.length() - 6) + ".wsdl";
        return wsdlUrl;
    }


}
