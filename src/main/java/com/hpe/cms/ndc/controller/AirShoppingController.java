package com.hpe.cms.ndc.controller;

import com.hpe.cms.ndc.client.common.WsClientTemplate;
import com.hpe.cms.ndc.server.v15_2.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 18:25 2019/7/8
 * @ Description：1
 * @ Modified By：
 */
@Controller
public class AirShoppingController {

    @Autowired
    private WsClientTemplate<AirShoppingRS, AirShoppingRQ> wsClientTemplate;

    @PutMapping("airShopping")
    public AirShoppingRQ airShopping() {
        AirShoppingRQ airShoppingRQ = new AirShoppingRQ();
        AirShopReqParamsType airShopReqParamsType = new AirShopReqParamsType();

        PointOfSaleType pointOfSaleType = new PointOfSaleType();
        PointOfSaleType.Location location = new PointOfSaleType.Location();
        CountryCode countryCode = new CountryCode();
        countryCode.setValue("CN");
        location.setCountryCode(countryCode);
        pointOfSaleType.setLocation(location);
        airShoppingRQ.setPointOfSale(pointOfSaleType);
        return wsClientTemplate.sendAndReceiveV15_2(airShoppingRQ);
    }

}
