package com.hpe.cms.frame.util;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class ProfileUtils implements InitializingBean {

    @Autowired
    private Environment env;

    public enum ProfileEnum {
        prod,test,dev,local
    }

    private static List<String> profiles = Collections.unmodifiableList(new ArrayList<String>());

    @Override
    public void afterPropertiesSet() throws Exception {
        profiles = Collections.unmodifiableList(Arrays.asList(env.getActiveProfiles()));
    }

    public static boolean startsWith(ProfileEnum profilePrefix) {
        return getProfiles().stream().filter(p -> p.startsWith(profilePrefix.name())).findAny().isPresent();
    }

    public static boolean is(ProfileEnum profile) {
        return getProfiles().stream().filter(p -> p.equals(profile.name())).findAny().isPresent();
    }

    public static List<String> getProfiles() {
        return profiles;
    }

}
