package com.hpe.cms.frame.config;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.collect.Lists;
import com.hpe.cms.frame.util.ProfileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 10:12 2019/7/25
 * @ Description：Swagger配置类  //local、dev、test环境启动swagger，其它环境不启动
 * @ Modified By：
 */
@Profile({"local", "dev", "test"})
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    private Environment env;

    @Autowired
    private TypeResolver typeResolver;

    private static final String KEY_REF_NAME = "Authorization";
    public static final String HEADER_STRING = "Authorization";

    private ApiKey apiKey() {
        return new ApiKey(KEY_REF_NAME, HEADER_STRING, "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(
                new SecurityReference(KEY_REF_NAME, authorizationScopes));
    }

    @Bean
    public Docket swaggerSpringMvcPlugin() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(env.getProperty("spring.application.name"))
                .select()
                //.apis(RequestHandlerSelectors.basePackage(Application.class.getPackage().getName()))
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/")
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(typeResolver.resolve(DeferredResult.class, typeResolver.resolve(ResponseEntity.class, WildcardType.class)), typeResolver.resolve(WildcardType.class)))
                //.enableUrlTemplating(true)
                .useDefaultResponseMessages(true)
                .forCodeGeneration(false)
                .host(host())
                .apiInfo(apiInfo())
                .securitySchemes(Lists.newArrayList(apiKey()))
                .securityContexts(Lists.newArrayList(securityContext()));
    }

    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder().build();
    }

    private String host() {
        String host = ProfileUtils.startsWith(ProfileUtils.ProfileEnum.dev)
                ? "localhost:" + env.getProperty("server.port")
                : "127.0.0.1:8081";
        return host;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(env.getProperty("spring.application.name") + " REST API")
                .description("仅在test/dev环境中启用用于调试，在prod环境中关闭。")
                .version(env.getProperty("app.project.version"))
                .build();
    }
}
