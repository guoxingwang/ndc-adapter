<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<soapenv:Header/>
	<soapenv:Body>
		<SeatAvailabilityRQ Version="2.0" PrimaryLangID="es" xmlns="http://www.iata.org/IATA/EDIST">
			<PointOfSale>
				<Location>
					<CountryCode>ES</CountryCode>
				</Location>
			</PointOfSale>
			<Document>
				<ReferenceVersion>1.0</ReferenceVersion>
			</Document>
			<Party>
				<Sender>
					<TravelAgencySender>
						<Name>AgencyName</Name>
						<Contacts>
							<Contact>
								<EmailContact>
									<Address>AgencyMail@xxx.com</Address>
								</EmailContact>
								<PhoneContact>
									<Application>MOBILE</Application>
									<Number>+624545242</Number>
								</PhoneContact>
								<Name>
									<Surname>Perez</Surname>
									<Given>Pepe</Given>
								</Name>
							</Contact>
						</Contacts>
						<IATA_Number>78224285</IATA_Number>
						<AgencyID>Traveltool SL/Logitravel</AgencyID>
					</TravelAgencySender>
				</Sender>
			</Party>
			<ShoppingResponseIDs>
				<ResponseID>Qm9va2luZ0AxNTMyNTAzNTIwMzMxLUk4MWo2</ResponseID>
				<AssociatedIDs>
					<AssociatedID>
						<OfferItemID Owner="IB">IB54be0b968c0a4d838d267b2758e18e62_01</OfferItemID>
					</AssociatedID>
				</AssociatedIDs>
			</ShoppingResponseIDs>
		</SeatAvailabilityRQ>
	</soapenv:Body>
</soapenv:Envelope>