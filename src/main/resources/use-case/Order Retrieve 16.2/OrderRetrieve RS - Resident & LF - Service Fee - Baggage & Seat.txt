<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <OrderViewRS Version="3.001" PrimaryLangID="es" xmlns="http://www.iata.org/IATA/EDIST">
         <Document/>
		          <Party>
            <Sender>
               <TravelAgencySender>
                  <Name>AGENCYNAME</Name>
                  <Contacts>
                     <Contact>
                        <EmailContact>
                           <Address>AGENCYMAIL@XXX.COM</Address>
                        </EmailContact>
                        <PhoneContact>
                           <Application>MOBILE</Application>
                           <Number>624545242</Number>
                        </PhoneContact>
                        <Name>
                           <Surname>PEPEPEREZ</Surname>
                        </Name>
                     </Contact>
                  </Contacts>
                  <IATA_Number>XXXXXXXX</IATA_Number>
                  <AgencyID>XXXXXXXX</AgencyID>
               </TravelAgencySender>
            </Sender>
         </Party>
         <Success/>
         <Response>
            <OrderViewProcessing>
               <Remarks>
                  <Remark DisplayInd="true">JOSE GARCIA PLATA residencia verificado.</Remark>
               </Remarks>
            </OrderViewProcessing>
            <Passengers>
               <Passenger ObjectKey="ADULT_01">
                  <PTC>ADT</PTC>
                  <Age>
                     <BirthDate>1970-01-01</BirthDate>
                  </Age>
                  <Name>
                     <Surname>GARCIA PLATA</Surname>
                     <Given>JOSE</Given>
                  </Name>
                  <Contacts>
                     <Contact>
                        <EmailContact>
                           <Address>foo@bar.com</Address>
                        </EmailContact>
                        <PhoneContact>
                           <Number>666000111</Number>
                        </PhoneContact>
                     </Contact>
                  </Contacts>
                  <PassengerIDInfo>
                     <FOID>
                        <Type>NI</Type>
                        <ID>30000529L</ID>
                     </FOID>
                     <PassengerDocument>
                        <Type>F1</Type>
                        <ID>10</ID>
                        <ApplicabilityLocation>380380</ApplicabilityLocation>
                     </PassengerDocument>
                     <PassengerDocument>
                        <Type>AI</Type>
                        <ID>123131414143</ID>
                        <ApplicabilityLocation>060830</ApplicabilityLocation>
                     </PassengerDocument>
                  </PassengerIDInfo>
                  <AdditionalRoles PrimaryContactInd="true"/>
               </Passenger>
            </Passengers>
            <Order>
               <OrderID Owner="IB">IB3df3e9166e934ab8b20897d933581af3</OrderID>
               <BookingReferences>
                  <BookingReference>
                     <ID>N08D4</ID>
                     <AirlineID>IB</AirlineID>
                  </BookingReference>
               </BookingReferences>
               <TotalOrderPrice>
                  <DetailCurrencyPrice>
                     <Total Code="EUR">99.82</Total>
                     <Taxes>
                        <Total Code="EUR">19.97</Total>
                     </Taxes>
                  </DetailCurrencyPrice>
               </TotalOrderPrice>
               <OrderItems>
                  <OrderItem>
                     <OrderItemID Owner="IB">IB3df3e9166e934ab8b20897d933581af3_03</OrderItemID>
                     <FlightItem>
                        <Price>
                           <BaseAmount Code="EUR">41.85</BaseAmount>
                           <Taxes>
                              <Total Code="EUR">19.97</Total>
                              <Breakdown>
                                 <Tax>
                                    <Amount Code="EUR">15.75</Amount>
                                    <TaxCode>JD</TaxCode>
                                 </Tax>
                                 <Tax>
                                    <Amount Code="EUR">0.59</Amount>
                                    <TaxCode>OG</TaxCode>
                                 </Tax>
                                 <Tax>
                                    <Amount Code="EUR">3.63</Amount>
                                    <TaxCode>QV</TaxCode>
                                 </Tax>
                              </Breakdown>
                           </Taxes>
                        </Price>
                        <OriginDestination>
                           <OriginDestinationKey>MADLPA20180317084000</OriginDestinationKey>
                           <Flight>
                              <SegmentKey>IB382620180317</SegmentKey>
                              <Departure>
                                 <AirportCode>MAD</AirportCode>
                                 <Date>2018-03-17</Date>
                                 <Time>08:45</Time>
                                 <AirportName>Madrid</AirportName>
                                 <Terminal>
                                    <Name>4</Name>
                                 </Terminal>
                              </Departure>
                              <Arrival>
                                 <AirportCode>LPA</AirportCode>
                                 <Date>2018-03-17</Date>
                                 <Time>10:35</Time>
                                 <ChangeOfDay>0</ChangeOfDay>
                                 <AirportName>Gran Canaria</AirportName>
                                 <Terminal/>
                              </Arrival>
                              <MarketingCarrier>
                                 <AirlineID>IB</AirlineID>
                                 <Name>Iberia</Name>
                                 <FlightNumber>3826</FlightNumber>
                              </MarketingCarrier>
                              <OperatingCarrier>
                                 <AirlineID>I2</AirlineID>
                                 <Name>Iberia Express</Name>
                                 <Disclosures>
                                    <Description>
                                       <Text>I2</Text>
                                    </Description>
                                 </Disclosures>
                              </OperatingCarrier>
                           </Flight>
                        </OriginDestination>
                     </FlightItem>
                     <Associations>
                        <Passengers>
                           <PassengerReferences>ADULT_01</PassengerReferences>
                        </Passengers>
                     </Associations>
                  </OrderItem>
                  <OrderItem>
                     <OrderItemID Owner="IB">IB3df3e9166e934ab8b20897d933581af3_01</OrderItemID>
                     <BaggageItem>
                        <BagDetails>
                           <BagDetail>
                              <ValidatingCarrier>IB</ValidatingCarrier>
                              <CheckedBags>
                                 <CheckedBag>
                                    <PieceAllowance>
                                       <ApplicableParty>Traveler</ApplicableParty>
                                       <TotalQuantity>1</TotalQuantity>
                                       <PieceMeasurements Quantity="1"/>
                                    </PieceAllowance>
                                 </CheckedBag>
                              </CheckedBags>
                              <Price>
                                 <DetailCurrencyPrice>
                                    <Total Code="EUR">30</Total>
                                    <Taxes>
                                       <Total Code="EUR">0</Total>
                                    </Taxes>
                                 </DetailCurrencyPrice>
                              </Price>
                           </BagDetail>
                        </BagDetails>
                     </BaggageItem>
                     <Associations>
                        <Passengers>
                           <PassengerReferences>ADULT_01</PassengerReferences>
                        </Passengers>
                     </Associations>
                  </OrderItem>
                  <OrderItem>
                     <OrderItemID Owner="IB">IB3df3e9166e934ab8b20897d933581af3_02</OrderItemID>
                     <SeatItem>
                        <Price>
                           <Total Code="EUR">8</Total>
                           <Taxes>
                              <Total Code="EUR">0</Total>
                           </Taxes>
                        </Price>
                        <Location>
                           <Column>C</Column>
                           <Row>
                              <Number>15</Number>
                           </Row>
                           <Associations>
                              <Flight>
                                 <SegmentReferences>IB382620180317</SegmentReferences>
                              </Flight>
                           </Associations>
                        </Location>
                     </SeatItem>
                     <Associations>
                        <Passengers>
                           <PassengerReferences>ADULT_01</PassengerReferences>
                        </Passengers>
                     </Associations>
                  </OrderItem>
               </OrderItems>
            </Order>
            <Payments>
               <Payment>
                  <Method>
                     <OtherMethod>
                        <Remarks>
                           <Remark>ServiceFee</Remark>
                        </Remarks>
                     </OtherMethod>
                  </Method>
                  <Amount Code="EUR">10.00</Amount>
                  <Associations>
                     <Passengers>
                        <PassengerReference>ADULT_01</PassengerReference>
                     </Passengers>
                  </Associations>
               </Payment>
            </Payments>
            <TicketDocInfos>
               <TicketDocInfo>
                  <TicketDocument>
                     <TicketDocNbr>0752372788835</TicketDocNbr>
                     <Type>
                        <Code>ET</Code>
                     </Type>
                     <NumberofBooklets>1</NumberofBooklets>
                     <DateOfIssue>2018-02-15</DateOfIssue>
                     <TimeOfIssue>13:46</TimeOfIssue>
                     <CouponInfo>
                        <CouponNumber>1</CouponNumber>
                        <FareBasisCode>
                           <Code>ZDENOA4K</Code>
                        </FareBasisCode>
                        <CouponValid>
                           <EffectiveDatePeriod>
                              <Effective>2018-03-17</Effective>
                           </EffectiveDatePeriod>
                        </CouponValid>
                        <Status>
                           <Code>OPEN FOR USE</Code>
                        </Status>
                        <SoldAirlineInfo>
                           <DepartureDateTime ShortDate="2018-03-17" Time="08:45"/>
                           <Departure>
                              <AirportCode>MAD</AirportCode>
                              <Date>2018-03-17</Date>
                              <Time>08:45</Time>
                           </Departure>
                           <Arrival>
                              <AirportCode>LPA</AirportCode>
                           </Arrival>
                           <OperatingCarrier>
                              <AirlineID>IB</AirlineID>
                           </OperatingCarrier>
                           <MarketingCarrier>
                              <AirlineID>IB</AirlineID>
                              <FlightNumber>3826</FlightNumber>
                           </MarketingCarrier>
                           <Status>
                              <Code>OK</Code>
                           </Status>
                        </SoldAirlineInfo>
                     </CouponInfo>
                  </TicketDocument>
                  <TicketDocument>
                     <TicketDocNbr>0755555339410</TicketDocNbr>
                     <Type>
                        <Code>J</Code>
                        <Definition>EMD-A (Associated)</Definition>
                        <TableName>1001 (Document/ Message Name, coded)</TableName>
                     </Type>
                     <NumberofBooklets>1</NumberofBooklets>
                     <DateOfIssue>2018-02-15</DateOfIssue>
                     <TimeOfIssue>13:46</TimeOfIssue>
                     <Remark/>
                     <CouponInfo>
                        <CouponNumber>1</CouponNumber>
                        <Status>
                           <Code>OPEN FOR USE</Code>
                        </Status>
                        <DateOfService>2018-03-17</DateOfService>
                        <Value Code="EUR">8.00</Value>
                        <InConnectionWithInfo AssociateInd="true">
                           <InConnectionDocNbr>0752372788835</InConnectionDocNbr>
                           <InConnectonCpnNbr>1</InConnectonCpnNbr>
                        </InConnectionWithInfo>
                        <ReasonForIssuance>
                           <RFIC>
                              <Code>A</Code>
                           </RFIC>
                           <Code>0B5</Code>
                           <Description>SEAT ASSIGNMENT</Description>
                        </ReasonForIssuance>
                     </CouponInfo>
                  </TicketDocument>
                  <TicketDocument>
                     <TicketDocNbr>0755555339409</TicketDocNbr>
                     <Type>
                        <Code>J</Code>
                        <Definition>EMD-A (Associated)</Definition>
                        <TableName>1001 (Document/ Message Name, coded)</TableName>
                     </Type>
                     <NumberofBooklets>1</NumberofBooklets>
                     <DateOfIssue>2018-02-15</DateOfIssue>
                     <TimeOfIssue>13:46</TimeOfIssue>
                     <Remark/>
                     <CouponInfo>
                        <CouponNumber>1</CouponNumber>
                        <Status>
                           <Code>OPEN FOR USE</Code>
                        </Status>
                        <DateOfService>2018-03-17</DateOfService>
                        <Value Code="EUR">30.00</Value>
                        <InConnectionWithInfo AssociateInd="true">
                           <InConnectionDocNbr>0752372788835</InConnectionDocNbr>
                           <InConnectonCpnNbr>1</InConnectonCpnNbr>
                        </InConnectionWithInfo>
                        <ReasonForIssuance>
                           <RFIC>
                              <Code>C</Code>
                           </RFIC>
                           <Code>0GO</Code>
                           <Description>EXTRA PIECE</Description>
                        </ReasonForIssuance>
                     </CouponInfo>
                  </TicketDocument>
                  <PassengerReference>ADULT_01</PassengerReference>
               </TicketDocInfo>
            </TicketDocInfos>
         </Response>
      </OrderViewRS>
   </soap:Body>
</soap:Envelope>