package com.hpe.cms.ndc.server;

import com.hpe.cms.ndc.common.util.JaxbUtil;
import com.hpe.cms.ndc.common.util.SourceUtil;
import com.hpe.cms.ndc.server.v15_2.model.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.xml.transform.StringSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

/**
 * AirShoppingEndpoint Tester.
 *
 * @author guoxingwang
 * @version 1.0
 * @since
 */
@RunWith(SpringRunner.class)
//@SpringBootTest(classes={Application.class})
@Slf4j
public class JAXBTest {

    @BeforeClass
    public static void beforeClass() throws Exception {

    }

    @AfterClass
    public static void afterClass() throws Exception {

    }

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void testSendAndReceive() throws Exception {

        Source requestPayload = SourceUtil.getSourceFromFilePath("use-case/AirShoppingRQ.xml");
        Source responsePayload = SourceUtil.getSourceFromFilePath("use-case/AirShoppingRS.xml");

        log.info(String.valueOf(requestPayload));
        log.info(String.valueOf(responsePayload));

//        ResponseActions responseActions = mockClient.sendRequest(withPayload(requestPayload));
//        responseActions.andExpect(payload(responsePayload));
    }

    @Test
    public void jaxbTest() {

        StreamSource source = new StringSource(

                "<holidayRequest xmlns='http://mycompany.com/hr/webservice'>" +

                        "<name>����</name>" +

                        "</holidayRequest>");

        try {

            JAXBContext context = JAXBContext.newInstance(AirShoppingRQ.class);

            Marshaller marshaller = context.createMarshaller();
            Unmarshaller unmarshaller = context.createUnmarshaller();

            AirShoppingRQ airShoppingRQ = new AirShoppingRQ();
            AirShopReqParamsType airShopReqParamsType = new AirShopReqParamsType();

            PointOfSaleType pointOfSaleType = new PointOfSaleType();
            PointOfSaleType.Location location = new PointOfSaleType.Location();
            CountryCode countryCode = new CountryCode();
            countryCode.setValue("CN");
            location.setCountryCode(countryCode);
            pointOfSaleType.setLocation(location);
            airShoppingRQ.setPointOfSale(pointOfSaleType);

            marshaller.marshal(airShoppingRQ, System.out);
            System.out.println();


            Source requestPayload = new StringSource(
                    "<AirShoppingRQ Version=\"2.0\" PrimaryLangID=\"es\" xmlns=\"http://www.iata.org/IATA/EDIST\">\n" +
                            "         <PointOfSale>\n" +
                            "            <Location>\n" +
                            "               <CountryCode>ES</CountryCode>\n" +
                            "            </Location>\n" +
                            "         </PointOfSale>\n" +
                            "         <Document>\n" +
                            "            <ReferenceVersion>2.000</ReferenceVersion>\n" +
                            "         </Document>\n" +
                            "         <Party>\n" +
                            "            <Sender>\n" +
                            "               <TravelAgencySender>\n" +
                            "                  <Name>AgencyName</Name>\n" +
                            "                  <Contacts>\n" +
                            "                     <Contact>\n" +
                            "                        <EmailContact>\n" +
                            "                           <Address>AgencyMail@prueba.com</Address>\n" +
                            "                        </EmailContact>\n" +
                            "                        <PhoneContact>\n" +
                            "                           <Application>MOBILE</Application>\n" +
                            "                           <Number>+624545242</Number>\n" +
                            "                        </PhoneContact>\n" +
                            "                        <Name>\n" +
                            "                           <Surname>Perez</Surname>\n" +
                            "                           <Given>Pepe</Given>\n" +
                            "                        </Name>\n" +
                            "                     </Contact>\n" +
                            "                  </Contacts>\n" +
                            "                  <IATA_Number>${#Project#IATA_NUMBER}</IATA_Number>\n" +
                            "                  <AgencyID>${#Project#AGENCY_NAME}</AgencyID>\n" +
                            "               </TravelAgencySender>\n" +
                            "            </Sender>\n" +
                            "            <!--<Add Participants Tag next if applies for your test case>-->\n" +
                            "            <!--Participants>\n" +
                            "               <Participant>\n" +
                            "                  <AggregatorParticipant SequenceNumber=\"1\">\n" +
                            "                     <AggregatorID>${#Project#AGGREGATOR_NUMBER}</AggregatorID>\n" +
                            "                  </AggregatorParticipant>\n" +
                            "               </Participant>\n" +
                            "            </Participants-->\n" +
                            "         </Party>\n" +
                            "         <Travelers>\n" +
                            "            <Traveler>\n" +
                            "               <AnonymousTraveler>\n" +
                            "                  <PTC Quantity=\"1\">ADT</PTC>\n" +
                            "               </AnonymousTraveler>\n" +
                            "            </Traveler>\n" +
                            "         </Travelers>\n" +
                            "         <CoreQuery>\n" +
                            "            <OriginDestinations>\n" +
                            "               <OriginDestination>\n" +
                            "                  <Departure>\n" +
                            "                     <AirportCode>MAD</AirportCode>\n" +
                            "                     <Date>${#Project#firstDate}</Date>\n" +
                            "                  </Departure>\n" +
                            "                  <Arrival>\n" +
                            "                     <AirportCode>BUE</AirportCode>\n" +
                            "                  </Arrival>\n" +
                            "               </OriginDestination>\n" +
                            "            </OriginDestinations>\n" +
                            "         </CoreQuery>\n" +
                            "         <Preferences>\n" +
                            "            <Preference>\n" +
                            "               <PricingMethodPreference>\n" +
                            "                  <BestPricingOption>C</BestPricingOption>\n" +
                            "               </PricingMethodPreference>\n" +
                            "            </Preference>\n" +
                            "            <Preference>\n" +
                            "               <FlightPreferences>\n" +
                            "                  <Characteristic>\n" +
                            "                     <NonStopPreferences PreferencesContext=\"maxStops\">0</NonStopPreferences>\n" +
                            "                  </Characteristic>\n" +
                            "               </FlightPreferences>\n" +
                            "            </Preference>\n" +
                            "         </Preferences>\n" +
                            "      </AirShoppingRQ>\n");


            JAXBContext contextRs = JAXBContext.newInstance(AirShoppingRQ.class);
            Unmarshaller unmarshallerRq = context.createUnmarshaller();
            Marshaller marshallerRq = context.createMarshaller();
            AirShoppingRQ airShoppingRQ1 = (AirShoppingRQ) unmarshallerRq.unmarshal(requestPayload);
            marshallerRq.marshal(airShoppingRQ1, System.out);

/*

            JAXBContext unmarshallerRQ = JAXBContext.newInstance(AirShoppingRQ.class);
//            unmarshallerRQ.

            JAXBContext unmarshaller = JAXBContext.newInstance(AirShoppingRS.class);
            JAXBResult jaxbResult = new JAXBResult(unmarshaller);

            //StreamResult result = new StreamResult(System.out);
//            client.sendSourceAndReceiveToResult(source, jaxbResult);
            AirShoppingRS holidayResponse = (AirShoppingRS) jaxbResult.getResult();
            System.out.println(holidayResponse.toString());*/
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() throws JAXBException {
        String filePath = Thread.currentThread().getContextClassLoader().getResource("use-case/AirShoppingRS.xml").getPath();
        AirShoppingRS rs = (AirShoppingRS) JaxbUtil.converyToJavaBeanFromFile(filePath, AirShoppingRS.class);
        System.out.println("rs = " + rs);
    }


}
