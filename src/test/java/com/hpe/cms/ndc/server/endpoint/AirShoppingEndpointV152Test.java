package com.hpe.cms.ndc.server.endpoint;

import com.hpe.cms.Application;
import com.hpe.cms.ndc.common.util.SourceUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.ws.test.server.ResponseActions;

import javax.xml.transform.Source;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

/**
 * AirShoppingEndpoint Tester.
 *
 * @author guoxingwang
 * @version 1.0
 * @since
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@Slf4j
public class AirShoppingEndpointV152Test {
    private MockWebServiceClient mockClient;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void before() throws Exception {
        mockClient = MockWebServiceClient.createClient(applicationContext);
    }

    @Test
    public void testSendAndReceive() throws Exception {

        Source requestPayload = SourceUtil.getSourceFromFilePath("use-case/AirShoppingRQ.xml");
        Source responsePayload = SourceUtil.getSourceFromFilePath("use-case/AirShoppingRS.xml");

        log.info(String.valueOf(requestPayload));
        log.info(String.valueOf(responsePayload));

        ResponseActions responseActions = mockClient.sendRequest(withPayload(requestPayload));
        responseActions.andExpect(payload(responsePayload));
    }
}
