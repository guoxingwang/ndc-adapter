package com.hpe.cms.ndc.client;

import com.hpe.cms.ndc.client.common.WsClientTemplate;
import com.hpe.cms.ndc.common.util.JaxbUtil;
import com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ;
import com.hpe.cms.ndc.server.v15_2.model.AirShoppingRS;
import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ Author     ：guoxingwang.
 * @ Date       ：Created in 19:07 2019/7/13
 * @ Description：
 * @ Modified By：
 */
@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {Application.class})
@Slf4j
public class WsClientTemplateTest {
//
//    @Autowired
//    private WsClientTemplate wsClientTemplate;

    @BeforeClass
    public static void beforeClass() throws Exception {

    }

    @AfterClass
    public static void afterClass() throws Exception {

    }

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void testSendAndReceive() throws Exception {
        WsClientTemplate<AirShoppingRQ,AirShoppingRS> wsClientTemplate = new WsClientTemplate<AirShoppingRQ,AirShoppingRS>();
        String filePath = Thread.currentThread().getContextClassLoader().getResource("use-case/AirShoppingRQ.xml").getPath();
        AirShoppingRQ rq = (AirShoppingRQ) JaxbUtil.converyToJavaBeanFromFile(filePath, AirShoppingRQ.class);
        AirShoppingRS rs = (AirShoppingRS) wsClientTemplate.sendAndReceiveV15_2(rq);

        log.debug("RS is {}" + JaxbUtil.convertToXml(rs));

    }

}
