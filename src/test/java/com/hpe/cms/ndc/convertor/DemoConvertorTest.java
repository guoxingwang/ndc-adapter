package com.hpe.cms.ndc.convertor;

import com.hpe.cms.Application;
import com.hpe.cms.ndc.common.util.JacksonUtil;
import com.hpe.cms.ndc.common.util.JaxbUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.dozer.Mapper;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;

/**
 * DemoConvertor Tester.
 *
 * @author guoxingwang
 * @version 1.0
 * @since <pre>���� 14, 2019</pre>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes={Application.class})
@Slf4j
public class DemoConvertorTest {

    @Autowired
    private Mapper mapper;

    @BeforeClass
    public static void beforeClass() throws Exception {

    }

    @AfterClass
    public static void afterClass() throws Exception {

    }

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: airShoppingRQ_v15_2_to_v16_1(com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ airShoppingRQ_v15_2)
     */
    @Test
    public void testAirShoppingRQ_v15_2_to_v16_1() throws Exception {
        System.out.println("DemoConvertorTest.testAirShoppingRQ_v15_2_to_v16_1");

        String filename = "E:\\work\\hpe\\NDC\\workspace\\ndc\\ndc-ws-server\\src\\test\\resources\\use-case\\AirShoppingRQ.xml";
        File file = new File(filename);
        String originalContent = null;
        try {
            originalContent = FileUtils.readFileToString(file, "utf-8");
            originalContent = JaxbUtil.removeSoapActionXml(originalContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
//
//        com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ originalObject = (com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ) JaxbUtil.converyToJavaBeanFromString(originalContent, com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ.class);
////        com.hpe.cms.ndc.server.v16_1.model.AirShoppingRQ destObject = DemoConvertor.airShoppingRQ_v15_2_to_v16_1(originalObject);
//
//        String destContent = JaxbUtil.convertToXml(destObject);
//        System.out.println("destContent = " + destContent);
//
//        Assert.assertEquals(originalContent, destContent);

    }
    @Test
    public void testAirShoppingRQ_v15_2() throws Exception {

        String filePath = "E:\\work\\hpe\\NDC\\workspace\\ndc\\ndc-ws-bean\\src\\test\\resources\\use-case\\AirShoppingRQ.xml";
        com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ originalObject = (com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ) JaxbUtil.converyToJavaBeanFromFile(filePath, com.hpe.cms.ndc.server.v15_2.model.AirShoppingRQ.class);

        com.hpe.cms.ndc.server.convertor.model.AirShoppingRQ destObject = mapper.map(originalObject, com.hpe.cms.ndc.server.convertor.model.AirShoppingRQ.class);

        log.info(JacksonUtil.bean2Json(originalObject));
        log.info(JacksonUtil.bean2Json(destObject));

//        assertEquals(res.getGetWeatherByZipCode().getZipCode(),to2.getOne());

        /*// Map from TestObject to XMLBeans
        TestObject to = new TestObject();
        to.setOne("one");
        GetWeatherByZipCodeDocument doc =
                mapper.map(to, GetWeatherByZipCodeDocument.class);
        assertEquals(to.getOne(),
                doc.getGetWeatherByZipCode().getZipCode());

        // Map from XMLBeans to TestObject
        GetWeatherByZipCodeDocument res =
                GetWeatherByZipCodeDocument.Factory.newInstance();
        GetWeatherByZipCode zipCode =
                res.addNewGetWeatherByZipCode();
        zipCode.setZipCode("one");
        TestObject to2 = mapper.map(res, TestObject.class);
        assertEquals(res.getGetWeatherByZipCode().getZipCode(),
                to2.getOne());*/





    }


}
